﻿using UnityEngine;
using System.Collections;

namespace Assets.Games.Photon.Scripts
{
    public class ChangeRotateObj : MonoBehaviour
    {
        [Range(-80, 80)]
        public float rotX;

        private void Update()
        {
            transform.localRotation = Quaternion.Euler(rotX, 0, 0);
        }
    }
}