﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Games.Photon.Scripts.NetUI;
using Photon.Pun;

namespace Assets.Games.Photon.Scripts.Controller
{
    public class Net_Controller : MonoBehaviourPun
    {

        public static bool IsPauseGame, IsCanRevival;
        public static Dictionary<float, Vector3> Directions;
        public static float TimeStart;
        public static Net_Controller ins;
        public static Vector2 camSize;
        public LayerMask impediment, free_knife, wall, buffType;
        public bool isHasCharacterSpawn;
        public bool isHasFB;
        public static bool IsStartGame;
        public static GameMode mode;
        #region Data
        public KnifeData knifeData;
        public MaskData maskData;
        public EnemyAIData dareDevilData,
            below40Data,
            above40DataLikeIdle,
            above40LikeMove,
            for50DataLikeIdle,
            for50DataLikeMove;
        public DailyGiftData giftData;
        public BaseIconData iconData;
        public IngameConfigData configData;
        public List<EnemyConfigSet> configSets;
        public List<MissionData> missionDatas;
        public RankData rankData;
        public BotNameData botData;
        public RankSelectionComboData rankselectCombo;
        public ColorData colorData;
        public BoosterData boosterData;

        public List<RankAIData> rankAIData;

        #endregion

        #region Controller

        public List<HeroInfor> heroInfors;

        public List<Net_KnifeController> activeControllers;
        public Net_KnifeController player;

        #endregion

        #region Effect

        public GameObject slowAreaGO,
            slowBladeGo,
            magnetGO,
            protectedGO,
            runGO,
            buffKnifeSpeedGO,
            waitSpawnEffect,
            hitEffect,
            buffSpeedQuada;
        #endregion

        void Awake()
        {
            ins = this;
            Directions = new Dictionary<float, Vector3>()
       {
           {0, Direction.E },
           {45, Direction.NE },
           {90, Direction.N },
           {135, Direction.NW },
           {180, Direction.W },
           {225, Direction.SW },
           {270, Direction.S },
           {315, Direction.SE },
           {22.5f, GameHelper.GetDirectionFromAngle(22.5f) },
           {67.5f, GameHelper.GetDirectionFromAngle(67.5f) },
           {112.5f, GameHelper.GetDirectionFromAngle(112.5f) },
           {157.5f, GameHelper.GetDirectionFromAngle(157.5f) },
           {202.5f, GameHelper.GetDirectionFromAngle(202.5f) },
           {247.5f, GameHelper.GetDirectionFromAngle(247.5f) },
           {292.5f, GameHelper.GetDirectionFromAngle(292.5f) },
           {337.5f, GameHelper.GetDirectionFromAngle(337.5f) },
       };
        }

        private void Start()
        {
            GetBorder();
        }

        void GetBorder()
        {
            camSize.y = Camera.main.orthographicSize;
            camSize.x = camSize.y * (Screen.width / (float)Screen.height);
        }

        public void StartGame(GameMode mode)
        {
            base.photonView.RPC(nameof(CacheOnStartGame), RpcTarget.All, JsonHelper.ToJson(RoomStatus.ins.heroes), (int) mode);
        }

        [PunRPC]
        public void CacheOnStartGame(string data, int gameMode)
        {
            AudioManager.ins.PlayMusic(MusicType.GamePlayMusic);
            mode = (GameMode)gameMode;
            heroInfors = JsonHelper.FromJson<HeroInfor>(data);

            foreach (var item in heroInfors)
            {
                if(item.heroName == PhotonNetwork.LocalPlayer.NickName)
                {
                    item.isPlayer = true;
                }
                else
                {
                    item.isPlayer = false;
                }

                item.iconAvt = botData.botNames.Find(s => s.countryCode == item.heroCountry).icon;
            }

            IsPauseGame = true;
            activeControllers = new List<Net_KnifeController>();

            if (PhotonNetwork.IsMasterClient)
                SpawnOnStartGame();
            Net_UI.Ins.ShowGamePlay();

            Net_UI.Ins.inGame.gamePlay.showOnStart.OnShow(() =>
            {
                isHasFB = true;
                this.PostEvent(EventID.PlayMatch);
                IsPauseGame = false;
                IsCanRevival = false;
                isHasCharacterSpawn = false;
                TimeStart = Time.time;
                IsStartGame = true;
                Net_MapController.ins.SpawnKnife();
            });
        }

        private void SpawnOnStartGame()
        {
            Net_MapController.ins.GenerateMap(heroInfors);
        }

        public void StartAnyCoroutine(IEnumerator ct)
        {
            StartCoroutine(ct);
        }
    }

    public enum GameMode
    {
        SinglePlayer,
        TeamBattle
    }
}