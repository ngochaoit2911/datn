﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Assets.Games.Photon.Scripts.Data;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Assets.Games.Photon.Scripts.NetUI;

namespace Assets.Games.Photon.Scripts.Controller
{
    public class Net_MapController : MonoBehaviour
    {

        public static Net_MapController ins;

        public Vector2 mapSize;
        public List<GameObject> impediments;
        public List<GameObject> maps;
        public GameObject player, enemy, knife, ingameItem;
        private MapStage curMap;
        public CameraFollower camFollower;

        public Transform cameraOnDie;

        private void Awake()
        {
            ins = this;
        }

        private void Start()
        {
            if(PhotonNetwork.IsMasterClient)
                Net_Controller.ins.StartGame(RoomStatus.ins.mode);

            Poolers.ins.ReInit();
        }

        private void OnEnable()
        {
            PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
        }

        private void OnDisable()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
        }

        private void NetworkingClient_EventReceived(ExitGames.Client.Photon.EventData obj)
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                if(obj.Code == EventGameCode.OnSetUpMap)
                {
                    var m = (Vector3)obj.CustomData;

                    var mapSize = (Vector2)m;
                    camFollower.SetUpBounderies(mapSize + Vector2.one * 2.5f);
                    return;
                }

                if(obj.Code == EventGameCode.OnSetUpPlayerForController)
                {
                    
                    var data = (object[])obj.CustomData;
                    Debug.Log("Before: " + Net_Controller.ins.activeControllers.Count);
                    foreach (var item in data)
                    {
                        var pv = PhotonView.Find((int)item);
                        var player = pv.GetComponent<Net_KnifeController>();
                        Net_Controller.ins.activeControllers.Add(player);
                        if (pv.IsMine)
                        {
                            camFollower.SetUpFollower(player.m_transform);
                            Net_Controller.ins.player = player;
                        }
                    }                  
                    
                }
            }
        }

        public void GenerateMap(List<HeroInfor> heroes)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                var pRank = GameManager.ins.PlayerInformation.playerRank;
                var x = Mathf.Clamp((int)pRank * 2, 0, 8);
                var prefab = maps[Random.Range(x, x + 2)];
                var map = MasterManager.InstantiateScenceNetworkGo(prefab, Vector3.zero, Quaternion.identity);
                curMap = map.GetComponent<MapStage>();
                curMap.OnSpawnNetHeroes(heroes);
            }
           

        }

        public void SpawnKnife()
        {
            if (!PhotonNetwork.IsMasterClient)
                return;
            curMap.Net_SpawnKnifePerSecond();
            curMap.Net_SpawnItemInGame();
        }

        public void SetUpPlayer()
        {
            var data = new object[Net_Controller.ins.activeControllers.Count];
            var c = 0;
            foreach (var item in Net_Controller.ins.activeControllers)
            {
                data[c] = (item.photonView.ViewID);
                c++;
            }
            PhotonNetwork.RaiseEvent(EventGameCode.OnSetUpPlayerForController, data, RaiseEventOptions.Default, SendOptions.SendReliable);
        }

        public void SetCameraFollower(Transform p)
        {
            camFollower.SetUpFollower(p);
        }

        public void DisableFollower()
        {
            camFollower.DisableFollow();
        }

        public void SetUpBounding(Vector2 mapSize)
        {
            camFollower.SetUpBounderies(mapSize + Vector2.one * 2.5f);
            PhotonNetwork.RaiseEvent(EventGameCode.OnSetUpMap, (Vector3)mapSize, RaiseEventOptions.Default, SendOptions.SendUnreliable);
        }

        public Vector3 GetRandomPos()
        {
            var x = Random.Range(0, curMap.spawnCharacterPos.Count);
            return curMap.spawnCharacterPos[x].position;
        }

        public void SetUpCamOnDie()
        {
            cameraOnDie.Show();
            Net_UI.Ins.quiBtn.Show();
            camFollower.SetUpFollower(cameraOnDie);
        }
    }
}