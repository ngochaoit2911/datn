﻿using UnityEngine;
using System.Collections;

namespace Assets.Games.Photon.Scripts.Data
{
    [CreateAssetMenu(menuName ="Server/GameSetting")]
    public class GameSettings : ScriptableObject
    {

        [SerializeField]
        private string _gameVersion = "1.0.0";
        public string GameVersion
        {
            get => _gameVersion;
        }
        [SerializeField]
        private string _nickName = "Meme";
        public string NickName
        {
            get
            {
                int v = Random.Range(0, 2000);
                return _nickName + v.ToString();
            }
        }
    }
}