﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Games.Photon.Scripts.Game;
using Photon.Pun;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Assets.Games.Photon.Scripts.Data
{
    [CreateAssetMenu(menuName = "Server/MasterManager")]
    public class MasterManager : ScriptObjectableSingleton<MasterManager>
    {
        [SerializeField]
        private GameSettings _gameSettings;
        public GameSettings GameSettings { get => _gameSettings; }

        [SerializeField]
        private List<NetworkPrefabs> prefabs;

        public static GameObject InstantiateNetworkGo(GameObject go, Vector3 pos, Quaternion rot)
        {
            foreach (var item in Instance.prefabs)
            {
                if(item.prefab == go)
                {
                    GameObject r = PhotonNetwork.Instantiate(item.path, pos, rot);
                    return r;
                }
            }
            return null;

        }

        public static GameObject InstantiateScenceNetworkGo(GameObject go, Vector3 pos, Quaternion rot)
        {
            foreach (var item in Instance.prefabs)
            {
                if (item.prefab == go)
                {
                    GameObject r = PhotonNetwork.InstantiateSceneObject(item.path, pos, rot);
                    return r;
                }
            }
            return null;

        }

#if UNITY_EDITOR

        [ContextMenu("SetUpPath")]
        private void SetupPath()
        {
            foreach (var item in prefabs)
            {
                string path = AssetDatabase.GetAssetPath(item.prefab);
                item.path = ConvertPath(path);

            }
        }

        private string ConvertPath(string path)
        {
            var ext = System.IO.Path.GetExtension(path).Length;
            var index = path.ToLower().IndexOf("resources");
            var addition = 10;
            if(index == -1)
            {
                return "";
            }
            else
            {
                return path.Substring(index + 10, path.Length - (index + addition + ext));
            }
        }

#endif
    }
}