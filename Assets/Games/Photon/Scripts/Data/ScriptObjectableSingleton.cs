﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptObjectableSingleton<T> : ScriptableObject where T : ScriptableObject
{
    private static T _instance;
    public static T Instance
    {
        get
        {
            if(_instance == null)
            {
                var rs = Resources.FindObjectsOfTypeAll<T>();
                if(rs.Length == 0)
                {
                    Debug.LogError("Singleton Null!");
                    return null;
                }
                else
                {
                    _instance = rs[0];
                }
            }
            return _instance;
        }
    }
}
