﻿using Assets.Games.Photon.Scripts.Controller;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Games.Photon.Scripts.Game
{
    public class CameraControllerOnDie : MonoBehaviour
    {
        private Transform t;
        public float speed;
        private Vector3 lastPos;
        private Camera cam;
        private Vector3 lastDirection;
        private float moveSpeed;

        private bool isCanMove = true;
        // Use this for initialization
        void Start()
        {
            t = transform;
            cam = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
#if !PLATFORM_ANDROID
            OnEditor();
#endif

#if PLATFORM_ANDROID
        OnAndroid();
#endif

        }

        private void OnAndroid()
        {
            if (Input.touchCount > 0)
            {
                var touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    lastPos = touch.position;
                    lastPos.z = 10;
                    lastPos = cam.ScreenToWorldPoint(lastPos);
                }
                else if (touch.phase == TouchPhase.Moved)
                {
                    if (!Net_Controller.IsPauseGame)
                    {
                        Vector3 newPos = touch.position;
                        newPos.z = 10;
                        newPos = cam.ScreenToWorldPoint(newPos);
                        var direction = newPos - lastPos;
                        var maxDistance = speed * Time.deltaTime;

                        if (direction.magnitude < maxDistance)
                        {
                            moveSpeed = Mathf.Clamp(direction.magnitude / maxDistance, 0.8f, 1) * speed;
                        }
                        else
                        {
                            moveSpeed = speed;
                        }

                        direction = Vector2.Lerp(lastDirection, direction, Time.deltaTime * 8);
                        if (isCanMove)
                            t.position += direction.normalized * moveSpeed * Time.deltaTime;
                        lastDirection = direction;
                        lastPos = newPos;
                    }
                }
                else if (touch.phase == TouchPhase.Stationary)
                {
                    if (!Net_Controller.IsPauseGame)
                    {
                        Vector3 newPos = touch.position;
                        newPos.z = 10;
                        newPos = cam.ScreenToWorldPoint(newPos);
                        if (isCanMove)
                            t.position += lastDirection.normalized * speed * 0.8f * Time.deltaTime;
                        lastPos = newPos;
                    }
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                   
                }
            }
        }

        private void OnEditor()
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastPos = Input.mousePosition;
                lastPos.z = 10;
                lastPos = cam.ScreenToWorldPoint(lastPos);
            }
            else if (Input.GetMouseButton(0))
            {
                if (!Net_Controller.IsPauseGame)
                {
                    var newPos = Input.mousePosition;
                    newPos.z = 10;
                    newPos = cam.ScreenToWorldPoint(newPos);
                    var direction = newPos - lastPos;
                    var maxDistance = speed * Time.deltaTime;

                    if (direction.magnitude < maxDistance)
                    {
                        moveSpeed = Mathf.Clamp(direction.magnitude / maxDistance, 0.5f, 1) * speed;
                    }
                    else
                    {
                        moveSpeed = speed;
                    }

                    direction = Vector2.Lerp(lastDirection, direction, Time.deltaTime * 10);
                    if (isCanMove)
                        t.position += direction.normalized * moveSpeed * Time.deltaTime;
                    lastDirection = direction;
                    lastPos = newPos;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                
            }
        }
    }
}