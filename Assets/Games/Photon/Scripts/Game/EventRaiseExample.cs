﻿using UnityEngine;
using System.Collections;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

namespace Assets.Games.Photon.Scripts.Game
{
    public class EventRaiseExample : MonoBehaviourPun
    {

        public SpriteRenderer sr;
        private const byte Color_Raise_Event = 0;


        private void Update()
        {
            if(base.photonView.IsMine && Input.GetKeyDown(KeyCode.Space))
            {
                ChangeColor();
            }
        }

        private void OnEnable()
        {
            PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;  
        }
        private void OnDisable()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
        }

        private void NetworkingClient_EventReceived(EventData obj)
        {
            if (obj.Code == Color_Raise_Event)
            {
                var datas = (object[])obj.CustomData;
                if((int)datas[3] == base.photonView.ViewID)
                {
                    sr.color = new Color((float)datas[0], (float)datas[1], (float)datas[2], 1);
                }
            }
        }

        private void ChangeColor()
        {
            var r = Random.Range(0f, 1f);
            var g = Random.Range(0f, 1f);
            var b = Random.Range(0f, 1f);

            sr.color = new Color(r, g, b, 1);

            var datas = new object[] { r, g, b, base.photonView.ViewID};
            PhotonNetwork.RaiseEvent(Color_Raise_Event, datas, RaiseEventOptions.Default, SendOptions.SendUnreliable);
        }

    }
}