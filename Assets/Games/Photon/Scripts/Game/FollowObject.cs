﻿using Assets.Games.Photon.Scripts.Controller;
using System.Collections;
using UnityEngine;

namespace Assets.Games.Photon.Scripts.Game
{
    public class FollowObject : MonoBehaviour
    {

        private Net_KnifeController controller;
        private bool isOnCamera = false;
        private Transform t;
        private Vector3 curPos;
        private Vector3 curPlayerPos;
        private Vector3 d;
        private Vector3 camPos;
        private Vector2 camSize;

        // Use this for initialization
        void Start()
        {
            t = transform;
            controller = GetComponent<Net_KnifeController>();
        }
        private void LateUpdate()
        {
            if (controller.isPlayer || !controller.isOnStartGame)
                return;
            if (!controller.isDeath)
            {
                if (!GameHelper.IsInner(t.position, Net_MapController.ins.camFollower.GetCamPos(), Net_Controller.camSize))
                {
                    isOnCamera = true;
                    controller.moveFollowItem.Show();
                    curPos = t.position;
                    curPlayerPos = Net_Controller.ins.player.m_transform.position;
                    d = (curPos - curPlayerPos).normalized;
                    camPos = Net_MapController.ins.camFollower.GetCamPos();
                    camSize = Net_Controller.camSize;
                    if (GameHelper.CheckYStraight(curPos, curPlayerPos, camPos.y + camSize.y, camPos.x - camSize.x,
                     camPos.x + camSize.x))
                    {
                        controller.moveFollowItem.m_transform.position =
                            Vector3.Lerp(controller.moveFollowItem.m_transform.position,
                                Net_MapController.ins.camFollower.Net_GetPosAtTop(curPos, curPlayerPos), Time.deltaTime * 10);
                        controller.moveFollowItem.m_transform.up = d;
                    }
                    else if (GameHelper.CheckXStraight(curPos, curPlayerPos, camPos.x - camSize.x, camPos.y - camSize.y,
                   camPos.y + camSize.y))
                    {
                        controller.moveFollowItem.m_transform.position =
                            Vector3.Lerp(controller.moveFollowItem.m_transform.position,
                                Net_MapController.ins.camFollower.Net_GetPosAtLeft(curPos, curPlayerPos), Time.deltaTime * 10);
                        controller.moveFollowItem.m_transform.up = d;
                    }
                    else if (GameHelper.CheckXStraight(curPos, curPlayerPos, camPos.x + camSize.x, camPos.y - camSize.y,
                        camPos.y + camSize.y))
                    {
                        controller.moveFollowItem.m_transform.position =
                            Vector3.Lerp(controller.moveFollowItem.m_transform.position,
                                Net_MapController.ins.camFollower.Net_GetPosAtRight(curPos, curPlayerPos), Time.deltaTime * 10);
                        controller.moveFollowItem.m_transform.up = d;
                    }
                    else if (GameHelper.CheckYStraight(curPos, curPlayerPos, camPos.y - camSize.y, camPos.x - camSize.x,
                        camPos.x + camSize.x))
                    {
                        controller.moveFollowItem.m_transform.position =
                            Vector3.Lerp(controller.moveFollowItem.m_transform.position,
                                Net_MapController.ins.camFollower.Net_GetPosAtBot(curPos, curPlayerPos), Time.deltaTime * 10);
                        controller.moveFollowItem.m_transform.up = d;
                    }
                }
                else
                {
                    isOnCamera = false;
                    controller.moveFollowItem.Hide();
                }
            }
        }
    }
}