﻿using UnityEngine;
using System.Collections;
using Photon.Pun;

namespace Assets.Games.Photon.Scripts.Game
{
    public class MoveObject : MonoBehaviourPun, IPunObservable
    {
        private Transform t;
        public float speed;
        // Use this for initialization
        void Start()
        {
            t = transform;
        }

        // Update is called once per frame
        void Update()
        {
            if (base.photonView.IsMine)
            {
                var x = Input.GetAxis("Horizontal");
                var y = Input.GetAxis("Vertical");
                transform.position += new Vector3(x, y).normalized * speed * Time.deltaTime;
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(t.position);
                stream.SendNext(t.rotation);
            }
            else if(stream.IsReading)
            {
                t.position = (Vector3)stream.ReceiveNext();
                t.rotation = (Quaternion)stream.ReceiveNext();
            }
        }
    }
}