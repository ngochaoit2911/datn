﻿using Assets.Games.Photon.Scripts.Controller;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Net_EnemyAI : MonoBehaviourPun, IController
{
    private Transform t;
    private Net_KnifeController controller;

    private Dictionary<AIState, Action> onExcutions;
    public bool isChangeState, isAFK, isDareDevil, isStartAFK, isStartDareDevil, isIdleOn40, isMoveFoundTarget;
    private EnemyAIData aiData;
    public EnemyConfigSet configSet;
    private RankAIData rankAI;
    public AIState curState;
    public float speed;
    public Vector3 direction;
    public float maxDistance;
    public float point;
    private bool isLikeIdle;
    private float timeMoveCount, timeIdleCount;
    private Vector3 lastDirection;
    private float timeToAFK, timeToAFKCount;
    private float timeToDareDevil;
    private float timeToDareDevilCount;
    private float moveSpeed;
    private float distance;
    private float radius;
    private Net_KnifeController targetController;
    public bool isOnCamera;

    private void Start()
    {
        t = transform;
        controller = GetComponent<Net_KnifeController>();
    }

    public void Init(EnemyConfigSet set, RankAIData rankAI, float r, float r2)
    {
        isCanMove = true;
        onExcutions = new Dictionary<AIState, Action>
        {
            {AIState.Move, OnMove},
            {AIState.Idle, OnIdle}
        };
        configSet = set;
        isChangeState = true;

        if (r < rankAI.probilityDareDevil)
        {
            isDareDevil = true;
            timeToDareDevil = Random.Range(20, 100f);
            timeToDareDevilCount = timeToDareDevil;
        }
        else
        {
            isDareDevil = false;
            if (r < rankAI.probabilityAFK)
            {
                isAFK = true;
                timeToAFK = Random.Range(1, 100f);
                timeToAFKCount = timeToAFK;
            }
            else
            {
                isAFK = false;
            }
        }


        if (r2 < Net_Controller.ins.configData.probabilityLikeIdle)
        {
            isLikeIdle = true;
        }
        else
        {
            isLikeIdle = false;
        }
        isStartAFK = false;
        isStartDareDevil = false;
        aiData = Net_Controller.ins.below40Data;
        timeMoveCount = aiData.timeMove;
        timeIdleCount = aiData.timeIdle;
        maxDistance = speed * aiData.timeMove;
        this.rankAI = rankAI;
    }

    public void Init(int x, int r)
    {
        var r1 = Random.value;
        var r2 = Random.value;
        base.photonView.RPC(nameof(RPC_InitEnemy), RpcTarget.All, x, r, r1, r2);
    }

    [PunRPC]
    public void RPC_InitEnemy(int x, int r, float r1, float r2)
    {
        var set = Net_Controller.ins.configSets[x];
        var rankAI = Net_Controller.ins.rankAIData[r];
        Init(set, rankAI, r1, r2);
    }

    void Update()
    {
        if (Net_Controller.IsPauseGame || !PhotonNetwork.IsMasterClient)
            return;

        if (isAFK)
        {
            if (!isStartAFK)
            {
                if (timeToAFKCount <= 0)
                {
                    isStartAFK = true;
                    controller.SwitchToDefenseState();
                }
                else
                {
                    timeToAFKCount -= Time.deltaTime;
                    OnNormal();
                }
            }

            return;
        }

        if (isDareDevil)
        {
            if (!isStartDareDevil)
            {
                if (timeToDareDevilCount <= 0)
                {
                    isStartDareDevil = true;
                    aiData = Net_Controller.ins.dareDevilData;
                }
                else
                {
                    timeToDareDevilCount -= Time.deltaTime;
                    OnNormal();
                }
            }
            else
            {
                OnDareDevil();
            }

            return;
        }

        OnNormal();
    }

    private void CheckDirection()
    {
        var allP = Net_Controller.ins.activeControllers.Where(s => s.GetInstanceID() != controller.GetInstanceID())
            .ToList();
        if (allP.Count >= 1)
        {
            var nearest = GameHelper.GetNearestController(allP, t.position, controller.heroInfor.teamId);
            var d = Vector3.Distance(t.position, nearest.m_transform.position);

        }
        moveSpeed = Random.Range(speed * 0.6f, speed);
        var maxPoint = -5000f;
        direction = Vector3.zero;
        var lPoint = new List<Temp>();
        maxDistance = moveSpeed * aiData.timeMove + controller.radius;
        var size = new Vector2(maxDistance + 0.2f, controller.GetRadius() * 2);
        var isCheckDefense = Random.value < rankAI.probabilityCheckDefense;

        float k = 0;

        foreach (var key in Net_Controller.Directions.Keys)
        {
            float curPoint = 0;
            var pos = t.position + maxDistance / 2 * Net_Controller.Directions[key];
            var nextPos = t.position + moveSpeed * timeMoveCount * Net_Controller.Directions[key];
            var hitKnife = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.free_knife);
            var hitImpediment = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.impediment);
            var hitWall = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                Net_Controller.ins.wall);
            curPoint += hitKnife.Length * aiData.pointForKnife * configSet.numeralForKnife;
            if (allP.Count >= 1)
            {
                var nearest = GameHelper.GetNearestController(allP, nextPos, controller.heroInfor.teamId);
                var d = Vector3.Distance(nextPos, nearest.m_transform.position);
                var point = d > 6
                    ? 0
                    : (d > 4
                        ? ((6 - d) * aiData.pointForHitEnemy * 0.2f)
                        : (d > 2
                            ? (aiData.pointForDistanceToNearestEnemy * 0.4f + (4 - d) * aiData.pointForDistanceToNearestEnemy * 0.4f)
                            : aiData.pointForDistanceToNearestEnemy * 1.2f + (2 - d) * aiData.pointForDistanceToNearestEnemy * 1.1f));

                if (controller.KnifeCount >= nearest.KnifeCount)
                {
                    if (!isCheckDefense)
                    {
                        curPoint += point * configSet.numeralForAttack;
                    }
                    else
                    {
                        if (controller.KnifeCount - nearest.KnifeCount > 15)
                        {
                            curPoint += point * configSet.numeralForAttack;
                        }
                        else
                        {
                            if (nearest.state == KnifeState.Defense)
                                curPoint -= point * configSet.numeralForAttack * 3;
                        }
                    }
                }
                else
                {
                    curPoint += point * configSet.numeralForAvoid;
                }
            }

            curPoint += hitWall.Length > 0 ? aiData.pointForOverWall : 0;
            curPoint += hitImpediment.Length > 0 ? aiData.pointForHitImpediment : 0;

            if (rankAI.isCheckItem)
            {
                var hitItem = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                    Net_Controller.ins.buffType);
                curPoint += hitItem.Length > 0 ? aiData.pointForItem : 0;
            }

            lPoint.Add(new Temp
            {
                key = key,
                point = curPoint
            });
            if (curPoint > maxPoint)
            {
                maxPoint = curPoint;
            }

            //Debug.Log(key + "  " + curPoint);
            //GameHelper.DebugDrawBox(pos, size, key, Color.green, timeMoveCount);
        }

        point = maxPoint;
        var r = Random.value;
        if (r < aiData.probabilityMoveOnLastDirection)
        {
            foreach (var temp in lPoint)
            {
                if (Math.Abs(temp.point - maxPoint) <= 0)
                {
                    if (Net_Controller.Directions[temp.key] == lastDirection)
                    {
                        direction = Net_Controller.Directions[temp.key];
                        if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
                        {
                            k = temp.key;
                            controller.SwitchToAttackState();
                        }
                        GameHelper.DebugDrawBox(t.position + maxDistance / 2 * Net_Controller.Directions[k], size, k, Color.green, timeMoveCount);
                        return;
                    }
                }
            }

            lPoint.Shuffle();
            k = lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key;

            direction = Net_Controller.Directions[k];
            lastDirection = direction;
            //Debug.Log(k);
        }
        else
        {
            lPoint.Shuffle();
            k = lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key;

            direction = Net_Controller.Directions[k];
            lastDirection = direction;
            //Debug.Log(k);
        }

        if (direction == Vector3.zero)
        {
            controller.SwitchToDefenseState();
            curState = AIState.Idle;
        }
        else
        {
            if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
            {
                controller.SwitchToAttackState();
            }
        }
        //GameHelper.DebugDrawBox(t.position + maxDistance / 2 * Net_Controller.Directions[k], size, k, Color.red, timeMoveCount);
    }

    private void CheckDirection1()
    {
        var allP = Net_Controller.ins.activeControllers.Where(s => s.GetInstanceID() != controller.GetInstanceID())
            .ToList();

        moveSpeed = Random.Range(speed * 0.8f, speed);
        var maxPoint = -5000f;
        direction = Vector3.zero;
        var lPoint = new List<Temp>();
        maxDistance = moveSpeed * aiData.timeMove + controller.radius;
        var size = new Vector2(maxDistance + 0.2f, controller.GetRadius() * 2);
        var isCheckDefense = Random.value < rankAI.probabilityCheckDefense;
        float k = 0;

        foreach (var key in Net_Controller.Directions.Keys)
        {
            float curPoint = 0;
            var pos = t.position + maxDistance / 2 * Net_Controller.Directions[key];
            var nextPos = t.position + moveSpeed * timeMoveCount * Net_Controller.Directions[key];
            var hitKnife = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.free_knife);
            var hitImpediment = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.impediment);
            var hitWall = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                Net_Controller.ins.wall);
            curPoint += hitKnife.Length * aiData.pointForKnife * configSet.numeralForKnife;
            if (allP.Count >= 1)
            {
                var nearest = GameHelper.GetNearestController(allP, nextPos, controller.heroInfor.teamId);
                var d = Vector3.Distance(nextPos, nearest.m_transform.position);
                var point = d > 6
                    ? 0
                    : (d > 4
                        ? ((6 - d) * aiData.pointForHitEnemy * 0.2f)
                        : (d > 2
                            ? (aiData.pointForDistanceToNearestEnemy * 0.4f + (4 - d) * aiData.pointForDistanceToNearestEnemy * 0.4f)
                            : aiData.pointForDistanceToNearestEnemy * 1.2f + (2 - d) * aiData.pointForDistanceToNearestEnemy * 1.1f));

                if (controller.KnifeCount >= nearest.KnifeCount)
                {
                    if (!isCheckDefense)
                    {
                        curPoint += point * configSet.numeralForAttack;
                    }
                    else
                    {
                        if (controller.KnifeCount - nearest.KnifeCount > 15)
                        {
                            curPoint += point * configSet.numeralForAttack;
                        }
                        else
                        {
                            if (nearest.state == KnifeState.Defense)
                                curPoint -= point * configSet.numeralForAttack * 3;
                        }
                    }
                }
                else
                {
                    curPoint += point * configSet.numeralForAvoid;
                }
            }

            if (rankAI.isCheckItem)
            {
                var hitItem = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                    Net_Controller.ins.buffType);
                curPoint += hitItem.Length > 0 ? aiData.pointForItem : 0;
            }

            curPoint += hitWall.Length > 0 ? aiData.pointForOverWall : 0;
            curPoint += hitImpediment.Length > 0 ? aiData.pointForHitImpediment : 0;
            lPoint.Add(new Temp
            {
                key = key,
                point = curPoint
            });
            if (curPoint > maxPoint)
            {
                maxPoint = curPoint;
            }

            //Debug.Log(key + "  " + curPoint);
            //GameHelper.DebugDrawBox(pos, size, key, Color.green, timeMoveCount);
        }

        point = maxPoint;
        var r = Random.value;
        if (r < aiData.probabilityMoveOnLastDirection)
        {
            foreach (var temp in lPoint)
            {
                if (Math.Abs(temp.point - maxPoint) <= 0)
                {
                    if (Net_Controller.Directions[temp.key] == lastDirection)
                    {
                        direction = Net_Controller.Directions[temp.key];
                        if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
                        {
                            k = temp.key;
                            controller.SwitchToAttackState();
                        }
                        GameHelper.DebugDrawBox(t.position + maxDistance / 2 * Net_Controller.Directions[k], size, k, Color.green, timeMoveCount);
                        return;
                    }
                }
            }

            lPoint.Shuffle();
            k = lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key;

            direction = Net_Controller.Directions[k];
            lastDirection = direction;
        }
        else
        {
            lPoint.Shuffle();
            k = lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key;

            direction = Net_Controller.Directions[k];
            lastDirection = direction;
        }

        if (direction == Vector3.zero)
        {
            controller.SwitchToDefenseState();
            curState = AIState.Idle;
        }
        else
        {
            if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
            {
                controller.SwitchToAttackState();
            }
        }
        GameHelper.DebugDrawBox(t.position + maxDistance / 2 * Net_Controller.Directions[k], size, k, Color.red, timeMoveCount);
    }

    private void CheckDirection2()
    {
        var allP = Net_Controller.ins.activeControllers.Where(s => s.GetInstanceID() != controller.GetInstanceID())
            .ToList();
        moveSpeed = Random.Range(speed * 0.9f, speed);
        var maxPoint = -5000f;
        direction = Vector3.zero;
        var lPoint = new List<Temp>();
        maxDistance = moveSpeed * aiData.timeMove + controller.radius;
        var size = new Vector2(maxDistance + 0.2f, controller.GetRadius() * 2);
        var isCheckDefense = Random.value < rankAI.probabilityCheckDefense;
        float k = 0;

        foreach (var key in Net_Controller.Directions.Keys)
        {
            float curPoint = 0;
            var pos = t.position + maxDistance / 2 * Net_Controller.Directions[key];
            var nextPos = t.position + moveSpeed * timeMoveCount * Net_Controller.Directions[key];
            var hitKnife = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.free_knife);
            var hitImpediment = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.impediment);
            var hitWall = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                Net_Controller.ins.wall);
            curPoint += hitKnife.Length * aiData.pointForKnife * configSet.numeralForKnife;
            if (allP.Count >= 1)
            {
                var nearest = GameHelper.GetNearestController(allP, nextPos, controller.heroInfor.teamId);
                var d = Vector3.Distance(nextPos, nearest.m_transform.position);
                var point = d > 6
                    ? 0
                    : (d > 4
                        ? ((6 - d) * aiData.pointForHitEnemy * 0.2f)
                        : (d > 2
                            ? (aiData.pointForDistanceToNearestEnemy * 0.4f + (4 - d) * aiData.pointForDistanceToNearestEnemy * 0.4f)
                            : aiData.pointForDistanceToNearestEnemy * 1.2f + (2 - d) * aiData.pointForDistanceToNearestEnemy * 1.1f));

                if (controller.KnifeCount >= nearest.KnifeCount)
                {
                    if (!isCheckDefense)
                    {
                        curPoint += point * configSet.numeralForAttack;
                    }
                    else
                    {
                        if (controller.KnifeCount - nearest.KnifeCount > 15)
                        {
                            curPoint += point * configSet.numeralForAttack;
                        }
                        else
                        {
                            if (nearest.state == KnifeState.Defense)
                                curPoint -= point * configSet.numeralForAttack * 3;
                        }
                    }
                }
                else
                {
                    curPoint += point * configSet.numeralForAvoid;
                }
            }

            if (rankAI.isCheckItem)
            {
                var hitItem = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                    Net_Controller.ins.buffType);
                curPoint += hitItem.Length > 0 ? aiData.pointForItem : 0;
            }

            curPoint += hitWall.Length > 0 ? aiData.pointForOverWall : 0;
            curPoint += hitImpediment.Length > 0 ? aiData.pointForHitImpediment : 0;
            lPoint.Add(new Temp
            {
                key = key,
                point = curPoint
            });
            if (curPoint > maxPoint)
            {
                maxPoint = curPoint;
            }

            GameHelper.DebugDrawBox(pos, size, key, Color.green, timeMoveCount);
        }

        point = maxPoint;
        var r = Random.value;
        if (r < aiData.probabilityMoveOnLastDirection)
        {
            foreach (var temp in lPoint)
            {
                if (Math.Abs(temp.point - maxPoint) <= 0)
                {
                    if (Net_Controller.Directions[temp.key] == lastDirection)
                    {
                        direction = Net_Controller.Directions[temp.key];
                        if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
                        {
                            k = temp.key;
                            controller.SwitchToAttackState();
                        }
                        GameHelper.DebugDrawBox(t.position + maxDistance / 2 * Net_Controller.Directions[k], size, k, Color.green, timeMoveCount);
                        return;
                    }
                }
            }

            lPoint.Shuffle();
            k = lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key;

            direction = Net_Controller.Directions[k];
            lastDirection = direction;
            Debug.Log(k);
        }
        else
        {
            lPoint.Shuffle();
            k = lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key;

            direction = Net_Controller.Directions[k];
            lastDirection = direction;
            Debug.Log(k);
        }

        if (direction == Vector3.zero)
        {
            controller.SwitchToDefenseState();
            curState = AIState.Idle;
        }
        else
        {
            if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
            {
                controller.SwitchToAttackState();
            }
        }
        GameHelper.DebugDrawBox(t.position + maxDistance / 2 * Net_Controller.Directions[k], size, k, Color.red, timeMoveCount);
    }

    private void OnIdle()
    {
        if (timeIdleCount <= 0)
        {
            timeIdleCount = Random.Range(aiData.timeIdle - 0.1f, aiData.timeIdle + 0.1f);
            isChangeState = true;
        }
        else
        {
            timeIdleCount -= Time.deltaTime;
        }
    }

    public void OnMove()
    {
        if (timeMoveCount <= 0)
        {
            timeMoveCount = Random.Range(aiData.timeMove - 0.1f, aiData.timeMove + 0.1f);
            isChangeState = true;
        }
        else
        {
            timeMoveCount -= Time.deltaTime;
            if (isCanMove)
                t.position += direction * moveSpeed * Time.deltaTime;
        }
    }

    public void OnNormal()
    {
        if (isChangeState)
        {
             GetDistanceToNearestE(out distance, out targetController);
            controller.GetRadius(out radius);
            if (distance > 5 + radius)
            {
                var r = Random.value;
                isChangeState = false;
                if (r < aiData.probabilityIdle)
                {
                    controller.SwitchToDefenseState();
                    curState = AIState.Idle;
                }
                else
                {
                    curState = AIState.Move;

                    CheckDirection();
                }
            }
            else if (distance > 3 + radius)
            {
                CheckDirection1();
            }
            else if (distance > 1 + radius)
            {
                CheckDirection2();
            }
            else
            {
                if (controller.KnifeCount > targetController.KnifeCount)
                {
                    CheckDirection2();
                }
                else
                {
                    var r = Random.value;
                    if (r < aiData.probabilityIdleOnFace)
                    {
                        controller.SwitchToDefenseState();
                        curState = AIState.Idle;
                    }
                    else
                    {
                        CheckDirection2();
                    }
                }
            }
        }

        onExcutions[curState].Invoke();
    }

    public void OnDareDevil()
    {
        if (isChangeState)
        {
            var r = Random.value;
            isChangeState = false;
            if (r < aiData.probabilityIdle)
            {
                controller.SwitchToDefenseState();
                curState = AIState.Idle;
            }
            else
            {
                curState = AIState.Move;

                CheckDirectionForDareDevil();

            }

        }

        onExcutions[curState].Invoke();
    }

    private void CheckDirectionForDareDevil()
    {
        moveSpeed = Random.Range(speed * 0.8f, speed);
        var maxPoint = -5000f;
        direction = Vector3.zero;
        var lPoint = new List<Temp>();
        maxDistance = moveSpeed * aiData.timeMove + controller.radius;
        var size = new Vector2(maxDistance, controller.GetRadius() * 2);
        var allP = Net_Controller.ins.activeControllers.Where(s => s.GetInstanceID() != controller.GetInstanceID())
            .ToList();
        var isCheckDefense = (Random.value < rankAI.probabilityCheckDefense);

        foreach (var key in Net_Controller.Directions.Keys)
        {
            float curPoint = 0;
            var pos = t.position + maxDistance / 2 * Net_Controller.Directions[key];
            var nextPos = t.position + moveSpeed * timeMoveCount * Net_Controller.Directions[key];
            var hitKnife = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.free_knife);
            var hitImpediment = Physics2D.OverlapBoxAll(pos, size, key, Net_Controller.ins.impediment);
            var hitWall = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                Net_Controller.ins.wall);
            curPoint += hitKnife.Length * aiData.pointForKnife * configSet.numeralForKnife;
            if (allP.Count > 1)
            {
                var nearest = GameHelper.GetNearestController(allP, nextPos, controller.heroInfor.teamId);
                var d = Vector3.Distance(nextPos, nearest.m_transform.position);
                var point = d > 8
                    ? 0
                    : (d > 7
                        ? ((8 - d) * aiData.pointForHitEnemy * 0.2f)
                        : (d > 5
                            ? (aiData.pointForDistanceToNearestEnemy * 0.2f + (7 - d) * aiData.pointForDistanceToNearestEnemy * 0.2f)
                            : aiData.pointForDistanceToNearestEnemy * 0.6f + (5 - d) * aiData.pointForDistanceToNearestEnemy * 0.1f));
                if (controller.KnifeCount >= nearest.KnifeCount)
                {
                    if (!isCheckDefense)
                    {
                        curPoint += point * configSet.numeralForAttack;
                    }
                    else
                    {
                        if (controller.KnifeCount - nearest.KnifeCount > 15)
                        {
                            curPoint += point * configSet.numeralForAttack;
                        }
                        else
                        {
                            if (nearest.state == KnifeState.Defense)
                                curPoint -= point * configSet.numeralForAttack * 3;
                        }
                    }
                }
                else
                {
                    curPoint += point * configSet.numeralForAvoid;
                }
            }

            curPoint += hitWall.Length > 0 ? aiData.pointForOverWall : 0;
            curPoint += hitImpediment.Length > 0 ? aiData.pointForHitImpediment : 0;

            if (rankAI.isCheckItem)
            {
                var hitItem = Physics2D.RaycastAll(transform.position, Net_Controller.Directions[key], maxDistance,
                    Net_Controller.ins.buffType);
                curPoint += hitItem.Length > 0 ? aiData.pointForItem : 0;
            }

            lPoint.Add(new Temp
            {
                key = key,
                point = curPoint
            });
            if (curPoint > maxPoint)
            {
                maxPoint = curPoint;
            }

            GameHelper.DebugDrawBox(pos, size, key, Color.green, 3);
        }

        point = maxPoint;
        var r = Random.value;
        if (r < aiData.probabilityMoveOnLastDirection)
        {
            foreach (var temp in lPoint)
            {
                if (Math.Abs(temp.point - maxPoint) <= 0)
                {
                    if (Net_Controller.Directions[temp.key] == lastDirection)
                    {
                        direction = Net_Controller.Directions[temp.key];
                        if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
                        {
                            controller.SwitchToAttackState();
                        }

                        return;
                    }
                }
            }

            lPoint.Shuffle();
            direction = Net_Controller.Directions[lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key];

            lastDirection = direction;
        }
        else
        {
            lPoint.Shuffle();
            direction = Net_Controller.Directions[lPoint.Find(s => Math.Abs(s.point - maxPoint) <= 0).key];
            lastDirection = direction;
        }

        if (direction == Vector3.zero)
        {
            controller.SwitchToDefenseState();
            curState = AIState.Idle;
        }
        else
        {
            if (controller.state == KnifeState.Defense || controller.maskController.isHasDefense)
            {
                controller.SwitchToAttackState();
            }
        }
    }

    public void GetDistanceToNearestE(out float distance, out Net_KnifeController c)
    {
        var allP = Net_Controller.ins.activeControllers.Where(s => s.GetInstanceID() != controller.GetInstanceID())
            .ToList();
        if (allP.Count > 0)
        {
            c = GameHelper.GetNearestController(allP, t.position, controller.heroInfor.teamId);
            distance = Vector3.Distance(c.m_transform.position, t.position);
        }

        distance = 1000;
        c = null;
    }

    [System.Serializable]
    public struct Temp
    {
        public float key;
        public float point;
    }

    #region Controller

    public void OnRemoveKnifeFromImpediment()
    {
        if (!isDareDevil)
            aiData = controller.KnifeCount > 40 ? (isLikeIdle ? Net_Controller.ins.above40DataLikeIdle : Net_Controller.ins.above40LikeMove) : Net_Controller.ins.below40Data;
        isChangeState = true;
    }

    public void OnRemoveKnifeFromE()
    {

        if (!isDareDevil)
            aiData = controller.KnifeCount > 40 ? (isLikeIdle ? Net_Controller.ins.above40DataLikeIdle : Net_Controller.ins.above40LikeMove) : Net_Controller.ins.below40Data;

        isChangeState = true;
        isCanMove = false;
        if (canMoveCT != null)
        {
            StopCoroutine(canMoveCT);
        }

        var r = Random.value;

        if (r > rankAI.probabilityDontCanMove)
            canMoveCT = StartCoroutine(GameHelper.WaitForSecond(() => { isCanMove = true; }, Config.BASE_TIME_CAN_MOVE));
    }

    private bool isCanMove;
    private Coroutine canMoveCT;

    public void OnAddKnife()
    {
        aiData = controller.KnifeCount > 40 ? (controller.KnifeCount == 50 ? (isLikeIdle ? Net_Controller.ins.for50DataLikeIdle : Net_Controller.ins.for50DataLikeMove) : (isLikeIdle ? Net_Controller.ins.above40DataLikeIdle : Net_Controller.ins.above40LikeMove)) : Net_Controller.ins.below40Data;
    }

    public void UpSpeed(float addSpeed)
    {
        speed = (1 + addSpeed) * speed;
    }

    public void DescreaseSpeed(float downSpeed)
    {
        speed = (1 - downSpeed) * speed;
    }

    public void RestoreSpeedAfterDescrease(float downSpeed)
    {
        speed = speed / (1 - downSpeed);
    }

    public void RestoreSpeedAfterUp(float addSpeed)
    {
        speed = speed / (1 + addSpeed);
    }

    public void SetUpSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetCanMove()
    {
        isCanMove = true;
    }

    public bool IsOnCamera()
    {
        return isOnCamera;
    }

    #endregion

}
