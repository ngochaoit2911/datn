﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Photon.Pun;
using Assets.Games.Photon.Scripts.Controller;
using Assets.Games.Photon.Scripts.NetUI;

namespace Assets.Games.Photon.Scripts.Game
{
    public class Net_Knife : PoolItem
    {
        public PhotonView pv;
        public KnifeType knifeType;
        public SpriteRenderer render;
        public Transform childModel;
        public Rigidbody2D r2;

        public bool isFreeKnife, isKnifeOnSkill, isOnRemove;
        public float force, torque;

        //private Transform target;
        private Net_KnifeController controller;
        public int knifeIndex;
        private Tween moveOnMagnet, skillTween;
        private KnifeInfor knifeInfor;
        public Collider2D col1, col2;

        private void Awake()
        {
            pv = GetComponent<PhotonView>();
        }

        public void OnInstance(Net_KnifeController controller, int knifeIndex, KnifeType type)
        {
            knifeType = type;
            knifeInfor = Net_Controller.ins.knifeData.knifeData[(int)type];
            render.sprite = knifeInfor.icon;
            isFreeKnife = false;
            isKnifeOnSkill = false;
            this.knifeIndex = knifeIndex;
            this.controller = controller;
            //target = controller.transform;
            render.sortingOrder = 100 - knifeIndex;
            m_transform.tag = Tags.INNER_KNIFE;
            m_Obj.layer = LayerMask.NameToLayer(LayerNames.INNER_KNIFE);
            childModel.localRotation = Quaternion.identity;
            col1.enabled = false;
            col2.enabled = true;
            if (knifeInfor.isRotate)
            {
                InvokeRepeating(nameof(RotateChild), 0, Time.deltaTime);
            }
            else
            {
                CancelInvoke(nameof(RotateChild));
            }
        }

        public void OnInstanceOnSkill(Net_KnifeController controller, int knifeIndex, int total, float startAngle,
            KnifeType type)
        {
            pv.RPC(nameof(RPC_InstanceSkill), RpcTarget.All, controller.photonView.ViewID, knifeIndex, total, startAngle, (int)type);
        }

        [PunRPC]
        public void RPC_InstanceSkill(int viewId, int knifeIndex, int total, float startAngle, int type)
        {

            col1.enabled = false;
            col2.enabled = true;
            childModel.localRotation = Quaternion.identity;
            CancelInvoke(nameof(RotateChild));
            skillTween?.Kill();
            knifeType = (KnifeType)type;
            render.sprite = Net_Controller.ins.knifeData.knifeData[(int)type].icon;
            isFreeKnife = false;
            isKnifeOnSkill = true;
            this.controller = PhotonView.Find(viewId).GetComponent<Net_KnifeController>();
            m_transform.tag = Tags.INNER_KNIFE;
            m_Obj.layer = LayerMask.NameToLayer(LayerNames.INNER_KNIFE);
            GetPosOnSpawnSkill(knifeIndex, total, startAngle);
            m_transform.right = (m_transform.position - controller.m_transform.position).normalized;
            r2.velocity = m_transform.right * Config.BASE_KNIFE_SKILL_MOVE_SPEED;
            skillTween = DOVirtual.DelayedCall(3, m_Obj.Hide);
        }

        public void OnInstance(KnifeType type)
        {
            pv.RPC(nameof(RPCInstance), RpcTarget.All, (int)type);
        }

        [PunRPC]
        public void RPCInstance(int t)
        {
            var type = (KnifeType)t;
            m_transform.SetParent(null);
            col1.enabled = true;
            col2.enabled = false;
            childModel.localRotation = Quaternion.identity;
            CancelInvoke(nameof(RotateChild));
            knifeType = type;
            render.sprite = Net_Controller.ins.knifeData.knifeData[(int)type].icon;
            isKnifeOnSkill = false;
            isFreeKnife = true;
            isOnRemove = false;
            m_transform.tag = Tags.FREE_KNIFE;
            m_Obj.layer = LayerMask.NameToLayer(LayerNames.FREE_KNIFE);
        }

        public void OnInstanceEnemyKnife()
        {
            isFreeKnife = true;
            m_transform.tag = Tags.INNER_KNIFE;
            m_Obj.layer = LayerMask.NameToLayer(LayerNames.INNER_KNIFE);
        }

        public void MoveOnSpawn()
        {
            if (controller.countForCalculation != 0)
            {

                var x = Mathf.Sin(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                        controller.radius;
                var y = Mathf.Cos(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                        controller.radius;
                m_transform.localPosition = new Vector3(x, y);
            }
        }

        public void GetPosOnSpawnSkill(int index, int total, float startAngle)
        {
            var x = Mathf.Sin(startAngle * Mathf.Deg2Rad + index * Mathf.PI / total * 2) *
                    0.2f;
            var y = Mathf.Cos(startAngle * Mathf.Deg2Rad + index * Mathf.PI / total * 2) *
                    0.2f;
            m_transform.position = new Vector3(x, y) + controller.m_transform.position;
        }

        public Vector3 GetNextPosOnDefenseMode()
        {
            var x = Mathf.Sin(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                    Config.BASE_DEFENSE_RADIUS;
            var y = Mathf.Cos(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                    Config.BASE_DEFENSE_RADIUS;
            return new Vector3(x, y);
        }

        public Vector3 GetNextPos()
        {
            if (controller.countForCalculation > 0.01f)
            {
                var x = Mathf.Sin(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                        controller.radius;
                var y = Mathf.Cos(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                        controller.radius;
                return new Vector3(x, y);
            }

            return m_transform.localPosition;
        }

        private Tween waitTween;
        public void MoveOnRemove(float t, Vector3 f, Vector3 localPos, Quaternion localRot)
        {
            childModel.localRotation = Quaternion.identity;
            CancelInvoke(nameof(RotateChild));
            isOnRemove = true;
            isFreeKnife = true;
            isKnifeOnSkill = false;
            m_transform.tag = Tags.FREE_KNIFE;
            m_Obj.layer = LayerMask.NameToLayer(LayerNames.FREE_KNIFE);
            m_transform.position = localPos;
            m_transform.rotation = localRot;
            m_transform.SetParent(null);
            r2.isKinematic = false;

            r2.AddTorque(t);
            r2.AddForce(f);
            waitTween = DOVirtual.DelayedCall(1.5f, () =>
            {
                //gameObject.Hide();
                r2.velocity = Vector2.zero;
                r2.angularVelocity = 0;
                isOnRemove = false;
                r2.isKinematic = true;
                col1.enabled = true;
                col2.enabled = false;
            });
        }

        public void GetForceAndTorque(out float t, out Vector3 f)
        {
            t = Random.Range(torque * 0.7f, torque * 2);
            f = (transform.right + transform.up * Random.Range(-1f, 1)).normalized * Random.Range(force * 0.7f, force * 1.2f);
        }

        public void RotateChild()
        {
            childModel.Rotate(Vector3.forward * Config.SPEED_ROTATE_CHILD * Time.deltaTime);
        }

        public void SetY(float y)
        {
            childModel.localScale = new Vector3(1, y, 1) * 0.35f;
        }

        [PunRPC]
        public void OnCollisionImpediment(Vector3 pos)
        {
            r2.velocity = Vector2.zero;
            r2.angularVelocity = 0;
            isOnRemove = false;
            col1.enabled = true;
            col2.enabled = false;
            m_transform.position = pos;
        }

        private Coroutine ct;

        void OnTriggerEnter2D(Collider2D col)
        {
            if (!PhotonNetwork.IsMasterClient || !Net_Controller.IsStartGame)
                return;

            if (isFreeKnife)
            {
                if (col.CompareTag(Tags.IMPEDIMENT))
                {
                    //gameObject.Hide();
                    var pos = (Vector3)GameHelper.GetPosOuterBox(col.bounds, col2.bounds, col.transform.position,
                m_transform.position);
                    pv.RPC(nameof(OnCollisionImpediment), RpcTarget.All, pos);
                    return;
                }

            }

            if (!Net_Controller.IsPauseGame)
            {

                if (!isFreeKnife)
                {
                    if (!isKnifeOnSkill)
                    {
                        if (col.CompareTag(Tags.FREE_KNIFE))
                        {
                            var knife = col.GetComponent<Net_Knife>();
                            if (!knife.isOnRemove)
                            {
                                if (controller.state == KnifeState.Attack && controller.KnifeCount < Config.MAX_KNIFE)
                                {
                                    if (controller.isCanChange)
                                    {
                                        if (!controller.IsOnAdd(knife.GetInstanceID()))
                                        {
                                            moveOnMagnet?.Kill();
                                            //col.Hide();
                                            controller.AddID(knife.GetInstanceID());
                                            controller.AddKnife(knife, knifeIndex);
                                            if (controller.isPlayer)
                                            {
                                                AudioManager.ins.PlayAudioClip(ClipType.acl);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (col.CompareTag(Tags.INNER_KNIFE))
                    {
                        if (isKnifeOnSkill)
                        {
                            var m = col.GetComponent<Net_Knife>();
                            if (!m.isOnRemove)
                            {
                                if (m.controller.heroInfor.teamId != controller.heroInfor.teamId && 
                                    m.controller.GetInstanceID() != controller.GetInstanceID())
                                {
                                    gameObject.Hide();
                                    Poolers.ins.GetObject(Net_Controller.ins.hitEffect, m_transform.position,
                                        Quaternion.identity);
                                    skillTween?.Kill();
                                }
                            }
                        }
                        else
                        {
                            if (!controller.isProtected)
                            {
                                var m = col.GetComponent<Net_Knife>();
                                var oneRemove = false;
                                if (controller.isCanChange && controller.state == KnifeState.Attack)
                                {

                                    if (!m.isOnRemove)
                                    {
                                        if (m.controller.heroInfor.teamId != controller.heroInfor.teamId && 
                                            m.controller.GetInstanceID() != controller.GetInstanceID())
                                        {
                                            if (!controller.IsOnRemove(this.GetInstanceID()) &&
                                                !controller.IsOnAttacker(m.GetInstanceID()))
                                            {
                                                oneRemove = true;
                                            }
                                        }
                                    }
                                }

                                if (oneRemove)
                                {
                                    controller.AddRemoveID(this.GetInstanceID());
                                    controller.RemoveKnifeOnMaster(this, this.GetInstanceID(), m.GetInstanceID());
                                    controller.iInput.OnRemoveKnifeFromE();
                                    controller.AddAttackerID(m.GetInstanceID());
                                    if (m.controller.isPlayer)
                                    {
                                        Poolers.ins.GetObject(Net_Controller.ins.hitEffect, m_transform.position,
                                            Quaternion.identity);
                                        this.PostEvent(EventID.KnockKnife);
                                    }

                                    if (controller.isPlayer)
                                    {
                                        Net_MapController.ins.camFollower.CameraShake();
                                        AudioManager.ins.PlayVacham();
                                    }
                                }
                            }

                        }
                    }

                    if (col.CompareTag(Tags.IMPEDIMENT))
                    {
                        if (controller.isCanChange && controller.state == KnifeState.Attack && !controller.isProtected)
                        {
                            if (!controller.IsOnRemove(this.GetInstanceID()))
                            {
                                var r = Random.value;
                                if (r > controller.doggeValue)
                                {
                                    isOnRemove = true;
                                    controller.iInput.OnRemoveKnifeFromImpediment();
                                    controller.RemoveKnifeOnMaster(this, this.GetInstanceID(), -100000);
                                    if (controller.isPlayer)
                                    {
                                        Poolers.ins.GetObject(Net_Controller.ins.hitEffect, m_transform.position,
                                            Quaternion.identity);
                                        Net_MapController.ins.camFollower.CameraShake();
                                        AudioManager.ins.PlayVacham();

                                    }
                                }

                            }
                        }
                    }

                    if (col.CompareTag(Tags.PLAYER))
                    {
                        if (!isOnRemove)
                        {
                            var c = col.GetComponent<Net_KnifeController>();
                            if (c.heroInfor.teamId != controller.heroInfor.teamId && !c.isProtected && !controller.isProtected && 
                                c.GetInstanceID() != controller.GetInstanceID())
                            {
                                Net_Controller.ins.StartAnyCoroutine((GameHelper.EndOfFrame(() =>
                                {
                                    if (!c.isDeath && c.isCanDeath)
                                    {
                                        c.OnDie();
                                        controller.AddKillId(c.GetInstanceID());
                                        Net_UI.Ins.inGame.gamePlay.uiKill.AddKill(new KillData
                                        {
                                            p1Name = controller.heroInfor.heroName,
                                            p2Name = c.heroInfor.heroName,
                                            icon1 = controller.heroInfor.iconAvt,
                                            icon2 = c.heroInfor.iconAvt
                                        });
                                    }
                                })));

                            }
                        }

                    }

                }
                else
                {
                    if (col.CompareTag(Tags.MAGNET))
                    {
                        var c = col.GetComponentInParent<Net_KnifeController>();
                        if (c.KnifeCount < 50)
                        {
                            moveOnMagnet?.Kill();
                            moveOnMagnet = m_transform.DOMove(col.transform.position, Config.TIME_MAGNET);
                        }
                    }

                    if (col.CompareTag(Tags.PLAYER))
                    {
                        if (!isOnRemove)
                        {
                            var c = col.GetComponent<Net_KnifeController>();
                            if (c.state == KnifeState.Attack && c.KnifeCount < Config.MAX_KNIFE)
                            {
                                if (c.isCanChange)
                                {
                                    if (!c.IsOnAdd(this.GetInstanceID()))
                                    {
                                        moveOnMagnet?.Kill();
                                        //col.Hide();
                                        c.AddID(this.GetInstanceID());
                                        c.AddKnife(this, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}