﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Games.Photon.Scripts.Controller;
using Assets.Games.Photon.Scripts.Data;
using Assets.Games.Photon.Scripts.Game;
using Assets.Games.Photon.Scripts.NetUI;
using DG.Tweening;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using static KnifeController;
using Random = UnityEngine.Random;

public class Net_KnifeController : MonoBehaviourPun
{
    public Transform m_transform;
    public GameObject m_Obj;
    public GameObject knife;
    public Transform content;
    public GameObject directionItem;

    [Header("Player Infor")]
    public HeroInfor heroInfor;

    public Net_MaskController maskController;
    public List<Net_Knife> knifes;
    public KnifeState state;
    public KnifeType knifeType;
    public IController iInput;
    public int KnifeCount
    {
        get => knifeCount;
        set
        {
            knifeCount = value;
            OnChangeKnife();
        }
    }
    private int knifeCount;
    [Header("Config Value")]
    public float speed;
    public float speedCalc, speedCalcOnRemove;
    public float radius;
    public float doggeValue;
    public bool isRotateKnife, isCanDeath, isProtected, isDeath;
    public float countForCalculation;
    private Tween calTween, switchTween;
    public bool isPlayer;
    private List<int> addKnifeIDs;
    private List<int> removeKnifeIDs;
    private List<int> attackerIDs;
    public bool isCanChange;
    public bool isOnSwitchState;
    public float timeWaitForAction;
    private Action skillAction;
    private Coroutine descreaseKnifePerSecond, skillCoroutine;
    public DirectionItem moveFollowItem;
    public PlayerDefineID defineID;
    private Coroutine iDefenseCT;

    void Start()
    {
        countForCalculation = KnifeCount;

        //OnInstance(KnifeType.Batman);
    }

    #region CallBack

    void OnApplicationQuit()
    {
        this.SendQuitEvent();
    }

    // optional
    void OnApplicationPause(bool paused)
    {
        if (paused)
        {
            this.SendQuitEvent();
        }
    }

    // optional
    //void OnApplicationFocus(bool focused)
    //{
    //    if (!focused)
    //    {
    //        this.SendQuitEvent();
    //    }
    //}

    void SendQuitEvent()
    {
        if (base.photonView.IsMine)
        {
            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                PhotonNetwork.SetMasterClient(GetLocalPlayerConnect());
            }
            // send event, add your code here
            base.photonView.RPC(nameof(ReleaseAllKnife), RpcTarget.All, base.photonView.ViewID);
            PhotonNetwork.SendAllOutgoingCommands(); // send it right now
        }

    }

    [PunRPC]
    public void ReleaseAllKnife(int viewId)
    {
        if (base.photonView.ViewID == viewId && !heroInfor.isAI)
        {
            GetComponent<Net_PlayerController>().enabled = false;
            Net_Controller.ins.activeControllers.Remove(this);
        }

    }


    private Player GetLocalPlayerConnect()
    {
        foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
        {
            if (!item.IsMasterClient && !item.IsInactive)
            {
                return item;
            }
        }

        return PhotonNetwork.LocalPlayer;
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
    }

    private void NetworkingClient_EventReceived(ExitGames.Client.Photon.EventData obj)
    {
        if (PhotonNetwork.IsMasterClient)
            return;

        if (obj.Code == EventGameCode.InitController)
        {
            var data = (object[])obj.CustomData;
           

            if ((int)data[0] == base.photonView.ViewID)
            {
                Client_Instance((string)data[1]);
                for (int i = 2; i < data.Length; i++)
                {
                    InitKnifeFromServer((int)data[i], i - 2);
                }
            }
            return;
        }

        if (obj.Code == EventGameCode.OnAddknife)
        {
            var data = (object[])obj.CustomData;
            if (base.photonView.ViewID == (int)data[0])
            {
                AddKnifeFromServer((int)data[1]);
            }
            return;
        }

        if (obj.Code == EventGameCode.OnRemoveKnife)
        {
            var data = (object[])obj.CustomData;
            if (base.photonView.ViewID == (int)data[0])
            {
                RemoveKnifeFromServer((int)data[1], (int)data[2], (int)data[3], (float)data[4], (Vector3)data[5], (Vector3)data[6], (Quaternion)data[7]);
            }
            return;
        }

        if (obj.Code == EventGameCode.OnRemoveKnifeFromDefense)
        {
            var data = (object[])obj.CustomData;
            if (base.photonView.ViewID == (int)data[0])
            {
                RemoveKnifeDefenseFromServer((int)data[1], (float)data[2], (Vector3)data[3], (Vector3)data[4], (Quaternion)data[5]);
            }
            return;
        }

        if (obj.Code == EventGameCode.OnAddKnifeFromMask)
        {
            var data = (object[])obj.CustomData;
            if (base.photonView.ViewID == (int)data[0])
            {
                MaskSpawnKnifeFromServer((int)data[1]);
            }
        }
    }

    #endregion

    public bool isOnStartGame = false;
    public void OnInstance(HeroInfor heroInfor)
    {
        if (PhotonNetwork.IsMasterClient)
            isOnStartGame = true;
        else
            isOnStartGame = false;
        SetUpOnStart(heroInfor);
        SpawnMultiKnife();

    }

    private void SetUpOnStart(HeroInfor heroInfor)
    {
        this.heroInfor = heroInfor;
        heroInfor.isCanRevival = true;
        knifeType = heroInfor.knifeType;
        iInput = GetComponent<IController>();
        var data = Net_Controller.ins.knifeData.knifeData[(int)heroInfor.knifeType];
        var maskInfor = Net_Controller.ins.maskData.maskData[(int)heroInfor.maskType];

        isDeath = false;
        isCanDeath = true;
        isPlayer = heroInfor.isPlayer;
        state = KnifeState.Attack;
        //if (isPlayer)
        //{
        //    SwitchToDefenseState();
        //}
        isCanChange = true;
        content.DetachChildren();
        knifes = new List<Net_Knife>();
        addKnifeIDs = new List<int>();
        removeKnifeIDs = new List<int>();
        killIDs = new List<int>();
        attackerIDs = new List<int>();
        speed = Config.BASE_ROTATE_SPEED * (1 + data.addSpeed + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddSpeed : 0));
        KnifeCount = Config.BASE_KNIFE_COUNT + (data.addKnifeInStart + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddKnifeInStart : 0));
        radius = Config.BASE_RADIUS;
        doggeValue = data.avoidObstacle + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAvoidObstacle : 0);
        iInput.SetUpSpeed(Config.BASE_MOVE_SPEED);
        switch (data.buffType)
        {
            case BuffType.None:
                break;
            case BuffType.UpDownAttack:
                timeWaitForAction = data.value;
                skillAction = UpDownSkill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            case BuffType.LeftRightAttack:
                timeWaitForAction = data.value;
                skillAction = LeftRightSkill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            case BuffType.DamageFour:
                timeWaitForAction = data.value;
                skillAction = Dmg1Skill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            case BuffType.DamageX:
                timeWaitForAction = data.value;
                skillAction = Dmg2Skill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        countForCalculation = KnifeCount;
        maskController.SetUp(maskInfor);
        if (iDefenseCT != null)
        {
            StopCoroutine(iDefenseCT);
        }
        iDefenseCT = StartCoroutine(IAnim());
        heroInfor.heroPoint = KnifeCount;
        iInput.SetCanMove();
        defineID.Net_SetUp(heroInfor);
        if (!isPlayer)
        {
            moveFollowItem = Instantiate(directionItem, m_transform.position, Quaternion.identity).GetComponent<DirectionItem>();
            moveFollowItem.OnSpawn(heroInfor.color);
        }
        else
        {
            var f = GameManager.ins.GetMotion(MotionType.Start);
            if (f.isHasEquip)
            {
                maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
            }
        }
        isOnStartGame = true;
    }

    public void Client_Instance(string nickName)
    {
        var heroInfo = Net_Controller.ins.heroInfors.Find(s => s.heroName == nickName);
        SetUpOnStart(heroInfo);
    }

    //#if UNITY_EDITOR
    //    void Update()
    //    {
    //        if (Input.GetKeyDown(KeyCode.J))
    //        {
    //            AddRemoveID(this.GetInstanceID());
    //            RemoveKnife(knifes[0]);
    //            //AddAttackerID(m.GetInstanceID());
    //        }

    //        if (Input.GetKeyDown(KeyCode.K))
    //        {
    //            AddRemoveID(this.GetInstanceID());
    //            iInput.OnRemoveKnifeFromImpediment();
    //            RemoveKnife(knifes[Random.Range(0, knifes.Count)]);
    //            RemoveKnife(knifes[Random.Range(0, knifes.Count)]);
    //            RemoveKnife(knifes[Random.Range(0, knifes.Count)]);
    //        }

    //        if (Input.GetKeyDown(KeyCode.L))
    //        {
    //            //AddID(col.GetInstanceID());
    //            AddKnife(0);
    //            AddKnife(0);
    //        }

    //        if (Input.GetKeyDown(KeyCode.R))
    //        {
    //            OnDie();
    //        }
    //    }

    //#endif
    public void SwitchToDefenseState()
    {
        base.photonView.RPC(nameof(RPC_Defense), RpcTarget.All);
    }

    public void SwitchToAttackState()
    {
        base.photonView.RPC(nameof(RPC_Attack), RpcTarget.All);
    }

    [PunRPC]
    public void RPC_Defense()
    {
        isOnSwitchState = true;

        switchTween?.Kill();
        calTween?.Kill();
        var lastV = radius;
        var lastCount = countForCalculation;
        if (isPlayer)
        {
            Net_MapController.ins.camFollower.ZoomCamera(Config.DEFNSE_CAMSZIE);
        }

        switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 7, value =>
        {
            foreach (var item in knifes)
            {
                radius = Mathf.Lerp(lastV, Config.BASE_DEFENSE_RADIUS, value * 2);
                countForCalculation = Mathf.Lerp(lastCount, KnifeCount, value);
                var curRot = item.m_transform.right;
                var des = (item.m_transform.position - m_transform.position).normalized;
                item.m_transform.right = Vector3.Lerp(curRot, des, value).normalized;
                item.MoveOnSpawn();
                item.SetY(-1);
            }
        }).OnComplete(() =>
        {
            if (descreaseKnifePerSecond != null)
            {
                StopCoroutine(descreaseKnifePerSecond);
            }

            if (m_Obj.activeInHierarchy)
                descreaseKnifePerSecond = StartCoroutine(IDefense());
            state = KnifeState.Defense;
            isOnSwitchState = false;
            if (isPlayer)
                maskController.Defense();
        });
    }

    [PunRPC]
    public void RPC_Attack()
    {
        isOnSwitchState = true;
        calTween?.Kill();
        if (isPlayer)
        {
            Net_MapController.ins.camFollower.ZoomCamera(Config.ATTACK_CAMSZIE);
        }
        state = KnifeState.Attack;
        if (isPlayer)
            maskController.DisableDefense();
        if (descreaseKnifePerSecond != null)
        {
            StopCoroutine(descreaseKnifePerSecond);
        }
        switchTween?.Kill();
        var lastV = radius;
        var lastCount = countForCalculation;
        switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 4, value =>
        {
            foreach (var item in knifes)
            {
                radius = Mathf.Lerp(lastV, GetRadius(), value);
                countForCalculation = Mathf.Lerp(lastCount, KnifeCount, value);
                var curRot = item.m_transform.up;
                var des = (item.m_transform.position - m_transform.position).normalized;
                item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                item.MoveOnSpawn();
                item.SetY(1);
            }
        }).OnComplete(() =>
        {
            isOnSwitchState = false;
        });
    }

    private void SpawnMultiKnife()
    {
        countForCalculation = KnifeCount;
        StartCoroutine(IMultiSpawn());
    }

    IEnumerator IMultiSpawn()
    {
        data = new object[KnifeCount + 2];
        data[0] = base.photonView.ViewID;
        data[1] = heroInfor.heroName;
        for (int i = 0; i < KnifeCount; i++)
        {
            SpawnKnife(i);
            yield return null;
        }

        PhotonNetwork.RaiseEvent(EventGameCode.InitController, data, RaiseEventOptions.Default, SendOptions.SendReliable);
    }

    private object[] data;

    public void SpawnKnife(int index)
    {
        var knife = MasterManager.InstantiateScenceNetworkGo(this.knife, m_transform.position, Quaternion.identity).GetComponent<Net_Knife>();
        //knife.m_transform.localPosition = Vector3.up;

        var controller = knife.Cast<Net_Knife>();
        controller.OnInstance(this, index, knifeType);
        controller.m_transform.SetParent(content);
        knifes.Add(controller);

        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            if (Math.Abs(countForCalculation) > 0)
            {
                controller.MoveOnSpawn();
            }
            knife.m_transform.up = (knife.m_transform.position - m_transform.position).normalized;
        }));

        data[index + 2] = knife.pv.ViewID;
    }

    private void InitKnifeFromServer(int viewId, int index)
    {
        var p = PhotonView.Find(viewId);
        var knife = p.GetComponent<Net_Knife>();

        var controller = knife.Cast<Net_Knife>();
        controller.OnInstance(this, index, knifeType);
        controller.m_transform.SetParent(content);
        knifes.Add(controller);

        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            if (Math.Abs(countForCalculation) > 0)
            {
                controller.MoveOnSpawn();
            }
            knife.m_transform.up = (knife.m_transform.position - m_transform.position).normalized;
        }));
    }

    private void AddKnifeFromServer(int viewId)
    {
        var p = PhotonView.Find(viewId);
        var k = p.GetComponent<Net_Knife>();

        KnifeCount++;
        CalculatorRadius();
        //ChangeKnifeIndexAfterAdd(index);
        SpawnKnife(k, KnifeCount - 1);

        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            CalcValue(KnifeCount, KnifeCount - 1);
        }));
    }

    public void SpawnKnifeFromMask(int index)
    {
        var knife = MasterManager.InstantiateScenceNetworkGo(this.knife, new Vector3(100, 100), Quaternion.identity).GetComponent<Net_Knife>();
        knife.m_transform.SetParent(content);
        if (knifes.Count > 0)
        {
            knife.m_transform.localPosition = knifes[0].m_transform.localPosition;
        }
        else
        {
            knife.m_transform.localPosition = Vector3.right * Config.BASE_RADIUS;
        }
        //knife.m_transform.localPosition = Vector3.up;

        var controller = knife.Cast<Net_Knife>();
        controller.OnInstance(this, index, knifeType);

        knifes.Add(controller);
        Net_Controller.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            if (Math.Abs(countForCalculation) > 0)
            {
                controller.MoveOnSpawn();
            }
            knife.m_transform.up = (knife.m_transform.position - m_transform.position).normalized;
        }));
        var data = new object[] { base.photonView.ViewID, knife.pv.ViewID };
        PhotonNetwork.RaiseEvent(EventGameCode.OnAddKnifeFromMask, data, RaiseEventOptions.Default, SendOptions.SendReliable);
    }

    public void SpawnKnifeFromMaskServer(int viewId)
    {
        var p = PhotonView.Find(viewId);
        var knife = p.GetComponent<Net_Knife>();
        if (knifes.Count > 0)
        {
            knife.m_transform.localPosition = knifes[0].m_transform.localPosition;
        }
        else
        {
            knife.m_transform.localPosition = Vector3.right * Config.BASE_RADIUS;
        }
        //knife.m_transform.localPosition = Vector3.up;

        var controller = knife.Cast<Net_Knife>();
        controller.m_transform.SetParent(content);
        controller.OnInstance(this, KnifeCount - 1, knifeType);


        knifes.Add(controller);
        Net_Controller.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            if (Math.Abs(countForCalculation) > 0)
            {
                controller.MoveOnSpawn();
            }
            knife.m_transform.up = (knife.m_transform.position - m_transform.position).normalized;
        }));
    }

    public void AddKnife(int startIndex)
    {
        isCanChange = false;
        iInput.OnAddKnife();
        KnifeCount++;
        OnChangeKnife();
        CalculatorRadius();
        ChangeKnifeIndexAfterAdd(startIndex);
        SpawnKnife(startIndex);
        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            addKnifeIDs.Clear();
            isCanChange = true;
            CalcValue(KnifeCount);
        }));
    }

    public void MaskSpawnKnife()
    {
        isCanChange = false;
        iInput.OnAddKnife();
        KnifeCount++;
        OnChangeKnife();
        CalculatorRadius();
        //ChangeKnifeIndexAfterAdd(KnifeCount - 1);
        SpawnKnifeFromMask(knifeCount - 1);
        Net_Controller.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            addKnifeIDs.Clear();
            isCanChange = true;
            if (state == KnifeState.Attack && !isOnSwitchState)
            {
                CalcValue(KnifeCount);
            }
            else
            {
                CalcValueOnDefense(KnifeCount);
            }
        }));
    }

    public void MaskSpawnKnifeFromServer(int viewId)
    {
        isCanChange = false;
        iInput.OnAddKnife();
        KnifeCount++;
        OnChangeKnife();
        CalculatorRadius();
        //ChangeKnifeIndexAfterAdd(KnifeCount - 1);
        SpawnKnifeFromMaskServer(viewId);
        Net_Controller.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            addKnifeIDs.Clear();
            isCanChange = true;
            if (state == KnifeState.Attack && !isOnSwitchState)
            {
                CalcValue(KnifeCount);
            }
            else
            {
                CalcValueOnDefense(KnifeCount);
            }
        }));
    }

    public void SpawnKnife(Net_Knife k, int index)
    {
        k.m_transform.SetParent(content);
        k.OnInstance(this, index, knifeType);
        knifes.Add(k);
    }

    public void AddKnife(Net_Knife k, int index)
    {
        isCanChange = false;
        iInput.OnAddKnife();
        KnifeCount++;
        OnChangeKnife();
        CalculatorRadius();
        //ChangeKnifeIndexAfterAdd(index);
        SpawnKnife(k, KnifeCount - 1);
        if (isPlayer)
        {
            this.PostEvent(EventID.HasKnife);
        }
        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            addKnifeIDs.Clear();
            isCanChange = true;
            CalcValue(KnifeCount, KnifeCount - 1);
        }));

        var data = new object[] { base.photonView.ViewID, k.pv.ViewID };
        PhotonNetwork.RaiseEvent(EventGameCode.OnAddknife, data, RaiseEventOptions.Default, SendOptions.SendUnreliable);
    }

    struct VTemp
    {
        public Vector3 rot;
        public Vector3 pos;
    }

    public void CalcValue(int knifeCount, int index)
    {
        isOnCalc = true;
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        var l = new List<VTemp>();
        foreach (var m in knifes)
        {
            l.Add(new VTemp
            {
                pos = m.m_transform.localPosition,
                rot = m.m_transform.up,
            });
            m.SetY(1);
        }

        calTween = DOVirtual.Float(last, knifeCount, speedCalc, value =>
        {
            countForCalculation = value;
            var f = 2 * (value - last) / (knifeCount - last);
            for (int i = 0; i < knifes.Count; i++)
            {
                if (knifes[i].knifeIndex != index)
                {
                    if (Mathf.Abs(countForCalculation) > 0.1f)
                    {

                        var v = Vector3.Slerp(l[i].pos, knifes[i].GetNextPos(), f * 2);
                        //Debug.Log();
                        if (!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                        var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                        knifes[i].m_transform.up = Vector3.Lerp(l[i].rot, des, value);

                    }
                }
                else
                {
                    if (Mathf.Abs(countForCalculation) > 0.1f)
                    {
                        //var nextPos = knifes[i].GetNextPos();
                        var v = Vector3.Slerp(l[i].pos, knifes[i].GetNextPos(), f);
                        //var x = Mathf.Lerp(l[i].pos.x, nextPos.x, f);
                        //var y = Mathf.Lerp(l[i].pos.y, nextPos.y, f);
                        if (!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                        var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                        knifes[i].m_transform.up = Vector3.Lerp(l[i].rot, des, value);
                    }
                }

            }
        }).OnComplete(() =>
        {
            isOnCalc = false;
            isOnSwitchState = false;
        });
    }



    public void AddID(int id)
    {
        addKnifeIDs.Add(id);
    }

    public void AddRemoveID(int id)
    {
        removeKnifeIDs.Add(id);
    }

    public void AddAttackerID(int id)
    {
        removeKnifeIDs.Add(id);
    }

    public bool IsOnRemove(int id)
    {
        return removeKnifeIDs.Contains(id);
    }

    public bool IsOnAdd(int id)
    {
        return addKnifeIDs.Contains(id);
    }
    public bool IsOnAttacker(int id)
    {
        return attackerIDs.Contains(id);
    }

    public void DefenseRemoveKnife()
    {
        var item = knifes.Find(s => s.knifeIndex == KnifeCount - 1);

        item.GetForceAndTorque(out float t, out Vector3 f);
        var data = new object[] { photonView.ViewID, item.pv.ViewID, t, f, item.m_transform.localPosition, item.m_transform.localRotation };
        RemoveKnifeFromeDefense(item, t, f, item.m_transform.position, item.m_transform.rotation);

        PhotonNetwork.RaiseEvent(EventGameCode.OnRemoveKnifeFromDefense, data, RaiseEventOptions.Default, SendOptions.SendReliable);

    }

    public void RemoveKnifeDefenseFromServer(int viewId, float t, Vector3 f, Vector3 localPos, Quaternion localRot)
    {
        var pv = PhotonView.Find(viewId);
        var item = pv.GetComponent<Net_Knife>();
        RemoveKnifeFromeDefense(item, t, f, localPos, localRot);
    }

    private void RemoveKnifeFromeDefense(Net_Knife item, float t, Vector3 f, Vector3 localPos, Quaternion localRot)
    {
        isCanDeath = false;
        isCanChange = false;
        iInput.OnRemoveKnifeFromImpediment();
        KnifeCount--;
        OnChangeKnife();
        CalculatorRadius();
        //ChangeKnifeIndexAfterRemove(startIndex);
        item.MoveOnRemove(t, f, localPos, localRot);
        knifes.Remove(item);
        Net_Controller.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            isCanChange = true;
            attackerIDs.Clear();
            removeKnifeIDs.Clear();
            if (!isOnSwitchState && state != KnifeState.Defense)
            {
                CalcValueOnRemove(KnifeCount);
            }
            else
            {
                if (state == KnifeState.Defense)
                {
                    CalcValueOnDefense(KnifeCount);
                }
                else
                {
                    countForCalculation = KnifeCount;
                }

            }

        }));
    }

    public void RemoveKnifeOnMaster(Net_Knife knife, int removeId, int attackId)
    {
        if (knifes.Contains(knife))
        {
            knife.GetForceAndTorque(out float t, out Vector3 f);
            var data = new object[] { base.photonView.ViewID, knife.pv.ViewID, removeId, attackId, t, f, knife.m_transform.localPosition, knife.m_transform.localRotation };
            RemoveKnife(knife, removeId, attackId, t, f, knife.m_transform.position, knife.m_transform.rotation);
            PhotonNetwork.RaiseEvent(EventGameCode.OnRemoveKnife, data, RaiseEventOptions.Default, SendOptions.SendUnreliable);
        }
    }

    public void RemoveKnife(Net_Knife knife, int removeId, int attackId, float t, Vector3 f, Vector3 localPos,Quaternion localRot)
    {
        if (knifes.Contains(knife))
        {
            AddRemoveID(removeId);
            AddAttackerID(attackId);
            isCanChange = false;
            KnifeCount--;
            CalculatorRadius();
            ChangeKnifeIndexAfterRemove(knife.knifeIndex);
            knifes.Remove(knife);
            knife.MoveOnRemove(t, f, localPos, localRot);
            OnChangeKnife();
            Net_Controller.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
            {
                isCanDeath = true;
                isCanChange = true;
                attackerIDs.Clear();
                removeKnifeIDs.Clear();
                if (!isOnSwitchState && state != KnifeState.Defense)
                {
                    CalcValueOnRemove(KnifeCount);
                }
                else
                {
                    if (state == KnifeState.Defense)
                    {
                        CalcValueOnDefense(KnifeCount);
                    }
                    else
                    {
                        switchTween?.Kill();
                        CalcValueOnRemove(KnifeCount);
                    }

                }

            }));
        }
    }

    public void RemoveKnifeFromServer(int viewId, int removeId, int attackId, float t, Vector3 f, Vector3 localPos, Quaternion localRot)
    {
        var p = PhotonView.Find(viewId);
        var knife = p.GetComponent<Net_Knife>();
        RemoveKnife(knife, removeId, attackId, t, f, localPos, localRot);
    }

    public void CalcValueOnDefense(int destination)
    {
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        if (!isProtected)
        {
            isCanDeath = true;
        }
        var l = new List<Vector3>();
        foreach (var m in knifes)
        {
            l.Add(m.m_transform.right);
            m.SetY(-1);
        }
        calTween = DOVirtual.Float(last, destination, speedCalc, value =>
        {
            var f = 2 * (value - last) / (destination - last);
            countForCalculation = value;
            for (int i = 0; i < knifes.Count; i++)
            {
                var des = (knifes[i].m_transform.position - m_transform.position).normalized;

                knifes[i].m_transform.right = Vector3.Lerp(l[i], des, f).normalized;
                var v = Vector3.Lerp(knifes[i].m_transform.localPosition, knifes[i].GetNextPosOnDefenseMode(), f);
                if (!float.IsNaN(v.x))
                    knifes[i].m_transform.localPosition = v;

            }

        }).OnComplete(() => { isOnSwitchState = false; });

    }

    public bool isOnCalc;
    public void CalcValue(int destination)
    {
        isOnCalc = true;
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        var l = new List<Vector3>();
        foreach (var m in knifes)
        {
            l.Add(m.m_transform.up);
            m.SetY(1);
        }

        calTween = DOVirtual.Float(last, destination, speedCalc / 4, value =>
        {
            countForCalculation = value;
            var f = 2 * (value - last) / (destination - last);

            for (int i = 0; i < knifes.Count; i++)
            {
                if (Mathf.Abs(countForCalculation) > 0.1f)
                {
                    try
                    {
                        var v = Vector3.Slerp(knifes[i].m_transform.localPosition, knifes[i].GetNextPos(), f);
                        if (!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                        var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                        knifes[i].m_transform.up = Vector3.Lerp(l[i], des, value);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }



                }
            }

        }).OnComplete(() =>
        {
            isOnCalc = false;
            isOnSwitchState = false;
        });

    }

    public void CalcValueOnRemove(int destination)
    {
        isOnCalc = true;
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        var l = new List<Vector3>();
        foreach (var m in knifes)
        {
            l.Add(m.m_transform.up);
            var des = (m.m_transform.position - m_transform.position).normalized;
            m.m_transform.up = des.normalized;
        }
        calTween = DOVirtual.Float(last, destination, speedCalcOnRemove, value =>
        {
            countForCalculation = value;
            var f = 2 * (value - last) / (destination - last);

            for (int i = 0; i < knifes.Count; i++)
            {
                if (Mathf.Abs(countForCalculation) > 0.1f)
                {
                    try
                    {
                        var v = Vector3.Lerp(knifes[i].m_transform.localPosition, knifes[i].GetNextPos(), f);
                        //Debug.Log(v + "   " + knifes[i].GetNextPos() + "  " + knifes[i].m_transform.position + "   " + f);
                        if (!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }


                    var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                    knifes[i].m_transform.up = Vector3.Lerp(l[i], des, f).normalized;
                }
            }

        }).OnComplete(() =>
        {
            isOnCalc = false;
            isOnSwitchState = false;
        });
    }

    public void CalculatorRadius()
    {
        var t = (Config.BASE_RADIUS + (KnifeCount - Config.BASE_KNIFE_COUNT) * Config.BASE_ADD_RADIUS);
        radius = (t > Config.MAX_RADIUS ? Config.MAX_RADIUS : t);
    }

    public float GetRadius()
    {
        var t = (Config.BASE_RADIUS + (KnifeCount - Config.BASE_KNIFE_COUNT) * Config.BASE_ADD_RADIUS);
        return (t > Config.MAX_RADIUS ? Config.MAX_RADIUS : t);
    }

    public void GetRadius(out float distance)
    {
        distance = (Config.BASE_RADIUS + (KnifeCount - Config.BASE_KNIFE_COUNT) * Config.BASE_ADD_RADIUS);
    }

    private void ChangeKnifeIndexAfterAdd(int startIndex)
    {
        foreach (var item in knifes)
        {
            var i = GameHelper.GetIndexAfterAdd(item.knifeIndex, startIndex);
            item.knifeIndex = i;
            item.render.sortingOrder = 100 - i;
        }
    }

    private void ChangeKnifeIndexAfterRemove(int startIndex)
    {
        foreach (var item in knifes)
        {
            var i = GameHelper.GetIndexAfterRemove(item.knifeIndex, startIndex);
            item.knifeIndex = i;
            item.render.sortingOrder = 100 - i;
        }
    }

    #region Animation

    public void AnimationOff(int f, int s, int k)
    {
        if (!isOnSwitchState && !isOnCalc)
        {
            switchTween?.Kill();

            var lastV = radius;
            switchTween = DOVirtual.Float(0, 0.05f, Config.BASE_TIME_SWITCH_STATE / s, value =>
            {
                foreach (var item in knifes)
                {
                    radius = Mathf.Lerp(lastV, Config.BASE_DEFENSE_RADIUS, value * f);
                    var curRot = item.m_transform.right;
                    var des = (item.m_transform.position - m_transform.position).normalized;
                    item.m_transform.right = Vector3.Lerp(curRot, des, value).normalized;
                    item.MoveOnSpawn();
                }
            }).OnComplete(() =>
            {
                var m = radius;
                switchTween = DOVirtual.DelayedCall(Config.BASE_TIME_SWITCH_STATE / 10, () =>
                {
                    switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / k, value =>
                    {
                        foreach (var item in knifes)
                        {
                            radius = Mathf.Lerp(m, GetRadius(), value);
                            var curRot = item.m_transform.up;
                            var des = (item.m_transform.position - m_transform.position).normalized;
                            item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                            item.MoveOnSpawn();
                        }
                    });
                });

            });
        }
    }

    public void AnimationOn()
    {
        if (!isOnSwitchState && !isOnCalc)
        {
            switchTween?.Kill();
            var l = new List<float>();
            foreach (var m in knifes)
            {
                //var temp = m.m_transform.localRotation.eulerAngles.z;
                //temp = temp + 15 > 360 ? temp - 360 : temp;
                if (m.m_transform.localRotation.eulerAngles.z > 345)
                {

                    l.Add(m.m_transform.localRotation.eulerAngles.z - 360);
                }
                else
                {
                    l.Add(m.m_transform.localRotation.eulerAngles.z);
                }
            }

            switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 5, value =>
            {
                var x = Mathf.Lerp(0, 15, value);
                var count = 0;
                foreach (var item in knifes)
                {
                    var localRotation = item.m_transform.localRotation;
                    var rotation = localRotation.eulerAngles;
                    rotation.z = l[count] + x;
                    localRotation.eulerAngles = rotation;
                    item.m_transform.localRotation = localRotation;
                    count++;
                }
            }).OnComplete(() =>
            {
                switchTween = DOVirtual.DelayedCall(Config.BASE_TIME_SWITCH_STATE / 4, () =>
                {
                    switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 5, value =>
                    {
                        foreach (var item in knifes)
                        {
                            var curRot = item.m_transform.up;
                            var des = (item.m_transform.position - m_transform.position).normalized;
                            item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                        }
                    });
                });
            });
        }
    }

    IEnumerator IAnim()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(2, 5f));
            if (PhotonNetwork.IsMasterClient)
            {
                if (KnifeCount > 15 && !isOnSwitchState && state == KnifeState.Attack)
                {
                    var f = UnityEngine.Random.Range(5, 8);
                    var s = UnityEngine.Random.Range(4, 8);
                    var k = UnityEngine.Random.Range(2, 5);
                    var r = UnityEngine.Random.value;
                    //if (r > 0.5f)
                    //{
                    //    AnimationOff(f, s, k);

                    //}
                    //else
                    //{
                    //    AnimationOn();
                    //}
                    base.photonView.RPC(nameof(Net_Anim), RpcTarget.All, r, f, s, k);
                }
            }

        }
    }

    [PunRPC]
    public void Net_Anim(float r, int f, int s, int k)
    {
        if (r > 0.5f)
        {
            AnimationOff(f, s, k);
        }
        else
        {
            AnimationOn();
        }
    }

    #endregion

    #region KnifeSkill

    public void UpDownSkill()
    {
        for (int i = 0; i < 2; i++)
        {
            var m = MasterManager.InstantiateScenceNetworkGo(knife, m_transform.position, Quaternion.identity).GetComponent<Net_Knife>();
            m.OnInstanceOnSkill(this, i, 2, 0, knifeType);

        }
    }

    public void LeftRightSkill()
    {
        for (int i = 0; i < 2; i++)
        {
            var m = MasterManager.InstantiateScenceNetworkGo(knife, m_transform.position, Quaternion.identity).GetComponent<Net_Knife>();
            m.Cast<Net_Knife>().OnInstanceOnSkill(this, i, 2, 90, knifeType);

        }
    }

    public void Dmg1Skill()
    {
        for (int i = 0; i < 4; i++)
        {
            var m = MasterManager.InstantiateScenceNetworkGo(knife, m_transform.position, Quaternion.identity).GetComponent<Net_Knife>();
            m.Cast<Net_Knife>().OnInstanceOnSkill(this, i, 4, 0, knifeType);

        }
    }

    public void Dmg2Skill()
    {
        for (int i = 0; i < 4; i++)
        {
            var m = MasterManager.InstantiateScenceNetworkGo(knife, m_transform.position, Quaternion.identity).GetComponent<Net_Knife>();
            m.Cast<Net_Knife>().OnInstanceOnSkill(this, i, 4, 45, knifeType);

        }
    }

    IEnumerator ISkillAction()
    {
        var wt = new WaitForSeconds(timeWaitForAction);
        while (true)
        {
            yield return wt;
            if (PhotonNetwork.IsMasterClient && !Net_Controller.IsPauseGame)
                skillAction.Invoke();
        }
    }

    #endregion

    IEnumerator IDefense()
    {
        var wt = new WaitForSeconds(Config.BASE_TIME_DESCREASE_KNIFE);
        while (true)
        {
            yield return wt;
            if (PhotonNetwork.IsMasterClient)
            {
                if (KnifeCount > 0)
                {
                    DefenseRemoveKnife();
                }
            }

        }
    }


    private Tween endTween;
    public void OnDie()
    {
        var s = JsonHelper.ToJson(GetDieDirection());
        photonView.RPC(nameof(RPC_OnDie), RpcTarget.All, s);
    }

    [PunRPC]
    public void RPC_OnDie(string direction)
    {
        var temps = JsonHelper.FromJson<Temp_Force>(direction);

        OnDieWithForce(temps);
    }

    private void OnDieWithForce(List<Temp_Force> temps)
    {
        isDeath = true;
        isCanDeath = false;
        calTween?.Kill();
        switchTween?.Kill();
        StopAllCoroutines();
        KnifeCount = 0;
        Net_Controller.ins.activeControllers.Remove(this);

        for (int i = 0; i < knifes.Count; i++)
        {
            knifes[i].m_transform.SetParent(null);
            if (temps.Count > i)
            {
                knifes[i].MoveOnRemove(temps[i].t, temps[i].f, temps[i].pos, temps[i].rot);
            }
            else
            {
                knifes[i].GetForceAndTorque(out float m, out Vector3 f);
                knifes[i].MoveOnRemove(m, f, knifes[i].m_transform.localPosition, knifes[i].m_transform.localRotation);
            }

        }
        knifes.Clear();

        var t = Time.time;
        heroInfor.timeDie = t - Net_Controller.TimeStart;
        if (!isPlayer)
        {
            moveFollowItem.Hide();
        }
        m_Obj.Hide();

        if (isPlayer || iInput.IsOnCamera())
        {
            AudioManager.ins.PlayAudioClip(ClipType.defeat);
        }

        if (isPlayer)
        {

            endTween?.Kill();
            endTween = DOVirtual.DelayedCall(0.5f, () =>
            {
                if(Net_Controller.mode == GameMode.SinglePlayer)
                {
                    Debug.Log("Die SinglePlayer");
                    AudioManager.ins.PlayAudioClip(ClipType.defeat);
                    endTween = DOVirtual.DelayedCall(1, () => { Net_UI.Ins.ShowOnEndGame(); });
                }
                else
                {
                    if(Net_Controller.ins.heroInfors.FindIndex(s => s.teamId == heroInfor.teamId && s.timeDie == 0) != -1)
                    {
                        Debug.Log("Show Cam OnDie");
                        Net_MapController.ins.SetUpCamOnDie();
                    }
                    else
                    {
                        Debug.Log("Die TeamBattle");
                        AudioManager.ins.PlayAudioClip(ClipType.defeat);
                        endTween = DOVirtual.DelayedCall(1, () => { Net_UI.Ins.ShowOnEndGame(); });
                    }
                }                                  
            });
        }
        else
        {
            Net_Controller.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
            {
                Debug.Log(Net_Controller.ins.activeControllers.Count);
                if(Net_Controller.mode == GameMode.TeamBattle)
                {
                    if (Net_Controller.ins.activeControllers.FindIndex(s => s.heroInfor.teamId == heroInfor.teamId) == -1)
                    {
                        endTween?.Kill();

                        endTween = DOVirtual.DelayedCall(0.5f, () =>
                        {
                            if (!Net_Controller.ins.player.isDeath)
                                Net_UI.Ins.inGame.gamePlay.uiVictory.Show();
                            if (isPlayer)
                            {
                                var f = GameManager.ins.GetMotion(MotionType.Victory);
                                if (f.isHasEquip)
                                {
                                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                                }
                            }

                            endTween = DOVirtual.DelayedCall(2, () =>
                            {
                                Net_UI.Ins.inGame.gamePlay.uiVictory.Hide();
                                Net_UI.Ins.ShowOnEndGame();
                                //Net_Controller.IsPauseGame = true;
                            });
                        });
                    }
                }
                else
                {
                    if (Net_Controller.ins.activeControllers.Count <= 1)
                    {
                        endTween?.Kill();

                        endTween = DOVirtual.DelayedCall(0.5f, () =>
                        {
                            if (!Net_Controller.ins.player.isDeath)
                                Net_UI.Ins.inGame.gamePlay.uiVictory.Show();
                            if (isPlayer)
                            {
                                var f = GameManager.ins.GetMotion(MotionType.Victory);
                                if (f.isHasEquip)
                                {
                                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                                }
                            }

                            endTween = DOVirtual.DelayedCall(2, () =>
                            {
                                Net_UI.Ins.inGame.gamePlay.uiVictory.Hide();
                                Net_UI.Ins.ShowOnEndGame();
                                //Net_Controller.IsPauseGame = true;
                            });
                        });
                    }
                }
                
            }));

        }
    }

    [Serializable]
    public class Temp_Force {
        public float t;
        public Vector3 f;
        public Vector3 pos;
        public Quaternion rot;
    }

    private List<Temp_Force> GetDieDirection()
    {
        var temps = new List<Temp_Force>();
        foreach (var item in knifes)
        {
           item.GetForceAndTorque(out float m, out Vector3 k);
            temps.Add(new Temp_Force
            {
                t = m,
                f = k,
                pos = item.m_transform.localPosition,
                rot = item.m_transform.localRotation
            });
        }
        return temps;
    }

    public void OnChangeKnife()
    {
        heroInfor.heroPoint = KnifeCount + heroInfor.killPoint * 10;
        defineID.SetUpPoint(KnifeCount);
        Net_UI.Ins.inGame.gamePlay.uiRanking.OnRegisterRanking();
    }

    private List<int> killIDs;
    private Coroutine addKillCt;

    public void AddKillId(int id)
    {
        photonView.RPC(nameof(RPC_AddKill), RpcTarget.All, id);
    }

    [PunRPC]
    public void RPC_AddKill(int id)
    {
        if (!killIDs.Contains(id))
        {
            killIDs.Add(id);
            if (addKillCt != null)
            {
                Net_Controller.ins.StopCoroutine(addKillCt);
            }

            addKillCt = Net_Controller.ins.StartCoroutine(GameHelper.EndOfFrame(AddKillPoint));
        }
    }

    public void AddKillPoint()
    {

        heroInfor.killPoint += killIDs.Count;
        killIDs = new List<int>();
        OnChangeKnife();
        if (heroInfor.killPoint == 2)
        {
            this.PostEvent(EventID.DoubleKill);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.doublekill);
                var f = GameManager.ins.GetMotion(MotionType.DoubleKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }
        else if (heroInfor.killPoint == 3)
        {
            this.PostEvent(EventID.TripleKill);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.triple);
                var f = GameManager.ins.GetMotion(MotionType.TripleKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }
        else if (heroInfor.killPoint == 4)
        {
            this.PostEvent(EventID.Quadrakill);
            maskController.BuffBladeSpeed_Quadra(3, 1);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.quadra);
                var f = GameManager.ins.GetMotion(MotionType.QuadraKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }
        else if (heroInfor.killPoint == 5)
        {
            this.PostEvent(EventID.Pentakill);
            maskController.BuffBladeSpeed_Quadra(4, 1);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.penta);
                var f = GameManager.ins.GetMotion(MotionType.PentaKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }
        else
        {

            if (heroInfor.killPoint == 6)
            {
                maskController.BuffBladeSpeed_Quadra(5, 1);
                if (isPlayer)
                {
                    AudioManager.ins.PlayAudioClip(ClipType.hexa);
                    var f = GameManager.ins.GetMotion(MotionType.HexaKill);
                    if (f.isHasEquip)
                    {
                        maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                    }
                }
                this.PostEvent(EventID.HexaKill);
            }
            else if (heroInfor.killPoint > 6)
            {
                if (isPlayer)
                {
                    AudioManager.ins.PlayAudioClip(ClipType.legendary);
                    var f = GameManager.ins.GetMotion(MotionType.Legendary);
                    if (f.isHasEquip)
                    {
                        maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                    }
                }
            }
        }

        if (Net_Controller.ins.isHasFB)
        {
            AudioManager.ins.PlayAudioClip(ClipType.firstblood);
            Net_Controller.ins.isHasFB = false;
            Net_UI.Ins.inGame.gamePlay.ShowHighLight(1);
            if (isPlayer)
            {
                var f = GameManager.ins.GetMotion(MotionType.FirstBlood);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }

        if (heroInfor.isPlayer && heroInfor.killPoint >= 2)
        {
            Net_UI.Ins.inGame.gamePlay.ShowHighLight(heroInfor.killPoint);
        }

        if (Net_Controller.ins.activeControllers.Count <= 1)
        {
            AudioManager.ins.PlayAudioClip(ClipType.ace);
        }

    }

    public void OnRevival()
    {

        heroInfor.isCanRevival = false;
        knifeType = heroInfor.knifeType;
        iInput = GetComponent<IController>();
        var data = Net_Controller.ins.knifeData.knifeData[(int)heroInfor.knifeType];

        isDeath = false;
        isCanDeath = true;
        state = KnifeState.Attack;
        isCanChange = true;
        content.DetachChildren();
        knifes = new List<Net_Knife>();
        addKnifeIDs = new List<int>();
        removeKnifeIDs = new List<int>();
        attackerIDs = new List<int>();
        speed = Config.BASE_ROTATE_SPEED * (1 + data.addSpeed + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddSpeed : 0));
        radius = Config.BASE_RADIUS;
        doggeValue = data.avoidObstacle + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAvoidObstacle : 0);
        iInput.SetUpSpeed(Config.BASE_MOVE_SPEED);

        if (KnifeCount == 0)
            KnifeCount = 2;
        KnifeCount = Config.BASE_KNIFE_COUNT + (data.addKnifeInStart + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddKnifeInStart : 0));
        countForCalculation = KnifeCount;

        heroInfor.heroPoint = KnifeCount;
        iInput.SetCanMove();
        defineID.Net_SetUp(heroInfor);
        if (!isPlayer)
        {
            moveFollowItem = Poolers.ins.GetObject(directionItem, m_transform.position, Quaternion.identity).Cast<DirectionItem>();
            moveFollowItem.OnSpawn(heroInfor.color);
        }
        else
        {
            DOVirtual.DelayedCall(1, () =>
            {
                var f = GameManager.ins.GetMotion(MotionType.Revive);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = Net_Controller.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            });

        }
    }

    public void WaitSpawn()
    {
        Net_Controller.ins.isHasCharacterSpawn = false;
        m_transform.position = MapController.ins.GetRandomPos();
        var fx = Poolers.ins.GetObject(Net_Controller.ins.waitSpawnEffect, m_transform.position,
            Quaternion.Euler(-90, 0, 0));
        OnRevival();
        endTween = DOVirtual.DelayedCall(1.5f, () =>
        {
            Net_Controller.ins.isHasCharacterSpawn = false;
            maskController.BuffProtected(2f);
            fx.Hide();
            m_Obj.Show();
            Net_Controller.ins.activeControllers.Add(this);
            if (iDefenseCT != null)
            {
                StopCoroutine(iDefenseCT);
            }
            iDefenseCT = StartCoroutine(IAnim());
            SpawnMultiKnife();
            var maskInfor = Net_Controller.ins.maskData.maskData[(int)heroInfor.maskType];
            maskController.SetUp(maskInfor);
            var data = Net_Controller.ins.knifeData.knifeData[(int)heroInfor.knifeType];

            switch (data.buffType)
            {
                case BuffType.None:
                    break;
                case BuffType.UpDownAttack:
                    timeWaitForAction = data.value;
                    skillAction = UpDownSkill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                case BuffType.LeftRightAttack:
                    timeWaitForAction = data.value;
                    skillAction = LeftRightSkill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                case BuffType.DamageFour:
                    timeWaitForAction = data.value;
                    skillAction = Dmg1Skill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                case BuffType.DamageX:
                    timeWaitForAction = data.value;
                    skillAction = Dmg2Skill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        });
    }

}


