﻿using UnityEngine;
using System.Collections;

namespace Assets.Games.Photon.Scripts.Game
{
    public class Net_Rotate : MonoBehaviour
    {
        private Transform t;
        private Net_KnifeController controller;

        void Start()
        {
            t = transform;
            controller = GetComponentInParent<Net_KnifeController>();
        }

        // Update is called once per frame
        void Update()
        {
            t.Rotate(Vector3.back * controller.speed * Time.deltaTime, Space.World);
        }
    }
}