﻿using UnityEngine;
using System.Collections;

namespace Assets.Games.Photon.Scripts.Game
{
    [System.Serializable]
    public class NetworkPrefabs
    { 
        public GameObject prefab;
        public string path;
    }
}