﻿using UnityEngine;
using System.Collections;

namespace Assets.Games.Photon.Scripts.Game
{
    public class RotateObject : MonoBehaviour
    {

        public float speed;

        private Transform t;

        private void Start()
        {
            t = transform;
        }

        private void Update()
        {
            t.Rotate( speed * Time.deltaTime * Vector3.back, Space.World);
        }

    }
}