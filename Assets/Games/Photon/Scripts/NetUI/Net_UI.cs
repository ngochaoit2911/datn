﻿using UnityEngine;
using System.Collections;
using System;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using UnityEngine.UI;

namespace Assets.Games.Photon.Scripts.NetUI
{
    public class Net_UI : MonoBehaviourPunCallbacks
    {
        public static Net_UI Ins;
        public Net_UI_InGame inGame;
        private bool isOnRoom;
        public Button quiBtn;
        public RankUp rankUp;

        private void Awake()
        {
            Ins = this;
        }

        private void Start()
        {
            quiBtn.onClick.AddListener(QuitCam);
        }

        public void ShowGamePlay()
        {
            isOnRoom = true;
            inGame.Show();
            inGame.OnShowGamePlay();
            quiBtn.Hide();
        }

        public void ShowOnEndGame()
        {
            inGame.ShowOnEndGame();
        }

        internal void ShowDefault()
        {
            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                PhotonNetwork.SetMasterClient(GetLocalPlayerConnect());
            }
            PhotonNetwork.LeaveRoom();
            StartCoroutine(ILoadLevel());
        }

        private Player GetLocalPlayerConnect()
        {
            foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
            {
                if (!item.IsMasterClient && !item.IsInactive)
                {
                    return item;
                }
            }

            return PhotonNetwork.LocalPlayer;
        }

        IEnumerator ILoadLevel()
        {
            yield return new WaitUntil(() => !isOnRoom);
            SceneManager.LoadScene(1);

        }

        public void QuitCam()
        {
            quiBtn.Hide();
            ShowOnEndGame();
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
            isOnRoom = false;
        }
    }
}