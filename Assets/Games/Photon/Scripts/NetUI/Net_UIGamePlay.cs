﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Assets.Games.Photon.Scripts.Controller;
using Photon.Pun;

namespace Assets.Games.Photon.Scripts.NetUI
{
    public class Net_UIGamePlay : MonoBehaviour
    {

        public Net_UIRanking uiRanking;
        public Text timeCDText;
        private Coroutine cdCt;
        public Net_UIKill uiKill;
        public GameObject uiVictory;
        public Net_UIHighLight uiHighlight;
        public Net_ShowOnStart showOnStart;

        public void OnStartNewGame()
        {
            if (cdCt != null)
            {
                StopCoroutine(cdCt);
            }
            uiHighlight.Hide();
            uiVictory.Hide();
            uiKill.OnStartNewGame();
            cdCt = StartCoroutine(ICountDown());
        }

        public void OnEndGame()
        {
            if (cdCt != null)
            {
                StopCoroutine(cdCt);
            }
            uiHighlight.Restart();
            uiRanking.StopRanking();
        }

        IEnumerator ICountDown()
        {
            var x = new WaitForSeconds(1);
            var t = 90;
            while (true)
            {
                yield return x;
                t -= 1;
                if (t > 0)
                {
                    var m = TimeSpan.FromSeconds(t);
                    var answer = m.Minutes + ":" + m.Seconds;
                    timeCDText.text = answer;
                    if (Net_Controller.IsCanRevival && t < 15f)
                    {
                        Net_Controller.IsCanRevival = false;
                    }
                }
                else
                {
                    Net_UI.Ins.ShowOnEndGame();
                    break;
                }
            }
        }

        public void ShowHighLight(int kill)
        {
            uiHighlight.OnRegister(kill);
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

    }
}