﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Assets.Games.Photon.Scripts.NetUI
{
    public class Net_UIKill : MonoBehaviour
    {

        public Image p1Im, p2Im;
        public Text p1Name, p2Name;
        public float timeShow = 1;

        public Queue<KillData> kills;
        private Coroutine ct;
        public Animation anim;

        public void AddKill(KillData data)
        {
            this.Show();
            kills.Enqueue(data);
            ShowText();
        }

        public void Show(KillData data)
        {
            anim.Play("KillAnim");
            p1Im.sprite = data.icon1;
            p2Im.sprite = data.icon2;
            p1Name.text = data.p1Name;
            p2Name.text = data.p2Name;
        }

        public void OnStartNewGame()
        {
            kills = new Queue<KillData>();
            if (ct != null)
            {
                StopCoroutine(ct);
            }
        }

        public void ShowText()
        {
            if (ct != null)
            {
                StopCoroutine(ct);
            }

            ct = StartCoroutine(IShow());
        }

        private IEnumerator IShow()
        {
            var wt = new WaitForSeconds(timeShow);
            var wt1 = new WaitForSeconds(timeShow / 3);
            while (kills.Count > 0)
            {
                Show(kills.Dequeue());
                if (kills.Count > 0)
                {
                    yield return wt1;
                }
                else
                {
                    yield return wt;
                }

            }

            this.Hide();
        }
    }
}