﻿using UnityEngine;
using System.Collections;

namespace Assets.Games.Photon.Scripts.NetUI
{
    public class Net_UI_InGame : MonoBehaviour
    {

        public Net_UIGamePlay gamePlay;
        public Net_UIRevenge revenge;
        public Net_UIChampion champion;
        public Net_UIChampionBonus bonus;

        public void ShowDefault()
        {
            gamePlay.Hide();
            revenge.Hide();
            champion.Hide();
            bonus.Hide();
        }

        public void ShowOnEndGame()
        {
            champion.OnShow();
            gamePlay.OnEndGame();
        }

        public void OnShowGamePlay()
        {
            gamePlay.Show();
            champion.isCanShow = true;
            gamePlay.uiRanking.Generate();
        }
    }
}