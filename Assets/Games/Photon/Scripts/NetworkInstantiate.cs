﻿using Assets.Games.Photon.Scripts.Data;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkInstantiate : MonoBehaviour
{
    public GameObject networkGo;
    private void Awake()
    {
        var pos = new Vector3(Random.Range(-3, 3f), Random.Range(-5, 5f));
        MasterManager.InstantiateNetworkGo(networkGo, pos, Quaternion.identity);
    }
}
