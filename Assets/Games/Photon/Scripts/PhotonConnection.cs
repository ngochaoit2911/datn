﻿using Assets.Games.Photon.Scripts.Data;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonConnection : MonoBehaviourPunCallbacks
{
    public int sendRatde = 20;
    public int serialzationRate = 10;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("OnConnecting");
        Application.targetFrameRate = 60;
        PhotonNetwork.SendRate = sendRatde;
        PhotonNetwork.SerializationRate = serialzationRate;
        PhotonNetwork.AutomaticallySyncScene = true;
        var auth = new AuthenticationValues(GameManager.ins.PlayerInformation.playerName);
        PhotonNetwork.AuthValues = auth;
        PhotonNetwork.NickName = GameManager.ins.PlayerInformation.playerName;
        PhotonNetwork.GameVersion = MasterManager.Instance.GameSettings.GameVersion;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("OnConnected: " + PhotonNetwork.LocalPlayer.NickName);
        if(!PhotonNetwork.InLobby)
            PhotonNetwork.JoinLobby();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("OnDisconnect: " + cause.ToString());
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("OnJoinedLobby");
    }

}
