﻿using UnityEngine;
using System.Collections;
using Photon.Pun;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine.UI;
using System;

namespace Assets.Games.Photon.Scripts.Room
{
    public class UICreateOrJoinRoom : MonoBehaviourPunCallbacks
    {
        public GameObject roomItem;
        public RectTransform content;

        public InputField roomNameTxt;
        [SerializeField]
        private Button _createBtn;

        public InputField roomName2Txt;
        public Button _joinBtn;

        private UITeamBattle _roomCanvases;
        private ExitGames.Client.Photon.Hashtable _roomProperties;

        internal void OnShow()
        {
            this.Show();
            ShowRoomList(RoomStatus.ins.RoomList);
        }

        private string[] _customListInLobby;

        public void Initialize(UITeamBattle r)
        {
            _roomCanvases = r;
        }

        private void Start()
        {
            _createBtn.onClick.AddListener(CreateNewRoom);
            _joinBtn.onClick.AddListener(JoinRoom);
        }

        private void JoinRoom()
        {
            if (!PhotonNetwork.IsConnected)
                return;
            PhotonNetwork.JoinRoom(roomName2Txt.text);
        }

        private void CreateNewRoom()
        {
            if (!PhotonNetwork.IsConnected)
                return;
            _roomProperties = new ExitGames.Client.Photon.Hashtable()
            {
                {EventGameCode.TeamBattle, true }
            };

            _customListInLobby = new string[] { EventGameCode.TeamBattle };

            RoomOptions opt = new RoomOptions
            {
                MaxPlayers = (byte)((GameManager.ins.GetMaxPlayerInRoom() / 2) * 2),
                PlayerTtl = 1000,
                IsOpen = true,
                IsVisible = true,
                BroadcastPropsChangeToAll = true,
                CustomRoomProperties = _roomProperties,
                CustomRoomPropertiesForLobby = _customListInLobby,
                PublishUserId = true
            };
            
            PhotonNetwork.CreateRoom(roomNameTxt.text, opt, TypedLobby.Default);
        }

        public override void OnCreatedRoom()
        {
            base.OnCreatedRoom();
            Debug.Log("OnCreatedRoom");
            var p = GameManager.ins.PlayerInformation;
            var _myCustomProperty = new ExitGames.Client.Photon.Hashtable
                    {
                        { "PlayerName", p.playerName },
                        { "PlayerCountry", p.countryCode },
                        { "PlayerKnife", (int)p.playerEquipKnife },
                        { "PlayerMask", (int)p.playerEquipMask },
                        { "PlayerRank", (int)p.playerRank },
                        {"Team", 0 }
                    };

            PhotonNetwork.LocalPlayer.SetCustomProperties(_myCustomProperty);
            this.Hide();
            _roomCanvases.currentRoom.OnShow();
            //Debug.Log(PhotonNetwork.CurrentRoom.CustomProperties[EventGameCode.TeamBattle].ToString());
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            base.OnCreateRoomFailed(returnCode, message);
            Debug.Log("CreateFail: " + message);
        }

        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            base.OnRoomListUpdate(roomList);
            ShowRoomList(roomList);
        }

        private void ShowRoomList(List<RoomInfo> roomList)
        {
            Poolers.ins.ClearItem(roomItem);
            //Debug.Log("RoomList: " + roomList.Count);
            foreach (var item in roomList)
            {
                if (!item.RemovedFromList && item.IsOpen && item.IsVisible &&
                    item.CustomProperties.ContainsKey(EventGameCode.TeamBattle) && (bool)item.CustomProperties[EventGameCode.TeamBattle])
                {
                    var go = Poolers.ins.GetObject(roomItem, content);
                    go.m_transform.SetAsLastSibling();
                    go.Cast<UIRoomItem>().OnShow(item);
                }
            }
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            var p = GameManager.ins.PlayerInformation;
            var _myCustomProperty = new ExitGames.Client.Photon.Hashtable
                    {
                        { "PlayerName", p.playerName },
                        { "PlayerCountry", p.countryCode },
                        { "PlayerKnife", (int)p.playerEquipKnife },
                        { "PlayerMask", (int)p.playerEquipMask },
                        { "PlayerRank", (int)p.playerRank },
                        {"Team", 0 }
                    };

            PhotonNetwork.LocalPlayer.SetCustomProperties(_myCustomProperty);
            _roomCanvases.currentRoom.OnShow();
            this.Hide();
        }
    }
}