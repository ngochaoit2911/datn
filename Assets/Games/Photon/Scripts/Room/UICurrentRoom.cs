﻿using Assets.Games.Photon.Scripts;
using Assets.Games.Photon.Scripts.Room;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICurrentRoom : MonoBehaviourPunCallbacks
{
    public Button leaveBtn;
    public Button startGame;
    private UITeamBattle _roomCanvases;
    public GameObject playerItem;
    public RectTransform content1, content2;
    private List<UIPlayerItem> items;
    public Button join1, join2;

    private void Start()
    {
        leaveBtn.onClick.AddListener(LeaveRoom);
        startGame.onClick.AddListener(LoadGame);
        join1.onClick.AddListener(() => JoinTeam(0));
        join2.onClick.AddListener(() => JoinTeam(1)); 
    }

    private void JoinTeam(int index)
    {
        var p = GameManager.ins.PlayerInformation;
        if (index == 0)
        {
            var _myCustomProperty = new ExitGames.Client.Photon.Hashtable
                    {
                        { "PlayerName", p.playerName },
                        { "PlayerCountry", p.countryCode },
                        { "PlayerKnife", (int)p.playerEquipKnife },
                        { "PlayerMask", (int)p.playerEquipMask },
                        { "PlayerRank", (int)p.playerRank },
                        {"Team", 0 }
                    };
            PhotonNetwork.LocalPlayer.SetCustomProperties(_myCustomProperty);
            var go = items.Find(s => s.playerName == PhotonNetwork.LocalPlayer.NickName);
            go.Hide();
            AddPlayer(PhotonNetwork.LocalPlayer, content1);
            join2.Show();
            join1.Hide();
        }
        else
        {
            var _myCustomProperty = new ExitGames.Client.Photon.Hashtable
                    {
                        { "PlayerName", p.playerName },
                        { "PlayerCountry", p.countryCode },
                        { "PlayerKnife", (int)p.playerEquipKnife },
                        { "PlayerMask", (int)p.playerEquipMask },
                        { "PlayerRank", (int)p.playerRank },
                        {"Team", 1 }
                    };
            PhotonNetwork.LocalPlayer.SetCustomProperties(_myCustomProperty);
            var go = items.Find(s => s.playerName == PhotonNetwork.LocalPlayer.NickName);
            go.Hide();
            AddPlayer(PhotonNetwork.LocalPlayer, content2);
            join2.Hide();
            join1.Show();
        }
    }

    private void LoadGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsVisible = false;
            PhotonNetwork.CurrentRoom.IsOpen = false;
            RoomStatus.ins.OnStartTeamBattle();
            PhotonNetwork.LoadLevel(2);
        }
    }

    public void OnShow()
    {
        this.Show();
        Poolers.ins.ClearItem(playerItem);
        items = new List<UIPlayerItem>();

        if (!PhotonNetwork.IsConnected)
            return;
        if (PhotonNetwork.CurrentRoom == null)
            return;
        var t1 = 0;
        var t2 = 0;
        foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
        {
            //Debug.Log(item.ActorNumber);
            //if(item.ActorNumber != -1)
            //{
            //    if (item.ActorNumber % 2 == 1)
            //    {
            //        AddPlayer(item, content1);
            //    }
            //    else
            //    {
            //        AddPlayer(item, content2);
            //    }
            //}
            if(item.NickName != PhotonNetwork.LocalPlayer.NickName)
            {
                if ((int)item.CustomProperties["Team"] == 0)
                {
                    t1++;
                    AddPlayer(item, content1);
                }
                else
                {
                    t2++;
                    AddPlayer(item, content2);
                }
            }         
        }

        if(t1 > t2)
        {
            var p = GameManager.ins.PlayerInformation;
            var _myCustomProperty = new ExitGames.Client.Photon.Hashtable
                    {
                        { "PlayerName", p.playerName },
                        { "PlayerCountry", p.countryCode },
                        { "PlayerKnife", (int)p.playerEquipKnife },
                        { "PlayerMask", (int)p.playerEquipMask },
                        { "PlayerRank", (int)p.playerRank },
                        {"Team", 1 }
                    };
            PhotonNetwork.LocalPlayer.SetCustomProperties(_myCustomProperty);
            AddPlayer(PhotonNetwork.LocalPlayer, content2);
            join2.Hide();
            join1.Show();
        }
        else
        {
            AddPlayer(PhotonNetwork.LocalPlayer, content1);
            join1.Hide();
            join2.Show();
        }

        startGame.gameObject.SetActive(PhotonNetwork.IsMasterClient);
    }

    private void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        this.Hide();
        _roomCanvases.roomList.OnShow();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        if ((int)newPlayer.CustomProperties["Team"] == 0)
        {
            AddPlayer(newPlayer, content1);
        }
        else
        {
            AddPlayer(newPlayer, content2);
        }
        //AddPlayer(newPlayer);
        startGame.gameObject.SetActive(PhotonNetwork.IsMasterClient);
    }

    private void AddPlayer(Player newPlayer, RectTransform content)
    {
        var go = Poolers.ins.GetObject(playerItem, content);
        go.m_transform.SetAsLastSibling();
        var item = go.Cast<UIPlayerItem>();
        item.OnShow(newPlayer);
        items.Add(item);

    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        var go = items.Find(s => s.playerName == otherPlayer.NickName);
        if (go != null)
        {
            go.Hide();
            items.Remove(go);
        }

        startGame.gameObject.SetActive(PhotonNetwork.IsMasterClient);

    }

    public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);
        Poolers.ins.ClearItem(playerItem);
        items = new List<UIPlayerItem>();

        if (!PhotonNetwork.IsConnected)
            return;
        if (PhotonNetwork.CurrentRoom == null)
            return;
        foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
        {       
            if ((int)item.CustomProperties["Team"] == 0)
            {
                AddPlayer(item, content1);
            }
            else
            {
                AddPlayer(item, content2);
            }
        }
    }

    public void Initialize(UITeamBattle r)
    {
        _roomCanvases = r;
    }

}
