﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Photon.Realtime;
using Assets.Games.Photon.Scripts.Controller;
using TMPro;

namespace Assets.Games.Photon.Scripts.Room
{
    public class UIPlayerItem : PoolItem
    {

        public Text playerNameTxt;
        public Image flagIm;
        public Image rankIm;
        public TextMeshProUGUI playerRank;

        public string playerName;

        public void OnShow(Player p)
        {
            playerNameTxt.text = p.NickName;
            playerName = p.NickName;
            var rank = (int)p.CustomProperties["PlayerRank"];
            var countryCode = (string)p.CustomProperties["PlayerCountry"];
            if (rank >= 0)
            {
                rankIm.sprite = Net_Controller.ins.rankData.rankIcon[rank / 3];
                playerRank.text = (rank % 3 + 1) + "";

            }
            else
            {
                rankIm.sprite = Net_Controller.ins.rankData.rankIcon[0];
                playerRank.text = (0) + "";
            }
            rankIm.SetNativeSize();

            flagIm.sprite = Net_Controller.ins.botData.botNames.Find(s => s.countryCode == countryCode).icon;

        }
    }
}