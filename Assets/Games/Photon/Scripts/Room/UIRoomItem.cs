﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Photon.Realtime;
using System;
using Photon.Pun;
using TMPro;

namespace Assets.Games.Photon.Scripts.Room
{
    public class UIRoomItem : PoolItem
    {

        public Button btn;
        public TextMeshProUGUI roomTxt;
        private RoomInfo _info;

        private void Start()
        {
            btn.onClick.AddListener(JoinRoom);
        }

        private void JoinRoom()
        {
            var p = GameManager.ins.PlayerInformation;
            var _myCustomProperty = new ExitGames.Client.Photon.Hashtable
                    {
                        { "PlayerName", p.playerName },
                        { "PlayerCountry", p.countryCode },
                        { "PlayerKnife", (int)p.playerEquipKnife },
                        { "PlayerMask", (int)p.playerEquipMask },
                        { "PlayerRank", (int)p.playerRank },
                        {"Team", 0 }
                    };

            PhotonNetwork.LocalPlayer.SetCustomProperties(_myCustomProperty);
            PhotonNetwork.JoinRoom(_info.Name);
        }

        public void OnShow(RoomInfo info)
        {
            this.Show();
            _info = info;
            roomTxt.text = info.Name + "  " + info.PlayerCount + "/" + info.MaxPlayers;
        }

    }
}