﻿using UnityEngine;
using System.Collections;
using System;
using Photon.Pun;

namespace Assets.Games.Photon.Scripts.Room
{
    public class UITeamBattle : MonoBehaviour
    {
        public static UITeamBattle ins;
        public UICreateOrJoinRoom roomList;
        public UICurrentRoom currentRoom;

        private void Awake()
        {
            ins = this;
            roomList.Initialize(this);
            currentRoom.Initialize(this);
        }

        public void ShowRoomList()
        {
            roomList.OnShow();
            currentRoom.Hide();
        }

        public void ShowCurrentRoom()
        {
            roomList.Hide();
            currentRoom.OnShow();
        }

        internal void OnShow()
        {
            this.Show();
            ShowRoomList();
        }

        public void Close()
        {
            if (PhotonNetwork.InRoom)
            {
                PhotonNetwork.LeaveRoom();
            }
            this.Hide();
        }
    }
}