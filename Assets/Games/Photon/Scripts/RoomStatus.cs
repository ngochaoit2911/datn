﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Assets.Games.Photon.Scripts.Controller;

namespace Assets.Games.Photon.Scripts
{
    public class RoomStatus : MonoBehaviourPunCallbacks
    {

        public static RoomStatus ins;
        public List<HeroInfor> heroes;
        public List<global::Photon.Realtime.RoomInfo> RoomList;
        public GameMode mode;

        private void Awake()
        {
            if (ins == null)
            {
                ins = this;
            }
        }

        public void OnStartGame(int characterCount)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                heroes = new List<HeroInfor>();
                Net_Controller.ins.colorData.colors.Shuffle();
                var c = 0;
                var pRank = 0;
                foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
                {
                    var data = item.CustomProperties;
                    Debug.Log((string)data["PlayerCountry"]);
                    heroes.Add(new HeroInfor
                    {
                        heroCountry = (string)data["PlayerCountry"],
                        heroName = item.NickName,
                        heroPoint = 0,
                        rankType = (RankType)data["PlayerRank"],
                        knifeType = (KnifeType)data["PlayerKnife"],
                        maskType = (MaskType)data["PlayerMask"],
                        isPlayer = false,
                        color = Net_Controller.ins.colorData.colors[c],
                        isAI = false,
                        teamId = c
                    });
                    if (item.IsMasterClient)
                    {
                        pRank = (int)data["PlayerRank"];
                    }
                    c++;
                }

                var indexRank = Mathf.Clamp(pRank / 3, 0, 6);
                var rankCombo = Net_Controller.ins.rankselectCombo.data[indexRank];
                for (int i = 0; i < characterCount - PhotonNetwork.CurrentRoom.PlayerCount; i++)
                {
                    var index = Random.Range(0, Net_Controller.ins.botData.botNames.Count);
                    var botCountry = Net_Controller.ins.botData.botNames[index];
                    var botName = Random.Range(0, botCountry.botName.Count);
                    var rank = (RankType)Mathf.Clamp(Random.Range(pRank - 1, pRank + 2), -1, 20);
                    var x1 = rankCombo.maskTypes[Random.Range(0, rankCombo.maskTypes.Count)];
                    var x2 = rankCombo.knifeTypes[Random.Range(0, rankCombo.knifeTypes.Count)];
                    heroes.Add(new HeroInfor
                    {
                        heroCountry = botCountry.countryCode,
                        heroName = botCountry.botName[botName],
                        heroPoint = 0,
                        rankType = rank,
                        knifeType = x2,
                        maskType = x1,
                        isPlayer = false,
                        iconAvt = botCountry.icon,
                        color = Net_Controller.ins.colorData.colors[c],
                        isAI = true,
                        teamId = c
                    }) ;
                    c++;
                }
            }
            mode = GameMode.SinglePlayer;
        }

        public void OnStartTeamBattle()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                heroes = new List<HeroInfor>();
                Net_Controller.ins.colorData.colors.Shuffle();
                var c = 0;
                var pRank = 0;
                var teamMember = PhotonNetwork.CurrentRoom.MaxPlayers / 2;
                var oneTeam = 0;
                var twoTeam = 0;
                foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
                {
                    var teamId = 0;
                    if((int)item.CustomProperties["Team"] == 0)
                    {
                        teamId = 0;
                        if(oneTeam >= teamMember)
                        {
                            teamId = 1;
                            twoTeam++;
                        }
                        else
                        {
                            oneTeam++;
                        }
                    }
                    else
                    {
                        teamId = 1;

                        if(twoTeam >= teamMember)
                        {
                            teamId = 0;
                            oneTeam++;
                        }
                        else
                        {
                            twoTeam++;

                        }
                    }
                    var data = item.CustomProperties;
                    heroes.Add(new HeroInfor
                    {
                        heroCountry = (string)data["PlayerCountry"],
                        heroName = item.NickName,
                        heroPoint = 0,
                        rankType = (RankType)data["PlayerRank"],
                        knifeType = (KnifeType)data["PlayerKnife"],
                        maskType = (MaskType)data["PlayerMask"],
                        isPlayer = false,
                        color = Net_Controller.ins.colorData.colors[teamId],
                        isAI = false,
                        teamId = teamId
                    });

                    if (item.IsMasterClient)
                    {
                        pRank = (int)data["PlayerRank"];
                    }
                }

                var indexRank = Mathf.Clamp(pRank / 3, 0, 6);
                var rankCombo = Net_Controller.ins.rankselectCombo.data[indexRank];
                for (int i = 0; i < teamMember - oneTeam; i++)
                {
                    var index = Random.Range(0, Net_Controller.ins.botData.botNames.Count);
                    var botCountry = Net_Controller.ins.botData.botNames[index];
                    var botName = Random.Range(0, botCountry.botName.Count);
                    var rank = (RankType)Mathf.Clamp(Random.Range(pRank - 1, pRank + 2), -1, 20);
                    var x1 = rankCombo.maskTypes[Random.Range(0, rankCombo.maskTypes.Count)];
                    var x2 = rankCombo.knifeTypes[Random.Range(0, rankCombo.knifeTypes.Count)];
                    heroes.Add(new HeroInfor
                    {
                        heroCountry = botCountry.countryCode,
                        heroName = botCountry.botName[botName],
                        heroPoint = 0,
                        rankType = rank,
                        knifeType = x2,
                        maskType = x1,
                        isPlayer = false,
                        iconAvt = botCountry.icon,
                        color = Net_Controller.ins.colorData.colors[0],
                        isAI = true,
                        teamId = 0
                    });
                }

                for (int i = 0; i < teamMember - twoTeam; i++)
                {
                    var index = Random.Range(0, Net_Controller.ins.botData.botNames.Count);
                    var botCountry = Net_Controller.ins.botData.botNames[index];
                    var botName = Random.Range(0, botCountry.botName.Count);
                    var rank = (RankType)Mathf.Clamp(Random.Range(pRank - 1, pRank + 2), -1, 20);
                    var x1 = rankCombo.maskTypes[Random.Range(0, rankCombo.maskTypes.Count)];
                    var x2 = rankCombo.knifeTypes[Random.Range(0, rankCombo.knifeTypes.Count)];
                    heroes.Add(new HeroInfor
                    {
                        heroCountry = botCountry.countryCode,
                        heroName = botCountry.botName[botName],
                        heroPoint = 0,
                        rankType = rank,
                        knifeType = x2,
                        maskType = x1,
                        isPlayer = false,
                        iconAvt = botCountry.icon,
                        color = Net_Controller.ins.colorData.colors[1],
                        isAI = true,
                        teamId = 1
                    });
                }
            }
            mode = GameMode.TeamBattle;
        }

        public override void OnRoomListUpdate(List<global::Photon.Realtime.RoomInfo> roomList)
        {
            base.OnRoomListUpdate(roomList);
            RoomList = roomList;
        }
    }
}
