﻿using UnityEngine;
using System.Collections;
using Assets.Games.Photon.Scripts.Data;

namespace Assets.Games.Photon.Scripts
{
    public class Singleton : MonoBehaviour
    {

        public MasterManager masterManager;
        public Poolers pool;
        private void Awake()
        {
            pool.OnStart();
        }
    }
}