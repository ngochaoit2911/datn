﻿using UnityEngine;
using System.Collections;

public class BaseSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T ins;

    /// <summary>
    /// Access singleton instance through this propriety.
    /// </summary>
    public static T Ins
    {
        get
        {

            if (ins == null)
            {
                // Search for existing instance.
                ins = FindObjectOfType<T>();

                // Create new instance if one doesn't already exist.
                if (ins == null)
                {
                    // Need to create a new GameObject to attach the singleton to.
                    var singletonObject = new GameObject();
                    ins = singletonObject.AddComponent<T>();
                    singletonObject.name = typeof(T).ToString() + " (Singleton)";

                }


                
            }
            return ins;
        }
    }
    
}