﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    public static AudioManager ins;

    public int maxAudioSource;
    public AudioSource musicAudio;
    public AudioClip homeMusic, ingameMusic;
    private List<AudioSource> audioSources;
    private bool isSoundOn;
    private MusicType musicType;
    public List<ClipInfor> listClip;

    public float homeVolume = 0.8f, ingameVolume = 0.8f;

    private void Awake()
    {
        if(ins == null)
        {
            ins = this;
            audioSources = new List<AudioSource>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }

    }

    private void Start()
    {
        isSoundOn = GameManager.ins.IsSoundOn;
        musicType = MusicType.HomeMusic;
        PlayMusic(musicType);
    }

    public AudioSource GetAudio()
    {
        for (int i = 0; i < audioSources.Count; i++)
        {
            if (!audioSources[i].isPlaying)
            {
                return audioSources[i];
            }
        }

        if (audioSources.Count < maxAudioSource)
        {
            var audio = gameObject.AddComponent<AudioSource>();
            audioSources.Add(audio);
            return audio;
        }
        return null;
    }

    public void PlayAudioClip(AudioClip clip, float volume = 1, bool isLoop = false)
    {
        if (isSoundOn)
        {
            var audio = GetAudio();
            if (audio != null)
            {
                audio.volume = volume;
                audio.loop = isLoop;
                audio.clip = clip;
                if (!isLoop)
                {
                    audio.PlayOneShot(clip);
                }
                else
                {
                    audio.Play();
                }
            }
        }
    }

    public void PlayAudioClip(ClipType type, bool isLoop = false)
    {
        var clip = GetClip(type);
        PlayAudioClip(clip.clip, clip.volume, isLoop);
    }

    ClipInfor GetClip(ClipType clipType)
    {
        foreach (var clipInfor in listClip)
        {
            if (clipInfor.type == clipType)
                return clipInfor;
        }

        return listClip[0];
    }

    public void PlayMusic(MusicType type)
    {
        musicType = type;
        if (isSoundOn)
        {
            musicAudio.clip = type == MusicType.HomeMusic ? homeMusic : ingameMusic;
            musicAudio.volume = type == MusicType.HomeMusic ? homeVolume : ingameVolume;
            musicAudio.Play();
        }
        else
        {
            musicAudio.Stop();
        }
    }

    public void PlayMusic()
    {
        if (isSoundOn)
        {
            musicAudio.clip = homeMusic;
            musicAudio.Play();
        }
    }


    public void StopMusic()
    {
        musicAudio.Pause();
    }

    public void ChangeSoundState()
    {
        isSoundOn = GameManager.ins.IsSoundOn;      
    }

    public void PlayButtonClick()
    {
        PlayAudioClip(ClipType.click);
    }

    private Dictionary<int, ClipType> dictionary = new Dictionary<int, ClipType>
    {
        {0, ClipType.vacham1 },
        {1, ClipType.vacham2 },
        {2, ClipType.vacham3 },
        {3, ClipType.vacham4 },
        {4, ClipType.vacham5 },
    };

    private Dictionary<ItemBuffType, ClipType> items = new Dictionary<ItemBuffType, ClipType>
    {
        {ItemBuffType.SpeedUp, ClipType.item_arrow },
        {ItemBuffType.Shield, ClipType.sndLife_mp3 },
        {ItemBuffType.BladeSpeedUp, ClipType.item_compass },
        {ItemBuffType.Magnet, ClipType.item_brush },
    };

    public void PlayVacham()
    {
        var r = Random.Range(0, 5);
        PlayAudioClip(dictionary[r]);
    }

    public void PlayItemBuff(ItemBuffType type)
    {
        PlayAudioClip(items[type]);
    }

    public void StopAudio(ClipType type)
    {
        var audioClip = GetClip(type);
        for (int i = 0; i < audioSources.Count; i++)
        {
            var clip = audioSources[i].clip;
            if (clip != null && clip == audioClip.clip)
            {
                audioSources[i].Stop();
            }
        }
    }

#if UNITY_EDITOR

    [ContextMenu("Add")]
    private void AddList()
    {
        var l = Enum.GetValues(typeof(ClipType)).Cast<ClipType>().ToList();
        var s = new List<ClipType>();
        foreach (var clipInfor in listClip)
        {
            s.Add(clipInfor.type);
        }

        foreach (var item in l)
        {
            if (!s.Contains(item))
            {
                var path = "Assets/Games/OGG/" + item.ToString() + ".ogg";
                if (AssetDatabase.LoadAssetAtPath<AudioClip>(path))
                {
                    listClip.Add(new ClipInfor
                    {
                        type = item,
                        clip = AssetDatabase.LoadAssetAtPath<AudioClip>(path),
                        volume = 1
                    });
                }
            }
        }
    }
#endif
}

[System.Serializable]
public struct ClipInfor
{
    public ClipType type;
    public AudioClip clip;
    public float volume;
}

public enum ClipType
{
    click,
    purchase,
    complete_star_2,
    episode_light,
    acl,
    vacham1,
    revive,
    minionspawn,
    defeat,
    vic2,
    fail,
    doublekill,
    triple,
    quadra,
    penta,
    hexa,
    legendary,
    ace,
    firstblood,
    godlike,
    shutdown,
    item_arrow,
    sndLife_mp3,
    item_compass,
    item_brush,
    vacham2,
    vacham3,
    vacham4,
    vacham5
}

public enum MusicType
{
    HomeMusic,
    GamePlayMusic
}
