﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Games.Photon.Scripts.Data;

public class GameManager : MonoBehaviour
{
    class Fixed
    {
        public const string IS_FIRST_PLAY = "IsFirstPlay111111111111111111111111";
        public const string PLAYER_COIN = "Player Coin";
        public const string PLAYER_GEM = "Player GEM";
        public const string PLAYER_INFOR = "Player Infor";
        public const string PLAYER_KNIFE = "Player Blade";
        public const string PLAYER_MASK = "Player Mask";
        public const string DAY_CONNECT = "Day Connect";
        public const string IS_SOUND_ON = "IsSoundOn";
        public const string Is_USE_VIBRATE = "IsVibrate";
        public const string DAILY_GIFT = "DailyGift";
        public const string DAY_ONLINE = "DayOnline";
        public const string MISSION_LEVEL = "MissionLevel";
        public const string MISSION = "Mission";
        public const string VIDEO = "Video";
        public const string Motion = "motion";
    }

    public static GameManager ins;
    public Poolers poolers;
    public ManagerAds ads;
    public MasterManager master;

    #region Field

    private PlayerInfor playerInfor;
    private int playerGem, playerCoin;
    private List<PlayerKnifeInfor> playerKnifes;
    private List<PlayerMaskInfor> playerMasks;
    private int dayConnect;
    private bool isSoundOn;
    private bool isVibrate;
    private List<bool> dailyGifts;
    private List<int> dayOnLine;
    private List<int> missionLevel;
    private List<PlayerMissionInfor> missionInfor;
    private int videoCount;
    private List<PlayerMotionEquipment> motions;

    private GameConfig gameConfig;
    private PlayerConfig playerConfig;
    #endregion

    #region Property

    public GameConfig GameConfig
    {
        get => gameConfig;
        set
        {
            gameConfig = value;
            DatabaseManager.PostData(new Dictionary<string, string>
            {
                {DataKey.GameConfig, JsonUtility.ToJson(gameConfig) }
            }, r =>
            {
                Debug.Log("PostComplete");
            }, e =>
            {
                Debug.Log(e.ErrorMessage);
            });
        }
    }

    public PlayerConfig PlayerConfig
    {
        get => playerConfig;
        set
        {
            playerConfig = value;
            DatabaseManager.PostData(new Dictionary<string, string>
            {
                {DataKey.PlayerConfig, JsonUtility.ToJson(playerConfig) }
            }, r =>
            {
                Debug.Log("PostComplete");
            }, e =>
            {
                Debug.Log(e.ErrorMessage);
            });
        }
    }

    public List<PlayerMotionEquipment> Motions
    {
        get => motions;
        set
        {
            motions = value;
            //EncryptHelper.SaveList(Fixed.Motion, value);
            playerConfig.motions = value;
            PlayerConfig = playerConfig;
        }
    }

    public int VideoCount
    {
        get { return videoCount; }
        set
        {
            videoCount = value;
            //EncryptHelper.SetInt(Fixed.VIDEO, value);
            gameConfig.videoCount = value;
            GameConfig = gameConfig;
        }
    }

    public List<PlayerMissionInfor> MissionInfor
    {
        get => missionInfor;
        set
        {
            missionInfor = value;
            //EncryptHelper.SaveList(Fixed.MISSION, value);
            DatabaseManager.PostData(new Dictionary<string, string>
            {
                {DataKey.MissionInfo, JsonHelper.ToJson(missionInfor) }
            }, r =>
            {
                Debug.Log("PostComplete");
            }, e =>
            {
                Debug.Log(e.ErrorMessage);
            });
        }
    }

    public List<int> MissionLevel
    {
        get => missionLevel;
        set
        {
            missionLevel = value;
            //EncryptHelper.SaveList(Fixed.MISSION_LEVEL, value);
            DatabaseManager.PostData(new Dictionary<string, string>
            {
                {DataKey.MissionLevel, JsonHelper.ToJson(missionLevel) }
            }, r =>
            {
                Debug.Log("PostComplete");
            }, e =>
            {
                Debug.Log(e.ErrorMessage);
            });
        }
    }

    public List<int> DayOnLine
    {
        get { return dayOnLine; }
        set
        {
            dayOnLine = value;
            //EncryptHelper.SaveList(Fixed.DAY_ONLINE, value);
            gameConfig.dayOnLine = value;
            GameConfig = gameConfig;
        }
    }

    public List<bool> DailyGifts
    {
        get { return dailyGifts; }
        set
        {
            dailyGifts = value;
            //EncryptHelper.SaveList(Fixed.DAILY_GIFT, value);
            gameConfig.dailyGifts = value;
            GameConfig = gameConfig;
        }
    }

    public bool IsVibrate
    {
        get { return isVibrate; }
        set
        {
            isVibrate = value;
            //PlayerPrefs.SetInt(Fixed.Is_USE_VIBRATE, isVibrate ? 1 : 0);
            gameConfig.isVibrate = value;
            GameConfig = gameConfig;
        }
    }

    public bool IsSoundOn
    {
        get { return isSoundOn; }
        set
        {
            isSoundOn = value;
            //PlayerPrefs.SetInt(Fixed.IS_SOUND_ON, isSoundOn ? 1: 0);
            gameConfig.isSoundOn = value;
            GameConfig = gameConfig;
        }
    }

    public int DayConnect
    {
        get { return dayConnect; }
        set
        {
            dayConnect = value;
            //EncryptHelper.SetInt(Fixed.DAY_CONNECT, value);
            gameConfig.dayConnect = value;
            GameConfig = gameConfig;
        }
    }

    public List<PlayerKnifeInfor> PlayerKnifes
    {
        get { return playerKnifes; }
        set
        {
            playerKnifes = value;
            //SavingDataWithJson.SaveListDataWithEncrypt(value, Fixed.PLAYER_KNIFE);
            playerConfig.playerKnifes = value;
            PlayerConfig = playerConfig;
        }
    }

    public List<PlayerMaskInfor> PlayerMasks
    {
        get { return playerMasks; }
        set
        {
            playerMasks = value;
            //SavingDataWithJson.SaveListDataWithEncrypt(value, Fixed.PLAYER_MASK);
            playerConfig.playerMasks = value;
            PlayerConfig = playerConfig;
        }
    }

    public int PlayerCoin
    {
        get { return playerCoin; }
        set
        {
            playerCoin = value;
            //EncryptHelper.SetInt(Fixed.PLAYER_COIN, value);
            DatabaseManager.PostData(new Dictionary<string, string>
            {
                {DataKey.PlayerCoin, value.ToString() }
            }, r =>
            {
                Debug.Log("PostComplete");
            }, e =>
            {
                Debug.Log(e.ErrorMessage);
            });
        }
    }

    public int PlayerGem
    {
        get { return playerGem; }
        set
        {
            playerGem = value;
            //EncryptHelper.SetInt(Fixed.PLAYER_GEM, value);
            DatabaseManager.PostData(new Dictionary<string, string>
            {
                {DataKey.PlayerGem, value.ToString() }
            }, r =>
            {
                Debug.Log("PostComplete");
            }, e =>
            {
                Debug.Log(e.ErrorMessage);
            });
        }
    }

    public PlayerInfor PlayerInformation
    {
        get { return playerInfor; }
        set
        {
            playerInfor = value;
            //EncryptHelper.SaveObject(Fixed.PLAYER_INFOR, value);
            playerConfig.playerInfor = value;
            PlayerConfig = playerConfig;
        }
    }

    #endregion

    private void Awake()
    {
        if(ins == null)
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
        poolers.OnStart();
        Application.targetFrameRate = 60;
#if !UNITY_EDITOR
        Debug.unityLogger.logEnabled = false;
#endif
        //IsFirstPlayGame();
    }

    public void InitAds()
    {
        ads.Onstart();
        RegisterMission();
    }

    private void IsFirstPlayGame()
    {
        if (!PlayerPrefs.HasKey(Fixed.IS_FIRST_PLAY))
        {
            PlayerPrefs.SetInt(Fixed.IS_FIRST_PLAY, 1);
            PlayerCoin = 0;
            PlayerGem = 0;
            DayConnect = 0;
            IsSoundOn = true;
            IsVibrate = true;
            VideoCount = 0;
            var t = DateTime.Now;
            var l = new List<int> {t.Day, t.Month, t.Year};
            DayOnLine = l;
            PlayerInformation = new PlayerInfor
            {
                countryCode = Application.systemLanguage.ToCountryCode(),
                countryName = Application.systemLanguage.ToCountryCode(),
                playerEquipKnife = (KnifeType.Carrot),
                playerEquipMask = MaskType.UGotMe,
                playerName = "PlayerName",
                playerPoint = 0,
                playerRank = RankType.DontRank

            };
            InitKnifes();
            InitMasks();
            InitDailyGift();
            InitMissionLevel();
            InitMission();
            InitMotion();
        }
        else
        {
            dayConnect = EncryptHelper.GetInt(Fixed.DAY_CONNECT);
            playerCoin = EncryptHelper.GetInt(Fixed.PLAYER_COIN);
            playerGem = EncryptHelper.GetInt(Fixed.PLAYER_GEM);
            playerInfor = EncryptHelper.LoadObject<PlayerInfor>(Fixed.PLAYER_INFOR);
            playerKnifes = EncryptHelper.LoadList<PlayerKnifeInfor>(Fixed.PLAYER_KNIFE);
            playerMasks = EncryptHelper.LoadList<PlayerMaskInfor>(Fixed.PLAYER_MASK);
            isSoundOn = PlayerPrefs.GetInt(Fixed.IS_SOUND_ON) == 1;
            isVibrate = PlayerPrefs.GetInt(Fixed.Is_USE_VIBRATE) == 1;
            dailyGifts = EncryptHelper.LoadList<bool>(Fixed.DAILY_GIFT);
            dayOnLine = EncryptHelper.LoadList<int>(Fixed.DAY_ONLINE);
            missionLevel = EncryptHelper.LoadList<int>(Fixed.MISSION_LEVEL);
            missionInfor = EncryptHelper.LoadList<PlayerMissionInfor>(Fixed.MISSION);
            videoCount = EncryptHelper.GetInt(Fixed.VIDEO);
            motions = EncryptHelper.LoadList<PlayerMotionEquipment>(Fixed.Motion);
            CheckNewDay();
        }
    }

    public void InitOnRegister(string userName, string country, Action onComplete, Action onError)
    {
        StartCoroutine(IInitOnRegister(userName, country,onComplete, onError));
    }

    private IEnumerator IInitOnRegister(string userName, string country, Action onComplete, Action onError)
    {
        playerCoin = 0;
        playerGem = 0;
        var t = DateTime.Now;
        var l = new List<int> { t.Day, t.Month, t.Year };
        gameConfig = new GameConfig
        {
            dayConnect = 0,
            isSoundOn = true,
            isVibrate = true,
            videoCount = 0,
            dailyGifts = InitDailyGift(),
            dayOnLine = l
        };

        playerConfig = new PlayerConfig
        {
            playerInfor = new PlayerInfor
            {
                countryCode = country,
                countryName = country,
                playerEquipKnife = (KnifeType.Carrot),
                playerEquipMask = MaskType.UGotMe,
                playerName = userName,
                playerPoint = 0,
                playerRank = RankType.DontRank

            },
            motions = InitMotion(),
            playerKnifes = InitKnifes(),
            playerMasks = InitMasks()
        };

        dayConnect = gameConfig.dayConnect;
        isSoundOn = gameConfig.isSoundOn;
        isVibrate = gameConfig.isVibrate;
        videoCount = gameConfig.videoCount;

        dayOnLine = gameConfig.dayOnLine;
        playerInfor = playerConfig.playerInfor;
        playerKnifes = playerConfig.playerKnifes;
        playerMasks = playerConfig.playerMasks;
        dailyGifts = gameConfig.dailyGifts;
        missionInfor = InitMission();
        missionLevel = InitMissionLevel();
        motions = playerConfig.motions;


        DatabaseManager.PostData(new Dictionary<string, string>
        {

            {DataKey.PlayerConfig, JsonUtility.ToJson(playerConfig) },
            {DataKey.GameConfig,JsonUtility.ToJson(gameConfig)  },
            {DataKey.PlayerCoin, playerCoin.ToString() },
            {DataKey.PlayerGem, playerGem.ToString() },
            {DataKey.MissionInfo, JsonHelper.ToJson(MissionInfor)},
            {DataKey.MissionLevel, JsonHelper.ToJson(missionLevel) }

        }, r =>
            {
                onComplete.Invoke();
                DatabaseManager.UpdateDisplayName(playerConfig.playerInfor.playerName + ":" + playerConfig.playerInfor.countryCode);
                DatabaseManager.UpdateRanking(RankType.DontRank);
            }, e =>
            {
                onError.Invoke();
            });

        yield return null;
    }

    public void LoadDataLogin(Action onComplete, Action onError)
    {
        StartCoroutine(ILoadDataLogin(onComplete, onError));
    }

    private IEnumerator ILoadDataLogin(Action onComplete, Action onError)
    {
        var isLoaded = false;
        DatabaseManager.GetAllData(r =>
        {
            var data = r.Data;
            playerCoin = DatabaseManager.GetInt(r, DataKey.PlayerCoin);
            playerGem = DatabaseManager.GetInt(r, DataKey.PlayerGem);
            missionInfor = DatabaseManager.GetList<PlayerMissionInfor>(r, DataKey.MissionInfo);
            missionLevel = DatabaseManager.GetList<int>(r, DataKey.MissionLevel);
            playerConfig = DatabaseManager.GetObject<PlayerConfig>(r, DataKey.PlayerConfig);
            gameConfig = DatabaseManager.GetObject<GameConfig>(r, DataKey.GameConfig);

            dayConnect = gameConfig.dayConnect;
            isSoundOn = gameConfig.isSoundOn;
            isVibrate = gameConfig.isVibrate;
            videoCount = gameConfig.videoCount;

            dayOnLine = gameConfig.dayOnLine;
            playerInfor = playerConfig.playerInfor;
            playerKnifes = playerConfig.playerKnifes;
            playerMasks = playerConfig.playerMasks;
            dailyGifts = gameConfig.dailyGifts;
            motions = playerConfig.motions;

            CheckNewDay();
            onComplete.Invoke();
            isLoaded = true;
        }, e =>
        {
            onError.Invoke();
            isLoaded = true;
        });
        yield return new WaitUntil(() => isLoaded);
    }

    private List<PlayerMotionEquipment> InitMotion()
    {
        motions = new List<PlayerMotionEquipment>();

        foreach (var m in Enum.GetValues(typeof(MotionType)).Cast<MotionType>())
        {
            motions.Add(new PlayerMotionEquipment
            {
                isHasEquip = false,
                motion = m,
                maskEquip = MaskType.UGotMe
            });
        }

        return motions;
    }

    private List<PlayerMissionInfor> InitMission()
    {
        var l = new List<PlayerMissionInfor>();
        var m = new List<MissionType>
        {
            MissionType.Top1, MissionType.DoubleKill, MissionType.PlayMatch, MissionType.UpRank, MissionType.UnlockNewKnife
        };
        for (int i = 0; i < 5; i++)
        {
            l.Add(new PlayerMissionInfor
            {
                state = MissionState.Doing,
                missionType = m[i],
                levelOfMission = 0,
                missionValue = 0
            });
        }

        return l;
    }

    private List<int> InitMissionLevel()
    {
        var l = new List<int>();
        foreach (var value in Enum.GetValues(typeof(MissionType)))
        {
            l.Add(0);
        }

        return l;
    }

    public void CheckNewDay()
    {
        var t = DateTime.Now;
        
        if (dayOnLine[0] != t.Day || dayOnLine[1] != t.Month || dayOnLine[2] != t.Year)
        {
            if (dayConnect < 6)
            {
                dayConnect++;
                var l = new List<int> {t.Day, t.Month, t.Year};
                dayOnLine = l;
            }
        }
    }

    private List<bool> InitDailyGift()
    {
        var s = new List<bool>();
        for (int i = 0; i < 7; i++)
        {
            s.Add(false);
        }

        return s;
    }

    List<PlayerKnifeInfor> InitKnifes()
    {
        var knifes = new List<PlayerKnifeInfor> ();
        knifes.Add(new PlayerKnifeInfor { isUnlock = true, knifeType = KnifeType.Carrot });
        foreach (var value in Enum.GetValues(typeof(KnifeType)).Cast<KnifeType>())
        {
            if (value != KnifeType.Carrot)
            {
                knifes.Add(new PlayerKnifeInfor
                {
                    knifeType = value,
                    isUnlock = false
                });
            }
        }

        return knifes;
    }

    List<PlayerMaskInfor> InitMasks()
    {
        var masks = new List<PlayerMaskInfor> ();
        masks.Add(new PlayerMaskInfor() { isUnlock = true, maskType = (MaskType)0 });
        foreach (var value in Enum.GetValues(typeof(MaskType)).Cast<MaskType>())
        {
            if (value != (MaskType)0)
            {
                masks.Add(new PlayerMaskInfor()
                {
                    maskType = value,
                    isUnlock = false
                });
            }
        }

        return masks;
    }

    public void ChangeKnifes(PlayerKnifeInfor infor)
    {
        var p = PlayerKnifes;
        var index = p.FindIndex(s => s.knifeType == infor.knifeType);
        p[index] = infor;
        PlayerKnifes = p;
    }

    public void ChangeMasks(PlayerMaskInfor infor)
    {
        var p = PlayerMasks;
        var index = p.FindIndex(s => s.maskType == infor.maskType);
        p[index] = infor;
        PlayerMasks = p;
    }

    public PlayerMotionEquipment GetMotion(MotionType m)
    {
        var f = motions.Find(s => s.motion == m);
        return f;
    }

    public int GetMaxPlayerInRoom()
    {
        var pRank = (int)playerInfor.playerRank;
        var characterCount = Mathf.Clamp(pRank / 2 + 6, 6, 17);
        return characterCount;
    }

    private void RegisterMission()
    {
        foreach (var item in Enum.GetValues(typeof(EventID)).Cast<EventID>())
        {
            if(item != EventID.None)
            {
                this.RegisterListener(item, o => {

                    var index = MissionInfor.FindIndex(s => s.missionType == (MissionType)((int)item - 1));
                    if (index != -1)
                    {
                        var m = MissionInfor;
                        var mission = m[index];
                        mission.missionValue++;
                        m[index] = mission;
                        MissionInfor = m;
                    }
                });
            }
        }    
    }

    [ContextMenu("GetUser")]
    private void GetUser()
    {
        DatabaseManager.Init();
        DatabaseManager.Login("hihi", "123456", r =>
        {
            Debug.Log("login success");
            //DatabaseManager.UpdateDisplayName("NgocHao");
            //DatabaseManager.UpdateRanking(RankType.Diamond1);
            DatabaseManager.GetLeaderBoard(m => { Debug.Log("Get Leaderboard");

                foreach (var item in m.Leaderboard)
                {
                    Debug.Log(item.DisplayName + "  " + item.Position + "  " + item.StatValue);
                }
            
            }, n => { Debug.Log(n.ErrorMessage.ToString()); });
        }, e => { });

        //DatabaseManager.GetSquare(N);

    }
}

[System.Serializable]
public struct PlayerInfor
{
    public string countryName;
    public string countryCode;
    public string playerName;
    public RankType playerRank;
    public int playerPoint;
    public KnifeType playerEquipKnife;
    public MaskType playerEquipMask;
}

[System.Serializable]
public struct PlayerKnifeInfor
{
    public KnifeType knifeType;
    public bool isUnlock;
    public int knifeVideoCount;
}

[System.Serializable]
public struct PlayerMaskInfor
{
    public MaskType maskType;
    public bool isUnlock;
}

[System.Serializable]
public class GameConfig
{
    public int videoCount;
    public int dayConnect;
    public bool isSoundOn;
    public bool isVibrate;
    public List<bool> dailyGifts;
    public List<int> dayOnLine;
}

[Serializable]
public class PlayerConfig
{
    public PlayerInfor playerInfor;
    public List<PlayerKnifeInfor> playerKnifes;
    public List<PlayerMaskInfor> playerMasks;
    public List<PlayerMotionEquipment> motions;
}