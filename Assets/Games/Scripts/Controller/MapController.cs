﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public static MapController ins;

    public Vector2 mapSize;
    public List<GameObject> impediments;
    public List<GameObject> maps;
    public GameObject player, enemy, knife, ingameItem;
    private MapStage curMap;
    public CameraFollower camFollower;

    private void Awake()
    {
        ins = this;
        Poolers.ins.ReInit();
    }


    public void GenerateMap(List<HeroInfor> heroes)
    {
        var pRank = GameManager.ins.PlayerInformation.playerRank;
        var x = Mathf.Clamp((int) pRank * 2, 0, 8);
        var prefab = maps[Random.Range(x, x + 2)];
        var map = Poolers.ins.GetObject(prefab, Vector3.zero, Quaternion.identity);
        curMap = map.Cast<MapStage>();
        curMap.OnSpawn(heroes);
        
    }

    public void SpawnKnife()
    {
        curMap.SpawnKnifePerSecond();
        curMap.SpawnItemInGame();
    }

    public void SetCameraFollower(Transform p)
    {
        camFollower.SetUpFollower(p);
    }

    public void DisableFollower()
    {
        camFollower.DisableFollow();
    }

    public void SetUpBounding(Vector2 mapSize)
    {
        camFollower.SetUpBounderies(mapSize + Vector2.one * 2.5f);
    }

    public Vector3 GetRandomPos()
    {
        var x = Random.Range(0, curMap.spawnCharacterPos.Count);
        return curMap.spawnCharacterPos[x].position;
    }

}
