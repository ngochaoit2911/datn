﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public GameObject knifePrefab, playerPrefab, enemyPrefab;
    public int knifeCount, enemyCount;
    public KnifeType knifeType;
    public MaskType maskType;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            //SpawnMultiKnife(knifeCount);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            SpawnMultiEnemyKnife(knifeCount);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
//            /SpawnPlayer();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            SpawnMultiEnemy(enemyCount);
        }

        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    SpawnEnemy();
        //}

    }

    

    public void SpawnMultiEnemy(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var pos = GameHelper.GetRandomPosInArea(new Vector3(-9.7f, 9.7f));
            //SpawnEnemyInPos(pos);
        }
    }

    public void SpawnMultiEnemyKnife(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var pos = new Vector3(Random.Range(-GameController.camSize.x, GameController.camSize.x),
                Random.Range(-GameController.camSize.y, GameController.camSize.y));
            var angle = Random.Range(0, 360);
            SpawnEnemyKnife(pos, Quaternion.Euler(0, 0, angle));
        }
    }

   

    public void SpawnEnemyKnife(Vector3 pos, Quaternion rot)
    {
        var k = Poolers.ins.GetObject(knifePrefab, pos, rot);
        k.m_transform.SetParent(null);
        k.Cast<Knife>().OnInstanceEnemyKnife();
    }

    //public void SpawnPlayer()
    //{
    //    var pos = GameHelper.GetRandomPosInArea(GameController.camSize);
    //    var p = Poolers.ins.GetObject(playerPrefab, pos, Quaternion.identity);
    //    p.Cast<KnifeController>().OnInstance(knifeType, maskType);
    //    GameController.ins.activeControllers.Add(p.Cast<KnifeController>());
    //}   

    //public void SpawnEnemyInPos(Vector3 pos)
    //{
    //    var p = Poolers.ins.GetObject(enemyPrefab, pos, Quaternion.identity);
    //    p.Cast<KnifeController>().OnInstance((KnifeType)Random.Range(0, 20), (MaskType)Random.Range(0, 10));
    //    var x = Random.Range(0, GameController.ins.configSets.Count);
    //    GameController.ins.activeControllers.Add(p.Cast<KnifeController>());
    //    p.GetComponent<EnemyAI>().Init(GameController.ins.configSets[x]);
    //}

}
