﻿
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class UI_KnifeController : MonoBehaviour
{
    public GameObject knifeInUI;
    private KnifeType knifeType;
    private List<KnifeInShop> knifes;
    private Transform t;
    private Tween switchTween;
    private float radius;
    private bool isOnAnim;

    private void Start()
    {
        t = transform;
    }

    public void OnShow(KnifeType type)
    {
        knifeType = type;
        Poolers.ins.ClearItem(knifeInUI);
        knifes = new List<KnifeInShop>();
        for (int i = 0; i < Config.KNIFE_UI_COUNT; i++)
        {
            var s = Poolers.ins.GetObject(knifeInUI, transform);
            knifes.Add(s.Cast<KnifeInShop>());
            s.Cast<KnifeInShop>().SetUp(knifeType, i, transform);
        }

        radius = Config.BASE_RADIUS_IN_UI;
        StartCoroutine(IAnim());
    }

    public void ChangeKnife(KnifeType type)
    {
        if(knifes == null)
            return;

        knifeType = type;
        foreach (var knife in knifes)
        {
            knife.SetUp(knifeType);
        }
    }

    void Update()
    {
        t.Rotate(Vector3.back * Config.BASE_ROTATE_SPEED * Time.deltaTime, Space.World);
    }

    public void AnimationOff()
    {
        switchTween?.Kill();
        isOnAnim = true;
        var lastV = radius;
        switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE, value =>
        {
            foreach (var item in knifes)
            {
                radius = Mathf.Lerp(lastV, lastV * 0.9f, value);
                var curRot = item.m_transform.right;
                var des = (item.m_transform.position - transform.position).normalized;
                item.m_transform.right = Vector3.Lerp(curRot, des, value).normalized;
                item.Move(transform, radius);
                item.SetUpY(-1);
            }
        }).OnComplete(() =>
        {
            var m = radius;
            switchTween = DOVirtual.DelayedCall(Config.BASE_TIME_SWITCH_STATE, () =>
            {
                switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE, value =>
                {
                    foreach (var item in knifes)
                    {
                        radius = Mathf.Lerp(m, Config.BASE_RADIUS_IN_UI, value);
                        var curRot = item.m_transform.up;
                        var des = (item.m_transform.position - transform.position).normalized;
                        item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                        item.Move(transform, radius);
                        item.SetUpY(1);
                    }
                }).OnComplete(() => isOnAnim = false);
            });

        });

    }

    public void AnimationOn()
    {
        switchTween?.Kill();
        isOnAnim = true;
        var l = new List<float>();
        foreach (var m in knifes)
        {
            //var temp = m.m_transform.localRotation.eulerAngles.z;
            //temp = temp + 15 > 360 ? temp - 360 : temp;
            if (m.m_transform.localRotation.eulerAngles.z > 345)
            {

                l.Add(m.m_transform.localRotation.eulerAngles.z - 360);
            }
            else
            {
                l.Add(m.m_transform.localRotation.eulerAngles.z);
            }
            m.SetUpY(1);
        }

        switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 5, value =>
        {
            var x = Mathf.Lerp(0, 15, value);
            var count = 0;
            foreach (var item in knifes)
            {
                var localRotation = item.m_transform.localRotation;
                var rotation = localRotation.eulerAngles;
                rotation.z = l[count] + x;
                localRotation.eulerAngles = rotation;
                item.m_transform.localRotation = localRotation;
                count++;
            }
        }).OnComplete(() =>
        {
            switchTween = DOVirtual.DelayedCall(Config.BASE_TIME_SWITCH_STATE / 4, () =>
            {
                switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 5, value =>
                {
                    foreach (var item in knifes)
                    {
                        var curRot = item.m_transform.up;
                        var des = (item.m_transform.position - transform.position).normalized;
                        item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                    }
                }).OnComplete(() => isOnAnim = false);
            });
        });

    }

    IEnumerator IAnim()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(2, 5f));
            if (!isOnAnim)
            {
                var r = UnityEngine.Random.value;
                if (r > 0.5f)
                    AnimationOff();
                else
                    AnimationOn();
            }
        }
    }

    private void OnDisable()
    {
        switchTween?.Kill();
    }
}

