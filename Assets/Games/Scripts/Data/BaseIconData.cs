﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/Icon")]
public class BaseIconData : ScriptableObject
{
    public Sprite coinIcon;
    public Sprite gemIcon;

    public Sprite canClaimBtn,
        cantClaimBtn,
        missionComplete,
        missionDoing,
        soundOn,
        soundOff,
        vibrateOn,
        vibrateOff,
        selectedNote,
        NoneSelectedNote,
        equipBtn,
        equipedBtn,
        baseFlag,
        canBuyBtn,
        cantBuyBtn;
}