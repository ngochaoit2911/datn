﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName =  "Data/Config/Booster")]
public class BoosterData : ScriptableObject
{
    public List<BoosterInfor> boosterData;

    [ContextMenu("Add")]
    public void Add()
    {
        boosterData = new List<BoosterInfor>();
        foreach (var t in Enum.GetValues(typeof(ItemBuffType)).Cast<ItemBuffType>().ToList()) 
        {
            boosterData.Add(new BoosterInfor
            {
                value = 0,
                type = t,
            });
        }
    }

}

[System.Serializable]
public struct BoosterInfor
{
    public ItemBuffType type;
    public Sprite icon;
    public float time;
    public float value;
}