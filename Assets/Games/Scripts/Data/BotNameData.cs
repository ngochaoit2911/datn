﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/BotName")]
public class BotNameData : ScriptableObject
{
    public List<CountryBotName> botNames;
}

[System.Serializable]
public struct CountryBotName
{
    public string countryName;
    public string countryCode;
    public Sprite icon;
    public List<string> botName;
}
