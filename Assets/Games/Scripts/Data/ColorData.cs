﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/Config/Color")]
public class ColorData : ScriptableObject
{
    public List<Color> colors;

    public Color bladeColor, heroColor;

}