﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/UI/DailyGift")]
public class DailyGiftData : ScriptableObject
{
    public List<DailyGiftInfor> giftData;
}

[System.Serializable]
public struct DailyGiftInfor
{
    public int day;
    public GiftType gift;
    public int value;
    public KnifeType knifeType;
    public MaskType maskType;
    public string description;
}

public enum GiftType
{
    Coin, Gem, Blade, Mask, Combo
}