﻿
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Config/AI")]
public class EnemyAIData : ScriptableObject
{
    public float probilityAFK;

    public float pointForKnife;
    public float pointForKnifeInLine;
    public float pointForHitImpediment;
    public float pointForDistanceToNearestEnemy;
    public float pointForOverWall;
    public float pointForHitEnemy;
    public float pointForItem;

    public float probabilityIdle, timeIdle;
    public float timeMove;
    public float probabilityMoveOnLastDirection = 0.7f;
    public float probabilityIdleOnFace = 0.1f;
}
