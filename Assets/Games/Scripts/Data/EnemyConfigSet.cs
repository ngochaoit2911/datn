﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/Config/AIConfigSet")]
public class EnemyConfigSet : ScriptableObject
{
    public float numeralForKnife;
    public float numeralForDontHitImpediment;
    public float numeralForAttack;
    public float numeralForAvoid;
    public float numeralForOverWall;
    public float numeralForHitEnemy;
}