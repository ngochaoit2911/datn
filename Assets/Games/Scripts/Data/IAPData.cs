﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/UI/IAP")]
public class IAPData : ScriptableObject
{
    public List<IAPInfor> iapData;
}

[System.Serializable]
public struct IAPInfor
{
    public string id;
    public string title;
    public float cost;
    public Sprite icon;
    public int getValue;
}