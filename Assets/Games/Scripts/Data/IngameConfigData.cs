﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/GameConfig")]
public class IngameConfigData : ScriptableObject
{
    public float probabilityAFK, probabilityDareDevil, probabilityToDefense;
    public float probabilityLikeIdle;

}