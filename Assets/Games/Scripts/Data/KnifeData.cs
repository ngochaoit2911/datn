﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Config/Knife")]
public class KnifeData : ScriptableObject
{
    public List<KnifeInfor> knifeData;

    [ContextMenu("Add")]
    void Add()
    {
        var types = Enum.GetValues(typeof(KnifeType)).Cast<KnifeType>().ToList();
        foreach (var type in types)
        {
            if (HasKey(type))
            {
                continue;
            }
            knifeData.Add(new KnifeInfor
            {
                knifeType = type
            });
        }
    }
    private bool HasKey(KnifeType type)
    {
        foreach (var mask in knifeData)
        {
            if (mask.knifeType == type)
                return true;
        }

        return false;
    }

    [ContextMenu("Add UIEffect")]
    void AddUiEffect()
    {
        for (int i = 0; i < knifeData.Count; i++)
        {
            if ((int)knifeData[i].knifeType <= 1)
            {
                var infor = knifeData[i];
                infor.uiEffectType = KnifeUIEffectType.None;
                knifeData[i] = infor;
            }
            else
            {
                if (knifeData[i].addSpeed == 0 && knifeData[i].addKnifeInStart == 0 &&
                    knifeData[i].avoidObstacle == 0 && knifeData[i].buffType == BuffType.None)
                {
                    var infor = knifeData[i];
                    infor.uiEffectType = KnifeUIEffectType.None;
                    knifeData[i] = infor;
                }
                else
                {
                    if ((int)knifeData[i].knifeType <= 6)
                    {
                        var infor = knifeData[i];
                        infor.uiEffectType = KnifeUIEffectType.Rank;
                        knifeData[i] = infor;
                    }
                    else
                    {
                        if (knifeData[i].setBuffAddKnifeInStart == 0 && knifeData[i].setBuffAddKnifeInStart == 0 &&
                            knifeData[i].setBuffAvoidObstacle == 0)
                        {
                            var infor = knifeData[i];
                            infor.uiEffectType = KnifeUIEffectType.Normal;
                            knifeData[i] = infor;
                        }
                        else
                        {
                            var infor = knifeData[i];
                            infor.uiEffectType = KnifeUIEffectType.Special;
                            knifeData[i] = infor;
                        }
                    }
                }
            }
        }
    }

}

[System.Serializable]
public struct KnifeInfor
{
    public KnifeType knifeType;
    public Sprite icon;
    public float addSpeed;
    public int addKnifeInStart;
    public float avoidObstacle;
    public bool isHasBuff;
    public BuffType buffType;
    public float value;
    public MaskType setTypeBuff;
    public float setBuffAddSpeed;
    public int setBuffAddKnifeInStart;
    public float setBuffAvoidObstacle;

    public CostBuy costType;
    public int valueToGet;

    public float scaleSizeOnShop;

    public KnifeUIEffectType uiEffectType;
    public bool isRotate;

}

public enum BuffType
{
    None,
    UpDownAttack,
    LeftRightAttack,
    DamageFour,
    DamageX,
}

public enum CostBuy
{
    Coin, Gem, Video, Day, Rank
}

public enum KnifeType
{   
    Carrot = 0,
    Radish,
    PennysWise,
    MrBanana,
    MicDrop,
    IceCony,
    Bread,
    Scallions,
    Umbrella,
    WowPlunger,
    Andrenaline,
    Bottle,
    Nagini,
    Trident,
    Colorful,
    IceCream,
    Sausage,
    Cactus,
    Drumstick,
    MeatStick,
    Fishy,
    Condom,
    Toothbrush,
    Corny,
    Cuttle,
    Rainbow,
    WizardStaff,
    Mouse,
    ScreamingChicken,
    Trumpet,
    MiddleFinger,
    Spoon,
    Candle,
    Feather,
    Bone,
    Knife,
    GoldenSword,
    PirateKing,
    Saw,
    Kunai,
    LoveArrow,
    ZeldaSword,
    CleaverKnife,
    BloodThirster,
    Bayonetta,
    FlipKnife,
    Keris,
    PaperKnife,
    Khopesh,
    Alienware,
    VikingAxe,
    DoubleAxe,
    Pyke,
    Rocket,
    Archaeozia,
    MagicStaff,
    DemonKing,
    Warrior,
    Vibrator,
    ChristmasTree,
    Superman,
    Yolo,
    EggWhisk,

}

public enum KnifeUIEffectType
{
    None, Normal, Rank, Special
}