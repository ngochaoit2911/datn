﻿
using UnityEngine;

[System.Serializable]
public struct MapData
{
    public RankType rank;
    public Vector2 size;
    public int difficult;
    public int spawnKnifePerSecond;
    public int knifeMax;
}

public enum RankType
{
    DontRank = -1,
    Bronze1,
    Bronze2,
    Bronze3,
    Silver1,
    Silver2,
    Silver3,
    Golden1,
    Golden2,
    Golden3,
    Platinum1,
    Platinum2,
    Platinum3,
    Diamond1,
    Diamond2,
    Diamond3,
    Master1,
    Master2,
    Master3,
    Legend1,
    Legend2,
    Legend3

}

