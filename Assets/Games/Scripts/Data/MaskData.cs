﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/Config/Mask")]
public class MaskData : ScriptableObject
{
    public List<MaskInfor> maskData;

    [ContextMenu("Add")]
    void Add()
    {
        var t = Enum.GetValues(typeof(MaskType)).Cast<MaskType>();
        foreach (var type in t)
        {
            if (HasKey(type))
            {
                continue;
            }
            maskData.Add(new MaskInfor
            {
                type = type,
                maskName = type.ToString()
            });
        }
    }

#if UNITY_EDITOR
    [ContextMenu("Add effect Type")]
    void AddEffectType()
    {
        var knife = AssetDatabase.LoadAssetAtPath<KnifeData>("Assets/Games/Data/KnifeData/KnifeData.asset");
        for (int i = 0; i < maskData.Count; i++)
        {
            if (maskData[i].buffType == MaskBuffType.None && maskData[i].value == 0)
            {
                var infor = maskData[i];
                infor.uiEffectType = MaskUIEffect.None;
                maskData[i] = infor;
            }
            else
            {
                var x = knife.knifeData.FindIndex(s => s.setTypeBuff == maskData[i].type);
                if (x == -1)
                {
                    var infor = maskData[i];
                    infor.uiEffectType = MaskUIEffect.Blue;
                    maskData[i] = infor;
                }
                else
                {
                    var infor = maskData[i];
                    infor.uiEffectType = MaskUIEffect.Orange;
                    maskData[i] = infor;
                }
            }
        }
    }

#endif
    private bool HasKey(MaskType type)
    {
        foreach (var mask in maskData)
        {
            if (mask.type == type)
                return true;
        }

        return false;
    }
}

[System.Serializable]
public struct MaskInfor
{
    public string maskName;
    public MaskType type;
    public Sprite icon;
    public MaskBuffType buffType;
    public float value;
    public CostBuy costType;
    public int valueToGet;
    public float scaleSizeOnShop;

    public MaskUIEffect uiEffectType;
}

public enum MaskType
{    
    UGotMe = 0,
    Stupid,
    TrollFace,
    BarnieAngry,
    YuNo,
    Spiderpman,
    KiddingMe,
    OhRealllly,
    HaHaHa,
    TheHell,
    GTFO,
    Okay,
    Pfftchchch,
    Rageeee,
    MrLonely,
    Huh,
    MouthShut,
    Ahhhhhhh,
    Happylol,
    HappyGirl,
    Serious,
    NoDoubt,
    Nononooo,
    Madmin,
    Surprise,
    Waittt,
    SadWhyyyy,
    Jkd,
    Wuttt,
    Oops,
    Teardrop,
    Heartbreak,
}

public enum MaskBuffType
{
    Magnet,
    Run,
    More,
    Protect,
    Slow,
    Dull,
    None
}

public enum MaskUIEffect
{
    None, Blue, Orange
}