﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/UI/Mission")]
public class MissionData : ScriptableObject
{
    public List<MissionInfor> missionData;

    [ContextMenu("Add")]

    void Add()
    {
        missionData = new List<MissionInfor>();
        foreach (var s in Enum.GetValues(typeof(MissionType)).Cast<MissionType>())
        {
            missionData.Add(new MissionInfor
            {
                mission = s.ToString(),
                missionType =  s,
                eventListen = (EventID)((int)(s + 1)),
                rewardType = MissionRewardType.Coin
            });
        }
    }

    [ContextMenu("Upper")]
    void Upper()
    {
        for (int i = 0; i < missionData.Count; i++)
        {
            var s = missionData[i].missionDescription.ToUpper();
            var infor = missionData[i];
            infor.missionDescription = s;
            missionData[i] = infor;
        }
    }

}

[System.Serializable]
public struct MissionInfor
{
    public string mission;
    public MissionType missionType;
    public EventID eventListen;
    public int valueToGet;
    public string missionDescription;
    public MissionRewardType rewardType;
    public int valueGet;
}

public enum MissionType
{
    Top1 = 0,
    DoubleKill,
    TripleKill,
    Quadrakill,
    Pentakill,
    HexaKill,
    HasKnife,
    PlayMatch,
    UnlockNewKnife,
    UnlockNewHero,
    UpRank,
    KnockKnife
}

public enum MissionRewardType
{
    Coin,
    Gem
}

[System.Serializable]
public struct PlayerMissionInfor
{
    public MissionType missionType;
    public int levelOfMission;
    public int missionValue;
    public MissionState state;
}

public enum MissionState
{
    Complete,
    Doing
}

