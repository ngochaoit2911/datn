﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/AI/RankAI")]
public class RankAIData : ScriptableObject
{
    public float probabilityDontCanMove;
    public float probabilityCheckDefense;
    public bool isCheckItem;
    public float probabilityLikeIdle;
    public float probabilityAFK;
    public float probilityDareDevil;
}