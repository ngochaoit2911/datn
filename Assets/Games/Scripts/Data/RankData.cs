﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName =  "Data/Config/Rank")]
public class RankData : ScriptableObject
{
    public List<RankInfor> rankData;
    public List<int> coinBonusMultiKill;
    public int pointKillOnEnemy = 1;
    public int pointDoubleKill = 1;
    public int pointTripleKill = 2;
    public int pointQuadraKill = 3;
    public int pointPentaKill = 4;
    public int pointHexaKill = 5;
    public int pointTop1 = 5;
    public int pointTop2 = 2;
    public int pointTop3 = 1;

    public int pointDontKill = 1;

    public List<Sprite> rankIcon;
    public List<Sprite> rankIconInRankUp;

    [ContextMenu("Add")]
    void Add()
    {
        rankData = new List<RankInfor>();
        foreach (var rankType in Enum.GetValues(typeof(RankType)).Cast<RankType>())
        {
            rankData.Add(new RankInfor
            {
                rank = rankType,
            });
        }
    }

#if UNITY_EDITOR
    [ContextMenu("Change")]
    void Change()
    {
        for (var i = 0; i < rankData.Count; i++)
        {
            Debug.Log("Á");
            var rankInfor = rankData[i];
            var infor = rankInfor;
            infor.posYText -= 2;
            rankData[i] = rankInfor;
        }

        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
    }

    [ContextMenu("Change Reward")]
    void Change1()
    {
        for (var i = 0; i < rankData.Count; i++)
        {
            var rankInfor = rankData[i];
            var infor = rankInfor;
            infor.coinRewardOnKill += 100;
            rankData[i] = rankInfor;
        }

        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
    }
#endif


}

[System.Serializable]
public struct RankInfor
{
    public RankType rank;
    public float probabilityRevival;
    public int pointToUp;
    public int coinRewardOnKill;
    public float posYText;
}