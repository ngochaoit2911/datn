﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/Config/RankCombo")]
public class RankSelectionComboData : ScriptableObject
{
    public List<RankComboInfor> data;

    [ContextMenu("Add")]
    void Add()
    {
        var bronze = new RankComboInfor
        {
            knifeTypes = new List<KnifeType>
            {
                KnifeType.Carrot, KnifeType.Radish, KnifeType.DoubleAxe, KnifeType.Cactus, KnifeType.Candle,
                KnifeType.Bone, KnifeType.Sausage, KnifeType.Saw, KnifeType.LoveArrow
                , KnifeType.FlipKnife,
                KnifeType.Keris, KnifeType.PaperKnife, KnifeType.Spoon, KnifeType.IceCream, KnifeType.VikingAxe, KnifeType.Yolo
            },

            maskTypes = new List<MaskType>
            {
                MaskType.UGotMe, MaskType.Okay, MaskType.Huh, MaskType.Stupid, MaskType.Serious, MaskType.MouthShut,
                MaskType.Rageeee, MaskType.UGotMe, MaskType.Okay, MaskType.Stupid
            }
        };

        var silver = new RankComboInfor
        {
            knifeTypes = new List<KnifeType>
            {
                KnifeType.Carrot, KnifeType.Radish, KnifeType.DoubleAxe, KnifeType.Cactus, KnifeType.Candle,
                KnifeType.Bone, KnifeType.Sausage, KnifeType.Saw, KnifeType.LoveArrow
                , KnifeType.FlipKnife,
                KnifeType.Keris, KnifeType.PaperKnife, KnifeType.Spoon, KnifeType.IceCream, KnifeType.VikingAxe, KnifeType.Yolo
            },

            maskTypes = new List<MaskType>
            {
                MaskType.Stupid, MaskType.Madmin, MaskType.TheHell, MaskType.BarnieAngry, MaskType.Jkd, MaskType.Surprise,
                MaskType.Stupid, MaskType.Stupid, MaskType.UGotMe, MaskType.UGotMe
            }
        };

        var golden = new RankComboInfor
        {
            knifeTypes = new List<KnifeType>
            {
                KnifeType.Carrot, KnifeType.Radish, KnifeType.DoubleAxe, KnifeType.Cactus, KnifeType.Candle,
                KnifeType.Bone, KnifeType.Sausage, KnifeType.Saw, KnifeType.LoveArrow
                , KnifeType.FlipKnife,
                KnifeType.Keris, KnifeType.PaperKnife, KnifeType.Spoon, KnifeType.IceCream, KnifeType.VikingAxe, KnifeType.Yolo
            },

            maskTypes = new List<MaskType>
            {
                MaskType.Stupid, MaskType.UGotMe, MaskType.HaHaHa, MaskType.Oops,
                MaskType.Wuttt, MaskType.Okay, MaskType.Stupid, MaskType.Waittt, MaskType.Teardrop,
                MaskType.Heartbreak, MaskType.HappyGirl, MaskType.Happylol, MaskType.Surprise, MaskType.BarnieAngry
            }
        };

        var platinum = new RankComboInfor
        {
            knifeTypes = new List<KnifeType>
            {
                KnifeType.MeatStick, KnifeType.Bottle, KnifeType.DoubleAxe, KnifeType.Fishy, KnifeType.Condom,
                KnifeType.Andrenaline, KnifeType.Scallions, KnifeType.Knife
                , KnifeType.GoldenSword
                , KnifeType.PirateKing,
                KnifeType.Kunai, KnifeType.Trumpet, KnifeType.Mouse, KnifeType.IceCream, KnifeType.VikingAxe, KnifeType.Yolo
            },

            maskTypes = new List<MaskType>
            {
                MaskType.TrollFace, MaskType.Stupid, MaskType.Serious, MaskType.Happylol, MaskType.HaHaHa,
                MaskType.TheHell, MaskType.MrLonely,
                MaskType.Huh, MaskType.GTFO
            }
        };

        var diamond = new RankComboInfor
        {
            knifeTypes = new List<KnifeType>
            {
                KnifeType.MeatStick, KnifeType.Bottle, KnifeType.DoubleAxe, KnifeType.Fishy, KnifeType.Condom,
                KnifeType.Andrenaline, KnifeType.Scallions, KnifeType.Knife
                , KnifeType.GoldenSword
                , KnifeType.PirateKing,
                KnifeType.Kunai, KnifeType.Trumpet, KnifeType.Mouse, KnifeType.IceCream, KnifeType.VikingAxe, KnifeType.Yolo

            },

            maskTypes = new List<MaskType>
            {
                MaskType.Nononooo, MaskType.Madmin, MaskType.Wuttt, MaskType.Waittt, MaskType.TrollFace,
                MaskType.Rageeee, MaskType.Pfftchchch, MaskType.KiddingMe
            }
        };

        var master = new RankComboInfor
        {
            knifeTypes = new List<KnifeType>
            {
                KnifeType.MiddleFinger, KnifeType.ZeldaSword, KnifeType.LoveArrow, KnifeType.Fishy, KnifeType.Condom,
                KnifeType.Andrenaline, KnifeType.Scallions, KnifeType.Knife
                , KnifeType.GoldenSword
                , KnifeType.Pyke,
                KnifeType.Kunai, KnifeType.Archaeozia, KnifeType.Mouse, KnifeType.ChristmasTree, KnifeType.BloodThirster, KnifeType.Yolo
            },

            maskTypes = new List<MaskType>
            {
                MaskType.TrollFace, MaskType.YuNo, MaskType.Happylol, MaskType.HaHaHa, MaskType.OhRealllly,
                MaskType.Spiderpman, MaskType.SadWhyyyy, MaskType.NoDoubt,
                MaskType.MrLonely, MaskType.Teardrop, MaskType.Heartbreak, MaskType.Pfftchchch, MaskType.GTFO, MaskType.Madmin,
                MaskType.Happylol
            }
        };

        var legend = new RankComboInfor
        {
            knifeTypes = new List<KnifeType>
            {
                KnifeType.MiddleFinger, KnifeType.ZeldaSword, KnifeType.LoveArrow, KnifeType.Fishy, KnifeType.Condom,
                KnifeType.Andrenaline, KnifeType.Scallions, KnifeType.Knife
                , KnifeType.GoldenSword
                , KnifeType.Pyke,
                KnifeType.Kunai, KnifeType.Archaeozia, KnifeType.Mouse, KnifeType.ChristmasTree, KnifeType.BloodThirster, KnifeType.Yolo
            },

            maskTypes = new List<MaskType>
            {
                MaskType.HaHaHa, MaskType.HaHaHa, MaskType.Happylol, MaskType.YuNo, MaskType.HappyGirl, MaskType.Rageeee,
                MaskType.HaHaHa,
                MaskType.TheHell, MaskType.Oops, MaskType.Teardrop, MaskType.Happylol, MaskType.Ahhhhhhh, MaskType.Wuttt,
                MaskType.SadWhyyyy
            }
        };

        data = new List<RankComboInfor> {bronze, silver, golden, platinum, diamond, master, legend};

    }

}

[System.Serializable]
public struct RankComboInfor
{
    public List<KnifeType> knifeTypes;
    public List<MaskType> maskTypes;
}