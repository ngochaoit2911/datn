﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Data/UI/Tip")]
public class TipData : ScriptableObject
{
    public List<string> tipData;
}