﻿
using System;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public static class DatabaseManager
{
    private const string TitleId = "ACF4C";
    private const string EmailExtension = "@gmail.com";
    public static bool IsLoginComplete = false;
    public static string UserName = "";
    public static bool IsFirstSwitchToOnline = false;
    public static bool LoadDataComplete = false;
    public static string RANKING = "Ranking";

    public static void Init()
    {
        PlayFabSettings.TitleId = TitleId;
    }

    public static void Login(string userName, string password, Action<LoginResult> r, Action<PlayFabError> e)
    {
        PlayFabClientAPI.LoginWithPlayFab(new LoginWithPlayFabRequest
        {
            Username = userName,
            Password = password,
            TitleId = TitleId
        }, r.Invoke, e.Invoke);
    }

    public static void Register(string userName, string password, Action<RegisterPlayFabUserResult> r,
        Action<PlayFabError> e)
    {
        PlayFabClientAPI.RegisterPlayFabUser(new RegisterPlayFabUserRequest
        {
            Username = userName,
            Password = password,
            Email = userName + EmailExtension
        }, r.Invoke, e.Invoke);
    }

    public static void GetAllData(Action<GetUserDataResult> r, Action<PlayFabError> e)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest(), r.Invoke, e.Invoke);
    }

    public static void GetData(List<string> keys, Action<GetUserDataResult> r, Action<PlayFabError> e)
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest
        {
            Keys = keys
        }, r.Invoke, e.Invoke);
    }

    public static void PostData(Dictionary<string, string> data, Action<UpdateUserDataResult> r, Action<PlayFabError> e)
    {
        PlayFabClientAPI.UpdateUserData(new UpdateUserDataRequest
        {
            Data = data,
            Permission = UserDataPermission.Public,
        }, r.Invoke, e.Invoke);
    }

    public static T GetObject<T>(this GetUserDataResult result, string key)
    {
        return JsonUtility.FromJson<T>(result.Data[key].Value);
    }

    public static List<T> GetList<T>(this GetUserDataResult result, string key)
    {
        return JsonHelper.FromJson<T>(result.Data[key].Value);
    }

    public static int GetInt(this GetUserDataResult result, string key)
    {
        return int.Parse(result.Data[key].Value);
    }

    public static void GetLeaderBoard(Action<GetLeaderboardResult> r, Action<PlayFabError> e)
    {
        var request = new GetLeaderboardRequest
        {
            StartPosition = 0,
            StatisticName = RANKING,
            MaxResultsCount = 20,
        };
        PlayFabClientAPI.GetLeaderboard(request,r.Invoke, e.Invoke);
    }

    public static void UpdateDisplayName(string displayName)
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = displayName
        }, r =>
        {
            Debug.Log("Change DisplayName Complete");
        }, e =>
        {

            Debug.Log(e.ErrorMessage.ToString());
        });
    }

    public static void UpdateRanking(RankType type)
    {
        PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
               new StatisticUpdate
               {
                   StatisticName = RANKING,
                   Value = (int) type
               } 
            }

        }, r => {

            Debug.Log("Update Ranking Complete");
        }, e => {
            Debug.Log(e.ErrorMessage.ToString());
        });
    }

}
