﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPEditor : EditorWindow
{
    private ProductCatalog catalog;

    [MenuItem("GameData/IAP")]
    public static void Init()
    {
        IAPEditor levelEditor = GetWindow<IAPEditor>();
        levelEditor.Show();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Refresh"))
        {
            var c = System.IO.File.ReadAllText("Assets/Resources/IAPProductCatalog.json");
            catalog = ProductCatalog.Deserialize(c);
        }

        if (GUILayout.Button("Load"))
        {
            //IAPData data = GameHelper.GetAllAssetAtPath<IAPData>(string.Empty, "Assets/Games/Data/UI/IAP")[0];
            IAPData data = AssetDatabase.LoadAssetAtPath<IAPData>("Assets/Games/Data/UI/IAP.asset");
            catalog.allProducts.Clear();

            if (catalog.allProducts.Count == 0)
            {
                for (int i = 0; i < data.iapData.Count; i++)
                {
                    var p = new ProductCatalogItem();
                    p.id = data.iapData[i].id;
                    p.type = ProductType.Consumable;
                    p.defaultDescription.googleLocale = TranslationLocale.en_US;
                    p.defaultDescription.Title = data.iapData[i].title;

                    p.googlePrice.value = (decimal)data.iapData[i].cost;
                    catalog.Add(p);
                }
            }
        }

        if (GUILayout.Button("Save"))
        {
            //this.dirty = false;
            System.IO.File.WriteAllText("Assets/Resources/IAPProductCatalog.json", ProductCatalog.Serialize(this.catalog));
            AssetDatabase.ImportAsset("Assets/Resources/IAPProductCatalog.json");
        }

        if (GUILayout.Button("Ping"))
        {
            GameHelper.PingObj("Assets/Games/Data/UI/IAP.asset");
        }
    }

}
