﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(KnifeData))]
public class KnifeDataEditor : Editor
{
    private KnifeData data;
    private string id = "1Ct2xl7I3kUsSyZHeklBZ90SmgaxfaWa483MZHY5r1Sc";
    private string sheetName = "Blade Meme";

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        data = (KnifeData) target;

        GUILayout.Space(20);

        GUILayout.BeginHorizontal("Box");

        if (GUILayout.Button("GetData"))
        {
            GetData();
        }


        if (GUILayout.Button("ChangeCost"))
        {
            ChangeCost();
        }

        if (GUILayout.Button("Change Size"))
        {
            ChangeSize();
        }

        GUILayout.EndHorizontal();

    }

    private void ChangeSize()
    {
        for (int i = 0; i < data.knifeData.Count; i++)
        {
            var infor = data.knifeData[i];
            infor.scaleSizeOnShop = 0.8f;
            data.knifeData[i] = infor;
        }

        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
    }

    private void ChangeCost()
    {
        ReadExcelOnline.Connect();
        ReadExcelOnline.GetTable(id, sheetName, dictionary =>
        {
            foreach (var row in dictionary.Keys)
            {
                if (row == 0)
                    continue;


                if (dictionary[row].ContainsKey(13))
                {
                    var s = dictionary[row][13];
                    if (s.Contains("Coin"))
                    {
                        var infor = data.knifeData[row - 1];
                        infor.costType = CostBuy.Coin;
                        infor.valueToGet = int.Parse(s.Replace("Coin", "").Trim());
                        data.knifeData[row - 1] = infor;
                    }

                    if (s.Contains("Gem"))
                    {
                        var infor = data.knifeData[row - 1];
                        infor.costType = CostBuy.Gem;
                        infor.valueToGet = int.Parse(s.Replace("Gem", "").Trim());
                        data.knifeData[row - 1] = infor;
                    }

                    if (s.Contains("videos"))
                    {
                        var infor = data.knifeData[row - 1];
                        infor.costType = CostBuy.Video;
                        infor.valueToGet = int.Parse(s.Replace("videos", "").Trim());
                        data.knifeData[row - 1] = infor;
                    }

                    if (s.Contains("Day"))
                    {
                        var infor = data.knifeData[row - 1];
                        infor.costType = CostBuy.Day;
                        infor.valueToGet = int.Parse(s.Replace("Day", "").Trim());
                        data.knifeData[row - 1] = infor;
                    }

                }

                ////foreach (var col in dictionary[row].Keys)
                ////{
                ////    Debug.Log(row +"  " + col + "  " + dictionary[row][col]);
                ////}
            }
        });
        EditorUtility.SetDirty(data);
        AssetDatabase.SaveAssets();
    }

    void GetData()
    {
        
        ReadExcelOnline.Connect();
        ReadExcelOnline.GetTable(id, "Blade Meme", dictionary =>
        {
            data.knifeData = new List<KnifeInfor>();
            foreach (var row in dictionary.Keys)
            {
                if (row == 0)
                    continue;
                var infor = new KnifeInfor();
                infor.knifeType = (KnifeType) (row - 1);
                var x = dictionary[row][2];
                switch (x)
                {
                    case "None":
                        infor.buffType = BuffType.None;
                        infor.isHasBuff = false;
                        break;
                    case "Dogge":
                        infor.buffType = BuffType.None;
                        infor.avoidObstacle = float.Parse(dictionary[row][3].Replace("%", "")) / 100;
                        break;
                    case "Add":
                        infor.buffType = BuffType.None;
                        infor.addKnifeInStart = int.Parse(dictionary[row][3]);
                        break;
                    case "Speed":
                        infor.buffType = BuffType.None;
                        infor.addSpeed = float.Parse(dictionary[row][3].Replace("%", "")) / 100;
                        break;
                    case "Dmg1":
                        infor.buffType = BuffType.DamageFour;
                        infor.isHasBuff = true;
                        infor.value = float.Parse(dictionary[row][3]);
                        break;
                    case "Dmg2":
                        infor.buffType = BuffType.DamageX;
                        infor.isHasBuff = true;
                        infor.value = float.Parse(dictionary[row][3]);
                        break;
                    case "Atk LR":
                        infor.buffType = BuffType.LeftRightAttack;
                        infor.isHasBuff = true;
                        infor.value = float.Parse(dictionary[row][3]);
                        break;
                    case "Atk UD":
                        infor.buffType = BuffType.UpDownAttack;
                        infor.isHasBuff = true;

                        infor.value = float.Parse(dictionary[row][3]);
                        break;

                }

                if (dictionary[row].ContainsKey(6))
                {

                    var s = dictionary[row][6];
                    Debug.Log(s);
                    if (s.Contains("Coin"))
                    {
                        infor.costType = CostBuy.Coin;
                        infor.valueToGet = int.Parse(s.Replace("Coin", ""));
                    }
                    else if (s.Contains("gems"))
                    {
                        infor.costType = CostBuy.Gem;
                        infor.valueToGet = int.Parse(s.Replace("gems", ""));
                    }
                    else if (s.Contains("videos"))
                    {
                        infor.costType = CostBuy.Video;
                        infor.valueToGet = int.Parse(s.Replace("videos", ""));

                    }
                    else if (s.Contains("Day"))
                    {
                        infor.costType = CostBuy.Day;
                        infor.valueToGet = int.Parse(s.Replace("Day", ""));
                    }
                    else
                    {
                        infor.costType = CostBuy.Coin;
                        infor.valueToGet = int.Parse(s.Replace("Coin", ""));
                    }
                }

                infor.scaleSizeOnShop = 1;
                data.knifeData.Add(infor);
            }
        });
        EditorUtility.SetDirty(data);
        AssetDatabase.SaveAssets();
    }
}