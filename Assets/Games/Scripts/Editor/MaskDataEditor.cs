﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MaskData))]
public class MaskDataEditor : Editor
{
    private MaskData data;
    private string id = "1Ct2xl7I3kUsSyZHeklBZ90SmgaxfaWa483MZHY5r1Sc";
    private string sheetName = "Hero Meme";

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        data = (MaskData)target;

        GUILayout.Space(20);

        GUILayout.BeginHorizontal("Box");

        if (GUILayout.Button("GetData"))
        {
            GetData();
        }

        if (GUILayout.Button("ChangeCost"))
        {
            ChangeCost();
        }

        if (GUILayout.Button("ChangeName"))
        {
            ChangeName();
        }

        if (GUILayout.Button("ChangeImage"))
        {
            ChangeImage();
        }

        GUILayout.EndHorizontal();

    }

    private void ChangeName()
    {
        for (int i = 0; i < data.maskData.Count; i++)
        {
            var infor = data.maskData[i];
            infor.maskName = infor.maskName.ToUpper();
            data.maskData[i] = infor;
        }
        EditorUtility.SetDirty(data);
        AssetDatabase.SaveAssets();

    }

    public void GetData()
    {
        ReadExcelOnline.Connect();
        ReadExcelOnline.GetTable(id, sheetName, dictionary =>
        {
            if (data.maskData.Count > 0)
            {

            }
            else
            {
                data.maskData = new List<MaskInfor>();
                foreach (var row in dictionary.Keys)
                {
                    if (row == 0)
                        continue;
                    var infor = new MaskInfor();
                    infor.type = (MaskType) (row - 1);
                    infor.maskName = dictionary[row][1];
                    infor.buffType = dictionary[row][2] == "Magnet" ? MaskBuffType.Magnet :
                        dictionary[row][2] == "Run" ? MaskBuffType.Run :
                        dictionary[row][2] == "More" ? MaskBuffType.More :
                        dictionary[row][2] == "Protect" ? MaskBuffType.Protect :
                        dictionary[row][2] == "Slow" ? MaskBuffType.Slow :
                        dictionary[row][2] == "Dull" ? MaskBuffType.Dull : MaskBuffType.None;

                    Debug.Log(dictionary[row][3]);
                    infor.value = (dictionary[row][3].Contains("%")
                        ? float.Parse(dictionary[row][3].Replace("%", "")) / 100
                        : dictionary[row][3].Contains("None") ? 0 : float.Parse(dictionary[row][3]));

                    if (dictionary[row].ContainsKey(4))
                    {
                        var s = dictionary[row][4];
                        if (s.Contains("Coin"))
                        {
                            infor.costType = CostBuy.Coin;
                            infor.valueToGet = int.Parse(s.Replace("Coin", "").Trim());
                        }

                        else if (s.Contains("gems"))
                        {
                            infor.costType = CostBuy.Gem;
                            infor.valueToGet = int.Parse(s.Replace("gems", "").Trim());
                        }

                        else if (s.Contains("videos"))
                        {
                            infor.costType = CostBuy.Video;
                            infor.valueToGet = int.Parse(s.Replace("videos", "").Trim());
                        }

                        else if (s.Contains("Day"))
                        {
                            infor.costType = CostBuy.Day;
                            infor.valueToGet = int.Parse(s.Replace("Day", "").Trim());
                        }
                        else
                        {
                            infor.costType = CostBuy.Coin;
                            infor.valueToGet = int.Parse(s.Trim());
                        }

                    }
                    data.maskData.Add(infor);
                }
            
                ////foreach (var col in dictionary[row].Keys)
                ////{
                ////    Debug.Log(row +"  " + col + "  " + dictionary[row][col]);
                ////}
            }
        });
        EditorUtility.SetDirty(data);
        AssetDatabase.SaveAssets();
    }

    private void ChangeCost()
    {
        ReadExcelOnline.Connect();
        ReadExcelOnline.GetTable(id, sheetName, dictionary =>
        {
            foreach (var row in dictionary.Keys)
            {
                if (row == 0)
                    continue;


                if (dictionary[row].ContainsKey(8))
                {
                    var s = dictionary[row][8];
                    if (s.Contains("Coin"))
                    {
                        var infor = data.maskData[row - 1];
                        infor.costType = CostBuy.Coin;
                        infor.valueToGet = int.Parse(s.Replace("Coin", "").Trim());
                        data.maskData[row - 1] = infor;
                    }

                    else if (s.Contains("gem"))
                    {
                        var infor = data.maskData[row - 1];
                        infor.costType = CostBuy.Gem;
                        infor.valueToGet = int.Parse(s.Replace("gem", "").Trim());
                        data.maskData[row - 1] = infor;
                    }

                    else if (s.Contains("videos"))
                    {
                        var infor = data.maskData[row - 1];
                        infor.costType = CostBuy.Video;
                        infor.valueToGet = int.Parse(s.Replace("videos", "").Trim());
                        data.maskData[row - 1] = infor;
                    }

                    else if (s.Contains("Day"))
                    {
                        var infor = data.maskData[row - 1];
                        infor.costType = CostBuy.Day;
                        infor.valueToGet = int.Parse(s.Replace("Day", "").Trim());
                        data.maskData[row - 1] = infor;
                    }
                    else
                    {
                        var infor = data.maskData[row - 1];
                        infor.costType = CostBuy.Coin;
                        infor.valueToGet = int.Parse(s.Trim());
                        data.maskData[row - 1] = infor;
                    }

                }
                ////foreach (var col in dictionary[row].Keys)
                ////{
                ////    Debug.Log(row +"  " + col + "  " + dictionary[row][col]);
                ////}
            }
        });
        EditorUtility.SetDirty(data);
        AssetDatabase.SaveAssets();
    }

    void ChangeImage()
    {
        
        //Debug.Log(objects.Length);
        //var sprites = objects.Where(q => q is Sprite).Cast<Sprite>().ToList();
        for (int i = 0; i < data.maskData.Count; i++)
        {
            var x = data.maskData[i];
            var path = "Assets/Games/Reskin/xuat52/xuatface/" + (i + 1) + ".png";
            var objects = AssetDatabase.LoadAssetAtPath<Sprite>(path);
            x.icon = objects;
            x.scaleSizeOnShop = 1;
            data.maskData[i] = x;
        }
        EditorUtility.SetDirty(data);
        AssetDatabase.SaveAssets();
    }

}