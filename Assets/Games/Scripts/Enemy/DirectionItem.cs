﻿using Assets.Games.Photon.Scripts.Controller;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionItem : PoolItem
{

    public SpriteRenderer main;
    public SpriteRenderer direction;
    public Net_KnifeController controller;
    private bool isOnCamera;

    public void OnSpawn(Color color)
    {
        main.color = color;
        direction.color = color;
    }
}
