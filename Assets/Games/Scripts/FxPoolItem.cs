﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class FxPoolItem : PoolItem
{
    private Tween existTween;
    public float sizeBase;

    public void OnSpawn(float timeExist, float scaleSize)
    {
        existTween?.Kill();
        existTween = DOVirtual.DelayedCall(timeExist, gameObject.Hide);

        m_transform.localScale = Vector3.one * scaleSize / sizeBase;

    }

    public void OnSpawn(float scaleSize)
    {
        m_transform.localScale = Vector3.one * scaleSize / sizeBase;
    }

}
