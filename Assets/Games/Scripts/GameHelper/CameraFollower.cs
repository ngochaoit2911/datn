﻿using System.Collections;
using System.Collections.Generic;
using Assets.Games.Photon.Scripts.Controller;
using Com.LuisPedroFonseca.ProCamera2D;
//using Cinemachine;
using DG.Tweening;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    //public CinemachineVirtualCamera cinemachine;
    private Transform t;
    public ProCamera2D proCamera;
    public ProCamera2DShake shake;
    public ProCamera2DNumericBoundaries bound;
    public Camera cam;

    private void Start()
    {
        t = transform;
    }

    public void SetUpFollower(Transform p)
    {
        proCamera.enabled = true;
        proCamera.CameraTargets = new List<CameraTarget>
        {
            new CameraTarget
            {
                TargetTransform = p,
                TargetInfluenceH = 1,
                TargetInfluenceV = 1,               
            }
        };
       // cinemachine.enabled = true;
        //cinemachine.Follow = p;
    }

    public void SetUpBounderies(Vector3 size)
    {
        bound.UseNumericBoundaries = true;
        bound.TopBoundary = size.y;
        bound.BottomBoundary = -size.y;
        bound.LeftBoundary = -size.x;
        bound.RightBoundary = size.x;
    }

    public void DisableFollow()
    {
        proCamera.enabled = false;
        //cinemachine.enabled = false;
    }

    public Vector3 GetCamPos()
    {
        return t.position;
    }

    private Tween zoomTween;
    public void ZoomCamera(float ortho)
    {
        zoomTween?.Kill();
        var x = cam.orthographicSize;
        zoomTween = DOVirtual.Float(x, ortho, 0.6f, value => { cam.orthographicSize = value; });
    }

    public void CameraShake()
    {
        if (GameManager.ins.IsVibrate)
        {
            shake.ShakeUsingPreset(0);
        }
    }

    //public Vector2 GetTopLeftPos()
    //{
    //    return new Vector2(t.position.x - GameController.camSize.x, t.position.y + GameController.camSize.y);
    //}

    //public Vector2 GetTopRightPos()
    //{
    //    return new Vector2(t.position.x + GameController.camSize.x, t.position.y + GameController.camSize.y);
    //}

    //public Vector2 GetBotLeftPos()
    //{
    //    return new Vector2(t.position.x - GameController.camSize.x, t.position.y - GameController.camSize.y);
    //}

    //public Vector2 GetBotRightPos()
    //{
    //    return new Vector2(t.position.x + GameController.camSize.x, t.position.y - GameController.camSize.y);
    //}

    public Vector2 GetPosAtTop(Vector2 p1, Vector2 p2)
    {
        var y = t.position.y + GameController.camSize.y - Config.DISTANCE_CAM;
        return GameHelper.GetPosY(p1, p2, y);
    }

    public Vector2 GetPosAtBot(Vector2 p1, Vector2 p2)
    {
        var y = t.position.y - GameController.camSize.y + Config.DISTANCE_CAM;
        return GameHelper.GetPosY(p1, p2, y);
    }

    public Vector2 GetPosAtRight(Vector2 p1, Vector2 p2)
    {
        var x = t.position.x + GameController.camSize.x - Config.DISTANCE_CAM;
        return GameHelper.GetPosX(p1, p2, x);
    }

    public Vector2 GetPosAtLeft(Vector2 p1, Vector2 p2)
    {
        var x = t.position.x - GameController.camSize.x + Config.DISTANCE_CAM;
        return GameHelper.GetPosX(p1, p2, x);
    }


    public Vector2 Net_GetPosAtTop(Vector2 p1, Vector2 p2)
    {
        var y = t.position.y + Net_Controller.camSize.y - Config.DISTANCE_CAM;
        return GameHelper.GetPosY(p1, p2, y);
    }

    public Vector2 Net_GetPosAtBot(Vector2 p1, Vector2 p2)
    {
        var y = t.position.y - Net_Controller.camSize.y + Config.DISTANCE_CAM;
        return GameHelper.GetPosY(p1, p2, y);
    }

    public Vector2 Net_GetPosAtRight(Vector2 p1, Vector2 p2)
    {
        var x = t.position.x + Net_Controller.camSize.x - Config.DISTANCE_CAM;
        return GameHelper.GetPosX(p1, p2, x);
    }

    public Vector2 Net_GetPosAtLeft(Vector2 p1, Vector2 p2)
    {
        var x = t.position.x - Net_Controller.camSize.x + Config.DISTANCE_CAM;
        return GameHelper.GetPosX(p1, p2, x);
    }
}
