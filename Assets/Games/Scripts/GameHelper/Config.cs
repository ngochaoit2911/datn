﻿
using UnityEngine;
public class Config
{
    public const int MAX_KNIFE = 50;
    public const int BASE_KNIFE_COUNT = 2;

    public const float BASE_RADIUS = 0.9f;
    public const float BASE_RADIUS_IN_UI = 135;
    public const float BASE_DEFENSE_RADIUS = 1;
    public const float BASE_ADD_RADIUS = 0.025f;
    public const float MAX_RADIUS = 2.1f;
    public const float BASE_MOVE_SPEED = 4.5f;
    public const float BASE_ROTATE_SPEED = 130;
    public const float BASE_KNIFE_SKILL_MOVE_SPEED = 10;
    public const float BASE_TIME_SWITCH_STATE = 2f;
    public const float BASE_TIME_DESCREASE_KNIFE = 3;
    public const float BASE_TIME_CAN_MOVE = 0.002f;
    public const float PROTECTED_TIME = 1;

    public const float TIME_AREA_EXIST = 3;
    public const float SLOW_BLADE_VALUE = 0.3f;
    public const float SLOW_MOVE_VALUE = 0.3f;
    public const float TIME_MAGNET = 0.3f;
    public const float TIME_RANKING = 1;
    public const float DISTANCE_CAM = 0.6f;

    public const int KNIFE_UI_COUNT = 7;
    public const float SPEED_ROTATE_CHILD = 400;

    public const float ATTACK_CAMSZIE = 6.4f;
    public const float DEFNSE_CAMSZIE = 5.6f;

}

public class LayerNames
{
    public const string PLAYER = "Player";
    public const string ENEMY = "Enemy";

    public const string IMPEDIMENT = "Impediment";
    public const string INNER_KNIFE = "Inner_Knife";
    public const string FREE_KNIFE = "Free_Knife";
}

public class Tags
{
    public const string PLAYER = "Player";
    public const string ENEMY = "ENEMY";

    public const string IMPEDIMENT = "Impediment";
    public const string INNER_KNIFE = "InnerKnife";
    public const string FREE_KNIFE = "Free_Knife";
    public const string MAGNET = "Magnet";
    public const string PROTECT = "Protect";
    public const string SLOW_BLADE = "SlowBlade";
    public const string SLOW_AREA = "SlowArea";
}

public class Direction
{
    public static Vector3 N = Vector2.up;
    public static Vector3 NE = Vector2.one.normalized;
    public static Vector3 E = Vector2.right;
    public static Vector3 SE = new Vector2(1, -1).normalized;
    public static Vector3 S = new Vector2(0, -1);
    public static Vector3 SW = new Vector2(-1, -1).normalized;
    public static Vector3 W = new Vector2(-1, 0).normalized;
    public static Vector3 NW = new Vector2(-1, 1).normalized;
}

public class MapConfig
{
    public const float MIN_SCALE_SIZE = 0.8f;
    public const float MAX_SCALE_SIZE = 1.4f;
    public const float MAX_DISTANCE_MOVE = 3f;
    public const float MOVE_SPEED = 2f;
    public const float SCALE_SPEED = 2f;
}

public class EventGameCode
{
    public const byte SpawnKnifeCode = 1;
    public const byte InitController = 2;
    public const byte OnAddknife = 3;
    public const byte OnRemoveKnife = 4;
    public const byte OnSetUpMap = 5;
    public const byte OnSetUpPlayerForController = 6;
    public const byte OnAddKnifeFromMask = 7;
    public const byte OnRemoveKnifeFromDefense = 8;
    public const byte OnAnimation = 9;
    public const string TeamBattle = "Team";

}

public class DataKey
{
    public const string PlayerCoin = "PlayerCoin";
    public const string PlayerGem = "PlayerGem";
    public const string PlayerConfig = "PlayerConfig";
    public const string GameConfig = "GameConfig";
    public const string MissionInfo = "MissionInfo";
    public const string MissionLevel = "MissionLevel";
}