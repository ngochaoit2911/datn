﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxExist : PoolItem
{
    public float timeExist;
    private void OnEnable()
    {
        Invoke(nameof(Disable),timeExist);
    }

    void Disable()
    {
        gameObject.Hide();
    }
}
