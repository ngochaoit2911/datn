﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using DG.Tweening;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public static class GameHelper
{
    public static IEnumerator EndOfFrame(Action callBack, Action onComplete = null)
    {
        yield return new WaitForEndOfFrame();
        callBack?.Invoke();
        onComplete?.Invoke();
    }

    public static IEnumerator WaitForSecond(Action callBack, float second)
    {
        yield return new WaitForSeconds(second);
        callBack?.Invoke();
    }

    public static Vector3 GetRandomPosInArea(Vector3 area)
    {
        return new Vector3(Random.Range(-area.x, area.x), Random.Range(-area.y, area.y));
    }

    public static int GetIndexAfterAdd(int lastIndex, int startAddIndex)
    {
        if (lastIndex >= startAddIndex)
        {
            return lastIndex + 1;
        }
        else
        {
            return lastIndex;
        }
    }

    public static int GetIndexAfterRemove(int lastIndex, int startIndex)
    {
        if (startIndex < lastIndex)
        {
            return lastIndex - 1;
        }
        else
        {
            return lastIndex;
        }
    }

    public static KnifeController GetNearestController(List<KnifeController> controllers, Vector3 t)
    {
       
        var min = float.MaxValue;
        var count = 0;
        var index = 0;
        foreach (var controller in controllers)
        {
            var d = Vector3.Distance(t, controller.m_transform.position);
            if (d < min)
                index = count;
            count++;
        }

        return controllers[index];
    }

    public static Net_KnifeController GetNearestController(List<Net_KnifeController> controllers, Vector3 t, int teamId)
    {

        var min = float.MaxValue;
        var count = 0;
        var index = 0;
        foreach (var controller in controllers)
        {
            if (controller.heroInfor.teamId != teamId)
            {
                var d = Vector3.Distance(t, controller.m_transform.position);
                if (d < min)
                    index = count;
            }
            count++;
        }
        return controllers[index];
    }

    public static void DebugDrawBox(Vector2 point, Vector2 size, float angle, Color color, float duration)
    {

        var orientation = Quaternion.Euler(0, 0, angle);

        // Basis vectors, half the size in each direction from the center.
        Vector2 right = orientation * Vector2.right * size.x / 2f;
        Vector2 up = orientation * Vector2.up * size.y / 2f;

        // Four box corners.
        var topLeft = point + up - right;
        var topRight = point + up + right;
        var bottomRight = point - up + right;
        var bottomLeft = point - up - right;

        // Now we've reduced the problem to drawing lines.
        Debug.DrawLine(topLeft, topRight, color, duration);
        Debug.DrawLine(topRight, bottomRight, color, duration);
        Debug.DrawLine(bottomRight, bottomLeft, color, duration);
        Debug.DrawLine(bottomLeft, topLeft, color, duration);
    }

    public static Vector2 GetDirectionFromAngle(float angle)
    {
        return new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
    }

    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static void ChangePosX(this Transform t, float x)
    {
        var p = t.position;
        p.x = x;
        t.position = p;
    }

    public static void ChangePosY(this RectTransform t, float y)
    {
        var p = t.anchoredPosition;
        p.y = y;
        t.anchoredPosition = p;
    }

    public static readonly Dictionary<SystemLanguage, string> COUTRY_CODES = new Dictionary<SystemLanguage, string>()
        {
            { SystemLanguage.Afrikaans, "ZA" },
            { SystemLanguage.Arabic    , "SA" },
            { SystemLanguage.Basque    , "US" },
            { SystemLanguage.Belarusian    , "BY" },
            { SystemLanguage.Bulgarian    , "BJ" },
            { SystemLanguage.Catalan    , "ES" },
            { SystemLanguage.Chinese    , "CN" },
            { SystemLanguage.Czech    , "HK" },
            { SystemLanguage.Danish    , "DK" },
            { SystemLanguage.Dutch    , "BE" },
            { SystemLanguage.English    , "US" },
            { SystemLanguage.Estonian    , "EE" },
            { SystemLanguage.Faroese    , "FU" },
            { SystemLanguage.Finnish    , "FI" },
            { SystemLanguage.French    , "FR" },
            { SystemLanguage.German    , "DE" },
            { SystemLanguage.Greek    , "JR" },
            { SystemLanguage.Hebrew    , "IL" },
            { SystemLanguage.Icelandic    , "IS" },
            { SystemLanguage.Indonesian    , "ID" },
            { SystemLanguage.Italian    , "IT" },
            { SystemLanguage.Japanese    , "JP" },
            { SystemLanguage.Korean    , "KR" },
            { SystemLanguage.Latvian    , "LV" },
            { SystemLanguage.Lithuanian    , "LT" },
            { SystemLanguage.Norwegian    , "NO" },
            { SystemLanguage.Polish    , "PL" },
            { SystemLanguage.Portuguese    , "PT" },
            { SystemLanguage.Romanian    , "RO" },
            { SystemLanguage.Russian    , "RU" },
            { SystemLanguage.SerboCroatian    , "SP" },
            { SystemLanguage.Slovak    , "SK" },
            { SystemLanguage.Slovenian    , "SI" },
            { SystemLanguage.Spanish    , "ES" },
            { SystemLanguage.Swedish    , "SE" },
            { SystemLanguage.Thai    , "TH" },
            { SystemLanguage.Turkish    , "TR" },
            { SystemLanguage.Ukrainian    , "UA" },
            { SystemLanguage.Vietnamese    , "VN" },
            { SystemLanguage.ChineseSimplified    , "CN" },
            { SystemLanguage.ChineseTraditional    , "CN" },
            { SystemLanguage.Unknown    , "Unknown" },
            { SystemLanguage.Hungarian    , "HU" },
        };

    /// <summary>
    /// Returns approximate country code of the language.
    /// </summary>
    /// <returns>Approximated country code.</returns>
    /// <param name="language">Language which should be converted to country code.</param>
    public static string ToCountryCode(this SystemLanguage language)
    {
        string result;
        if (COUTRY_CODES.TryGetValue(language, out result))
        {
            return result;
        }
        else
        {
            return COUTRY_CODES[SystemLanguage.Unknown];
        }
    }

    public static bool CheckXStraight(Vector2 p1, Vector2 p2, float x, float y1, float y2)
    {
        if (x > p1.x && x > p2.x)
            return false;
        if (x < p1.x && x < p2.x)
            return false;
        var a = (p2.y - p1.y) / (p2.x - p1.x);
        var b = p1.y - p1.x * a;

        var y = x * a + b;
        if ((y > y1 && y > y2) || (y < y1 && y < y2))
            return false;
        return true;
    }

    public static bool CheckYStraight(Vector2 p1, Vector2 p2, float y, float x1, float x2)
    {

        if (y > p1.y && y > p2.y)
            return false;
        if (y < p1.y && y < p2.y)
            return false;
        var a = (p2.y - p1.y) / (p2.x - p1.x);
        var b = p1.y - p1.x * a;

        var x = (y - b) / a;

        if ((x > x1 && x > x2) || (x < x1 && x < x2))

            return false;
        return true;
    }

    public static bool CheckStraight(Vector2 p1, Vector2 p2, Vector2 m1, Vector2 m2)
    {
        var d = Vector2.Distance(m1, m2);
        var d1 = GetDistanceToStraight(p1, p2, m1);
        var d2 = GetDistanceToStraight(p1, p2, m2);

        if (d > d1 && d > d2)
            return true;
        return false;
    }

    public static Vector2 GetPosX(Vector2 p1, Vector2 p2, float x)
    {
        if (Mathf.Abs(p2.x - p1.x) > 0)
        {
            var a = (p2.y - p1.y) / (p2.x - p1.x);
            var b = p1.y - p1.x * a;

            var y = x * a + b;
            return new Vector2(x, y);
        }
        else
        {
            return new Vector2(x, 50);
        }

    }

    public static Vector2 GetPosY(Vector2 p1, Vector2 p2, float y)
    {
        if (Mathf.Abs(p2.x - p1.x) > 0)
        {
            var a = (p2.y - p1.y) / (p2.x - p1.x);
            var b = p1.y - p1.x * a;

            if (a != 0)
            {
                var x = (y - b) / a;
                return new Vector2(x, y);
            }
            else
            {
                return new Vector2(-50, y);
            }
        }
        else
        {
            return new Vector2(-50, y);
        }
    }

    public static float GetDistanceToStraight(Vector2 p1, Vector2 p2, Vector2 p)
    {
        var d = p2 - p1;
        var a = -d.y;
        var b = d.x;
        var c = (p1.x * d.y - d.x * p1.y);
        return (Mathf.Abs(a * p1.x + b * p1.y + c) / (Mathf.Sqrt(a * a + b * b)));
    }

    public static bool IsInner(Vector2 m, Vector2 topLeft, Vector2 topRight, Vector2 botLeft, Vector2 botRight)
    {
        if (m.x < topLeft.x || m.x > botRight.x)
        {
            return false;
        }

        if (m.y > topLeft.y || m.y < botRight.y)
        {
            return false;
        }

        return true;
    }

    public static bool IsInner(Vector2 m, Vector2 p, Vector2 size)
    {
        if (m.x > p.x + size.x || m.x < p.x - size.x)
            return false;
        if (m.y > p.y + size.y || m.y < p.y - size.y)
            return false;
        return true;
    }

    public static Vector2 GetPosOuterBox(Bounds boundBox, Bounds obj, Vector2 boxPos, Vector2 pos)
    {
        return (Vector2)boundBox.ClosestPoint(pos) + (pos - boxPos).normalized;
    }


#if UNITY_EDITOR

    public static List<T> GetAllAssetAtPath<T>(string filter, string path)
    {
        string[] findAssets = UnityEditor.AssetDatabase.FindAssets(filter, new[] { path });
        List<T> os = new List<T>();
        foreach (var findAsset in findAssets)
        {
            os.Add((T)Convert.ChangeType(UnityEditor.AssetDatabase.LoadAssetAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(findAsset), typeof(T)), typeof(T)));
        }
        return os;
    }

    public static void PingObj(string path)
    {
        var obj = UnityEditor.AssetDatabase.LoadAssetAtPath<Object>(path);
        UnityEditor.Selection.activeObject = obj;
        UnityEditor.EditorGUIUtility.PingObject(obj);
    }


#endif

}

