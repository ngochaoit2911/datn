﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObj : MonoBehaviour
{
    private Transform t;
    private KnifeController controller;

    void Start()
    {
        t = transform;
        controller = GetComponentInParent<KnifeController>();
    }

    // Update is called once per frame
    void Update()
    {
        t.Rotate(Vector3.back * controller.speed * Time.deltaTime, Space.World);
    }
}
