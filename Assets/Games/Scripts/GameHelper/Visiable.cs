﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Visiable : MonoBehaviour
{
    public Renderer render;

    private void OnBecameInvisible()
    {
        render.enabled = false;
    }

    private void OnBecameVisible()
    {
        render.enabled = true;
    }
}
