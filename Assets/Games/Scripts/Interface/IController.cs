﻿
public interface IController
{
    void OnRemoveKnifeFromImpediment();
    void OnRemoveKnifeFromE();
    void OnAddKnife();
    void UpSpeed(float addSpeed);
    void DescreaseSpeed(float downSpeed);

    void RestoreSpeedAfterDescrease(float downSpeed);
    void RestoreSpeedAfterUp(float addSpeed);

    void SetUpSpeed(float speed);

    void SetCanMove();

    bool IsOnCamera();

}

