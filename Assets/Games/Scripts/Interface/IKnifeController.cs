﻿using UnityEngine;
using System.Collections;
using static KnifeController;

public interface IKnifeController
{

    void OnInstance(HeroInfor info);
    void SwitchToDefenseState();
    void SwitchToAttackState();
    void SpawnKinfe(int index);
    void SpawnKnifeFromMask(int index);
    void AddKnife(int startIndex);
    void SpawnKnife(Iknife k, int index);
    void AddKnife(Iknife k, int index);

}