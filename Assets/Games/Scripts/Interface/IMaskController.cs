﻿using System.Collections;
using UnityEngine;

namespace Assets.Games.Scripts.Interface
{
    public interface IMaskController
    {

        void BuffSpeed(float time, float value);
        void BuffBladeSpeed(float time, float value);
        void BuffMagnet(float time, float size);
        void BuffProtected(float time);

        bool IsPlayer();
    }
}