﻿using UnityEngine;
using System.Collections;

public interface Iknife
{
    void OnInstance(IKnifeController controller, int index, KnifeType type);
    void SetParent(Transform parent);

}
