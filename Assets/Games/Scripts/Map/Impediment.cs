﻿using Assets.Games.Photon.Scripts.Controller;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Impediment : PoolItem
{
    private Dictionary<ImpedimentType, Action> onExcutions;
    private ImpedimentType curType;

    //Scale
    private bool isScaleSmall;

    //MoveHorizontal
    private bool isMoveLeft;
    private float minPosX, maxPosX;

    //MoveVertical
    private bool isMoveDown;
    private float minPosY, maxPosY;

    private void Start()
    {
        onExcutions = new Dictionary<ImpedimentType, Action>
        {
            {ImpedimentType.Idle, OnIdle},
            {ImpedimentType.MoveLoopHorizontal, OnMoveHorizontal},
            {ImpedimentType.MoveLoopVertical, OnMoveVertical},
            {ImpedimentType.Scale, OnScale}
        };
    }

    public void OnInstance()
    {
        var x = Random.Range(0.7f, 1.1f);
        m_transform.localScale = Vector3.one * x;
        var r = Random.value;
        if (r < 0.5f)
        {
            curType = ImpedimentType.Idle;
        }
        else if (r < 0.65f)
        {
            isMoveLeft = true;
            minPosX = m_transform.position.x - MapConfig.MAX_DISTANCE_MOVE;
            maxPosX = m_transform.position.x + MapConfig.MAX_DISTANCE_MOVE;
            curType = ImpedimentType.MoveLoopHorizontal;
        }
        else if (r < 0.8f)
        {
            isMoveDown = true;
            minPosY = m_transform.position.y - MapConfig.MAX_DISTANCE_MOVE;
            maxPosY = m_transform.position.y + MapConfig.MAX_DISTANCE_MOVE;
            curType = ImpedimentType.MoveLoopVertical;
        }
        else
        {
            isScaleSmall = true;
            curType = ImpedimentType.Scale;
        }
    }

    public void Net_OnInstance()
    {
        var x = Random.Range(0.7f, 1.1f);
        var r = Random.value;
        GetComponent<PhotonView>().RPC(nameof(RPCInstance), RpcTarget.All, x, r);
    }

    [PunRPC]
    public void RPCInstance(float scale, float r)
    {
        m_transform.localScale = Vector3.one * scale;
        if (r < 0.5f)
        {
            curType = ImpedimentType.Idle;
        }
        else if (r < 0.65f)
        {
            isMoveLeft = true;
            minPosX = m_transform.position.x - MapConfig.MAX_DISTANCE_MOVE;
            maxPosX = m_transform.position.x + MapConfig.MAX_DISTANCE_MOVE;
            curType = ImpedimentType.MoveLoopHorizontal;
        }
        else if (r < 0.8f)
        {
            isMoveDown = true;
            minPosY = m_transform.position.y - MapConfig.MAX_DISTANCE_MOVE;
            maxPosY = m_transform.position.y + MapConfig.MAX_DISTANCE_MOVE;
            curType = ImpedimentType.MoveLoopVertical;
        }
        else
        {
            isScaleSmall = true;
            curType = ImpedimentType.Scale;
        }
    }

    private void Update()
    {
        if(!GameController.IsPauseGame || !Net_Controller.IsPauseGame)
            onExcutions[curType].Invoke();
    }

    private void OnScale()
    {
        if (isScaleSmall)
        {
            m_transform.localScale -= Vector3.one * Time.deltaTime * MapConfig.SCALE_SPEED;
            if (m_transform.localScale.x < MapConfig.MIN_SCALE_SIZE)
            {
                isScaleSmall = false;
            }
        }
        else
        {
            m_transform.localScale += Vector3.one * Time.deltaTime * MapConfig.SCALE_SPEED;
            if (m_transform.localScale.x > MapConfig.MAX_SCALE_SIZE)
            {
                isScaleSmall = true;
            }
        }
    }

    private void OnMoveHorizontal()
    {
        if (isMoveLeft)
        {
            m_transform.position += Vector3.left * Time.deltaTime * MapConfig.MOVE_SPEED;
            if (m_transform.position.x < minPosX)
            {
                isMoveLeft = false;
            }
        }
        else
        {
            m_transform.position += Vector3.right * Time.deltaTime * MapConfig.MOVE_SPEED;
            if (m_transform.position.x > maxPosX)
            {
                isMoveLeft = true;
            }
        }
    }

    private void OnMoveVertical()
    {
        if (isMoveDown)
        {
            m_transform.position += Vector3.down * Time.deltaTime * MapConfig.MOVE_SPEED;
            if (m_transform.position.y < minPosY)
            {
                isMoveDown = false;
            }
        }
        else
        {
            m_transform.position += Vector3.up * Time.deltaTime * MapConfig.MOVE_SPEED;
            if (m_transform.position.y > maxPosY)
            {
                isMoveDown = true;
            }
        }
    }

    private void OnIdle()
    {
        // Donothing
    }
}

public enum ImpedimentType
{
    Idle,
    MoveLoopHorizontal,
    MoveLoopVertical,
    Scale
}