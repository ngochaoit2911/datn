﻿using Assets.Games.Photon.Scripts.Controller;
using Assets.Games.Photon.Scripts.Data;
using Assets.Games.Photon.Scripts.Game;
using DG.Tweening;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapStage : PoolItem
{
    public MapData map;
    public List<Transform> impedimentPos;
    public List<Transform> spawnCharacterPos;
    private int knifeSpawnCount;
    public float minSpawnItem1 = 6f, maxSpawnItem1 = 9f;

    public void OnSpawn(List<HeroInfor> heroes)
    {
        GameController.ins.activeControllers.Clear();
        knifeSpawnCount = 0;
        SpawnImpediment();
        SpawnCharacters(heroes);
        MapController.ins.SetUpBounding(map.size);
        SpawnMultiKnife(map.knifeMax / 2);
        knifeSpawnCount = map.knifeMax / 2;
    }

    public void OnSpawnNetHeroes(List<HeroInfor> heroes)
    {
        Net_Controller.ins.activeControllers.Clear();
        knifeSpawnCount = 0;
        Net_SpawnImpediment();
        Net_SpawnCharacters(heroes);
        Net_SpawnMultiKnife(map.knifeMax / 2);
        knifeSpawnCount = map.knifeMax / 2;

        Net_MapController.ins.SetUpBounding(map.size);
    }

    public void SpawnImpediment()
    {
        foreach (var pos in impedimentPos)
        {
            var id = Random.Range(0, MapController.ins.impediments.Count);
            var im = Poolers.ins.GetObject(MapController.ins.impediments[id], pos.position, Quaternion.identity);
            im.Cast<Impediment>().OnInstance();
        }
    }

    public void Net_SpawnImpediment()
    {
        foreach (var pos in impedimentPos)
        {
            var id = Random.Range(0, Net_MapController.ins.impediments.Count);
            //var im = Poolers.ins.GetObject(Net_MapController.ins.impediments[id], pos.position, Quaternion.identity);
            var im = MasterManager.InstantiateScenceNetworkGo(Net_MapController.ins.impediments[id], pos.position, Quaternion.identity)
                .GetComponent<Impediment>();
            im.Net_OnInstance();
        }
    }

    public void SpawnCharacters(List<HeroInfor> heroes)
    {
        spawnCharacterPos.Shuffle();
        var b = true;
        var count = 0;
        var p = heroes.Find(s => s.isPlayer);
        var es = heroes.Where(s => !s.isPlayer).ToList();
        foreach (var pos in spawnCharacterPos)
        {
            if (b)
            {
                b = false;
                SpawnPlayer(pos.position, p);
            }
            else
            {
                SpawnEnemy(pos.position, es[count]);
                count++;
            }
        }
    }

    public void Net_SpawnCharacters(List<HeroInfor> heroes)
    {
        spawnCharacterPos.Shuffle();
        for (int i = 0; i < heroes.Count; i++)
        {
            if (heroes[i].isAI)
            {
                Net_SpawnEnemy(spawnCharacterPos[i].position, heroes[i]);
            }
            else
            {
                Net_SpawnPlayer(spawnCharacterPos[i].position, heroes[i]);
            }
        }
        Net_MapController.ins.SetUpPlayer();
    }

    public void SpawnPlayer(Vector3 pos, HeroInfor heroInfor)
    {
        var p = Poolers.ins.GetObject(MapController.ins.player, pos, Quaternion.identity);
        p.Cast<KnifeController>().OnInstance(heroInfor);
        GameController.ins.activeControllers.Add(p.Cast<KnifeController>());
        GameController.ins.player = p.Cast<KnifeController>();
        MapController.ins.SetCameraFollower(p.m_transform);
    }

    public void Net_SpawnPlayer(Vector3 pos, HeroInfor heroInfo)
    {
        var p = MasterManager.InstantiateScenceNetworkGo(Net_MapController.ins.player, pos, Quaternion.identity).GetComponent<Net_KnifeController>();

        p.OnInstance(heroInfo);
        Net_Controller.ins.activeControllers.Add(p);
        if (heroInfo.isPlayer)
        {
            Net_MapController.ins.SetCameraFollower(p.m_transform);
            Net_Controller.ins.player = p;
        }
        p.photonView.TransferOwnership(GetPlayer(heroInfo.heroName));
    }

    private Player GetPlayer(string nickName)
    {
        foreach (var item in PhotonNetwork.CurrentRoom.Players.Values)
        {
            if(item.NickName == nickName)
            {
                return item;
            }
        }
        return PhotonNetwork.LocalPlayer;
    }

    public void SpawnEnemy(Vector3 pos, HeroInfor heroInfor)
    {
        Debug.Log(heroInfor.maskType + " " + heroInfor.heroName);

        var p = Poolers.ins.GetObject(MapController.ins.enemy, pos, Quaternion.identity);
        p.Cast<KnifeController>().OnInstance(heroInfor);
        var x = Random.Range(0, GameController.ins.configSets.Count);
        GameController.ins.activeControllers.Add(p.Cast<KnifeController>());
        p.GetComponent<EnemyAI>().Init(GameController.ins.configSets[x], GameController.ins.rankAIData[Mathf.Clamp((int)heroInfor.rankType / 3, 0, 10)]);
    }

    public void Net_SpawnEnemy(Vector3 pos, HeroInfor heroInfor)
    {
        Debug.Log(heroInfor.maskType + " " + heroInfor.heroName);

        var p = MasterManager.InstantiateScenceNetworkGo(Net_MapController.ins.enemy, pos, Quaternion.identity).GetComponent<Net_KnifeController>();
        p.OnInstance(heroInfor);
        var x = Random.Range(0, Net_Controller.ins.configSets.Count);
        var r = Mathf.Clamp((int)heroInfor.rankType / 3, 0, 10);
        Net_Controller.ins.activeControllers.Add(p);

        p.GetComponent<Net_EnemyAI>().Init(x, r);
    }

    public void SpawnKnife(Vector3 pos, Quaternion rot)
    {
        var k = Poolers.ins.GetObject(MapController.ins.knife, pos, rot);
        k.m_transform.SetParent(null);
        var x = Random.Range(0, GameController.ins.knifeData.knifeData.Count);
        k.Cast<Knife>().OnInstance((KnifeType)x);
    }

    public void Net_SpawnKnife(Vector3 pos, Quaternion rot)
    {
        var k = MasterManager.InstantiateScenceNetworkGo(Net_MapController.ins.knife, pos, rot).GetComponent<Net_Knife>();
        var x = Random.Range(0, Net_Controller.ins.knifeData.knifeData.Count);
        k.Cast<Net_Knife>().OnInstance((KnifeType)x);
    }

    public void SpawnMultiKnife(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var pos = GameHelper.GetRandomPosInArea(map.size);
            var angle = Random.Range(0, 360);
            SpawnKnife(pos, Quaternion.Euler(0, 0, angle));
        }
    }

    public void Net_SpawnMultiKnife(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var pos = GameHelper.GetRandomPosInArea(map.size);
            var angle = Random.Range(0, 360);
            Net_SpawnKnife(pos, Quaternion.Euler(0, 0, angle));
        }
    }

    private Coroutine knifeCT;
    public void SpawnKnifePerSecond()
    {
        if (knifeCT != null)
        {
            StopCoroutine(knifeCT);
        }

        knifeCT = StartCoroutine(ISpawnKnife());
    }

    public void Net_SpawnKnifePerSecond()
    {
        if (knifeCT != null)
        {
            StopCoroutine(knifeCT);
        }

        knifeCT = StartCoroutine(Net_ISpawnKnife());
    }

    private IEnumerator ISpawnKnife()
    {
        var wt = new WaitForSeconds(1);
        while (true)
        {
            yield return wt;
            if (knifeSpawnCount <= map.knifeMax - map.spawnKnifePerSecond)
            {
                SpawnMultiKnife(map.spawnKnifePerSecond);
                knifeSpawnCount += map.spawnKnifePerSecond;
            }
            else if (knifeSpawnCount < map.knifeMax && knifeSpawnCount > map.knifeMax - map.spawnKnifePerSecond)
            {
                SpawnMultiKnife(map.knifeMax - knifeSpawnCount);
                knifeSpawnCount = map.knifeMax;
            }
            else
            {
                yield break;
            }
        }
    }

    private IEnumerator Net_ISpawnKnife()
    {
        var wt = new WaitForSeconds(1);
        while (true)
        {
            yield return wt;
            if (knifeSpawnCount <= map.knifeMax - map.spawnKnifePerSecond)
            {
                Net_SpawnMultiKnife(map.spawnKnifePerSecond);
                knifeSpawnCount += map.spawnKnifePerSecond;
            }
            else if (knifeSpawnCount < map.knifeMax && knifeSpawnCount > map.knifeMax - map.spawnKnifePerSecond)
            {
                Net_SpawnMultiKnife(map.knifeMax - knifeSpawnCount);
                knifeSpawnCount = map.knifeMax;
            }
            else
            {
                yield break;
            }
        }
    }


    private Coroutine itemCT;

    public void SpawnItemInGame()
    {
        if (itemCT != null)
        {
            StopCoroutine(itemCT);
        }

        itemCT = StartCoroutine(ISpawnItem());
    }

    public void Net_SpawnItemInGame()
    {
        if (itemCT != null)
        {
            StopCoroutine(itemCT);
        }

        itemCT = StartCoroutine(Net_ISpawnItem());
    }

    private IEnumerator ISpawnItem()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minSpawnItem1, maxSpawnItem1));
            var x = Random.Range(0, 2);
            SpawnMultiItem(x);
        }
    }

    private IEnumerator Net_ISpawnItem()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minSpawnItem1, maxSpawnItem1));
            var x = Random.Range(0, 2);
            Net_SpawnMultiItem(x);
        }
    }

    void SpawnMultiItem(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var pos = GameHelper.GetRandomPosInArea(map.size);
            SpawnItem(pos);
        }
    }

    void Net_SpawnMultiItem(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var pos = GameHelper.GetRandomPosInArea(map.size);
            Net_SpawnItem(pos);
        }
    }

    private void SpawnItem(Vector3 pos)
    {
        var k = Poolers.ins.GetObject(MapController.ins.ingameItem, pos, Quaternion.identity);
        k.m_transform.SetParent(null);
        var x = Random.Range(0, GameController.ins.boosterData.boosterData.Count);
        k.Cast<IngameItem>().SetUp((ItemBuffType)x);
    }
    private void Net_SpawnItem(Vector3 pos)
    {
        var k = MasterManager.InstantiateScenceNetworkGo(Net_MapController.ins.ingameItem, pos, Quaternion.identity).GetComponent<IngameItem>();
        var x = Random.Range(0, Net_Controller.ins.boosterData.boosterData.Count);
        k.Net_SetUp(x);
    }
}
