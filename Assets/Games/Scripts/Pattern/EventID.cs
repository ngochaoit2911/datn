﻿

public enum EventID
{
    None = 0,

    #region Mission

    Top1,
    DoubleKill,
    TripleKill,
    Quadrakill,
    Pentakill,
    HexaKill,
    HasKnife,
    PlayMatch,
    UnlockNewKnife,
    UnlockNewHero,
    UpRank,
    KnockKnife

    #endregion

}