﻿using Assets.Games.Photon.Scripts.Controller;
using Assets.Games.Scripts.Interface;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameItem : PoolItem
{
    public SpriteRenderer sr;
    public ItemBuffType type;
    public float timeExist;
    public float value;

    public void SetUp(ItemBuffType buff)
    {
        type = buff;
        var s = GameController.ins.boosterData.boosterData[(int)type];
        sr.sprite = s.icon;
        timeExist = s.time;
        value = s.value;
    }

    public void Net_SetUp(int t)
    {
        GetComponent<PhotonView>().RPC(nameof(RPCSetUp), RpcTarget.All, t);
    }

    [PunRPC]
    public void RPCSetUp(int t)
    {
        var buff = (ItemBuffType)t;
        type = buff;
        var s = Net_Controller.ins.boosterData.boosterData[(int)type];
        sr.sprite = s.icon;
        timeExist = s.time;
        value = s.value;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.PLAYER))
        {
            var controller = other.GetComponent<IMaskController>();
            switch (type)
            {
                case ItemBuffType.Magnet:
                    controller.BuffMagnet(timeExist, value);
                    break;
                case ItemBuffType.Shield:
                    controller.BuffProtected(timeExist);
                    break;
                case ItemBuffType.SpeedUp:
                    controller.BuffSpeed(timeExist, value);
                    break;
                case ItemBuffType.BladeSpeedUp:
                    controller.BuffBladeSpeed(timeExist, value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (controller.IsPlayer())
            {
                AudioManager.ins.PlayItemBuff(type);
            }
            this.Hide();
        }
    }
}

public enum ItemBuffType
{
    Magnet, Shield, SpeedUp, BladeSpeedUp
}
