﻿using System.Collections;
using System.Collections.Generic;
using Assets.Games.Photon.Scripts.Controller;
using DG.Tweening;
using UnityEngine;

public class Knife : PoolItem
{
    public KnifeType knifeType;
    public SpriteRenderer render;
    public Transform childModel;
    public Rigidbody2D r2;

    public bool isFreeKnife, isKnifeOnSkill, isOnRemove;
    public float force, torque;

    //private Transform target;
    private KnifeController controller;
    public int knifeIndex;
    private Tween moveOnMagnet, skillTween;
    private KnifeInfor knifeInfor;
    public Collider2D col1, col2;

    public void OnInstance(KnifeController controller, int knifeIndex, KnifeType type)
    {
        knifeType = type;
        knifeInfor = GameController.ins.knifeData.knifeData[(int) type];
        render.sprite = knifeInfor.icon;
        isFreeKnife = false;
        isKnifeOnSkill = false;
        this.knifeIndex = knifeIndex;
        this.controller = controller;
        //target = controller.transform;
        render.sortingOrder = 100 - knifeIndex;
        m_transform.tag = Tags.INNER_KNIFE;
        m_Obj.layer = LayerMask.NameToLayer(LayerNames.INNER_KNIFE);
        childModel.localRotation = Quaternion.identity;
        col1.enabled = false;
        col2.enabled = true;
        if (knifeInfor.isRotate)
        {
            InvokeRepeating(nameof(RotateChild), 0, Time.deltaTime);
        }
        else
        {
            CancelInvoke(nameof(RotateChild));
        }
    }

    public void OnInstanceOnSkill(KnifeController controller, int knifeIndex, int total, float startAngle,
        KnifeType type)
    {
        col1.enabled = false;
        col2.enabled = true;
        childModel.localRotation = Quaternion.identity;
        CancelInvoke(nameof(RotateChild));
        skillTween?.Kill();
        knifeType = type;
        render.sprite = GameController.ins.knifeData.knifeData[(int) type].icon;
        isFreeKnife = false;
        isKnifeOnSkill = true;
        this.controller = controller;
        m_transform.tag = Tags.INNER_KNIFE;
        m_Obj.layer = LayerMask.NameToLayer(LayerNames.INNER_KNIFE);
        GetPosOnSpawnSkill(knifeIndex, total, startAngle);
        m_transform.right = (m_transform.position - controller.m_transform.position).normalized;
        r2.velocity = m_transform.right * Config.BASE_KNIFE_SKILL_MOVE_SPEED;
        skillTween = DOVirtual.DelayedCall(3, m_Obj.Hide);

    }

    public void OnInstance(KnifeType type)
    {
        col1.enabled = true;
        col2.enabled = false;
        childModel.localRotation = Quaternion.identity;
        CancelInvoke(nameof(RotateChild));
        knifeType = type;
        render.sprite = GameController.ins.knifeData.knifeData[(int)type].icon;
        isKnifeOnSkill = false;
        isFreeKnife = true;
        isOnRemove = false;
        m_transform.tag = Tags.FREE_KNIFE;
        m_Obj.layer = LayerMask.NameToLayer(LayerNames.FREE_KNIFE);
    }

    public void OnInstanceEnemyKnife()
    {
        isFreeKnife = true;
        m_transform.tag = Tags.INNER_KNIFE;
        m_Obj.layer = LayerMask.NameToLayer(LayerNames.INNER_KNIFE);
    }

    public void MoveOnSpawn()
    {
        if (controller.countForCalculation != 0)
        {

            var x = Mathf.Sin(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                    controller.radius;
            var y = Mathf.Cos(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                    controller.radius;
            m_transform.localPosition = new Vector3(x, y);
        }
    }

    public void GetPosOnSpawnSkill(int index, int total, float startAngle)
    {
        var x = Mathf.Sin(startAngle * Mathf.Deg2Rad + index * Mathf.PI / total * 2) *
                0.2f;
        var y = Mathf.Cos(startAngle * Mathf.Deg2Rad + index * Mathf.PI / total * 2) *
                0.2f;
        m_transform.position = new Vector3(x, y) + controller.m_transform.position;
    }

    public Vector3 GetNextPosOnDefenseMode()
    {
        var x = Mathf.Sin(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                Config.BASE_DEFENSE_RADIUS;
        var y = Mathf.Cos(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                Config.BASE_DEFENSE_RADIUS;
        return new Vector3(x, y);
    }

    public Vector3 GetNextPos()
    {
        if (controller.countForCalculation > 0.01f)
        {
            var x = Mathf.Sin(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                    controller.radius;
            var y = Mathf.Cos(knifeIndex * Mathf.PI / controller.countForCalculation * 2) *
                    controller.radius;
            return new Vector3(x, y);
        }

        return m_transform.localPosition;
    }

    private Tween waitTween;
    public void MoveOnRemove()
    {
        childModel.localRotation = Quaternion.identity;
        CancelInvoke(nameof(RotateChild));
        isOnRemove = true;
        isFreeKnife = true;
        isKnifeOnSkill = false;
        m_transform.tag = Tags.FREE_KNIFE;
        m_Obj.layer = LayerMask.NameToLayer(LayerNames.FREE_KNIFE);
        m_transform.SetParent(null);
        r2.isKinematic = false;
        r2.AddTorque(Random.Range(torque * 0.7f, torque * 2));
        r2.AddForce((transform.right + transform.up * Random.Range(-1f, 1)).normalized * Random.Range(force * 0.7f, force * 1.2f));
        waitTween = DOVirtual.DelayedCall(1, () =>
        {
            //gameObject.Hide();
            r2.velocity = Vector2.zero;
            r2.angularVelocity = 0;
            isOnRemove = false;
            r2.isKinematic = true;
            col1.enabled = true;
            col2.enabled = false;
        });
    }

    public void RotateChild()
    {
        childModel.Rotate(Vector3.forward * Config.SPEED_ROTATE_CHILD * Time.deltaTime);
    }

    public void SetY(float y)
    {
        childModel.localScale = new Vector3(1, y, 1) * 0.35f;
    }

    private Coroutine ct;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (isOnRemove || isFreeKnife)
        {
            if (col.CompareTag(Tags.IMPEDIMENT))
            {                
                //gameObject.Hide();
                r2.velocity = Vector2.zero;
                r2.angularVelocity = 0;
                isOnRemove = false;
                col1.enabled = true;
                col2.enabled = false;
                m_transform.position = GameHelper.GetPosOuterBox(col.bounds, col2.bounds, col.transform.position,
                    m_transform.position);
                return;
            }
           
        }

        if (!GameController.IsPauseGame)
        {

            if (!isFreeKnife)
            {
                if (!isKnifeOnSkill)
                {
                    if (col.CompareTag(Tags.FREE_KNIFE))
                    {
                        var knife = col.GetComponent<Knife>();
                        if (!knife.isOnRemove)
                        {
                            if (controller.state == KnifeState.Attack && controller.knifeCount < Config.MAX_KNIFE)
                            {
                                if (controller.isCanChange)
                                {
                                    if (!controller.IsOnAdd(knife.GetInstanceID()))
                                    {
                                        moveOnMagnet?.Kill();
                                        //col.Hide();
                                        controller.AddID(knife.GetInstanceID());
                                        controller.AddKnife(knife, knifeIndex);
                                        if (controller.isPlayer)
                                        {
                                            AudioManager.ins.PlayAudioClip(ClipType.acl);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (col.CompareTag(Tags.INNER_KNIFE))
                {
                    if (isKnifeOnSkill)
                    {
                        var m = col.GetComponent<Knife>();
                        if (!m.isOnRemove)
                        {
                            if (m.controller.GetInstanceID() != controller.GetInstanceID())
                            {
                                gameObject.Hide();
                                Poolers.ins.GetObject(GameController.ins.hitEffect, m_transform.position,
                                    Quaternion.identity);
                                skillTween?.Kill();
                            }
                        }
                    }
                    else
                    {
                        if (!controller.isProtected)
                        {
                            var m = col.GetComponent<Knife>();
                            var oneRemove = false;
                            if (controller.isCanChange && controller.state == KnifeState.Attack)
                            {

                                if (!m.isOnRemove)
                                {
                                    if (m.controller.GetInstanceID() != controller.GetInstanceID())
                                    {
                                        if (!controller.IsOnRemove(this.GetInstanceID()) &&
                                            !controller.IsOnAttacker(m.GetInstanceID()))
                                        {
                                            oneRemove = true;
                                        }
                                    }
                                }
                            }

                            if (oneRemove)
                            {
                                controller.AddRemoveID(this.GetInstanceID());
                                controller.RemoveKnife(this);
                                controller.iInput.OnRemoveKnifeFromE();
                                controller.AddAttackerID(m.GetInstanceID());
                                if (m.controller.isPlayer)
                                {
                                    Poolers.ins.GetObject(GameController.ins.hitEffect, m_transform.position,
                                        Quaternion.identity);
                                    this.PostEvent(EventID.KnockKnife);
                                }

                                if (controller.isPlayer)
                                {
                                    MapController.ins.camFollower.CameraShake();
                                    AudioManager.ins.PlayVacham();
                                }
                            }
                        }

                    }
                }

                if (col.CompareTag(Tags.IMPEDIMENT))
                {
                    if (controller.isCanChange && controller.state == KnifeState.Attack && !controller.isProtected)
                    {
                        if (!controller.IsOnRemove(this.GetInstanceID()))
                        {
                            var r = Random.value;
                            if (r > controller.doggeValue)
                            {
                                controller.AddRemoveID(this.GetInstanceID());
                                isOnRemove = true;
                                controller.iInput.OnRemoveKnifeFromImpediment();
                                controller.RemoveKnife(this);
                                if (controller.isPlayer)
                                {
                                    Poolers.ins.GetObject(GameController.ins.hitEffect, m_transform.position,
                                        Quaternion.identity);
                                    MapController.ins.camFollower.CameraShake();
                                    AudioManager.ins.PlayVacham();

                                }
                            }

                        }
                    }
                }

                if (col.CompareTag(Tags.PLAYER))
                {
                    if (!isOnRemove)
                    {
                        var c = col.GetComponent<KnifeController>();
                        if (!c.isProtected && !controller.isProtected && c.GetInstanceID() != controller.GetInstanceID())
                        {
                            GameController.ins.StartAnyCoroutine((GameHelper.EndOfFrame(() =>
                            {
                                if (!c.isDeath && c.isCanDeath)
                                {
                                    c.OnDie();
                                    controller.AddKillId(c.GetInstanceID());
                                    UI.Ins.inGame.gamePlay.uiKill.AddKill(new KillData
                                    {
                                        p1Name = controller.heroInfor.heroName,
                                        p2Name = c.heroInfor.heroName,
                                        icon1 = controller.heroInfor.iconAvt,
                                        icon2 = c.heroInfor.iconAvt
                                    });
                                }
                            })));

                        }
                    }

                }

            }
            else
            {
                if (col.CompareTag(Tags.MAGNET))
                {
                    var c = col.GetComponentInParent<KnifeController>();
                    if (c.knifeCount < 50)
                    {
                        moveOnMagnet?.Kill();
                        moveOnMagnet = m_transform.DOMove(col.transform.position, Config.TIME_MAGNET);
                    }
                }

                if (col.CompareTag(Tags.PLAYER))
                {
                    if (!isOnRemove)
                    {
                        var c = col.GetComponent<KnifeController>();
                        if (c.state == KnifeState.Attack && c.knifeCount < Config.MAX_KNIFE)
                        {
                            if (c.isCanChange)
                            {
                                if (!c.IsOnAdd(this.GetInstanceID()))
                                {
                                    moveOnMagnet?.Kill();
                                    //col.Hide();
                                    c.AddID(this.GetInstanceID());
                                    c.AddKnife(this, 0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

