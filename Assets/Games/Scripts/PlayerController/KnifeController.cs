﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class KnifeController : PoolItem
{
    public GameObject knife;
    public Transform content;
    public GameObject directionItem;

    [Header("Player Infor")]
    public HeroInfor heroInfor;

    public MaskController maskController;
    public List<Knife> knifes;
    public KnifeState state;
    public KnifeType knifeType;
    public IController iInput;
    [Header("Config Value")]
    public int knifeCount;
    public float speed;
    public float speedCalc, speedCalcOnRemove;
    public float radius;
    public float doggeValue;
    public bool isRotateKnife, isCanDeath, isProtected, isDeath;
    public float countForCalculation;
    private Tween calTween, switchTween;
    public bool isPlayer;
    private List<int> addKnifeIDs;
    private List<int> removeKnifeIDs;
    private List<int> attackerIDs;
    public bool isCanChange;
    public bool isOnSwitchState;
    public float timeWaitForAction;
    private Action skillAction;
    private Coroutine descreaseKnifePerSecond, skillCoroutine;
    public DirectionItem moveFollowItem;
    public PlayerDefineID defineID;
    private Coroutine iDefenseCT;

    void Start()
    {
        countForCalculation = knifeCount;
        
        //OnInstance(KnifeType.Batman);
    }

    public void OnInstance(HeroInfor heroInfor)
    {
        this.heroInfor = heroInfor;
        heroInfor.isCanRevival = true;
        knifeType = heroInfor.knifeType;
        iInput = GetComponent<IController>();
        var data = GameController.ins.knifeData.knifeData[(int) heroInfor.knifeType];
        var maskInfor = GameController.ins.maskData.maskData[(int) heroInfor.maskType];

        isDeath = false;
        isCanDeath = true;
        state = KnifeState.Attack;
        if (isPlayer)
        {
            SwitchToDefenseState();
        }
        isCanChange = true;
        content.DetachChildren();
        knifes = new List<Knife>();
        addKnifeIDs = new List<int>();
        removeKnifeIDs = new List<int>();
        killIDs = new List<int>();
        attackerIDs = new List<int>();
        speed = Config.BASE_ROTATE_SPEED * (1 + data.addSpeed + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddSpeed : 0));
        knifeCount = Config.BASE_KNIFE_COUNT + (data.addKnifeInStart+ (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddKnifeInStart : 0));
        radius = Config.BASE_RADIUS;
        doggeValue = data.avoidObstacle + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAvoidObstacle : 0);
        iInput.SetUpSpeed(Config.BASE_MOVE_SPEED);
        switch (data.buffType)
        {
            case BuffType.None:
                break;
            case BuffType.UpDownAttack:
                timeWaitForAction = data.value;
                skillAction = UpDownSkill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            case BuffType.LeftRightAttack:
                timeWaitForAction = data.value;
                skillAction = LeftRightSkill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            case BuffType.DamageFour:
                timeWaitForAction = data.value;
                skillAction = Dmg1Skill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            case BuffType.DamageX:
                timeWaitForAction = data.value;
                skillAction = Dmg2Skill;
                if (skillCoroutine != null)
                {
                    StopCoroutine(skillCoroutine);
                }

                skillCoroutine = StartCoroutine(ISkillAction());
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        countForCalculation = knifeCount;
        maskController.SetUp(maskInfor);
        if (iDefenseCT != null)
        {
            StopCoroutine(iDefenseCT);
        }
        iDefenseCT = StartCoroutine(IAnim());
        SpawnMultiKnife();
        heroInfor.heroPoint = knifeCount;
        iInput.SetCanMove();
        defineID.SetUp(heroInfor);
        if (!isPlayer)
        {
            moveFollowItem = Poolers.ins.GetObject(directionItem, m_transform.position, Quaternion.identity).Cast<DirectionItem>();
            moveFollowItem.OnSpawn(heroInfor.color);
        }
        else
        {
            var f = GameManager.ins.GetMotion(MotionType.Start);
            if (f.isHasEquip)
            {
                maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
            }
        }

    }
#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            AddRemoveID(this.GetInstanceID());
            RemoveKnife(knifes[0]);           
            //AddAttackerID(m.GetInstanceID());
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            AddRemoveID(this.GetInstanceID());
            iInput.OnRemoveKnifeFromImpediment();
            RemoveKnife(knifes[Random.Range(0, knifes.Count)]);
            RemoveKnife(knifes[Random.Range(0, knifes.Count)]);
            RemoveKnife(knifes[Random.Range(0, knifes.Count)]);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            //AddID(col.GetInstanceID());
            AddKnife(0);
            AddKnife(0);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            OnDie();
        }
    }

#endif
    public void SwitchToDefenseState()
    {
        isOnSwitchState = true;
        
        switchTween?.Kill();
        calTween?.Kill();
        var lastV = radius;
        var lastCount = countForCalculation;
        if (isPlayer)
        {
            MapController.ins.camFollower.ZoomCamera(Config.DEFNSE_CAMSZIE);
        }

        switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 7, value =>
        {
            foreach (var item in knifes)
            {
                radius = Mathf.Lerp(lastV, Config.BASE_DEFENSE_RADIUS, value * 2);
                countForCalculation = Mathf.Lerp(lastCount, knifeCount, value);
                var curRot = item.m_transform.right;
                var des = (item.m_transform.position - m_transform.position).normalized;
                item.m_transform.right = Vector3.Lerp(curRot, des, value).normalized;
                item.MoveOnSpawn();
                item.SetY(-1);
            }
        }).OnComplete(() =>
        {
            if (descreaseKnifePerSecond != null)
            {
                StopCoroutine(descreaseKnifePerSecond);
            }

            if(m_Obj.activeInHierarchy)
                descreaseKnifePerSecond = StartCoroutine(IDefense());
            state = KnifeState.Defense;
            isOnSwitchState = false;
            if(isPlayer)
                maskController.Defense();
        });
    }

    public void SwitchToAttackState()
    {
        isOnSwitchState = true;
        calTween?.Kill();
        if (isPlayer)
        {
            MapController.ins.camFollower.ZoomCamera(Config.ATTACK_CAMSZIE);
        }
        state = KnifeState.Attack;
        if (isPlayer)
            maskController.DisableDefense();
        if (descreaseKnifePerSecond != null)
        {
            StopCoroutine(descreaseKnifePerSecond);
        }
        switchTween?.Kill();
        var lastV = radius;
        var lastCount = countForCalculation;
        switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 4, value =>
        {
            foreach (var item in knifes)
            {
                radius = Mathf.Lerp(lastV, GetRadius(), value);
                countForCalculation = Mathf.Lerp(lastCount, knifeCount, value);
                var curRot = item.m_transform.up;
                var des = (item.m_transform.position - m_transform.position).normalized;
                item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                item.MoveOnSpawn();
                item.SetY(1);
            }
        }).OnComplete(() =>
        {
            isOnSwitchState = false;
        });
    }

    private void SpawnMultiKnife()
    {
        countForCalculation = knifeCount;
        StartCoroutine(IMultiSpawn());
    }

    IEnumerator IMultiSpawn()
    {
        for (int i = 0; i < knifeCount; i++)
        {
            SpawnKnife(i);
            yield return null;
        }
    }

    public void SpawnKnife(int index)
    {
        var knife = Poolers.ins.GetObject(this.knife, content);
        //knife.m_transform.localPosition = Vector3.up;
        
        var controller = knife.Cast<Knife>();
        controller.OnInstance(this, index, knifeType);
        knifes.Add(controller);

        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            if (Math.Abs(countForCalculation) > 0)
            {
                controller.MoveOnSpawn();
            }
            knife.m_transform.up = (knife.m_transform.position - m_transform.position).normalized;
        }));
    }

    public void SpawnKnifeFromMask(int index)
    {
        var knife = Poolers.ins.GetObject(this.knife, content);
        if (knifes.Count > 0)
        {
            knife.m_transform.localPosition = knifes[0].m_transform.localPosition;
        }
        else
        {
            knife.m_transform.localPosition = Vector3.right * Config.BASE_RADIUS;
        }
        //knife.m_transform.localPosition = Vector3.up;

        var controller = knife.Cast<Knife>();
        controller.OnInstance(this, index, knifeType);

        knifes.Add(controller);
        GameController.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            if (Math.Abs(countForCalculation) > 0)
            {
                controller.MoveOnSpawn();
            }
            knife.m_transform.up = (knife.m_transform.position - m_transform.position).normalized;
        }));
    }

    public void AddKnife(int startIndex)
    {
        isCanChange = false;
        iInput.OnAddKnife();
        knifeCount++;
        OnChangeKnife();
        CalculatorRadius();
        ChangeKnifeIndexAfterAdd(startIndex);
        SpawnKnife(startIndex);
        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            addKnifeIDs.Clear();
            isCanChange = true;
            CalcValue(knifeCount);
        }));
    }

    public void SpawnKnife(Knife k, int index)
    {
        k.m_transform.SetParent(content);
        k.OnInstance(this, index, knifeType);
        knifes.Add(k);      
    }

    public void AddKnife(Knife k, int index)
    {
        isCanChange = false;
        iInput.OnAddKnife();
        knifeCount++;
        OnChangeKnife();
        CalculatorRadius();
        //ChangeKnifeIndexAfterAdd(index);
        SpawnKnife(k, knifeCount - 1);
        if (isPlayer)
        {
            this.PostEvent(EventID.HasKnife);
        }
        StartCoroutine(GameHelper.EndOfFrame(() =>
        {
            addKnifeIDs.Clear();
            isCanChange = true;
            CalcValue(knifeCount, knifeCount - 1);
        }));
    }

    struct  VTemp
    {
        public Vector3 rot;
        public Vector3 pos;
    }

    public void CalcValue(int knifeCount, int index)
    {
        isOnCalc = true;
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        var l = new List<VTemp>();
        foreach (var m in knifes)
        {
            l.Add(new VTemp
            {
                pos = m.m_transform.localPosition,
                rot = m.m_transform.up,
            });
            m.SetY(1);
        }

        calTween = DOVirtual.Float(last, knifeCount, speedCalc, value =>
        {
            countForCalculation = value;
            var f = 2 * (value - last) / (knifeCount - last);
            for (int i = 0; i < knifes.Count; i++)
            {
                if (knifes[i].knifeIndex != index)
                {
                    if (Mathf.Abs(countForCalculation) > 0.1f)
                    {

                        var v = Vector3.Slerp(l[i].pos, knifes[i].GetNextPos(), f * 2 );
                        //Debug.Log();
                        if(!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                        var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                        knifes[i].m_transform.up = Vector3.Lerp(l[i].rot, des, value);

                    }
                }
                else
                {
                    if (Mathf.Abs(countForCalculation) > 0.1f)
                    {
                        //var nextPos = knifes[i].GetNextPos();
                        var v = Vector3.Slerp(l[i].pos, knifes[i].GetNextPos(), f);
                        //var x = Mathf.Lerp(l[i].pos.x, nextPos.x, f);
                        //var y = Mathf.Lerp(l[i].pos.y, nextPos.y, f);
                        if (!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                        var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                        knifes[i].m_transform.up = Vector3.Lerp(l[i].rot, des, value);
                    }
                }
                
            }
        }).OnComplete(() =>
        {
            isOnCalc = false;
            isOnSwitchState = false;
        });
    }

    public void MaskSpawnKnife(int startIndex)
    {
        isCanChange = false;
        iInput.OnAddKnife();
        knifeCount++;
        OnChangeKnife();
        CalculatorRadius();
        ChangeKnifeIndexAfterAdd(startIndex);
        SpawnKnifeFromMask(startIndex);
        GameController.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            addKnifeIDs.Clear();
            isCanChange = true;
            if (state == KnifeState.Attack && !isOnSwitchState)
            {
                CalcValue(knifeCount);
            }
            else
            {
                CalcValueOnDefense(knifeCount);
            }
        }));
    }

    public void AddID(int id)
    {
        addKnifeIDs.Add(id);
    }

    public void AddRemoveID(int id)
    {
        removeKnifeIDs.Add(id);
    }

    public void AddAttackerID(int id)
    {
        removeKnifeIDs.Add(id);
    }

    public bool IsOnRemove(int id)
    {
        return removeKnifeIDs.Contains(id);
    }

    public bool IsOnAdd(int id)
    {
        return addKnifeIDs.Contains(id);
    }

    public bool IsOnAttacker(int id)
    {
        return attackerIDs.Contains(id);
    }

    public void RemoveKnife(int startIndex)
    {
        isCanDeath = false;
        isCanChange = false;
        iInput.OnRemoveKnifeFromImpediment();
        knifeCount--;
        OnChangeKnife();
        var item = knifes.Find(s => s.knifeIndex == startIndex);
        CalculatorRadius();
        ChangeKnifeIndexAfterRemove(startIndex);
        item.MoveOnRemove();
        knifes.Remove(item);
        GameController.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
        {
            isCanChange = true;
            attackerIDs.Clear();
            removeKnifeIDs.Clear();
            if (!isOnSwitchState && state != KnifeState.Defense)
            {
                CalcValueOnRemove(knifeCount);
            }
            else
            {
                if (state == KnifeState.Defense)
                {
                    CalcValueOnDefense(knifeCount);
                }
                else
                {
                    countForCalculation = knifeCount;
                }
               
            }
           
        }));
        
    }

    public void RemoveKnife(Knife knife)
    {
        if (knifes.Contains(knife))
        {
            isCanChange = false;
            knifeCount--;
            CalculatorRadius();
            ChangeKnifeIndexAfterRemove(knife.knifeIndex);
            knifes.Remove(knife);
            knife.MoveOnRemove();
            OnChangeKnife();
            GameController.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
            {
                isCanDeath = true;
                isCanChange = true;
                attackerIDs.Clear();
                removeKnifeIDs.Clear();
                if (!isOnSwitchState && state != KnifeState.Defense)
                {
                    CalcValueOnRemove(knifeCount);
                }
                else
                {
                    if (state == KnifeState.Defense)
                    {
                        CalcValueOnDefense(knifeCount);
                    }
                    else
                    {
                        switchTween?.Kill();
                        CalcValueOnRemove(knifeCount);
                    }

                }

            }));
        }
    }

    public void CalcValueOnDefense(int destination)
    {
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        if (!isProtected)
        {
            isCanDeath = true;
        }
        var l = new List<Vector3>();
        foreach (var m in knifes)
        {
            l.Add(m.m_transform.right);
            m.SetY(-1);
        }
        calTween = DOVirtual.Float(last, destination, speedCalc, value =>
        {
            var f = 2 * (value - last) / (destination - last);
            countForCalculation = value;
            for (int i = 0; i < knifes.Count; i++)
            {
                var des = (knifes[i].m_transform.position - m_transform.position).normalized;

                knifes[i].m_transform.right = Vector3.Lerp(l[i], des, f).normalized;
                var v = Vector3.Lerp(knifes[i].m_transform.localPosition, knifes[i].GetNextPosOnDefenseMode(), f);
                if(!float.IsNaN(v.x))
                    knifes[i].m_transform.localPosition = v;

            }

        }).OnComplete(() => { isOnSwitchState = false; });

    }

    public bool isOnCalc;
    public void CalcValue(int destination)
    {
        isOnCalc = true;
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        var l = new List<Vector3>();
        foreach (var m in knifes)
        {
            l.Add(m.m_transform.up);
            m.SetY(1);
        }
       
        calTween = DOVirtual.Float(last, destination, speedCalc / 4, value =>
        {
            countForCalculation = value;
            var f = 2 * (value - last) / (destination - last);

            for (int i = 0; i < knifes.Count; i++)
            {
                if (Mathf.Abs(countForCalculation) > 0.1f)
                {
                    try
                    {
                        var v = Vector3.Slerp(knifes[i].m_transform.localPosition, knifes[i].GetNextPos(), f);
                        if(!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                        var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                        knifes[i].m_transform.up = Vector3.Lerp(l[i], des, value);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }


                   
                }
            }

        }).OnComplete(() =>
        {
            isOnCalc = false;
            isOnSwitchState = false;
        });

    }

    public void CalcValueOnRemove(int destination)
    {
        isOnCalc = true;
        var last = Math.Abs(countForCalculation) > 0 ? countForCalculation : 0.1f;
        calTween?.Kill();
        switchTween?.Kill();
        var l = new List<Vector3>();
        foreach (var m in knifes)
        {
            l.Add(m.m_transform.up);
            var des = (m.m_transform.position - m_transform.position).normalized;
            m.m_transform.up = des.normalized;
        }
        calTween = DOVirtual.Float(last, destination, speedCalcOnRemove, value =>
        {
            countForCalculation = value;
            var f = 2 * (value - last) / (destination - last);

            for (int i = 0; i < knifes.Count; i++)
            {
                if (Mathf.Abs(countForCalculation) > 0.1f)
                {
                    try
                    {
                        var v = Vector3.Lerp(knifes[i].m_transform.localPosition, knifes[i].GetNextPos(), f);
                        //Debug.Log(v + "   " + knifes[i].GetNextPos() + "  " + knifes[i].m_transform.position + "   " + f);
                        if(!float.IsNaN(v.x))
                            knifes[i].m_transform.localPosition = v;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }


                    var des = (knifes[i].m_transform.position - m_transform.position).normalized;
                    knifes[i].m_transform.up = Vector3.Lerp(l[i], des, f).normalized;
                }
            }

        }).OnComplete(() =>
        {
            isOnCalc = false;
            isOnSwitchState = false;
        });
    }

    public void CalculatorRadius()
    {
        var t = (Config.BASE_RADIUS + (knifeCount - Config.BASE_KNIFE_COUNT) * Config.BASE_ADD_RADIUS);
        radius = (t > Config.MAX_RADIUS ? Config.MAX_RADIUS : t);
    }

    public float GetRadius()
    {
        var t = (Config.BASE_RADIUS + (knifeCount - Config.BASE_KNIFE_COUNT) * Config.BASE_ADD_RADIUS);
        return (t > Config.MAX_RADIUS ? Config.MAX_RADIUS : t);
    }

    public void GetRadius(out float distance)
    {
        distance = (Config.BASE_RADIUS + (knifeCount - Config.BASE_KNIFE_COUNT) * Config.BASE_ADD_RADIUS);        
    }

    private void ChangeKnifeIndexAfterAdd(int startIndex)
    {
        foreach (var item in knifes)
        {
            var i = GameHelper.GetIndexAfterAdd(item.knifeIndex, startIndex);
            item.knifeIndex = i;
            item.render.sortingOrder = 100 - i;
        }
    }

    private void ChangeKnifeIndexAfterRemove(int startIndex)
    {
        foreach (var item in knifes)
        {
            var i = GameHelper.GetIndexAfterRemove(item.knifeIndex, startIndex);
            item.knifeIndex = i;
            item.render.sortingOrder = 100 - i;
        }
    }

    #region Animation

    public void AnimationOff()
    {
        if (!isOnSwitchState && !isOnCalc)
        {
            switchTween?.Kill();
            var f = UnityEngine.Random.Range(5, 8);
            var s = UnityEngine.Random.Range(4, 8);
            var k = UnityEngine.Random.Range(2, 5);
            var lastV = radius;
            switchTween = DOVirtual.Float(0, 0.05f, Config.BASE_TIME_SWITCH_STATE / s, value =>
            {
                foreach (var item in knifes)
                {
                    radius = Mathf.Lerp(lastV, Config.BASE_DEFENSE_RADIUS, value * f);
                    var curRot = item.m_transform.right;
                    var des = (item.m_transform.position - m_transform.position).normalized;
                    item.m_transform.right = Vector3.Lerp(curRot, des, value).normalized;
                    item.MoveOnSpawn();
                }
            }).OnComplete(() =>
            {
                var m = radius;
                switchTween = DOVirtual.DelayedCall(Config.BASE_TIME_SWITCH_STATE / 10, () =>
                {
                    switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / k, value =>
                    {
                        foreach (var item in knifes)
                        {
                            radius = Mathf.Lerp(m, GetRadius(), value);
                            var curRot = item.m_transform.up;
                            var des = (item.m_transform.position - m_transform.position).normalized;
                            item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                            item.MoveOnSpawn();
                        }
                    });
                });

            });
        }
    }

    public void AnimationOn()
    {
        if (!isOnSwitchState && !isOnCalc)
        {
            switchTween?.Kill();
            var l = new List<float>();
            foreach (var m in knifes)
            {
                //var temp = m.m_transform.localRotation.eulerAngles.z;
                //temp = temp + 15 > 360 ? temp - 360 : temp;
                if (m.m_transform.localRotation.eulerAngles.z > 345)
                {

                    l.Add(m.m_transform.localRotation.eulerAngles.z - 360);
                }
                else
                {
                    l.Add(m.m_transform.localRotation.eulerAngles.z);
                }
            }

            switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 5, value =>
            {
                var x = Mathf.Lerp(0, 15, value);
                var count = 0;
                foreach (var item in knifes)
                {
                    var localRotation = item.m_transform.localRotation;
                    var rotation = localRotation.eulerAngles;
                    rotation.z = l[count] + x;
                    localRotation.eulerAngles = rotation;
                    item.m_transform.localRotation = localRotation;
                    count++;
                }
            }).OnComplete(() =>
            {
                switchTween = DOVirtual.DelayedCall(Config.BASE_TIME_SWITCH_STATE / 4, () =>
                {
                    switchTween = DOVirtual.Float(0, 1, Config.BASE_TIME_SWITCH_STATE / 5, value =>
                    {
                        foreach (var item in knifes)
                        {
                            var curRot = item.m_transform.up;
                            var des = (item.m_transform.position - m_transform.position).normalized;
                            item.m_transform.up = Vector3.Lerp(curRot, des, value).normalized;
                        }
                    });
                });
            });
        }
    }

    IEnumerator IAnim()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(2, 5f));
            if (knifeCount > 15 && !isOnSwitchState && state == KnifeState.Attack)
            {
                var r = UnityEngine.Random.value;
                if (r > 0.5f)
                    AnimationOff();
                else
                    AnimationOn();
            }
        }
    }

    #endregion

    #region KnifeSkill

    public void UpDownSkill()
    {
        for (int i = 0; i < 2; i++)
        {
            var m = Poolers.ins.GetObject(knife, m_transform.position, Quaternion.identity);
            m.Cast<Knife>().OnInstanceOnSkill(this, i, 2, 0, knifeType);
            
        }
    }

    public void LeftRightSkill()
    {
        for (int i = 0; i < 2; i++)
        {
            var m = Poolers.ins.GetObject(knife, m_transform.position, Quaternion.identity);
            m.Cast<Knife>().OnInstanceOnSkill(this, i, 2, 90, knifeType);

        }
    }

    public void Dmg1Skill()
    {
        for (int i = 0; i < 4; i++)
        {
            var m = Poolers.ins.GetObject(knife, m_transform.position, Quaternion.identity);
            m.Cast<Knife>().OnInstanceOnSkill(this, i, 4, 0, knifeType);

        }
    }

    public void Dmg2Skill()
    {
        for (int i = 0; i < 4; i++)
        {
            var m = Poolers.ins.GetObject(knife, m_transform.position, Quaternion.identity);
            m.Cast<Knife>().OnInstanceOnSkill(this, i, 4, 45, knifeType);

        }
    }

    IEnumerator ISkillAction()
    {
        var wt = new WaitForSeconds(timeWaitForAction);
        while (true)
        {
            yield return wt;
            if(!GameController.IsPauseGame)
                skillAction.Invoke();
        }
    }

    #endregion

    IEnumerator IDefense()
    {
        var wt = new WaitForSeconds(Config.BASE_TIME_DESCREASE_KNIFE);
        while (true)
        {
            yield return wt;
            if (knifeCount > 0)
            {
                RemoveKnife(0);
            }
        }
    }


    private Tween endTween;
    public void OnDie()
    {
        isDeath = true;
        isCanDeath = false;
        calTween?.Kill();
        switchTween?.Kill();
        StopAllCoroutines();
        knifeCount = 0;
        OnChangeKnife();
        GameController.ins.StartAnyCoroutine(GameHelper.EndOfFrame(()=>
        {
            for (int i = 0; i < knifes.Count; i++)
            {
                knifes[i].m_transform.SetParent(null);
                knifes[i].MoveOnRemove();               
            }
            knifes.Clear();

            GameController.ins.activeControllers.Remove(this);
            var t = Time.time;
            heroInfor.timeDie = t - GameController.TimeStart;
            if (!isPlayer)
            {
                moveFollowItem.Hide();
            }
            m_Obj.Hide();

            if (isPlayer || iInput.IsOnCamera())
            {
                AudioManager.ins.PlayAudioClip(ClipType.defeat);
            }

            if (isPlayer)
            {
                endTween?.Kill();
                endTween = DOVirtual.DelayedCall(0.5f, () =>
                {
                    if (heroInfor.isCanRevival && GameController.IsCanRevival && !GameController.ins.isHasCharacterSpawn)
                    {
                        UI.Ins.inGame.revenge.OnShow();
                    }
                    else
                    {
                        AudioManager.ins.PlayAudioClip(ClipType.defeat);
                        endTween = DOVirtual.DelayedCall(1, () => { UI.Ins.ShowOnEndGame(); });
                    }
                });
            }
            else
            {
                GameController.ins.StartAnyCoroutine(GameHelper.EndOfFrame(() =>
                {
                    if (!heroInfor.isCanRevival || !GameController.IsCanRevival)
                    {
                        if (!GameController.ins.isHasCharacterSpawn && GameController.ins.activeControllers.Count <= 1)
                        {
                            endTween?.Kill();

                            endTween = DOVirtual.DelayedCall(0.5f, () =>
                            {
                                if(!GameController.ins.player.isDeath)
                                    UI.Ins.inGame.gamePlay.uiVictory.Show();
                                if (isPlayer)
                                {
                                    var f = GameManager.ins.GetMotion(MotionType.Victory);
                                    if (f.isHasEquip)
                                    {
                                        maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                                    }
                                }

                                endTween = DOVirtual.DelayedCall(2, () =>
                                {
                                    UI.Ins.inGame.gamePlay.uiVictory.Hide();
                                    UI.Ins.ShowOnEndGame();
                                    GameController.IsPauseGame = true;
                                });
                            });
                           
                        }
                    }
                    else
                    {
                        var r = Random.value;
                        if (r < GameController.ins.rankData.rankData.Find(s => s.rank == heroInfor.rankType)
                                .probabilityRevival)
                        {
                            GameController.ins.isHasCharacterSpawn = true;
                            endTween?.Kill();
                            endTween = DOVirtual.DelayedCall(Random.Range(2, 3f), WaitSpawn);
                        }
                        else
                        {
                            if (!GameController.ins.isHasCharacterSpawn && GameController.ins.activeControllers.Count <= 1)
                            {
                                endTween?.Kill();

                                endTween = DOVirtual.DelayedCall(0.5f, () =>
                                {
                                    if (!GameController.ins.player.isDeath)
                                        UI.Ins.inGame.gamePlay.uiVictory.Show();

                                    endTween = DOVirtual.DelayedCall(2, () =>
                                    {
                                        UI.Ins.inGame.gamePlay.uiVictory.Hide();
                                        UI.Ins.ShowOnEndGame();
                                        GameController.IsPauseGame = true;
                                    });
                                });

                            }
                        }
                    }
                }));

            }

        }));
    }


    public void OnChangeKnife()
    {
        heroInfor.heroPoint = knifeCount + heroInfor.killPoint * 10;
        defineID.SetUpPoint(knifeCount);
        UI.Ins.inGame.gamePlay.uiRanking.OnRegisterRanking();
    }

    private List<int> killIDs;
    private Coroutine addKillCt;

    public void AddKillId(int id)
    {
        if (!killIDs.Contains(id))
        {
            killIDs.Add(id);
            if (addKillCt != null)
            {
                GameController.ins.StopCoroutine(addKillCt);
            }

            addKillCt = GameController.ins.StartCoroutine(GameHelper.EndOfFrame(AddKillPoint));
        }
    }

    public void AddKillPoint()
    {
        
        heroInfor.killPoint += killIDs.Count;
        OnChangeKnife();
        if (heroInfor.killPoint == 2)
        {
            this.PostEvent(EventID.DoubleKill);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.doublekill);
                var f = GameManager.ins.GetMotion(MotionType.DoubleKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }else if (heroInfor.killPoint == 3)
        {
            this.PostEvent(EventID.TripleKill);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.triple);
                var f = GameManager.ins.GetMotion(MotionType.TripleKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }
        else if(heroInfor.killPoint == 4)
        {
            this.PostEvent(EventID.Quadrakill);
            maskController.BuffBladeSpeed_Quadra(3, 1);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.quadra);
                var f = GameManager.ins.GetMotion(MotionType.QuadraKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }
        else if(heroInfor.killPoint == 5)
        {
            this.PostEvent(EventID.Pentakill);
            maskController.BuffBladeSpeed_Quadra(4, 1);
            if (isPlayer)
            {
                AudioManager.ins.PlayAudioClip(ClipType.penta);
                var f = GameManager.ins.GetMotion(MotionType.PentaKill);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }
        else
        {
            
            if (heroInfor.killPoint == 6)
            {
                maskController.BuffBladeSpeed_Quadra(5, 1);
                if (isPlayer)
                {
                    AudioManager.ins.PlayAudioClip(ClipType.hexa);
                    var f = GameManager.ins.GetMotion(MotionType.HexaKill);
                    if (f.isHasEquip)
                    {
                        maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                    }
                }
                this.PostEvent(EventID.HexaKill);
            }else if (heroInfor.killPoint > 6)
            {
                if (isPlayer)
                {
                    AudioManager.ins.PlayAudioClip(ClipType.legendary);
                    var f = GameManager.ins.GetMotion(MotionType.Legendary);
                    if (f.isHasEquip)
                    {
                        maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                    }
                }
            }
        }

        if (GameController.ins.isHasFB)
        {
            AudioManager.ins.PlayAudioClip(ClipType.firstblood);
            GameController.ins.isHasFB = false;
            UI.Ins.inGame.gamePlay.ShowHighLight(1);
            if (isPlayer)
            {
                var f = GameManager.ins.GetMotion(MotionType.FirstBlood);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            }
        }

        if (heroInfor.isPlayer && heroInfor.killPoint >= 2)
        {
            UI.Ins.inGame.gamePlay.ShowHighLight(heroInfor.killPoint);
        }

        if (GameController.ins.activeControllers.Count <= 1)
        {
            AudioManager.ins.PlayAudioClip(ClipType.ace);
        }

        killIDs = new List<int>();
    }

    public void OnRevival()
    {
       
        heroInfor.isCanRevival = false;
        knifeType = heroInfor.knifeType;
        iInput = GetComponent<IController>();
        var data = GameController.ins.knifeData.knifeData[(int)heroInfor.knifeType];
        
        isDeath = false;
        isCanDeath = true;
        state = KnifeState.Attack;
        isCanChange = true;
        content.DetachChildren();
        knifes = new List<Knife>();
        addKnifeIDs = new List<int>();
        removeKnifeIDs = new List<int>();
        attackerIDs = new List<int>();
        speed = Config.BASE_ROTATE_SPEED * (1 + data.addSpeed + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddSpeed : 0));
        radius = Config.BASE_RADIUS;
        doggeValue = data.avoidObstacle + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAvoidObstacle : 0);
        iInput.SetUpSpeed(Config.BASE_MOVE_SPEED);
        
        if (knifeCount == 0)
            knifeCount = 2;
        knifeCount = Config.BASE_KNIFE_COUNT + (data.addKnifeInStart + (heroInfor.maskType == data.setTypeBuff ? data.setBuffAddKnifeInStart : 0));
        countForCalculation = knifeCount;

        heroInfor.heroPoint = knifeCount;
        iInput.SetCanMove();
        defineID.SetUp(heroInfor);
        if (!isPlayer)
        {
            moveFollowItem = Poolers.ins.GetObject(directionItem, m_transform.position, Quaternion.identity).Cast<DirectionItem>();
            moveFollowItem.OnSpawn(heroInfor.color);
        }
        else
        {
            DOVirtual.DelayedCall(1, () =>
            {
                var f = GameManager.ins.GetMotion(MotionType.Revive);
                if (f.isHasEquip)
                {
                    maskController.mask.sprite = GameController.ins.maskData.maskData.Find(s => s.type == f.maskEquip).icon;
                }
            });

        }
    }

    public void WaitSpawn()
    {
        GameController.ins.isHasCharacterSpawn = false;
        m_transform.position = MapController.ins.GetRandomPos();
        var fx = Poolers.ins.GetObject(GameController.ins.waitSpawnEffect, m_transform.position,
            Quaternion.Euler(-90, 0, 0));
        OnRevival();
        endTween = DOVirtual.DelayedCall(1.5f, () =>
        {
            GameController.ins.isHasCharacterSpawn = false;
            maskController.BuffProtected(2f);
            fx.Hide();
            m_Obj.Show();
            GameController.ins.activeControllers.Add(this);
            if (iDefenseCT != null)
            {
                StopCoroutine(iDefenseCT);
            }
            iDefenseCT = StartCoroutine(IAnim());
            SpawnMultiKnife();
            var maskInfor = GameController.ins.maskData.maskData[(int)heroInfor.maskType];
            maskController.SetUp(maskInfor);
            var data = GameController.ins.knifeData.knifeData[(int)heroInfor.knifeType];

            switch (data.buffType)
            {
                case BuffType.None:
                    break;
                case BuffType.UpDownAttack:
                    timeWaitForAction = data.value;
                    skillAction = UpDownSkill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                case BuffType.LeftRightAttack:
                    timeWaitForAction = data.value;
                    skillAction = LeftRightSkill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                case BuffType.DamageFour:
                    timeWaitForAction = data.value;
                    skillAction = Dmg1Skill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                case BuffType.DamageX:
                    timeWaitForAction = data.value;
                    skillAction = Dmg2Skill;
                    if (skillCoroutine != null)
                    {
                        StopCoroutine(skillCoroutine);
                    }

                    skillCoroutine = StartCoroutine(ISkillAction());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        });
    }
}

public enum KnifeState
{
    Attack,
    Defense
}

[System.Serializable]
public class HeroInfor
{
    public string heroName;
    public string heroCountry;
    public Sprite iconAvt;
    public int heroPoint;
    public Color color;
    public RankType rankType;
    public KnifeType knifeType;
    public MaskType maskType;
    public bool isPlayer;
    public int ranking;
    public float timeDie;
    public int killPoint;
    public bool isCanRevival;

    public bool isAI = false;
    public int teamId;
}

