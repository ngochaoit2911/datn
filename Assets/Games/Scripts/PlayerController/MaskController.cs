﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Games.Scripts.Interface;
using DG.Tweening;
using UnityEngine;

public class MaskController : MonoBehaviour, IMaskController
{
    private KnifeController controller;
    public SpriteRenderer mask;
    public CircleCollider2D buffCol, itemBuffCol, magnetBuffCol;
    public float magnetSize;
    public float moreKnifeTime;
    public float addMoveSpeed;
    public float slowAreaTime;
    public float protectTime;
    public float slowBladeAreaTime;

    public GameObject slowAreaPrefab;

    private Coroutine moreCT;

    void Awake()
    {
        controller = GetComponent<KnifeController>();
    }

    public bool IsPlayer()
    {
        return controller.isPlayer || controller.iInput.IsOnCamera();
    }

    public void SetUp(MaskInfor infor)
    {
        StopAllCoroutines();
        buffCol.enabled = false;
        switch (infor.buffType)
        {
            case MaskBuffType.Magnet:
                buffCol.enabled = true;
                magnetSize = infor.value;
                Magnet();
                break;
            case MaskBuffType.Run:
                addMoveSpeed = infor.value;
                Run();
                break;
            case MaskBuffType.More:
                moreKnifeTime = infor.value;
                More();
                break;
            case MaskBuffType.Protect:
                protectTime = infor.value;
                Protect();
                break;
            case MaskBuffType.Slow:
                slowAreaTime = infor.value;
                SlowArea();
                break;
            case MaskBuffType.Dull:
                slowBladeAreaTime = infor.value;
                SlowDull();
                break;
            case MaskBuffType.None:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        mask.sprite = infor.icon;
    }

    public void Magnet()
    {
        var fx = Poolers.ins.GetObject(GameController.ins.magnetGO, controller.m_transform);
        fx.m_transform.localPosition = Vector3.zero;
        fx.Cast<FxPoolItem>().OnSpawn(magnetSize);
        buffCol.radius = magnetSize;
        buffCol.tag = Tags.MAGNET;
    }

    public void Run()
    {
        controller.iInput.UpSpeed(addMoveSpeed);
    }

    public void More()
    {
        if (moreCT != null)
        {
            StopCoroutine(moreCT);
        }

        moreCT = StartCoroutine(IMoreKnife());
    }

    private Coroutine protectCT;
    public void Protect()
    {

        buffCol.enabled = false;
        if (protectCT != null)
        {
            StopCoroutine(protectCT);
        }

        protectCT = StartCoroutine(IProtected());
    }

    IEnumerator IProtected()
    {
        var w1 = new WaitForSeconds(Config.PROTECTED_TIME);
        var wt = new WaitForSeconds(protectTime);
        while (true)
        {
            yield return wt;
            buffCol.enabled = true;
            buffCol.radius = controller.GetRadius() + 0.3f;
            buffCol.tag = Tags.PROTECT;
            controller.isProtected = true;
            var fx = Poolers.ins.GetObject(GameController.ins.protectedGO, controller.m_transform);
            fx.ClearTimeDisable();
            fx.m_transform.localPosition = Vector3.zero;
            fx.Cast<FxPoolItem>().OnSpawn(buffCol.radius);
            yield return w1;
            buffCol.enabled = false;
            controller.isProtected = false;
            fx.Disable();
        }
    }

    private Coroutine slowAreaCT, slowDullCT;
    public void SlowArea()
    {
        slowAreaCT = StartCoroutine(ISlowArea());
    }

    public void SlowDull()
    {
        slowDullCT = StartCoroutine(ISlowDull());
    }

    public void SpawnSlowArea()
    {
        var b = Poolers.ins.GetObject(slowAreaPrefab, transform.position, Quaternion.identity);
        b.Cast<SlowArea>().OnInstance(false, controller.GetInstanceID(), 3);
    }

    public void SpawnSlowDull()
    {
        var b = Poolers.ins.GetObject(slowAreaPrefab, transform.position, Quaternion.identity);
        b.Cast<SlowArea>().OnInstance(true, controller.GetInstanceID(), 3);
    }

    IEnumerator IMoreKnife()
    {
        var wt = new WaitForSeconds(moreKnifeTime);
        while (true)
        {
            yield return wt;
            if (controller.knifeCount < 50)
            {
                if (controller.isCanChange)
                {
                    controller.MaskSpawnKnife(0);
                }               
            }
        }
    }

    IEnumerator ISlowArea()
    {
        var wt = new WaitForSeconds(slowAreaTime);
        while (true)
        {
            yield return wt;
            SpawnSlowArea();
        }
    }

    IEnumerator ISlowDull()
    {
        var wt = new WaitForSeconds(slowBladeAreaTime);
        while (true)
        {
            yield return wt;
            SpawnSlowDull();
        }
    }

    #region ItemBooster

    private Tween speedTween, bladeTween, bladeTween2;
    private bool isHasBladeBuff, isHasSpeedBuff, isHasBladeBuff2;
    private PoolItem bladeBuffCache, speedBuffCache, bladeBuffCache2;
    public void BuffSpeed(float time, float value)
    {
        speedTween?.Kill();
        if (!isHasSpeedBuff)
        {
            isHasSpeedBuff = true;
            controller.iInput.UpSpeed(value);
            speedBuffCache = Poolers.ins.GetObject(GameController.ins.runGO, controller.m_transform);
            speedBuffCache.m_transform.localPosition = Vector3.zero;
            //speedBuffCache.m_transform.GetChild(0).GetComponent<ParticleSystem>().Play();
            speedTween = DOVirtual.DelayedCall(time, () =>
            {
                controller.iInput.RestoreSpeedAfterUp(value);
                speedBuffCache.Hide();
                isHasSpeedBuff = false;
            });
        }
        else
        {
            speedTween = DOVirtual.DelayedCall(time, () =>
            {
                isHasSpeedBuff = false;
                speedBuffCache.Hide();
                controller.iInput.RestoreSpeedAfterUp(value);
            });
        }
    }

    public void BuffBladeSpeed(float time, float value)
    {
        bladeTween?.Kill();
        if (!isHasBladeBuff)
        {
            isHasBladeBuff = true;
            controller.speed = controller.speed * (1 + value);
            bladeBuffCache = Poolers.ins.GetObject(GameController.ins.buffKnifeSpeedGO, controller.m_transform);
            bladeBuffCache.m_transform.localPosition = Vector3.zero;
            bladeBuffCache.Cast<FxPoolItem>().OnSpawn(controller.GetRadius() + 0.1f);
            bladeTween = DOVirtual.DelayedCall(time, () =>
            {
                isHasBladeBuff = false;
                controller.speed = controller.speed / (1 + value);
                bladeBuffCache.Disable();
            });
        }
        else
        {
            bladeTween = DOVirtual.DelayedCall(time, () =>
            {
                isHasBladeBuff = false;
                controller.speed = controller.speed / (1 + value);
                bladeBuffCache.Disable();
            });
        }
    }

    public void BuffBladeSpeed_Quadra(float time, float value)
    {
        bladeTween2?.Kill();
        if (!isHasBladeBuff2)
        {
            isHasBladeBuff2 = true;
            controller.speed = controller.speed * (1 + value);
            bladeBuffCache2 = Poolers.ins.GetObject(GameController.ins.buffSpeedQuada, controller.m_transform);
            bladeBuffCache2.m_transform.localPosition = Vector3.zero;
            bladeBuffCache2.Cast<FxPoolItem>().OnSpawn(controller.GetRadius() + 0.1f);
            bladeTween2 = DOVirtual.DelayedCall(time, () =>
            {
                isHasBladeBuff2 = false;
                controller.speed = controller.speed / (1 + value);
                bladeBuffCache2.Disable();
            });
        }
        else
        {
            bladeTween2 = DOVirtual.DelayedCall(time, () =>
            {
                isHasBladeBuff2 = false;
                controller.speed = controller.speed / (1 + value);
                bladeBuffCache2.Disable();
            });
        }
    }

    private Tween magnetTween;
    private bool isHasMagnet;
    private PoolItem cacheMagnet;
    public void BuffMagnet(float time, float size)
    {
        magnetTween?.Kill();
        if (!isHasMagnet)
        {
            isHasMagnet = true;
            cacheMagnet = Poolers.ins.GetObject(GameController.ins.magnetGO, controller.m_transform);
            cacheMagnet.m_transform.localPosition = Vector3.zero;
            cacheMagnet.Cast<FxPoolItem>().OnSpawn(size);
            magnetBuffCol.enabled = true;
            magnetBuffCol.radius = magnetSize;
            magnetBuffCol.tag = Tags.MAGNET;

            magnetTween = DOVirtual.DelayedCall(time, () =>
            {
                isHasMagnet = false;
                cacheMagnet.Disable();
                magnetBuffCol.enabled = false;
            });
        }
        else
        {
            magnetTween = DOVirtual.DelayedCall(time, () =>
            {
                isHasMagnet = false;
                cacheMagnet.Disable();
                magnetBuffCol.enabled = false;
            });
        }
    }

    private Tween protectedTween;
    private bool isHasProtected;
    private PoolItem cacheProtected;
    public void BuffProtected(float time)
    {
        protectedTween?.Kill();
        if (!isHasProtected)
        {
            isHasProtected = true;
            itemBuffCol.enabled = true;
            itemBuffCol.radius = controller.GetRadius() + 0.3f;
            itemBuffCol.tag = Tags.PROTECT;
            controller.isProtected = true;
            cacheProtected = Poolers.ins.GetObject(GameController.ins.protectedGO, controller.m_transform);
            cacheProtected.ClearTimeDisable();
            cacheProtected.m_transform.localPosition = Vector3.zero;
            cacheProtected.Cast<FxPoolItem>().OnSpawn(buffCol.radius);

            protectedTween = DOVirtual.DelayedCall(time, () =>
            {
                isHasProtected = false;
                itemBuffCol.enabled = false;
                controller.isProtected = false;
                cacheProtected.Disable();
            });
        }
        else
        {
            protectedTween = DOVirtual.DelayedCall(time, () =>
            {
                isHasProtected = false;
                itemBuffCol.enabled = false;
                controller.isProtected = false;
                cacheProtected.Disable();
            });
        }
    }

    #endregion

    private PoolItem cacheDefense;
    public bool isHasDefense;
    public void Defense()
    {
        isHasDefense = true;
        itemBuffCol.enabled = true;
        itemBuffCol.radius = controller.GetRadius() + 0.3f;
        itemBuffCol.tag = Tags.PROTECT;
        //controller.isProtected = true;
        cacheDefense = Poolers.ins.GetObject(GameController.ins.protectedGO, controller.m_transform);
        cacheDefense.ClearTimeDisable();
        cacheDefense.m_transform.localPosition = Vector3.zero;
        cacheDefense.Cast<FxPoolItem>().OnSpawn(buffCol.radius);
    }

    public void DisableDefense()
    {
        if (isHasDefense)
        {
            isHasDefense = false;
            itemBuffCol.enabled = false;
            //controller.isProtected = false;
            cacheDefense.Disable();
        }
    }

}
