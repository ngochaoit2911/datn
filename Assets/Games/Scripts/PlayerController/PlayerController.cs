﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, IController
{
    public float speed;
    private Transform t;
    private KnifeController controller;
    private Vector3 lastPos;
    private Camera cam;
    private Vector3 lastDirection;
    private float moveSpeed;

    private bool isCanMove = true;

    private void Awake()
    {
        t = transform;
        cam = Camera.main;
        controller = GetComponent<KnifeController>();
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        OnEditor();
#endif

#if PLATFORM_ANDROID && !UNITY_EDITOR
        OnAndroid();
#endif

    }

    private void OnAndroid()
    {
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                controller.SwitchToAttackState();
                lastPos = touch.position;
                lastPos.z = 10;
                lastPos = cam.ScreenToWorldPoint(lastPos);
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                if (!GameController.IsPauseGame)
                {
                    Vector3 newPos = touch.position;
                    newPos.z = 10;
                    newPos = cam.ScreenToWorldPoint(newPos);
                    var direction = newPos - lastPos;
                    var maxDistance = speed * Time.deltaTime;

                    if (direction.magnitude < maxDistance)
                    {
                        moveSpeed = Mathf.Clamp(direction.magnitude / maxDistance, 0.8f, 1) * speed;
                    }
                    else
                    {
                        moveSpeed = speed;
                    }

                    direction = Vector2.Lerp(lastDirection, direction, Time.deltaTime * 8);
                    if (isCanMove)
                        t.position += direction.normalized * moveSpeed * Time.deltaTime;
                    lastDirection = direction;
                    lastPos = newPos;
                }
            }else if (touch.phase == TouchPhase.Stationary)
            {
                if (!GameController.IsPauseGame)
                {
                    Vector3 newPos = touch.position;
                    newPos.z = 10;
                    newPos = cam.ScreenToWorldPoint(newPos);
                    if (isCanMove)
                        t.position += lastDirection.normalized * speed * 0.8f * Time.deltaTime;
                    lastPos = newPos;
                }
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (!GameController.IsPauseGame)
                    controller.SwitchToDefenseState();
            }
        }
    }

    private void OnEditor()
    {
        if (Input.GetMouseButtonDown(0))
        {
            controller.SwitchToAttackState();
            lastPos = Input.mousePosition;
            lastPos.z = 10;
            lastPos = cam.ScreenToWorldPoint(lastPos);
        }
        else if (Input.GetMouseButton(0))
        {
            if (!GameController.IsPauseGame)
            {
                var newPos = Input.mousePosition;
                newPos.z = 10;
                newPos = cam.ScreenToWorldPoint(newPos);
                var direction = newPos - lastPos;
                var maxDistance = speed * Time.deltaTime;

                if (direction.magnitude < maxDistance)
                {
                    moveSpeed = Mathf.Clamp(direction.magnitude / maxDistance, 0.5f, 1) * speed;
                }
                else
                {
                    moveSpeed = speed;
                }

                direction = Vector2.Lerp(lastDirection, direction, Time.deltaTime * 10);
                if (isCanMove)
                    t.position += direction.normalized * moveSpeed * Time.deltaTime;
                lastDirection = direction;
                lastPos = newPos;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if(!GameController.IsPauseGame)
                controller.SwitchToDefenseState();
        }
    }

    #region controller

    public void OnRemoveKnifeFromImpediment()
    {

    }

    public void OnRemoveKnifeFromE()
    {
        //isCanMove = false;
        //if (canMoveCT != null)
        //{
        //    StopCoroutine(canMoveCT);
        //}

        //canMoveCT = StartCoroutine(GameHelper.WaitForSecond(() => { isCanMove = true; }, Config.BASE_TIME_CAN_MOVE));
    }

    private Coroutine canMoveCT;
  
    public void OnAddKnife()
    {
        
    }

    public void UpSpeed(float addSpeed)
    {
        speed = (1 + addSpeed) * speed;
    }

    public void DescreaseSpeed(float downSpeed)
    {
        speed = (1 - downSpeed) * speed;
    }

    public void RestoreSpeedAfterDescrease(float downSpeed)
    {
        speed = speed / (1 - downSpeed);
    }

    public void RestoreSpeedAfterUp(float addSpeed)
    {
        speed = speed / (1 + addSpeed);
    }

    public void SetUpSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetCanMove()
    {
        isCanMove = true;
    }

    public bool IsOnCamera()
    {
        return true;
    }

    #endregion

}
