﻿using Assets.Games.Photon.Scripts.Controller;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerDefineID : MonoBehaviour
{
    public SpriteRenderer flag, rankIm;
    public TextMeshPro pName, pRank, knifeCountText;
    public GameObject obj;

    public void SetUp(HeroInfor heroInfor)
    {
        flag.sprite = heroInfor.iconAvt;
        pName.text = heroInfor.heroName;
        if (heroInfor.rankType >= 0)
        {
            rankIm.sprite = GameController.ins.rankData.rankIcon[(int)heroInfor.rankType / 3];
            pRank.text = (((int)heroInfor.rankType) % 3 + 1) + "";

        }
        else
        {
            rankIm.sprite = GameController.ins.rankData.rankIcon[0];
            pRank.text = (0) + "";

        }

        knifeCountText.text = heroInfor.heroPoint + "";
        pName.color = heroInfor.color;
    }

    public void Net_SetUp(HeroInfor heroInfor)
    {
        flag.sprite = heroInfor.iconAvt;
        pName.text = heroInfor.heroName + (Net_Controller.mode == GameMode.TeamBattle ? "  Team " + (heroInfor.teamId + 1) : "");
        if (heroInfor.rankType >= 0)
        {
            rankIm.sprite = Net_Controller.ins.rankData.rankIcon[(int)heroInfor.rankType / 3];
            pRank.text = (((int)heroInfor.rankType) % 3 + 1) + "";

        }
        else
        {
            rankIm.sprite = Net_Controller.ins.rankData.rankIcon[0];
            pRank.text = (0) + "";

        }

        knifeCountText.text = heroInfor.heroPoint + "";
        pName.color = heroInfor.color;
    }

    public void SetUpPoint(int knifeCount)
    {
        knifeCountText.text = knifeCount + "";
    }

    public void SetCrown(bool isShow)
    {
        obj.SetActive(isShow);
    }

}
