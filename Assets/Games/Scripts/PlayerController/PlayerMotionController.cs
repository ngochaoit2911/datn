﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotionController : MonoBehaviour
{
    public SpriteRenderer sr;
    public Animation anim;
    public Queue<MaskType> maskShow;

    private const string animName = "MotionAnim";
    public void OnMiddle()
    {
        if (maskShow.Count <= 0)
        {
            // Do Nothing
        }
        else
        {
            var m = maskShow.Dequeue();
            var f = GameController.ins.maskData.maskData.Find(s => s.type == m);
            sr.sprite = f.icon;
            anim.Play(animName);
        }
    }

    public void OnComplete()
    {
        if (maskShow.Count <= 0)
        {
            this.Hide();
        }
        else
        {
            var m = maskShow.Dequeue();
            var f = GameController.ins.maskData.maskData.Find(s => s.type == m);
            sr.sprite = f.icon;
            anim.Play(animName);
        }
    }

    public void AddQueue(MaskType maskType)
    {
        if (maskShow == null)
        {
            maskShow = new Queue<MaskType>();
        }
        maskShow.Enqueue(maskType);
        if (!gameObject.activeInHierarchy || !anim.isPlaying)
        {
            this.Show();
            var m = maskShow.Dequeue();
            var f = GameController.ins.maskData.maskData.Find(s => s.type == m);
            sr.sprite = f.icon;
            anim.Play(animName);
        }
        
    }

}
