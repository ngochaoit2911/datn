﻿using System.Collections;
using System.Collections.Generic;
using Assets.Games.Photon.Scripts.Controller;
using DG.Tweening;
using Photon.Pun;
using UnityEngine;

public class SlowArea : PoolItem
{
    public bool isSlowBlade;
    private int controllerID;
    public float existTime = 3;
    private Tween tween;
    private bool isOnline;

    public void OnInstance(bool isSlowBlade, int id, float size)
    {
        this.isSlowBlade = isSlowBlade;
        isOnline = false;
        m_Obj.tag = isSlowBlade ? Tags.SLOW_BLADE : Tags.SLOW_AREA;
        if (isSlowBlade)
        {
            var f = Poolers.ins.GetObject(GameController.ins.slowBladeGo, m_transform);
            f.m_transform.localPosition = Vector3.zero;
            f.Cast<FxPoolItem>().OnSpawn(existTime, size);
        }
        else
        {
            var f = Poolers.ins.GetObject(GameController.ins.slowAreaGO, m_transform);
            f.m_transform.localPosition = Vector3.zero;
            f.Cast<FxPoolItem>().OnSpawn(existTime, size);
        }
        controllerID = id;
        tween?.Kill();
        tween = DOVirtual.DelayedCall(existTime, gameObject.Hide);
    }

    public void Net_OnInstance(bool isSlowBlade, int id, float size)
    {
        this.isSlowBlade = isSlowBlade;
        isOnline = true;
        m_Obj.tag = isSlowBlade ? Tags.SLOW_BLADE : Tags.SLOW_AREA;
        if (isSlowBlade)
        {
            var f = Poolers.ins.GetObject(Net_Controller.ins.slowBladeGo, m_transform);
            f.m_transform.localPosition = Vector3.zero;
            f.Cast<FxPoolItem>().OnSpawn(existTime, size);
        }
        else
        {
            var f = Poolers.ins.GetObject(Net_Controller.ins.slowAreaGO, m_transform);
            f.m_transform.localPosition = Vector3.zero;
            f.Cast<FxPoolItem>().OnSpawn(existTime, size);
        }
        controllerID = id;
        tween?.Kill();
        tween = DOVirtual.DelayedCall(existTime, gameObject.Hide);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.PLAYER))
        {
            if (!isOnline)
            {
                var p = other.GetComponent<KnifeController>();
                if (p.GetInstanceID() == controllerID) return;
                if (isSlowBlade)
                {
                    p.speed = p.speed * (1 - Config.SLOW_BLADE_VALUE);
                }
                else
                {
                    p.iInput.DescreaseSpeed(Config.SLOW_MOVE_VALUE);
                }

            }
            else
            {
                var p = other.GetComponent<Net_KnifeController>();
                if (p.GetInstanceID() == controllerID) return;
                if (isSlowBlade)
                {
                    p.speed = p.speed * (1 - Config.SLOW_BLADE_VALUE);
                }
                else
                {
                    p.iInput.DescreaseSpeed(Config.SLOW_MOVE_VALUE);
                }
            }
            
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.CompareTag(Tags.PLAYER)) return;
        if (!isOnline)
        {
            var p = other.GetComponent<KnifeController>();
            if (p.GetInstanceID() == controllerID) return;
            if (isSlowBlade)
            {
                p.speed = p.speed / (1 - Config.SLOW_BLADE_VALUE);
            }
            else
            {
                p.iInput.RestoreSpeedAfterDescrease(Config.SLOW_MOVE_VALUE);
            }
        }
        else
        {
            var p = other.GetComponent<Net_KnifeController>();
            if (p.GetInstanceID() == controllerID) return;
            if (isSlowBlade)
            {
                p.speed = p.speed / (1 - Config.SLOW_BLADE_VALUE);
            }
            else
            {
                p.iInput.RestoreSpeedAfterDescrease(Config.SLOW_MOVE_VALUE);
            }
        }
        
    }
}
