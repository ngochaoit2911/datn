﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCastTest : MonoBehaviour
{
    public float angle;

    public float distance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
                var size2 = new Vector2(5, 1);
                var direction = GameHelper.GetDirectionFromAngle(angle);
                Debug.Log(direction);
                var nextPos = (Vector2)transform.position + direction * 5;
                //var nextPos = (transform.position +   direction);
                var point = (transform.position + (Vector3) nextPos) / 2;

                
                var hitKnife = Physics2D.OverlapBoxAll(point, size2, angle, GameController.ins.impediment);
                foreach (var d in hitKnife)
                {
                    Debug.Log(d.name);
                }
                Debug.DrawLine(transform.position, nextPos);
                GameHelper.DebugDrawBox(point, size2, angle, Color.red, 2);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            var hitWall = Physics2D.RaycastAll(transform.position, GameHelper.GetDirectionFromAngle(angle), distance, GameController.ins.wall);
            Debug.Log(hitWall.Length);
            Debug.DrawRay(transform.position, GameHelper.GetDirectionFromAngle(angle), Color.cyan, 1);
            if(hitWall.Length > 0)
                Debug.DrawLine(transform.position, hitWall[0].point, Color.gray, 2);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.INNER_KNIFE))
        {
            Debug.Log("m");
            //var box = other.GetComponent<BoxCastTest>();
            //Debug.Log(box.name);
            //Debug.Log(gameObject.name);
        }
    }
}
