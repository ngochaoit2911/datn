﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestKnife : MonoBehaviour
{
    public LayerMask mask;
    public KnifeController controller;
    public float speed = 6;
    public float maxDistance;
    public EnemyAIData aiData;
    public EnemyConfigSet configSet;

    public bool isMove;

    private Vector3 direction;

    private float timeMoveCount;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            isMove = true;
            var maxPoint = -5000f;
            maxDistance = speed * timeMoveCount + controller.radius;
            var s = speed * timeMoveCount;
            var size = new Vector2(maxDistance, controller.GetRadius() * 2);
            foreach (var key in GameController.Directions.Keys)
            {
                float curPoint = 0;
                var pos = transform.position + maxDistance / 2 * GameController.Directions[key];
                var hitKnife = Physics2D.OverlapBoxAll(pos, size, key, GameController.ins.free_knife);
                var hitImpediment = Physics2D.OverlapBoxAll(pos, size, key, GameController.ins.impediment);
                var hitWall = Physics2D.RaycastAll(transform.position, GameController.Directions[key], 1.5f, GameController.ins.wall);
                GameHelper.DebugDrawBox(pos, size, key, new Color(Random.value, Random.value, Random.value, 1), 3);
                curPoint += hitKnife.Length * aiData.pointForKnife * configSet.numeralForKnife;
                curPoint -= (hitImpediment.Length > 0 ? 1000 : 0);
                curPoint -= (hitWall.Length > 0 ? 10000 : 0);
                Debug.Log(curPoint);
                if (curPoint > maxPoint)
                {
                    maxPoint = curPoint;
                    direction = GameController.Directions[key];
                }
            }

        }

        if (isMove)
        {
            if (timeMoveCount <= 0)
            {
                isMove = false;
                timeMoveCount = Random.Range(aiData.timeMove - 0.1f, aiData.timeMove + 0.1f);
            }
            else
            {
                timeMoveCount -= Time.deltaTime;
                transform.position += direction * speed * Time.deltaTime;
            }
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            var size1 = new Vector3(controller.radius, maxDistance);
            var size2 = new Vector3(controller.radius, maxDistance * 2);
            //var size2 = new Vector3(controller.radius, maxDistance);
            var pos = transform.position + maxDistance / 2 * GameController.Directions[0];
            var nextPos = transform.position + maxDistance * GameController.Directions[0];
            var hitKnife = Physics2D.BoxCastAll(pos, size2, 0, GameController.Directions[0], maxDistance,
                GameController.ins.free_knife);
            //Debug.DrawRay(nextPos, );
            foreach (var d in hitKnife)
            {
                Debug.Log(d.transform.position);
            }
        }

    }
}
