﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiTest : MonoBehaviour
{
    public void AddCoin()
    {
        GameManager.ins.PlayerCoin += 2000;
        UI.Ins.main.general.ShowCoin();
    }

    public void AddGem()
    {
        GameManager.ins.PlayerGem += 2000;
        UI.Ins.main.general.ShowGem();
    }

    public void AddDay()
    {
        GameManager.ins.DayConnect += 1;
    }

    public void AddMatch()
    {
        this.PostEvent(EventID.PlayMatch);
    }
    public void AddPoint()
    {
        var x = GameManager.ins.PlayerInformation;
        x.playerPoint += 20;
        GameManager.ins.PlayerInformation = x;
    }
}
