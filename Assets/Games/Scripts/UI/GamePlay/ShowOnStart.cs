﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowOnStart : MonoBehaviour
{
    public Image im;
    public List<Sprite> s;
    public Sprite start;
    public Animation anim;
    public string animName1, animName2;
    private Action callBack;

    public void OnShow(Action callBack)
    {
        this.Show();
        this.callBack = callBack;
        StartCoroutine(IStart());
    }

    private IEnumerator IStart()
    {
        var wt = new WaitForSeconds(0.5f);

        for (int i = 0; i < 3; i++)
        {
            anim.Play(animName1);
            
            im.sprite = s[i];
            im.SetNativeSize();
            yield return wt;         
        }

        AudioManager.ins.PlayAudioClip(ClipType.minionspawn);
        im.sprite = start;
        im.SetNativeSize();
        anim.Play(animName2);
    }

    public void OnComplete()
    {
        this.Hide();
        callBack?.Invoke();
    }
}
