﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIBonus : MonoBehaviour
{
    public TextMeshProUGUI rankTitle, coinBonusValue, coinRewardValue, totalCoinValue;
    public Button claimBtn, doubleBtn;
    public Text xReward;

    public UIRankPlayer uiRankPlayer;
    public TextMeshProUGUI pointRewardText;

    private int coinBonus, coinReward, totalCoin, point, xRewardValue;
    public Animation anim;
    private bool isCanGet;

    private void Start()
    {
        claimBtn.onClick.AddListener(OnClaim);
        doubleBtn.onClick.AddListener(OnClaimDouble);
    }

    private void OnClaimDouble()
    {
        if (isCanGet)
        {
            ManagerAds.Ins.ShowRewardedVideo((p) =>
            {
                AudioManager.ins.PlayAudioClip(ClipType.complete_star_2);
                AudioManager.ins.PlayMusic(MusicType.HomeMusic);
                GameManager.ins.VideoCount++;
                isCanGet = false;
                GameManager.ins.PlayerCoin += totalCoin * xRewardValue;
                UI.Ins.main.general.ShowCoin();
                anim.Play("LeaveBonus");
            });

        }
    }

    private void OnClaim()
    {
        if (isCanGet)
        {
            AudioManager.ins.PlayMusic(MusicType.HomeMusic);
            AudioManager.ins.PlayAudioClip(ClipType.complete_star_2);
            isCanGet = false;
            GameManager.ins.PlayerCoin += totalCoin;
            UI.Ins.main.general.ShowCoin();
            anim.Play("LeaveBonus");
        }
    }

    public void OnShow(int coinReward, int coinBonus,  int pointReward, int top)
    {
        this.Show();
        isCanGet = true;
        this.coinReward = coinReward;
        this.coinBonus = coinBonus;
        totalCoin = coinReward + coinBonus;
        point = pointReward;
        xRewardValue = Random.Range(2, 5);
        xReward.text = "x" + xRewardValue;
        rankTitle.text = "#" + top;


        DOVirtual.Float(0, coinBonus, 0.4f, value => { coinBonusValue.text = (int) value + ""; });
        DOVirtual.Float(0, coinReward, 0.4f, value => { coinRewardValue.text = (int) value + ""; });
        DOVirtual.Float(0, totalCoin, 0.4f, value => { totalCoinValue.text = (int) value + ""; });

        uiRankPlayer.SetUp(GameManager.ins.PlayerInformation.playerRank, GameManager.ins.PlayerInformation.playerPoint);
        DOVirtual.DelayedCall(1.5f, () =>
        {
            pointRewardText.Show();
            pointRewardText.text = "+" + point;
        });
    }

    public void OnComplete()
    {
        Poolers.ins.ClearPools();
        UI.Ins.ShowDefault();
    }
    
}
