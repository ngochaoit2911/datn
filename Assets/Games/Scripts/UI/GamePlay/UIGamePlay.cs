﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGamePlay : MonoBehaviour
{
    public UIRanking uiRanking;
    public Text timeCDText;
    private Coroutine cdCt;
    public UIKill uiKill;
    public GameObject uiVictory;
    public UIHighLight uiHighlight;
    public ShowOnStart showOnStart;

    public void OnStartNewGame()
    {
        if (cdCt != null)
        {
            StopCoroutine(cdCt);
        }
        uiHighlight.Hide();
        uiVictory.Hide();
        uiKill.OnStartNewGame();
        cdCt = StartCoroutine(ICountDown());
    }

    public void OnEndGame()
    {
        if (cdCt != null)
        {
            StopCoroutine(cdCt);
        }
        uiHighlight.Restart();
        uiRanking.StopRanking();
    }

    IEnumerator ICountDown()
    {
        var x = new WaitForSeconds(1);
        var t = 90;
        while (true)
        {
            yield return x;
            t -= 1;
            if (t > 0)
            {
                var m = TimeSpan.FromSeconds(t);
                var answer = m.Minutes + ":" + m.Seconds;            
                timeCDText.text = answer;
                if (GameController.IsCanRevival && t < 15f)
                {
                    GameController.IsCanRevival = false;
                }
            }
            else
            {
                UI.Ins.ShowOnEndGame();
                break;
            }
        }
    }

    public void ShowHighLight(int kill)
    {
        uiHighlight.OnRegister(kill);
    }

}
