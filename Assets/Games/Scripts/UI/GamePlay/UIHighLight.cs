﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class UIHighLight : MonoBehaviour
{
    public List<Sprite> sprites;
    public Image im;
    public Animation anim;
    private Queue<Action> animPlays;
    public List<string> animNames;

    private void Start()
    {
        animPlays = new Queue<Action>();
    }

    public void OnShow(int index)
    {
        this.Show();
        im.sprite = sprites[Mathf.Clamp(index - 1, 0, sprites.Count)];
        im.SetNativeSize();
        var r = Random.Range(0, animNames.Count);
        anim.Play(animNames[r]);
    }

    public void OnRegister(int index)
    {
        if (!gameObject.activeInHierarchy || !anim.isPlaying)
        {
            OnShow(index);
        }
        else
        {
            animPlays.Enqueue(() => OnShow(index));
        }
    }

    public void OnCompleteAnim()
    {
        if (animPlays.Count > 0)
        {
            animPlays.Dequeue().Invoke();
        }
        else
        {
            this.Hide();
        }
    }

    public void Restart()
    {
        animPlays?.Clear();
    }

}
