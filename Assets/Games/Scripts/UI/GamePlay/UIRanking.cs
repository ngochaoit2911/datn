﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIRanking : MonoBehaviour
{
    public List<UIRankingItem> items;
    private Coroutine rankCT, rankOnFrameCT;

    public void Generate()
    {
        rankOnFrameCT = StartCoroutine(GameHelper.EndOfFrame(Ranking, StartRanking));
    }

    public void Ranking()
    {
        var m = GameController.ins.heroInfors.OrderByDescending(s => s.heroPoint).ThenByDescending(s => s.killPoint)
            .ThenByDescending(s => s.timeDie).ToList();
        //var m = GameController.ins.activeControllers.OrderByDescending(s => s.heroInfor.heroPoint).ThenByDescending(s => s.heroInfor.killPoint)
        //    .ThenByDescending(s => s.heroInfor.timeDie).ToList();
        var heroName = m[0].heroName;

        for (int i = 0; i < items.Count; i++)
        {
            if (i < m.Count)
            {
                items[i].SetUp(m[i], i);
            }
            else
            {
                items[i].Hide();
            }
        }

        foreach (var controller in GameController.ins.activeControllers)
        {
            controller.defineID.SetCrown(controller.heroInfor.heroName == heroName);
        }

    }

    public void StartRanking()
    {
        if (rankCT != null)
        {
            StopCoroutine(rankCT);
        }
        UI.Ins.inGame.gamePlay.OnStartNewGame();
    }

    public void StopRanking()
    {
        if (rankCT != null)
        {
            StopCoroutine(rankCT);
        }
    }

    public void OnRegisterRanking()
    {
        if (rankOnFrameCT != null)
        {
            StopCoroutine(rankOnFrameCT);
        }
        rankOnFrameCT = StartCoroutine(GameHelper.EndOfFrame(Ranking));
    }

}
