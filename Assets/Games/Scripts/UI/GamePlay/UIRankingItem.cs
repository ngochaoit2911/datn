﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRankingItem : MonoBehaviour
{
    public Text rankID, pName, point;
    public Image circle, flag;

    public void SetUp(HeroInfor heroInfor, int rank)
    {
        rankID.text = "#" + (rank + 1);
        pName.text = heroInfor.heroName;
        point.text = Mathf.Max(heroInfor.heroPoint, 0) + "";
        flag.sprite = heroInfor.iconAvt;
        circle.color = heroInfor.color;
        if (heroInfor.isPlayer)
        {
            pName.color = Color.yellow;
            point.color = Color.yellow;
        }
        else
        {
            pName.color = Color.white;
            point.color = Color.white;
        }
    }

}
