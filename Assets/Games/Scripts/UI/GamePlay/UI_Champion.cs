﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UI_Champion : MonoBehaviour
{
    public Button okBtn;
    public RectTransform content;
    public GameObject championItem;
    private int coinBonus, coinReward, pointBonus, top;
    private List<UI_ChampionItem> items;
    public Animation anim;
    private int count = 0;

    private void Start()
    {
        count = 0;
        okBtn.onClick.AddListener(ShowBonus);
    }

    private void ShowBonus()
    {
        AudioManager.ins.PlayButtonClick();
        anim.Play("LeaveChampionAnim");
    }

    void OnLeave()
    {
        this.Hide();
        UI.Ins.inGame.bonus.OnShow(coinReward, coinBonus, pointBonus, top);
    }

    public bool isCanShow;
    public void OnShow()
    {
        if (isCanShow)
        {
            isCanShow = false;
            count++;
            ManagerAds.Ins.ShowBanner();
            if (count % 5 == 4)
            {
                ManagerAds.Ins.ShowInterstitial(() =>
                {
                    this.Show();
                    GameController.IsPauseGame = true;
                });
            }
            else
            {
                this.Show();
                GameController.IsPauseGame = true;
            }
        }
    }

    public void OnCallBack()
    {
        var m = GameController.ins.heroInfors.OrderByDescending(s => s.heroPoint).ThenByDescending(s => s.killPoint)
            .ThenByDescending(s => s.timeDie).ToList();
        if (m[0].isPlayer)
        {
            this.PostEvent(EventID.Top1);
        }

        items = new List<UI_ChampionItem>();

        for (int i = 0; i < m.Count; i++)
        {
            var item = Poolers.ins.GetObject(championItem, content);
            item.Cast<UI_ChampionItem>().SetUp(m[i], i + 1);

            items.Add(item.Cast<UI_ChampionItem>());

            if (m[i].isPlayer)
            {
                coinBonus = GameController.ins.rankData.coinBonusMultiKill[Mathf.Clamp(m[i].killPoint - 2, 0, 13)];

                if (m[i].rankType >= 0)
                {

                    coinReward = m[i].killPoint * GameController.ins.rankData.rankData[(int)m[i].rankType].coinRewardOnKill;
                }
                else
                {
                    coinReward = m[i].killPoint * GameController.ins.rankData.rankData[21].coinRewardOnKill;
                }

                var point = m[i].killPoint > 0
                    ? m[i].killPoint + (m[i].killPoint - 1) + (i == 0 ? 5 : i == 1 ? 2 : i == 2 ? 1 : 0)
                    : (1 + (i == 0 ? 5 : i == 1 ? 2 : i == 2 ? 1 : 0));
                var infor = GameManager.ins.PlayerInformation;
                infor.playerPoint += point;
                GameManager.ins.PlayerInformation = infor;

                pointBonus = point;
                top = i + 1;
            }
        }

        foreach (var item in items)
        {
            item.Hide();
        }

        Anim();
    }

    void Anim()
    {
        var s = DOTween.Sequence();

        foreach (var item in items)
        {
            s.Append(DOVirtual.DelayedCall(0.07f, () =>
            {
                item.Show();
                item.m_transform.DOScaleY(1.2f, 0.2f).OnComplete(() => { item.m_transform.DOScaleY(1, 0.1f); });
            }));
        }
        s.Restart();
    }

}
