﻿using Assets.Games.Photon.Scripts.Controller;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_ChampionItem : PoolItem
{
    public Text rankIngame, pName, coinValue;
    public Image rankIm, flagIm;
    public TextMeshProUGUI pRankTxt;

    public void SetUp(HeroInfor heroInfor, int index)
    {
        rankIngame.text = index + "";
        if (heroInfor.rankType >= 0)
        {
            rankIm.sprite = GameController.ins.rankData.rankIcon[(int)heroInfor.rankType / 3];
            pRankTxt.text = (((int)heroInfor.rankType) % 3 + 1) + "";

        }
        else
        {
            rankIm.sprite = GameController.ins.rankData.rankIcon[0];
            pRankTxt.text = (0) + "";

        }

        flagIm.sprite = heroInfor.iconAvt;
        pName.text = heroInfor.heroName;

        if (heroInfor.isPlayer)
        {
            rankIngame.color = Color.yellow;
            pName.color = Color.yellow;
            coinValue.color = Color.yellow;
        }
        else
        {
            rankIngame.color = Color.white;
            pName.color = Color.white;
            coinValue.color = Color.white;
        }
        var x = 0;
        if (heroInfor.rankType >= 0)
        {
            
            x = heroInfor.killPoint * GameController.ins.rankData.rankData[(int) heroInfor.rankType].coinRewardOnKill;
        }
        else
        {
            x = heroInfor.killPoint * GameController.ins.rankData.rankData[21].coinRewardOnKill;
        }

        coinValue.text = x + "";
    }

    public void Net_SetUp(HeroInfor heroInfor, int index)
    {
        rankIngame.text = index + "";
        if (heroInfor.rankType >= 0)
        {
            rankIm.sprite = Net_Controller.ins.rankData.rankIcon[(int)heroInfor.rankType / 3];
            pRankTxt.text = (((int)heroInfor.rankType) % 3 + 1) + "";

        }
        else
        {
            rankIm.sprite = Net_Controller.ins.rankData.rankIcon[0];
            pRankTxt.text = (0) + "";
        }

        flagIm.sprite = heroInfor.iconAvt;
        pName.text = heroInfor.heroName;

        if (heroInfor.isPlayer)
        {
            rankIngame.color = Color.yellow;
            pName.color = Color.yellow;
            coinValue.color = Color.yellow;
        }
        else
        {
            rankIngame.color = Color.white;
            pName.color = Color.white;
            coinValue.color = Color.white;
        }
        var x = 0;
        if (heroInfor.rankType >= 0)
        {

            x = heroInfor.killPoint * Net_Controller.ins.rankData.rankData[(int)heroInfor.rankType].coinRewardOnKill;
        }
        else
        {
            x = heroInfor.killPoint * Net_Controller.ins.rankData.rankData[21].coinRewardOnKill;
        }

        coinValue.text = x + "";
    }

}
