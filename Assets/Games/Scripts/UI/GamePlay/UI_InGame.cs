﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_InGame : MonoBehaviour
{
    public UIGamePlay gamePlay;
    public UI_Revenge revenge;
    public UI_Champion champion;
    public UIBonus bonus;

    public void ShowDefault()
    {
        gamePlay.Hide();
        revenge.Hide();
        champion.Hide();
        bonus.Hide();
    }

    public void ShowOnEndGame()
    {
        champion.OnShow();
        gamePlay.OnEndGame();
    }

    public void OnShowGamePlay()
    {
        gamePlay.Show();
        champion.isCanShow = true;
        gamePlay.uiRanking.Generate();
    }

}
