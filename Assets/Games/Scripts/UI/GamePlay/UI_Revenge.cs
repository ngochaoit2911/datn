﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Revenge : MonoBehaviour
{
    public Button revivalBtn, noBtn;
    public Image cdIm;
    private Coroutine ct;
    private int t = 0;
    public List<Sprite> s;

    private void Start()
    {
        revivalBtn.onClick.AddListener(ShowRewardVideo);
        noBtn.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayButtonClick();
            if (ct != null)
                StopCoroutine(ct);
            OnHide();
        });
    }

    private void ShowRewardVideo()
    {
        AudioManager.ins.PlayButtonClick();
        ManagerAds.Ins.ShowRewardedVideo((b) =>
        {
            if (b)
            {

                if (ct != null)
                    StopCoroutine(ct);

                GameController.ins.player.WaitSpawn();
                AudioManager.ins.PlayAudioClip(ClipType.revive);
                this.Hide();
            }
        });

    }

    public void OnShow()
    {
        this.Show();       
    }

    public void OnComplete()
    {
        if (ct != null)
        {
            StopCoroutine(ct);
        }

        ct = StartCoroutine(ICountDown());
    }

    IEnumerator ICountDown()
    {
        GameController.ins.isHasCharacterSpawn = true;
        cdIm.sprite = s[5];
        var wt = new WaitForSeconds(1);
        cdIm.sprite = s[4];
        cdIm.SetNativeSize();
        t = 4;
        yield return wt;
        while (t > 0)
        {
            
            t--;//3,2,1,0
            cdIm.sprite = s[t]; ;
            cdIm.SetNativeSize();
            yield return wt;
        }

        OnHide();
    }

    public void OnHide()
    {
        gameObject.Hide();
        cdIm.sprite = s[4];
        cdIm.SetNativeSize();
        UI.Ins.ShowOnEndGame();
    }
}
