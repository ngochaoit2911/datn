﻿using System.Collections;
using UnityEngine;

namespace Assets.Games.Scripts.UI.Login
{
    public class LoginController : MonoBehaviour
    {

        public LoginPopup login;
        public RegisterPopup register;
        public static LoginController ins;
        public GameObject loadingGo;

        private void Awake()
        {
            ins = this;
            DatabaseManager.Init();
        }

        public void Start()
        {
            login.OnShow();
            register.Hide();
        }

    }
}