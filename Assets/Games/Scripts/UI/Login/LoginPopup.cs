﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Games.Scripts.UI.Login
{
    public class LoginPopup : MonoBehaviour
    {

        public InputField userNameTxt;
        public InputField passwordTxt;
        public Button loginBtn, registerBtn;

        public GameObject loginFailGo;

        private void Start()
        {
            loginBtn.onClick.AddListener(Login);
            registerBtn.onClick.AddListener(ShowRegister);
        }

        public void OnShow()
        {
            this.Show();
            loginFailGo.Hide();
            userNameTxt.text = "";
            passwordTxt.text = "";
        }

        private void ShowRegister()
        {
            Debug.Log("AAAAAAAA");
            LoginController.ins.register.OnShow();
            this.Hide();
        }

        public void Login()
        {
            StartCoroutine(ILogin());
        }

        IEnumerator ILogin()
        {
            DatabaseManager.IsLoginComplete = false;
            LoginController.ins.loadingGo.Show();
            DatabaseManager.Login(userNameTxt.text, passwordTxt.text, r =>
            {

                DatabaseManager.UserName = userNameTxt.text;
                Debug.Log("Login Success");

                GameManager.ins.LoadDataLogin(() =>
                {
                    DatabaseManager.IsLoginComplete = true;

                }, () =>
                {
                    DatabaseManager.IsLoginComplete = true;
                    DatabaseManager.UserName = "";
                    loginFailGo.Show();
                });
            }, e =>
            {
                Debug.Log("Login Fail: " + userNameTxt.text + "  " + passwordTxt.text);
                DatabaseManager.IsLoginComplete = true;
                DatabaseManager.UserName = "";
                loginFailGo.Show();
            });

            yield return new WaitUntil(() => DatabaseManager.IsLoginComplete);

            if (DatabaseManager.UserName != "")
            {
                var l = SceneManager.LoadSceneAsync(1);
                while (!l.isDone)
                {
                    yield return null;
                }
                LoginController.ins.loadingGo.Hide();
            }
            else
            {
                LoginController.ins.loadingGo.Hide();
            }
        }

    }
}