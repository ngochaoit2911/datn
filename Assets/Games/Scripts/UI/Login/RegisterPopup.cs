﻿using Assets.Games.Photon.Scripts.Controller;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Games.Scripts.UI.Login
{
    public class RegisterPopup : MonoBehaviour
    {

        public InputField userNameTxt;
        public InputField passwordTxt;
        public InputField countryTxt;

        public Button loginBtn, registerBtn;
        public LoopVerticalScrollRect scrollView;
        public List<CountryBotName> temp;
        public GameObject registerFailGo;

        public int index;

        internal void OnChangeSelected(int index)
        {
            this.index = index;
            scrollView.RefreshCells();
            countryTxt.text = temp[index].countryCode;
            countryTxt.ForceLabelUpdate();
        }

        private void Start()
        {
            loginBtn.onClick.AddListener(ShowLogin);
            registerBtn.onClick.AddListener(Register);
            countryTxt.onEndEdit.AddListener(FindWithString);

        }

        public void OnShow()
        {
            this.Show();
            temp = Net_Controller.ins.botData.botNames;
            scrollView.totalCount = temp.Count;

            userNameTxt.text = "";
            countryTxt.text = "";
            passwordTxt.text = "";
            userNameTxt.ForceLabelUpdate();
            countryTxt.ForceLabelUpdate();
            passwordTxt.ForceLabelUpdate();
            scrollView.ScrollToCell(0, 1000);
            scrollView.RefillCells();
            registerFailGo.Hide();
            //selectedIm.SetParent(scrollView);
        }

        public void FindWithString(string m)
        {
            temp = Net_Controller.ins.botData.botNames.Where(s => Contains(s.countryName, m) || Contains(s.countryCode, m)).ToList();
            ShowBotName(temp, 0);
        }

        public void ShowBotName(List<CountryBotName> botNames, int index)
        {
            temp = botNames;

            scrollView.totalCount = temp.Count;
            scrollView.RefillCells();
            scrollView.ScrollToCell(index, 600);
        }


        bool Contains(string s1, string s2)
        {
            s1 = s1.ToLower();
            s2 = s2.ToLower();
            var arr = s2.ToArray();
            foreach (var a in arr)
            {
                if (s1.Contains(a))
                {
                    s1.Replace(a.ToString(), "");
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        private void Register()
        {
            StartCoroutine(IRegister());
        }

        IEnumerator IRegister()
        {
            DatabaseManager.IsLoginComplete = false;
            LoginController.ins.loadingGo.Show();
            DatabaseManager.Register(userNameTxt.text, passwordTxt.text, r =>
            {        
                DatabaseManager.UserName = r.Username;
                Debug.Log("Register Success");

                GameManager.ins.InitOnRegister(DatabaseManager.UserName, countryTxt.text, () =>
                {
                    DatabaseManager.IsLoginComplete = true;
                }, () =>
                {
                    DatabaseManager.IsLoginComplete = true;
                    DatabaseManager.UserName = "";
                    registerFailGo.Show();
                });
            }, e =>
            {
                DatabaseManager.IsLoginComplete = true;
                DatabaseManager.UserName = "";
                registerFailGo.Show();
            });

            yield return new WaitUntil(() => DatabaseManager.IsLoginComplete);
            if (DatabaseManager.UserName != "")
            {
                var l = SceneManager.LoadSceneAsync(1);
                while (!l.isDone)
                {
                    yield return null;    
                }
                LoginController.ins.loadingGo.Hide();

            }
            else
            {
                LoginController.ins.loadingGo.Hide();
            }
        }

        public void ShowLogin()
        {
            this.Hide();
            LoginController.ins.login.OnShow();
        }
    }
}