﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Games.Scripts.UI.Login
{
    public class Register_FlagItem : MonoBehaviour
    {
        public Image im;
        public GameObject selected;
        public Button btn;
        private int index;

        private void Start()
        {
            btn.onClick.AddListener(OnSelected);
        }

        private void OnSelected()
        {
            selected.Show();
            LoginController.ins.register.OnChangeSelected(index);
        }

        void ScrollCellIndex(int idx)
        {
            im.sprite = LoginController.ins.register.temp[idx].icon;
            index = idx;
            if (idx == LoginController.ins.register.index)
            {
                selected.Show();
            }
            else
            {
                selected.Hide();
            }
        }
    }
}