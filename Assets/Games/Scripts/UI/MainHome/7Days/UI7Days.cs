﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI7Days : MonoBehaviour
{
    public List<UI7Days_Item> items;
    public Button closeBtn, claimBtn;
    public int day;
    public UI7Days_Reward reward;

    private void Start()
    {
        closeBtn.onClick.AddListener(Close);
        claimBtn.onClick.AddListener(ClaimReward);
    }

    private void ClaimReward()
    {
        AudioManager.ins.PlayAudioClip(ClipType.complete_star_2);
        var d = GameManager.ins.DailyGifts;
        var c = GameManager.ins.DayConnect;
        d[c] = true;
        GameManager.ins.DailyGifts = d;
        Reward();
        ShowInfor();
    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.OnClose(() =>
        {
            this.Hide();
            UI.Ins.main.popup.@group.alpha = 1;
            UI.Ins.main.popup.Hide();
        });
    }

    private void OnEnable()
    {
        ShowInfor();
    }

    private void ShowInfor()
    {
        GameManager.ins.CheckNewDay();
        var d = GameManager.ins.DailyGifts;
        var c = GameManager.ins.DayConnect;
        day = c;
        if (!d[c])
        {
            claimBtn.Show();
        }
        else
        {
            claimBtn.Hide();
        }

        foreach (var item in items)
        {
            item.SetUp();
        }
    }

    public void Reward()
    {
        switch (day)
        {
            case 0:

                GameManager.ins.PlayerCoin += 2000;
                UI.Ins.main.general.ShowCoin();
                reward.ShowReward(reward.rewardCoinGem, true, false, 2000);

                break;
            case 1:

                var pKnifes = GameManager.ins.PlayerKnifes;
                var k = pKnifes.FindIndex(s => s.knifeType == KnifeType.DoubleAxe);
                var infor = pKnifes[k];
                infor.isUnlock = true;
                pKnifes[k] = infor;
                GameManager.ins.PlayerKnifes = pKnifes;

                reward.ShowReward(reward.rewardBlade, false, false, 0);


                break;

            case 2:

                GameManager.ins.PlayerGem += 50;
                UI.Ins.main.general.ShowGem();
                reward.ShowReward(reward.rewardCoinGem, false, true, 50);

                break;

            case 3:

                var pHeros = GameManager.ins.PlayerMasks;
                var m = pHeros.FindIndex(s => s.maskType == MaskType.Okay);
                var i = pHeros[m];
                i.isUnlock = true;
                pHeros[m] = i;
                GameManager.ins.PlayerMasks = pHeros;
                reward.ShowReward(reward.rewardHero, false, false, 0);

                break;

            case 4:
                GameManager.ins.PlayerCoin += 30000;
                UI.Ins.main.general.ShowCoin();
                reward.ShowReward(reward.rewardCoinGem, true, false, 30000);
                break;

            case 5:

                GameManager.ins.PlayerGem += 200;
                UI.Ins.main.general.ShowGem();
                reward.ShowReward(reward.rewardCoinGem, false, true, 200);

                break;

            case 6:
                break;

        }
    }

    void RewardSet()
    {
        var pKnifes = GameManager.ins.PlayerKnifes;
        var k = pKnifes.FindIndex(s => s.knifeType == KnifeType.Yolo);
        var infor = pKnifes[k];
        infor.isUnlock = true;
        pKnifes[k] = infor;
        GameManager.ins.PlayerKnifes = pKnifes;

        var pHeros = GameManager.ins.PlayerMasks;
        var m = pHeros.FindIndex(s => s.maskType == MaskType.KiddingMe);
        var i = pHeros[m];
        i.isUnlock = true;
        pHeros[m] = i;
        GameManager.ins.PlayerMasks = pHeros;
        reward.ShowReward(reward.rewardSet, false, false, 0);
    }

}
