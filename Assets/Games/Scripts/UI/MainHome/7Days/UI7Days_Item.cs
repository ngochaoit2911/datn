﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI7Days_Item : MonoBehaviour
{
    public GameObject tick;
    public Image box;
    //public Button selectBtn;
    public int dayIndex;

    [ContextMenu("Change Day")]
    void ChangDay()
    {
        var t = GetComponentInChildren<TextMeshProUGUI>();
        //selectBtn = GetComponent<Button>();
        t.text = "DAY " + int.Parse(transform.name.Replace("Day", ""));
        dayIndex = int.Parse(transform.name.Replace("Day", "")) - 1;
    }

    private void Start()
    {
        //selectBtn.onClick.AddListener(SelectDay);
    }

    private void SelectDay()
    {
        var d = GameManager.ins.DailyGifts;
        var c = GameManager.ins.DayConnect;
        if (!d[dayIndex] && c >= dayIndex)
        {

        }
    }

    public void SetUp()
    {
        var d = GameManager.ins.DailyGifts;
        var c = GameManager.ins.DayConnect;
        if (dayIndex == 6)
        {
            if (c == dayIndex)
            {
                box.DOFade(1, 0.1f);
                if (d[6])
                {
                    tick.Show();
                }
                else
                {
                    tick.Hide();
                }
            }
            else
            {
                box.DOFade(0, 0.1f);
                if (d[6])
                {
                    tick.Show();
                }
                else
                {
                    tick.Hide();
                }
            }
        }
        else
        {
            if (c == dayIndex)
            {
                
                if (d[dayIndex])
                {
                    box.DOFade(0, 0.1f);
                    tick.Show();
                }
                else
                {
                    box.DOFade(1, 0.1f);
                    tick.Hide();
                }
            }
            else
            {
                box.DOFade(0, 0.1f);
                if (d[dayIndex])
                {
                    tick.Show();
                }
                else
                {
                    tick.Hide();
                }
            }
        }
    }

}
