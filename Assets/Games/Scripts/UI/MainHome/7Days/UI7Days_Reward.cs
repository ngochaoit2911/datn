﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UI7Days_Reward : MonoBehaviour
{
    public GameObject rewardCoinGem, rewardBlade, rewardHero, rewardSet;
    public Button closeBtn;
    public CanvasGroup group;

    private void Start()
    {
        closeBtn.onClick.AddListener(CloseReward);
    }

    private void CloseReward()
    {
        AudioManager.ins.PlayButtonClick();
        @group.DOFade(0, 0.2f).OnComplete(() =>
        {
            this.Hide();
            @group.alpha = 1;
        });
    }

    public void ShowReward(GameObject obj ,bool isCoin, bool isGem, int value)
    {
        this.Show();
        obj.Show();
        closeBtn.Hide();
        Anim(obj);
        if (isCoin || isGem)
        {
            rewardCoinGem.GetComponent<UI_ChangeText>().SetUp(isCoin, value);
        }
    }

    public void Anim(GameObject obj)
    {
        var group = obj.GetComponent<CanvasGroup>();
        var t = obj.transform;
        var btn = closeBtn.gameObject.transform;
        group.alpha = 0;
        t.localScale = Vector3.one ;
        var s = DOTween.Sequence();
        s.Join(group.DOFade(1, 0.2f))
            .Join(t.DOScale(Vector3.one * 1.8f, 0.2f))
            .Append(t.DOScale(Vector3.one * 1.5f, 0.1f))
            .Append(DOVirtual.DelayedCall(0.1f, () =>
            {
                btn.Show();
                btn.localScale = Vector3.one * 1.3f;
                btn.DOScale(Vector3.one, 0.1f);
            }));
    }

}
