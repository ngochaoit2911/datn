﻿
using UnityEngine;
using UnityEngine.UI;

public class UI_ChangeText : MonoBehaviour
{
    public Text valueTxt;
    public Image icon;

    public void SetUp(bool isCoin, int value)
    {
        icon.sprite = isCoin ? GameController.ins.iconData.coinIcon : GameController.ins.iconData.gemIcon;
        icon.SetNativeSize();
        valueTxt.text = "+" + value;
    }

}
