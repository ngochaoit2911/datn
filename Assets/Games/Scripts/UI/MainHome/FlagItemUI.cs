﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlagItemUI : MonoBehaviour
{
    public Image im;
    public GameObject selected;
    public Button btn;
    private int index;

    private void Start()
    {
        btn.onClick.AddListener(OnSelected);
    }

    private void OnSelected()
    {
        selected.Show();
        UI.Ins.main.popup.rename.OnChangeSelected(index);
    }

    void ScrollCellIndex(int idx)
    {
        im.sprite = UI.Ins.main.popup.rename.temp[idx].icon;
        index = idx;
        if (idx == UI.Ins.main.popup.rename.index)
        {
            selected.Show();
        }
        else
        {
            selected.Hide();
        }
    }
}
