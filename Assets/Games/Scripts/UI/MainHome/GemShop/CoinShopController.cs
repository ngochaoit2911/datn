﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CoinShopController : MonoBehaviour
{
    public GameObject coinStoreItem;
    public RectTransform content;
    public Button closeBtn;
    private List<Transform> l;

    public List<int> v = new List<int>
    {
        5,
        10,
        20,
        50,
        100
    };

    public List<int> c = new List<int>
    {
        300,
        650,
        1500,
        4000,
        9000
    };

    private void Start()
    {
        closeBtn.onClick.AddListener(Close);
    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.OnClose(() =>
        {
            this.Hide();
            UI.Ins.main.popup.@group.alpha = 1;
            UI.Ins.main.popup.Hide();
        });
    }

    private void OnEnable()
    {
        OnShow();
    }

    void OnShow()
    {
        Poolers.ins.ClearItem(coinStoreItem);
        l = new List<Transform>();
        for (int i = 0; i < 5; i++)
        {
            var b = Poolers.ins.GetObject(coinStoreItem, content);
            b.Cast<CoinStoreItem>().SetUp(v[i], c[i]);
            l.Add(b.m_transform);
        }

        foreach (var t in l)
        {
            t.Hide();
        }

    }

    public void UpdateScence()
    {
        var x = 0;
        foreach (var m in l)
        {
            m.GetComponent<CoinStoreItem>().Change(v[x]);
            x++;
        }
    }

    public void OnShowItem()
    {
        var s = DOTween.Sequence();
        foreach (var t in l)
        {
            s.Append(DOVirtual.DelayedCall(0.07f, () =>
            {
                t.Show();
                t.DOScaleY(1.2f, 0.2f).OnComplete(() => { t.DOScaleY(1, 0.1f); });
            }));
            //
        }

        s.Restart();
    }

}
