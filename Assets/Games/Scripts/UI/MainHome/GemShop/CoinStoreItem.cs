﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CoinStoreItem : PoolItem
{
    public TextMeshProUGUI coinValue, gemValue;
    public Button buyBtn;
    private int coinReward, gemBuy;
    public Image im;

    void Start()
    {
        buyBtn.onClick.AddListener(Buy);
    }

    private void Buy()
    {
        AudioManager.ins.PlayAudioClip(ClipType.purchase);
        if (GameManager.ins.PlayerGem >= gemBuy)
        {
            GameManager.ins.PlayerCoin += coinReward;
            GameManager.ins.PlayerGem -= gemBuy;
            UI.Ins.main.general.ShowCoin();
            UI.Ins.main.general.ShowGem();
        }
    }

    public void SetUp(int v, int c)
    {
        gemBuy = v;
        coinReward = c;

        coinValue.text = coinReward + "";
        gemValue.text = gemBuy + "";

        im.sprite = GameManager.ins.PlayerGem >= gemBuy
            ? GameController.ins.iconData.canBuyBtn : GameController.ins.iconData.cantBuyBtn;
    }

    public void Change(int v)
    {
        im.sprite = GameManager.ins.PlayerGem >= v
            ? GameController.ins.iconData.canBuyBtn
            : GameController.ins.iconData.cantBuyBtn;
    }

}
