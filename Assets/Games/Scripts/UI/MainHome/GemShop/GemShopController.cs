﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GemShopController : MonoBehaviour
{
    public GameObject gemStoreItem;
    public RectTransform content;
    public Button closeBtn;
    public IAPData data;
    private List<Transform> l;

    private void Start()
    {
        closeBtn.onClick.AddListener(Close);
    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.OnClose(() =>
        {
            this.Hide();
            UI.Ins.main.popup.@group.alpha = 1;
            UI.Ins.main.popup.Hide();
        });
    }

    private void OnEnable()
    {
        OnShow();
    }

    void OnShow()
    {
        Poolers.ins.ClearItem(gemStoreItem);
        var count = 0;
        l = new List<Transform>();

        foreach (var iapInfor in data.iapData)
        {
            if (count >= 5)
            {
                break;                
            }
            var b = Poolers.ins.GetObject(gemStoreItem, content);
            b.Cast<GemStoreItem>().SetUp(iapInfor);
            l.Add(b.m_transform);
            
            count++;
        }

        foreach (var t in l)
        {
            t.Hide();
        }

    }

    public void OnShowItem()
    {
        var s = DOTween.Sequence();
        foreach (var t in l)
        {
            s.Append(DOVirtual.DelayedCall(0.07f, () =>
            {
                t.Show();
                t.DOScaleY(1.2f, 0.2f).OnComplete(() => { t.DOScaleY(1, 0.1f); });
            }));
            //
        }

        s.Restart();
    }

}
