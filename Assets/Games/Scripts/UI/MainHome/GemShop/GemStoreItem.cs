﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class GemStoreItem : PoolItem
{

    public Image icon;
    public TextMeshProUGUI value, cost;
    public IAPButton iapBtn;
    private IAPInfor infor;

    public void SetUp(IAPInfor infor)
    {
        this.infor = infor;
        iapBtn.productId = infor.id;
        //iapBtn.UpdateText();
        iapBtn.onPurchaseComplete.RemoveAllListeners();
        iapBtn.onPurchaseComplete.AddListener(OnPurchaseComplete);
        icon.sprite = infor.icon;
        icon.SetNativeSize();
        value.text = infor.getValue + "";
        cost.text = infor.cost + "$";
    }

    private void OnPurchaseComplete(Product arg0)
    {
        GameManager.ins.PlayerGem += infor.getValue;
        UI.Ins.main.general.ShowGem();
    }
}
