﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KnifeInShop : PoolItem
{
    public Image icon;
    public int index;
    private KnifeInfor knife;

    public void SetUp(KnifeType type, int index, Transform target)
    {
        this.index = index;
        knife = GameController.ins.knifeData.knifeData.Find(s => s.knifeType == type);
        icon.sprite = knife.icon;
        icon.SetNativeSize();


        var x = Mathf.Sin(index * Mathf.PI / Config.KNIFE_UI_COUNT * 2) *
                Config.BASE_RADIUS_IN_UI;
        var y = Mathf.Cos(index * Mathf.PI / Config.KNIFE_UI_COUNT * 2) *
                Config.BASE_RADIUS_IN_UI;
        m_transform.localPosition = new Vector3(x, y);
        var d = m_transform.position - target.position;
        m_transform.up = d.normalized;

        var localRotation = m_transform.localRotation;
        localRotation.x = 0;
        m_transform.localRotation = localRotation;
        m_transform.localScale = new Vector3(1, 1, 1) * 0.4f;
    }

    public void SetUp(KnifeType type)
    {
        knife = GameController.ins.knifeData.knifeData.Find(s => s.knifeType == type);
        icon.sprite = knife.icon;
        icon.SetNativeSize();
        m_transform.localScale = new Vector3(1, 1, 1) * 0.4f;
        if (!knife.isRotate)
        {
            var d = m_transform.position - m_transform.parent.position;
            m_transform.up = d.normalized;
        }
    }

    public void SetUpY(int y)
    {
        m_transform.localScale = new Vector3(1, y, 1) * 0.4f;
    }

    public void Move(Transform target, float radius)
    {
        var x = Mathf.Sin(index * Mathf.PI / Config.KNIFE_UI_COUNT * 2) *
                radius;
        var y = Mathf.Cos(index * Mathf.PI / Config.KNIFE_UI_COUNT * 2) *
                radius;
        m_transform.localPosition = new Vector3(x, y);
    }

    void Update()
    {
        if (knife.isRotate)
        {
            m_transform.Rotate(Vector3.forward * Config.SPEED_ROTATE_CHILD * Time.deltaTime);
        }
    }
}
