﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MatchingController : MonoBehaviour
{
    public TextMeshProUGUI titleMatching;
    public RectTransform topContent, middleContent, botContent;
    public GameObject matchingItem;
    public List<HeroInfor> enemies;
    public int characterCount;
    private int joinedCharacterCount;
    public Text tipTxt;
    public TipData tip;

    private Coroutine startGameCT;

    public void AddCountryEnemy()
    {
        joinedCharacterCount++;
        if (joinedCharacterCount == characterCount)
        {
            WaitForStartGame();
        }
    }

    public void WaitForStartGame()
    {
        DOVirtual.DelayedCall(0.5f, () =>
        {                      
            UI.Ins.main.home.LeaveToIngame();
        });
    }

    public void StartGame()
    {
        if (startGameCT != null)
            StopCoroutine(startGameCT);
        GameController.ins.CacheOnStartGame(enemies);
        this.Hide();
    }

    public void SetUp()
    {
        Poolers.ins.ClearItem(matchingItem);
        GeneratePlayer();
        var r = Random.Range(0, tip.tipData.Count);
        var s = tip.tipData[r];
        tipTxt.text = "";
        tipTxt.DOText(s, 2f);

        switch (characterCount)
        {
            case 6:
            case 7:
            case 8:
                var x2 = characterCount / 2;
                var x1 = characterCount - x2;
                topContent.Show();
                botContent.Show();
                middleContent.Hide();

                topContent.localScale = Vector3.one;
                botContent.localScale = Vector3.one;

                topContent.ChangePosY(40);
                botContent.ChangePosY(-100);

                for (int i = 0; i < x1; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, topContent);
                    p.Cast<MatchingItem>().SetUp(enemies[i]);
                }

                for (int i = 0; i < x2; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, botContent);
                    p.Cast<MatchingItem>().SetUp(enemies[x1 + i]);
                }

                break;
            case 9:
            case 10:
            case 11:
            case 12:
                topContent.Show();
                botContent.Show();
                middleContent.Show();

                topContent.localScale = Vector3.one;
                botContent.localScale = Vector3.one;
                middleContent.localScale = Vector3.one;

                topContent.ChangePosY(80);
                middleContent.ChangePosY(-30);
                botContent.ChangePosY(-140);

                var x4 = characterCount / 3;
                var x3 = characterCount - x4 * 2;
                for (int i = 0; i < x3; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, topContent);
                    p.Cast<MatchingItem>().SetUp(enemies[i]);
                }

                for (int i = 0; i < x4; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, botContent);
                    p.Cast<MatchingItem>().SetUp(enemies[x3 + i]);
                }

                for (int i = 0; i < x4; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, middleContent);
                    p.Cast<MatchingItem>().SetUp(enemies[x3 + x4 + i]);
                }

                break;
            case 13:
            case 14:
            case 15:
                topContent.Show();
                botContent.Show();
                middleContent.Show();

                topContent.localScale = Vector3.one * 0.8f;
                botContent.localScale = Vector3.one * 0.8f;
                middleContent.localScale = Vector3.one * 0.8f;

                topContent.ChangePosY(70);
                middleContent.ChangePosY(-30);
                botContent.ChangePosY(-130);

                var x8 = (characterCount + 1) / 3;
                var x7 = characterCount - x8 * 2;
                for (int i = 0; i < x7; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, topContent);
                    p.Cast<MatchingItem>().SetUp(enemies[i]);
                }

                for (int i = 0; i < x8; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, botContent);
                    p.Cast<MatchingItem>().SetUp(enemies[x7 + i]);
                }

                for (int i = 0; i < x8; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, middleContent);
                    p.Cast<MatchingItem>().SetUp(enemies[x7 + x8 + i]);
                }
                break;
            case 16:
                topContent.Show();
                botContent.Show();
                middleContent.Show();

                topContent.localScale = Vector3.one * 0.68f;
                botContent.localScale = Vector3.one * 0.68f;
                middleContent.localScale = Vector3.one * 0.68f;

                topContent.ChangePosY(70);
                middleContent.ChangePosY(-30);
                botContent.ChangePosY(-130);

                var x6 = characterCount / 3;
                var x5 = characterCount - x6 * 2;
                for (int i = 0; i < x5; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, topContent);
                    p.Cast<MatchingItem>().SetUp(enemies[i]);
                }

                for (int i = 0; i < x6; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, botContent);
                    p.Cast<MatchingItem>().SetUp(enemies[x5 + i]);
                }

                for (int i = 0; i < x6; i++)
                {
                    var p = Poolers.ins.GetObject(matchingItem, middleContent);
                    p.Cast<MatchingItem>().SetUp(enemies[x5 + x6 + i]);
                }

                break;
        }

        if (startGameCT != null)
        {
            StopCoroutine(startGameCT);
        }

        startGameCT = StartCoroutine(IStartFinding());

    }

    public void GeneratePlayer()
    {
        enemies = new List<HeroInfor>();
        GameController.ins.colorData.colors.Shuffle();
        var p = GameManager.ins.PlayerInformation;
        var pRank = (int)GameManager.ins.PlayerInformation.playerRank;
        if (p.countryCode == p.countryName)
        {
            p.countryName = GameController.ins.botData.botNames.Find(s => s.countryCode == p.countryCode).countryName;
            GameManager.ins.PlayerInformation = p;
        }

        var pCountry = GameController.ins.botData.botNames.Find(s => s.countryName == p.countryName);
        
        enemies.Add(new HeroInfor
        {
            rankType = p.playerRank,
            heroCountry = pCountry.countryName,
            knifeType = p.playerEquipKnife,
            maskType = p.playerEquipMask,
            iconAvt = pCountry.icon,
            heroName = p.playerName,
            heroPoint = 0,
            isPlayer = true,
            color = GameController.ins.colorData.colors[0],
            teamId = 0

        });
        characterCount = Mathf.Clamp(pRank / 2 + 6, 6, 17);
        var indexRank = Mathf.Clamp(pRank / 3, 0, 6);
        var rankCombo = GameController.ins.rankselectCombo.data[indexRank];
        joinedCharacterCount = 0;

        for (int i = 1; i < characterCount; i++)
        {
            var index = Random.Range(0, GameController.ins.botData.botNames.Count);
            var botCountry = GameController.ins.botData.botNames[index];
            var botName = Random.Range(0, botCountry.botName.Count);
            var rank = (RankType)Mathf.Clamp(Random.Range(pRank - 1, pRank + 2), -1, 20);
            var x1 = rankCombo.maskTypes[Random.Range(0, rankCombo.maskTypes.Count)];
            var x2 = rankCombo.knifeTypes[Random.Range(0, rankCombo.knifeTypes.Count)];
            enemies.Add(new HeroInfor
            {
                heroCountry = botCountry.countryName,
                heroName = botCountry.botName[botName],
                heroPoint =  0,
                rankType = rank,
                knifeType = x2,
                maskType = x1,
                isPlayer = false,
                iconAvt = botCountry.icon,
                color = GameController.ins.colorData.colors[i],
                teamId = i
            });
        }

        enemies.Shuffle();
    }

    private IEnumerator IStartFinding()
    {
        var count = 0.5f;
        var timeToSwitch = 0.5f;
        titleMatching.text = "Now Matching.";
        var index = 0;
        bool isCanChange = false;

        while (true)
        {
            if (count <= 0)
            {
                isCanChange = true;
                index++;
                count = Random.Range(timeToSwitch - 0.1f, timeToSwitch + 0.1f);
            }
            else
            {
                count -= Time.deltaTime;
            }

            if (isCanChange)
            {
                isCanChange = false;
                var x = index % 3;
                switch (x)
                {
                    case 0:
                        titleMatching.text = "Now Matching.";
                        break;
                    case 1:
                        titleMatching.text = "Now Matching..";
                        break;
                    default:
                        titleMatching.text = "Now Matching...";
                        break;
                }
            }

            yield return null;
        }
    }

    public void OnComplete()
    {
        SetUp();
    }

}
