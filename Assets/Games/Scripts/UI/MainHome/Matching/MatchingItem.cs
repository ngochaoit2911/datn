﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MatchingItem : PoolItem
{
    public Image flagIm;

    public void SetUp(HeroInfor heroInfor)
    {
        flagIm.sprite = GameController.ins.iconData.baseFlag;
        if (!heroInfor.isPlayer)
        {
            var r = Random.Range(1f, 4f);
            DOVirtual.DelayedCall(r, () =>
            {
                flagIm.sprite = heroInfor.iconAvt;
                SetUpCountry();
            });
        }
        else
        {
            flagIm.sprite = heroInfor.iconAvt;
            SetUpCountry();
        }
    }

    public void SetUpCountry()
    {
        UI.Ins.main.popup.matching.AddCountryEnemy();
    }

}
