﻿using System.Collections;
using System.Collections.Generic;
using Assets.Games.Photon.Scripts;
using DG.Tweening;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Games.Scripts.UI.MainHome.Matching
{
    public class Net_MatchingController : MonoBehaviourPunCallbacks
    {
        private int characterCount;
        public float timeChangeLoading = 0.4f;
        public int timeWaitingForMatching = 10;
        private float timeCount = 0;
        public TextMeshProUGUI loadingTxt;
        public TextMeshProUGUI countTxt;

        private ExitGames.Client.Photon.Hashtable _myCustomProperty = new ExitGames.Client.Photon.Hashtable();
        public void OnShow()
        {
            this.Show();
            count = 0;
            countTxt.text = "...";
            if (PhotonNetwork.IsConnected)
            {
                if (PhotonNetwork.InLobby)
                {
                    PhotonNetwork.JoinRandomRoom(new ExitGames.Client.Photon.Hashtable {
                        {EventGameCode.TeamBattle, false }
                    }, 0);
                }
                else
                {
                    PhotonNetwork.JoinLobby();
                    Debug.Log("join Lobby");
                }
            }
        }

        IEnumerator ILoading()
        {
            var wt = new WaitForSeconds(timeChangeLoading);
            loadingTxt.text = "Loading.";
            countTxt.text = 1 + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;
            var c = 0;
            var l = new List<string> { "Loading.", "Loading..", "Loading..." };
            while (true)
            {
                loadingTxt.text = l[c];
                    countTxt.text = PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;
                yield return wt;
                c = ++c % 3;
            }
        }

        IEnumerator IWaiting()
        {
            timeCount = timeWaitingForMatching;
            while (timeCount > 0)
            {
                yield return null;
                timeCount -= Time.deltaTime;
                if (PhotonNetwork.CurrentRoom != null)
                    if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
                    {
                        if (PhotonNetwork.IsMasterClient)
                        {
                            StopAllCoroutines();
                            RoomStatus.ins.OnStartGame(characterCount);
                            PhotonNetwork.LoadLevel(2);
                        }
                    }
            }


            if (PhotonNetwork.IsMasterClient)
            {
                StopAllCoroutines();
                RoomStatus.ins.OnStartGame(characterCount);
                PhotonNetwork.LoadLevel(2);
            }
        }
        int count = 0;
        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);
            if (count == 0)
            {
                PhotonNetwork.JoinRandomRoom(new ExitGames.Client.Photon.Hashtable {
                        {EventGameCode.TeamBattle, false }
                    }, 0);
                count++;
            }
            else
            {
                var p = GameManager.ins.PlayerInformation;
                var pRank = (int)p.playerRank;
                characterCount = Mathf.Clamp(pRank / 2 + 6, 6, 17);
                var options = new RoomOptions
                {
                    MaxPlayers = (byte)characterCount,
                    PlayerTtl = 1000,
                    BroadcastPropsChangeToAll = true,
                    IsOpen = true,
                    IsVisible = true,
                    CustomRoomPropertiesForLobby = new string[] {EventGameCode.TeamBattle},
                    CustomRoomProperties = new ExitGames.Client.Photon.Hashtable {
                        {EventGameCode.TeamBattle, false }
                    }
                };

                PhotonNetwork.CreateRoom(p.playerName + Random.Range(0, 999), options, TypedLobby.Default);
            }


        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            var p = GameManager.ins.PlayerInformation;
            _myCustomProperty = new ExitGames.Client.Photon.Hashtable
                    {
                        { "PlayerName", p.playerName },
                        { "PlayerCountry", p.countryCode },
                        { "PlayerKnife", (int)p.playerEquipKnife },
                        { "PlayerMask", (int)p.playerEquipMask },
                        { "PlayerRank", (int)p.playerRank }
                    };

            PhotonNetwork.LocalPlayer.SetCustomProperties(_myCustomProperty);
            StartCoroutine(IWaiting());
            StartCoroutine(ILoading());


        }

        public override void OnCreatedRoom()
        {
            base.OnCreatedRoom();
        }

        public override void OnJoinedLobby()
        {
            base.OnJoinedLobby();
            if(this.isActiveAndEnabled)
                PhotonNetwork.JoinRandomRoom(new ExitGames.Client.Photon.Hashtable {
                        {EventGameCode.TeamBattle, false }
                    }, 0);

            //PhotonNetwork.GetCustomRoomList()
        }
    }
}