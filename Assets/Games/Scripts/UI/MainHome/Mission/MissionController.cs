﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionController : MonoBehaviour
{
    public List<MissionItem> missionItem;
    public Button closeBtn;
    public MissionReward reward;

    public void OnRegister()
    {
        closeBtn.onClick.AddListener(Close);

        

    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.OnClose(() =>
        {
            this.Hide();
            UI.Ins.main.popup.@group.alpha = 1;
            UI.Ins.main.popup.Hide();
        });
    }

    void OnEnable()
    {
        OnShow();
    }

    public void OnShow()
    {
        var count = 0;
        var missions = GameManager.ins.MissionInfor;
        foreach (var item in missionItem)
        {
            item.SetUpMission(missions[count]);
            count++;
        }
    }

}
