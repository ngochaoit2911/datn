﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MissionItem : PoolItem
{
    public Image bannerIm, claimCoinBtnIm, claimGemBtnIm;
    public Image foreGroundIm;
    public TextMeshProUGUI description, valueInSlider, coinValue, gemValue;
    public GameObject claimedGO;
    public Button getCoinBtn, getGemBtn;

    public void SetUpMission(PlayerMissionInfor infor)
    {
        var mission = GameController.ins.missionDatas[infor.levelOfMission].missionData
            .Find(s => s.missionType == infor.missionType);

        claimedGO.Hide();
        description.text = mission.missionDescription.Replace("@", mission.valueToGet + "");
        var b = false;
        var x = (float)infor.missionValue / mission.valueToGet;
        foreGroundIm.DOFillAmount(x, 0.5f);
        valueInSlider.text = infor.missionValue + "/" + mission.valueToGet;

        if (infor.missionValue >= mission.valueToGet)
        {
            b = true;
            bannerIm.sprite = GameController.ins.iconData.missionComplete;
            claimCoinBtnIm.sprite = GameController.ins.iconData.canClaimBtn;
            claimGemBtnIm.sprite = GameController.ins.iconData.canClaimBtn;
        }
        else
        {
            bannerIm.sprite = GameController.ins.iconData.missionDoing;
            claimCoinBtnIm.sprite = GameController.ins.iconData.cantClaimBtn;
            claimGemBtnIm.sprite = GameController.ins.iconData.cantClaimBtn;
        }

        if (mission.rewardType == MissionRewardType.Coin)
        {
            getCoinBtn.Show();
            getGemBtn.Hide();
            coinValue.text = mission.valueGet + "";
            getCoinBtn.onClick.RemoveAllListeners();
            getCoinBtn.onClick.AddListener(() =>
            {
                if (b)
                {
                    
                    UI.Ins.main.popup.mission.reward.ShowReward(true, mission.valueGet);
                    getCoinBtn.Hide();
                    claimedGO.Show();
                    var m = GameManager.ins.MissionLevel;
                    var t = m[(int) mission.missionType];
                    if (t < 2)
                        t++;
                    m[(int) mission.missionType] = t;
                    GameManager.ins.MissionLevel = m;

                    #region

                    var newMissions = Enum.GetValues(typeof(MissionType)).Cast<MissionType>().ToList();
                    foreach (var missionInfor in GameManager.ins.MissionInfor)
                    {
                        newMissions.Remove(missionInfor.missionType);
                    }

                    var r = Random.Range(0, newMissions.Count);
                    var n = new PlayerMissionInfor
                    {
                        missionValue = 0,
                        missionType = newMissions[r],
                        state = MissionState.Doing,
                        levelOfMission = GameManager.ins.MissionLevel[(int)newMissions[r]]
                    };

                    var missions = GameManager.ins.MissionInfor;
                    var index = missions.FindIndex(s => s.missionType == infor.missionType);
                    missions.Insert(index, n);
                    missions.Remove(infor);

                    GameManager.ins.MissionInfor = missions;

                    #endregion

                }
            });
        }
        else
        {
            getCoinBtn.Hide();
            getGemBtn.Show();
            gemValue.text = mission.valueGet + "";
            getGemBtn.onClick.RemoveAllListeners();
            getGemBtn.onClick.AddListener(() =>
            {
                if (b)
                {
                   
                    ManagerAds.Ins.ShowRewardedVideo((p) =>
                    {
                        if (p)
                        {
                            GameManager.ins.VideoCount++;
                            UI.Ins.main.popup.mission.reward.ShowReward(false, mission.valueGet);
                            getGemBtn.Hide();
                            claimedGO.Show();

                            var m = GameManager.ins.MissionLevel;
                            var t = m[(int)mission.missionType];
                            if (t < 2)
                                t++;
                            m[(int)mission.missionType] = t;
                            GameManager.ins.MissionLevel = m;

                            #region

                            var newMissions = Enum.GetValues(typeof(MissionType)).Cast<MissionType>().ToList();
                            foreach (var missionInfor in GameManager.ins.MissionInfor)
                            {
                                newMissions.Remove(missionInfor.missionType);
                            }

                            var r = Random.Range(0, newMissions.Count);
                            var n = new PlayerMissionInfor
                            {
                                missionValue = 0,
                                missionType = newMissions[r],
                                state = MissionState.Doing,
                                levelOfMission = GameManager.ins.MissionLevel[(int)newMissions[r]]
                            };

                            var missions = GameManager.ins.MissionInfor;
                            var index = missions.FindIndex(s => s.missionType == infor.missionType);
                            missions.Insert(index, n);
                            missions.Remove(infor);

                            GameManager.ins.MissionInfor = missions;

                            #endregion
                        }
                    });                  
                }
            });
        }

    }

}
