﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MissionReward : MonoBehaviour
{
    public Button okBtn, xRewardBtn;
    public Text valueTxt;
    public UI_ChangeText uiChange;
    private int rewardValue, xRewardValue;
    private bool isCoin;

    public CanvasGroup group;

    void Start()
    {
        okBtn.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayAudioClip(ClipType.complete_star_2);
            UI.Ins.main.popup.mission.OnShow();
            if (isCoin)
            {
                GameManager.ins.PlayerCoin += rewardValue;
                UI.Ins.main.general.ShowCoin();
            }
            else
            {
                GameManager.ins.PlayerGem += rewardValue;
                UI.Ins.main.general.ShowGem();
            }

            Fade(() =>
            {
                uiChange.Hide();
                this.Hide();
            });
          
        });

        xRewardBtn.onClick.AddListener(() =>
        {
            AudioManager.ins.PlayAudioClip(ClipType.complete_star_2);
            ManagerAds.Ins.ShowRewardedVideo((p) =>
            {
                if (p)
                {
                    GameManager.ins.VideoCount++;
                    UI.Ins.main.popup.mission.OnShow();
                    if (isCoin)
                    {
                        GameManager.ins.PlayerCoin += rewardValue * xRewardValue;
                        UI.Ins.main.general.ShowCoin();
                    }
                    else
                    {
                        GameManager.ins.PlayerGem += rewardValue * xRewardValue;
                        UI.Ins.main.general.ShowGem();
                    }

                    Fade(() =>
                    {
                        uiChange.Hide();
                        this.Hide();
                    });
                }
            });

           
        });

    }

    public void ShowReward(bool isCoin, int value)
    {
        this.Show();
        this.isCoin = isCoin;
        rewardValue = value;
        xRewardValue = Random.Range(2, 5);
        valueTxt.text = "x" + xRewardValue;
        uiChange.Show();
        uiChange.SetUp(isCoin, value);
    }

    void Fade(TweenCallback callback)
    {
        @group.DOFade(0, 0.2f).OnComplete(callback);
    }

}
