﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PopupManager : MonoBehaviour
{

    public SettingController setting;
    public MissionController mission;
    public UI7Days ui7Day;
    public MatchingController matching;
    public GemShopController gemShop;
    public RankUp rankUp;
    public UIRename rename;
    public CoinShopController coinShop;
    public UIVerificationAge verification;
    public MotionPopup motion;

    public void ShowDefault()
    {
        setting.Hide();
        mission.Hide();
        ui7Day.Hide();
        matching.Hide();
        gemShop.Hide();
        rankUp.Hide();
        rename.Hide();
        coinShop.Hide();
    }

    public void ShowPopup(MonoBehaviour obj)
    {
        this.Show();
        obj.Show();
    }

    public void HidePopup(MonoBehaviour obj)
    {
        this.Hide();
        obj.Hide();
    }

    public CanvasGroup group;

    public void OnClose(Action callBack)
    {
        @group.DOFade(0, 0.3f).OnComplete(callBack.Invoke);
    }
}
