﻿using Assets.Games.Photon.Scripts.Controller;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RankUp : MonoBehaviour
{

    public TextMeshProUGUI rankLevel, rankText;
    public Image rankIcon;
    public Button greatBtn;
    private bool isNetWork;

    private void Start()
    {
        greatBtn.onClick.AddListener(Hidden);
    }

    private void Hidden()
    {
        AudioManager.ins.PlayButtonClick();
        this.Hide();
        if(!isNetWork)
            UI.Ins.main.popup.Hide();
    }

    public void SetUp()
    {
        this.Show();
        isNetWork = false;
        var p = GameManager.ins.PlayerInformation;
        var x = (int)p.playerRank / 3;
        var m = (int)p.playerRank - x * 3;
        switch (x)
        {
            case 0:
                rankText.text = "BRONZE " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = GameController.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-19);
                break;
            case 1:
                rankText.text = "SILVER " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = GameController.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-15);
                break;
            case 2:
                rankText.text = "GOLDEN " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = GameController.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-54);
                break;
            case 3:
                rankText.text = "PLATINUM " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = GameController.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-24);
                break;
            case 4:
                rankText.text = "DIAMOND " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = GameController.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-42);
                break;
            case 5:
                rankText.text = "MASTER " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = GameController.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(10);
                break;
            case 6:
                rankText.text = "LEGEND " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = GameController.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-18);
                break;
        }
        rankIcon.SetNativeSize();
        DatabaseManager.UpdateRanking(p.playerRank);
    }


    public void Net_SetUp()
    {
        this.Show();
        isNetWork = true;
        var p = GameManager.ins.PlayerInformation;
        var x = (int)p.playerRank / 3;
        var m = (int)p.playerRank - x * 3;
        switch (x)
        {
            case 0:
                rankText.text = "BRONZE " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = Net_Controller.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-19);
                break;
            case 1:
                rankText.text = "SILVER " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = Net_Controller.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-15);
                break;
            case 2:
                rankText.text = "GOLDEN " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = Net_Controller.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-54);
                break;
            case 3:
                rankText.text = "PLATINUM " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = Net_Controller.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-24);
                break;
            case 4:
                rankText.text = "DIAMOND " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = Net_Controller.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-42);
                break;
            case 5:
                rankText.text = "MASTER " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = Net_Controller.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(10);
                break;
            case 6:
                rankText.text = "LEGEND " + (m + 1);
                rankLevel.text = "" + (m + 1);
                rankIcon.sprite = Net_Controller.ins.rankData.rankIconInRankUp[x];
                rankLevel.rectTransform.ChangePosY(-18);
                break;
        }
        rankIcon.SetNativeSize();
        DatabaseManager.UpdateRanking(p.playerRank);
    }
}
