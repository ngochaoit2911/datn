﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PlayerPrefs = UnityEngine.PlayerPrefs;

public class SettingController : MonoBehaviour
{
    public Button soundBtn, vibrateBtn, shareBtn, rateBtn, moreBtn, closeBtn;
    public Image soundIm, vibrateIm;
    public Button leaderBtn, logOutBtn;
    //public GameObject bonusShare, bonusRate;
  
    private const string IsRateBefore = "IsRate";
    private const string IsShareBefore = "IsShare";

    private void Start()
    {
        soundBtn.onClick.AddListener(ChangeSound);
        vibrateBtn.onClick.AddListener(ChangeVibrate);
        shareBtn.onClick.AddListener(Share);
        rateBtn.onClick.AddListener(Rate);
        moreBtn.onClick.AddListener(More);
        closeBtn.onClick.AddListener(Close);
        leaderBtn.onClick.AddListener(ShowLeaderboard);
        logOutBtn.onClick.AddListener(LogOut);
    }

    private void LogOut()
    {
        SceneManager.LoadScene(0);
    }

    private void ShowLeaderboard()
    {
        UI.Ins.ShowLeaderboard();
    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.OnClose(() =>
        {
            this.Hide();
            UI.Ins.main.popup.@group.alpha = 1;
            UI.Ins.main.popup.Hide();
        });
    }

    private void OnEnable()
    {
        ShowInfor();
    }

    private void ShowInfor()
    {
        soundIm.sprite = GameManager.ins.IsSoundOn
            ? GameController.ins.iconData.soundOn
            : GameController.ins.iconData.soundOff;
        vibrateIm.sprite = GameManager.ins.IsVibrate
            ? GameController.ins.iconData.vibrateOn
            : GameController.ins.iconData.vibrateOff;

        //if (PlayerPrefs.HasKey(IsRateBefore))
        //{
        //    bonusRate.Hide();
        //}

        //if (PlayerPrefs.HasKey(IsShareBefore))
        //{
        //    bonusShare.Hide();
        //}

    }

    private void Rate()
    {
        AudioManager.ins.PlayButtonClick();
        ManagerAds.Ins.RateApp();
        //if (PlayerPrefs.HasKey(IsRateBefore))
        //{
        //    //NormalRate
        //    ManagerAds.Ins.RateApp();
        //}
        //else
        //{
        //    //Normal Rate
        //    ManagerAds.Ins.RateApp();
        //    PlayerPrefs.SetInt(IsRateBefore, 1);
        //    bonusRate.Hide();
        //    GameManager.ins.PlayerGem += 10;
        //    UI.Ins.main.general.ShowGem();
        //}
    }

    private void More()
    {
        AudioManager.ins.PlayButtonClick();
        ManagerAds.Ins.MoreGame();
    }

    private void Share()
    {
        AudioManager.ins.PlayButtonClick();
        ManagerAds.Ins.ShareApp();
        //if (PlayerPrefs.HasKey(IsShareBefore))
        //{
        //    //NormalShare
        //    ManagerAds.Ins.ShareApp();
        //}
        //else
        //{
        //    ManagerAds.Ins.ShareApp();
        //    bonusShare.Hide();
        //    PlayerPrefs.SetInt(IsShareBefore, 1);
        //    GameManager.ins.PlayerGem += 10;
        //    UI.Ins.main.general.ShowGem();
        //}
    }

    private void ChangeVibrate()
    {
        AudioManager.ins.PlayButtonClick();
        GameManager.ins.IsVibrate = !GameManager.ins.IsVibrate;
        ShowInfor();
    }

    private void ChangeSound()
    {
        AudioManager.ins.PlayButtonClick();
        GameManager.ins.IsSoundOn = !GameManager.ins.IsSoundOn;
        AudioManager.ins.ChangeSoundState();
        AudioManager.ins.PlayMusic(MusicType.HomeMusic);
        ShowInfor();

    }
}
