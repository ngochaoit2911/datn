﻿
using System;
using UnityEngine;
using UnityEngine.UI.Extensions;
using UnityEngine.UI.Extensions.Examples;

public class HeroShopContext
{
    int selectedIndex = -1;

    public int SelectedIndex
    {
        get { return selectedIndex; }
        set
        {
            if (value == selectedIndex)
            {
                return;
            }

            selectedIndex = value;

            if (OnSelectedIndexChanged != null)
            {
                OnSelectedIndexChanged(selectedIndex);
            }
        }
    }

    public Action<HeroShopItemController> OnPressedCell;
    public Action<int> OnSelectedIndexChanged;
}
