﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeroShopController : MonoBehaviour
{
    public Button closeBtn;
    public HeroShopScroll scrollView;
    public MaskType maskType;

    public Dictionary<int, List<MaskInfor>> pages;
    public GameObject buyBtnGo, equipBtnGo;
    public Image equipIm;
    public GameObject gemGo, coinGo, videoGo;
    public TextMeshProUGUI gemValue, coinValue, textGO, videoValue;
    public Button buyBtn, equipBtn;


    private void Awake()
    {
        pages = new Dictionary<int, List<MaskInfor>>();
        var key = 0;
        var count = 0;
        foreach (var item in GameController.ins.maskData.maskData)
        {
            if (count <= 5)
            {
                if (pages.ContainsKey(key))
                {
                    pages[key].Add(item);
                }
                else
                {
                    pages.Add(key, new List<MaskInfor> { item });
                }

                count++;
            }
            else
            {
                key++;

                pages.Add(key, new List<MaskInfor> { item });
                count = 1;
            }
        }

    }

    private void Start()
    {
        closeBtn.onClick.AddListener(Close);
        scrollView.OnStart();
        scrollView.OnSelectedIndexChanged(HandleSelectedIndexChanged);
        scrollView.UpdateData(new List<int> { 0, 1, 2, 3, 4, 5});
        scrollView.UpdateSelection(0);
    }

    private void ShowMotion()
    {
        AudioManager.ins.PlayButtonClick();
        this.Hide();
        UI.Ins.main.shop.Hide();
        UI.Ins.main.motion.OnShow();
        ManagerAds.Ins.ShowBanner();
    }

    void HandleSelectedIndexChanged(int index)
    {

    }

    private void OnEnable()
    {
        OnShow();
    }

    public void OnShow()
    {
        maskType = GameManager.ins.PlayerInformation.playerEquipMask;
        ShowButton();
    }

    public void OnChangeSelected(MaskType mask)
    {
        this.maskType = mask;
        ShowButton();
    }

    public void ShowButton()
    {
        var pInfor = GameManager.ins.PlayerMasks.Find(s => s.maskType == maskType);
        UI.Ins.main.shop.player.SetUpHero(pInfor.maskType);
        if (pInfor.isUnlock)
        {
            buyBtnGo.Hide();
            equipBtnGo.Show();
            var x = GameManager.ins.PlayerInformation.playerEquipMask == maskType;
            equipIm.sprite = x
                ? GameController.ins.iconData.equipedBtn
                : GameController.ins.iconData.equipBtn;
            if (x)
            {
                equipBtn.onClick.RemoveAllListeners();
            }
            else
            {
                equipBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayAudioClip(ClipType.purchase);
                    var infor = GameManager.ins.PlayerInformation;
                    infor.playerEquipMask = maskType;
                    GameManager.ins.PlayerInformation = infor;

                    equipIm.sprite = GameController.ins.iconData.equipedBtn;
                    UpdateInfor();
                });
            }
        }
        else
        {
            equipBtnGo.Hide();
            buyBtnGo.Show();

            var maskInfor = GameController.ins.maskData.maskData[(int)maskType];
            buyBtn.onClick.RemoveAllListeners();
            if (maskInfor.costType == CostBuy.Coin)
            {
                coinGo.Show();
                gemGo.Hide();
                videoGo.Hide();
                textGO.Hide();
                coinValue.text = maskInfor.valueToGet + "";
                buyBtn.image.sprite = GameManager.ins.PlayerCoin >= maskInfor.valueToGet
                    ? GameController.ins.iconData.canBuyBtn
                    : GameController.ins.iconData.cantBuyBtn;
                buyBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayButtonClick();
                    if (GameManager.ins.PlayerCoin >= maskInfor.valueToGet)
                    {
                        this.PostEvent(EventID.UnlockNewHero);
                        GameManager.ins.PlayerCoin -= maskInfor.valueToGet;
                        UI.Ins.main.general.ShowCoin();
                        var x = GameManager.ins.PlayerMasks;
                        var infor = x[(int)maskType];
                        infor.isUnlock = true;
                        x[(int)maskType] = infor;
                        GameManager.ins.PlayerMasks = x;

                        buyBtnGo.Hide();
                        equipBtnGo.Show();
                        equipIm.sprite =
                            GameController.ins.iconData.equipBtn;
                        equipBtn.onClick.AddListener(() =>
                        {
                            var i = GameManager.ins.PlayerInformation;
                            i.playerEquipMask = maskType;
                            GameManager.ins.PlayerInformation = i;

                            equipIm.sprite = GameController.ins.iconData.equipedBtn;
                            UpdateInfor();
                        });

                        UpdateInfor();
                    }
                });
            }
            else if(maskInfor.costType == CostBuy.Gem)
            {
                gemGo.Show();
                coinGo.Hide();

                videoGo.Hide();
                textGO.Hide();
                gemValue.text = maskInfor.valueToGet + "";
                buyBtn.image.sprite = GameManager.ins.PlayerGem >= maskInfor.valueToGet
                    ? GameController.ins.iconData.canBuyBtn
                    : GameController.ins.iconData.cantBuyBtn;
                buyBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayButtonClick();
                    if (GameManager.ins.PlayerGem >= maskInfor.valueToGet)
                    {
                        this.PostEvent(EventID.UnlockNewHero);
                        GameManager.ins.PlayerGem -= maskInfor.valueToGet;
                        UI.Ins.main.general.ShowGem();
                        var x = GameManager.ins.PlayerMasks;
                        var infor = x[(int)maskType];
                        infor.isUnlock = true;
                        x[(int)maskType] = infor;
                        GameManager.ins.PlayerMasks = x;

                        buyBtnGo.Hide();
                        equipBtnGo.Show();
                        equipIm.sprite =
                            GameController.ins.iconData.equipBtn;
                        equipBtn.onClick.AddListener(() =>
                        {
                            var i = GameManager.ins.PlayerInformation;
                            i.playerEquipMask = maskType;
                            GameManager.ins.PlayerInformation = i;

                            equipIm.sprite = GameController.ins.iconData.equipedBtn;
                        });

                        UpdateInfor();
                    }
                });
            }
            else if (maskInfor.costType == CostBuy.Video)
            {
                gemGo.Hide();
                coinGo.Hide();

                videoGo.Show();
                textGO.Hide();
                videoValue.text = GameManager.ins.VideoCount + "/" + maskInfor.valueToGet;
                buyBtn.image.sprite = GameManager.ins.VideoCount >= maskInfor.valueToGet
                    ? GameController.ins.iconData.canBuyBtn
                    : GameController.ins.iconData.cantBuyBtn;
                buyBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayButtonClick();
                    if (GameManager.ins.VideoCount >= maskInfor.valueToGet)
                    {
                        this.PostEvent(EventID.UnlockNewHero);
                        var x = GameManager.ins.PlayerMasks;
                        var infor = x[(int)maskType];
                        infor.isUnlock = true;
                        x[(int)maskType] = infor;
                        GameManager.ins.PlayerMasks = x;

                        buyBtnGo.Hide();
                        equipBtnGo.Show();
                        equipIm.sprite =
                            GameController.ins.iconData.equipBtn;
                        equipBtn.onClick.AddListener(() =>
                        {
                            var i = GameManager.ins.PlayerInformation;
                            i.playerEquipMask = maskType;
                            GameManager.ins.PlayerInformation = i;

                            equipIm.sprite = GameController.ins.iconData.equipedBtn;
                        });

                        UpdateInfor();
                    }
                });
            }
            else
            {
                gemGo.Hide();
                coinGo.Hide();
                videoGo.Hide();
                textGO.Show();
                textGO.text = "Day " + maskInfor.valueToGet;
                buyBtn.image.sprite = GameController.ins.iconData.cantBuyBtn;
            }
        }
    }

    public void UpdateInfor()
    {
        UI.Ins.main.shop.uiChiSo.UpdateInformation(GameManager.ins.PlayerInformation.playerEquipKnife, maskType);
        foreach (var item in scrollView.cells)
        {
            item.UpdateCurContent();
        }
        scrollView.UpdateSelection(GetPage((int)maskType));
    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        this.Hide();
        UI.Ins.main.shop.Hide();
        UI.Ins.main.home.OnShow();
        ManagerAds.Ins.ShowBanner();
    }

    public int GetPage(int index)
    {
        return index / 6;
    }
}
