﻿
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeroShopItem : MonoBehaviour
{
    public Button btn;
    public Image banner;
    public Image icon;
    public GameObject lockIcon;
    public TextMeshProUGUI coinValue, gemValue, videoValue, textGO;

    public GameObject coinGO, gemGO, videoGO, tickGO, selectedObj;
    public RectTransform iconPos;
    private MaskType type;
    public void SetUp(MaskInfor maskInfor, PlayerMaskInfor playerMaskInfor)
    {
        type = maskInfor.type;
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(OnSelected);

        selectedObj.SetActive(maskInfor.type == UI.Ins.main.shop.heroShop.maskType);
        icon.sprite = maskInfor.icon;
        icon.SetNativeSize();
        icon.transform.localScale = Vector3.one * maskInfor.scaleSizeOnShop;

        if (maskInfor.type == GameManager.ins.PlayerInformation.playerEquipMask)
        {
            tickGO.Show();
        }
        else
        {
            tickGO.Hide();
        }

        if (playerMaskInfor.isUnlock)
        {
            iconPos.ChangePosY(0);
            lockIcon.Hide();
            coinGO.Hide();
            gemGO.Hide();
            videoGO.Hide();
            textGO.Hide();
        }
        else
        {
            iconPos.ChangePosY(30);
            lockIcon.Show();
            coinGO.Hide();
            gemGO.Hide();
            videoGO.Hide();
            textGO.Hide();
            switch (maskInfor.costType)
            {
                case CostBuy.Coin:
                    coinGO.Show();
                    coinValue.text = maskInfor.valueToGet + "";
                    break;
                case CostBuy.Gem:
                    gemGO.Show();
                    gemValue.text = maskInfor.valueToGet + "";
                    break;
                case CostBuy.Video:

                    if (GameManager.ins.VideoCount >= maskInfor.valueToGet)
                    {
                        playerMaskInfor.isUnlock = true;
                        GameManager.ins.ChangeMasks(playerMaskInfor);
                        lockIcon.Hide();
                    }
                    else
                    {
                        videoGO.Show();
                        videoValue.text = GameManager.ins.VideoCount + "/" + maskInfor.valueToGet;
                    }

                    break;
                case CostBuy.Day:
                    lockIcon.Show();
                    textGO.Show();
                    textGO.text = "Day " + maskInfor.valueToGet;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        //switch (maskInfor.uiEffectType)
        //{
        //    case MaskUIEffect.None:
        //        effect1.Hide();
        //        effect2.Hide();
        //        break;
        //    case MaskUIEffect.Blue:
        //        effect1.Show();
        //        effect2.Hide();
        //        break;
        //    case MaskUIEffect.Orange:
        //        effect1.Hide();
        //        effect2.Show();
        //        break;
        //    default:
        //        throw new ArgumentOutOfRangeException();
        //}

    }

    private void OnSelected()
    {
        AudioManager.ins.PlayButtonClick();
        selectedObj.Show();
        UI.Ins.main.shop.heroShop.OnChangeSelected(type);

        UI.Ins.main.shop.heroShop.UpdateInfor();

    }
}

