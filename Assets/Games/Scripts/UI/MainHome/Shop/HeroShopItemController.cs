﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI.Extensions;

public class HeroShopItemController : FancyScrollViewCell<int, HeroShopContext>
{
    public List<HeroShopItem> items;

    [SerializeField] Animator animator = null;

    private int curIndex;
    static readonly int scrollTriggerHash = Animator.StringToHash("scroll");


    /// <summary>
    /// Updates the content.
    /// </summary>
    /// <param name="itemData">Item data.</param>
    public override void UpdateContent(int itemData)
    {
        curIndex = itemData;
        var l = UI.Ins.main.shop.heroShop.pages[itemData];
        SetUp(l);
    }

    /// <summary>
    /// Updates the position.
    /// </summary>
    /// <param name="position">Position.</param>
    public override void UpdatePosition(float position)
    {
        currentPosition = position;
        animator.Play(scrollTriggerHash, -1, position);
        animator.speed = 0;
    }

    void OnPressedCell()
    {
        if (Context != null)
        {
            Context.OnPressedCell(this);
        }
    }

    // GameObject が非アクティブになると Animator がリセットされてしまうため
    // 現在位置を保持しておいて OnEnable のタイミングで現在位置を再設定します
    float currentPosition = 0;

    void OnEnable()
    {
        UpdatePosition(currentPosition);
    }

    public void SetUp(List<MaskInfor> maskInfors)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (i >= maskInfors.Count)
            {
                items[i].Hide();
            }
            else
            {
                items[i].Show();
                items[i].SetUp(maskInfors[i], GameManager.ins.PlayerMasks[(int)maskInfors[i].type]);
            }
        }

    }

    public override void UpdateCurContent()
    {
        UpdateContent(curIndex);
    }
}
