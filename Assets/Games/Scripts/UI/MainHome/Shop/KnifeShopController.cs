﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions.Examples;

public class KnifeShopController : MonoBehaviour
{
    public Button closeBtn;
    public ShopScroll scrollView;
    public KnifeType knifeType;

    public Dictionary<int, List<KnifeInfor>> pages;
    public GameObject buyBtnGo, equiBtnGo;
    public Image equipIm;
    public GameObject gemGo, coinGo, videoGO;
    public TextMeshProUGUI gemValue, coinValue, videoValue, textGO;
    public Button buyBtn, equipBtn;


    private void Awake()
    {
        pages = new Dictionary<int, List<KnifeInfor>>();
        var key = 0;
        var count = 0;
        foreach (var item in GameController.ins.knifeData.knifeData)
        {
            if (count <= 8)
            {
                if (pages.ContainsKey(key))
                {
                    pages[key].Add(item);
                }
                else
                {
                    pages.Add(key, new List<KnifeInfor>{item});
                }

                count++;
            }
            else
            {
                key++;
                
                pages.Add(key, new List<KnifeInfor> { item });
                count = 1;
            }
        }

    }

    private void Start()
    {
        closeBtn.onClick.AddListener(Close);
        scrollView.OnStart();
        scrollView.OnSelectedIndexChanged(HandleSelectedIndexChanged);
        scrollView.UpdateData(new List<int> {0, 1, 2, 3, 4, 5, 6});
        scrollView.UpdateSelection(0);
    }

    void HandleSelectedIndexChanged(int index)
    {
        
    }

    private void OnEnable()
    {
        OnShow();
    }

    public void OnShow()
    {
        knifeType = GameManager.ins.PlayerInformation.playerEquipKnife;      
        ShowButton();
    }

    public void OnChangeSelected(KnifeType knifeType)
    {
        this.knifeType = knifeType;
        ShowButton();
    }

    public void ShowButton()
    {
        var pInfor = GameManager.ins.PlayerKnifes.Find(s => s.knifeType == knifeType);
        UI.Ins.main.shop.player.SetUpKnife(pInfor.knifeType);
        if (pInfor.isUnlock)
        {
            buyBtnGo.Hide();
            equiBtnGo.Show();
            var x = GameManager.ins.PlayerInformation.playerEquipKnife == knifeType;
            equipIm.sprite = x
                ? GameController.ins.iconData.equipedBtn
                : GameController.ins.iconData.equipBtn;
            if (x)
            {
                equipBtn.onClick.RemoveAllListeners();
            }
            else
            {
                equipBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayAudioClip(ClipType.purchase);
                    var infor = GameManager.ins.PlayerInformation;
                    infor.playerEquipKnife = knifeType;
                    GameManager.ins.PlayerInformation = infor;

                    equipIm.sprite = GameController.ins.iconData.equipedBtn;
                    UpdateInfor();
                });
            }
        }
        else
        {
            equiBtnGo.Hide();
            buyBtnGo.Show();

            var knife = GameController.ins.knifeData.knifeData[(int) knifeType];
            buyBtn.onClick.RemoveAllListeners();
            if (knife.costType == CostBuy.Coin)
            {
                videoGO.Hide();
                coinGo.Show();
                gemGo.Hide();
                textGO.Hide();
                coinValue.text = knife.valueToGet + "";
                buyBtn.image.sprite = GameManager.ins.PlayerCoin >= knife.valueToGet
                    ? GameController.ins.iconData.canBuyBtn
                    : GameController.ins.iconData.cantBuyBtn;
                buyBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayButtonClick();
                    if (GameManager.ins.PlayerCoin >= knife.valueToGet)
                    {
                        this.PostEvent(EventID.UnlockNewKnife);
                        GameManager.ins.PlayerCoin -= knife.valueToGet;
                        UI.Ins.main.general.ShowCoin();
                        var x = GameManager.ins.PlayerKnifes;
                        var infor = x[(int) knifeType];
                        infor.isUnlock = true;
                        x[(int) knifeType] = infor;
                        GameManager.ins.PlayerKnifes = x;

                        buyBtnGo.Hide();
                        equiBtnGo.Show();
                        equipIm.sprite =
                            GameController.ins.iconData.equipBtn;
                        equipBtn.onClick.AddListener(() =>
                        {
                            AudioManager.ins.PlayAudioClip(ClipType.purchase);
                            var i = GameManager.ins.PlayerInformation;
                            i.playerEquipKnife = knifeType;
                            GameManager.ins.PlayerInformation = i;

                            equipIm.sprite = GameController.ins.iconData.equipedBtn;
                            UpdateInfor();
                        });

                        UpdateInfor();
                    }
                });
            }
            else if (knife.costType == CostBuy.Gem)
            {
                gemGo.Show();
                coinGo.Hide();
                videoGO.Hide();
                textGO.Hide();
                gemValue.text = knife.valueToGet + "";
                buyBtn.image.sprite = GameManager.ins.PlayerGem >= knife.valueToGet
                    ? GameController.ins.iconData.canBuyBtn
                    : GameController.ins.iconData.cantBuyBtn;
                buyBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayButtonClick();
                    if (GameManager.ins.PlayerGem >= knife.valueToGet)
                    {
                        this.PostEvent(EventID.UnlockNewKnife);
                        GameManager.ins.PlayerGem -= knife.valueToGet;
                        UI.Ins.main.general.ShowGem();
                        var x = GameManager.ins.PlayerKnifes;
                        var infor = x[(int) knifeType];
                        infor.isUnlock = true;
                        x[(int) knifeType] = infor;
                        GameManager.ins.PlayerKnifes = x;

                        buyBtnGo.Hide();
                        equiBtnGo.Show();
                        equipIm.sprite =
                            GameController.ins.iconData.equipBtn;
                        equipBtn.onClick.AddListener(() =>
                        {
                            AudioManager.ins.PlayAudioClip(ClipType.purchase);
                            var i = GameManager.ins.PlayerInformation;
                            i.playerEquipKnife = knifeType;
                            GameManager.ins.PlayerInformation = i;

                            equipIm.sprite = GameController.ins.iconData.equipedBtn;
                        });

                        UpdateInfor();
                    }
                });
            }
            else if (knife.costType == CostBuy.Video)
            {
                gemGo.Hide();
                coinGo.Hide();
                videoGO.Show();
                textGO.Hide();
                videoValue.text = GameManager.ins.VideoCount + "/" + knife.valueToGet;
                buyBtn.image.sprite = GameManager.ins.VideoCount >= knife.valueToGet
                    ? GameController.ins.iconData.canBuyBtn
                    : GameController.ins.iconData.cantBuyBtn;
                buyBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayButtonClick();
                    if (GameManager.ins.VideoCount >= knife.valueToGet)
                    {
                        this.PostEvent(EventID.UnlockNewKnife);
                        var x = GameManager.ins.PlayerKnifes;
                        var infor = x[(int) knifeType];
                        infor.isUnlock = true;
                        x[(int) knifeType] = infor;
                        GameManager.ins.PlayerKnifes = x;

                        buyBtnGo.Hide();
                        equiBtnGo.Show();
                        equipIm.sprite =
                            GameController.ins.iconData.equipBtn;
                        equipBtn.onClick.AddListener(() =>
                        {
                            AudioManager.ins.PlayAudioClip(ClipType.purchase);
                            var i = GameManager.ins.PlayerInformation;
                            i.playerEquipKnife = knifeType;
                            GameManager.ins.PlayerInformation = i;

                            equipIm.sprite = GameController.ins.iconData.equipedBtn;
                        });

                        UpdateInfor();
                    }
                });
            }
            else if (knife.costType == CostBuy.Day)
            {
                gemGo.Hide();
                coinGo.Hide();
                videoGO.Hide();
                textGO.Show();
                textGO.text = "Day " + knife.valueToGet;
                buyBtn.image.sprite = GameController.ins.iconData.cantBuyBtn;
            }
            else
            {
                gemGo.Hide();
                coinGo.Hide();
                videoGO.Hide();
                textGO.Show();
                textGO.text = "UNLOCK";
                buyBtn.image.sprite = (int)GameManager.ins.PlayerInformation.playerRank > knife.valueToGet
                    ? GameController.ins.iconData.canBuyBtn
                    : GameController.ins.iconData.cantBuyBtn;

                buyBtn.onClick.AddListener(() =>
                {
                    AudioManager.ins.PlayButtonClick();
                    if ((int)GameManager.ins.PlayerInformation.playerRank > knife.valueToGet)
                    {
                        this.PostEvent(EventID.UnlockNewKnife);
                        var x = GameManager.ins.PlayerKnifes;
                        var infor = x[(int)knifeType];
                        infor.isUnlock = true;
                        x[(int)knifeType] = infor;
                        GameManager.ins.PlayerKnifes = x;

                        buyBtnGo.Hide();
                        equiBtnGo.Show();
                        equipIm.sprite =
                            GameController.ins.iconData.equipBtn;
                        equipBtn.onClick.AddListener(() =>
                        {
                            AudioManager.ins.PlayAudioClip(ClipType.purchase);
                            var i = GameManager.ins.PlayerInformation;
                            i.playerEquipKnife = knifeType;
                            GameManager.ins.PlayerInformation = i;

                            equipIm.sprite = GameController.ins.iconData.equipedBtn;
                        });

                        UpdateInfor();
                    }
                });
            }

        }
    }

    public void UpdateInfor()
    {
        UI.Ins.main.shop.uiChiSo.UpdateInformation(knifeType, GameManager.ins.PlayerInformation.playerEquipMask);

        foreach (var item in scrollView.cells)
        {
            item.UpdateCurContent();
        }
        scrollView.UpdateSelection(GetPage((int) knifeType));
    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        this.Hide();
        UI.Ins.main.shop.Hide();
        UI.Ins.main.home.OnShow();
        ManagerAds.Ins.ShowBanner();
    }

    public int GetPage(int index)
    {
        return index / 9;
    }

}
