﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KnifeShopItem : PoolItem
{
    public Button btn;
    public Image banner;
    public Image icon;
    public GameObject lockIcon;
    public TextMeshProUGUI coinValue, gemValue, videoValue, textGo;

    public GameObject coinGO, gemGO, videoGO, tickGO, selectedIcon;
    private KnifeType type;
    public RectTransform iconPos;
    public void SetUp(KnifeInfor knifeInfor, PlayerKnifeInfor playerKnifeInfor)
    {
        type = knifeInfor.knifeType;
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(OnSelected);

        //banner.sprite = knifeInfor.knifeType == UI.Ins.main.shop.knifeShop.knifeType
        //    ? GameController.ins.iconData.knifeShopSelected
        //    : GameController.ins.iconData.knifeShopUnSelected;
        selectedIcon.SetActive(knifeInfor.knifeType == UI.Ins.main.shop.knifeShop.knifeType);
        icon.sprite = knifeInfor.icon;
        icon.SetNativeSize();
        icon.transform.localScale = Vector3.one * knifeInfor.scaleSizeOnShop;

        if (knifeInfor.knifeType == GameManager.ins.PlayerInformation.playerEquipKnife)
        {
            tickGO.Show();
        }
        else
        {
            tickGO.Hide();
        }

        if (playerKnifeInfor.isUnlock)
        {
            iconPos.ChangePosY(0);
            lockIcon.Hide();
            coinGO.Hide();
            gemGO.Hide();
            videoGO.Hide();
            textGo.Hide();
        }
        else
        {
            iconPos.ChangePosY(20);
            lockIcon.Show();
            coinGO.Hide();
            gemGO.Hide();
            videoGO.Hide();
            textGo.Hide();
            switch (knifeInfor.costType)
            {
                case CostBuy.Coin:
                    coinGO.Show();
                    coinValue.text = knifeInfor.valueToGet + "";
                    break;
                case CostBuy.Gem:
                    gemGO.Show();
                    gemValue.text = knifeInfor.valueToGet + "";
                    break;
                case CostBuy.Video:

                    if (GameManager.ins.VideoCount >= knifeInfor.valueToGet)
                    {
                        playerKnifeInfor.isUnlock = true;
                        GameManager.ins.ChangeKnifes(playerKnifeInfor);
                        lockIcon.Hide();
                    }
                    else
                    {
                        videoGO.Show();
                        videoValue.text = GameManager.ins.VideoCount + "/" + knifeInfor.valueToGet;
                    }

                    break;
                case CostBuy.Day:
                    lockIcon.Show();
                    textGo.Show();
                    textGo.text = "Day " + knifeInfor.valueToGet;
                    break;
                case CostBuy.Rank:
                    textGo.Show();
                    textGo.text = knifeInfor.knifeType.ToString().ToUpper();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    private void OnSelected()
    {
        AudioManager.ins.PlayButtonClick();
        selectedIcon.Show();  
        UI.Ins.main.shop.knifeShop.OnChangeSelected(type);

        UI.Ins.main.shop.knifeShop.UpdateInfor();
       
    }
}
