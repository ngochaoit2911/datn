﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteController : MonoBehaviour
{
    public List<UINote> notes;

    public int index;

    public void OnShow()
    {
        foreach (var note in notes)
        {
            note.SetUp(this);
        }
    }

    public void Change(int index)
    {
        this.index = index;
        OnShow();

        if (UI.Ins.main.shop.tab == ShopTab.Blade)
        {
            UI.Ins.main.shop.knifeShop.scrollView.UpdateSelection(index);
        }
        else
        {
            UI.Ins.main.shop.heroShop.scrollView.UpdateSelection(index);
        }
    }

    public void ShowValue(int valueCount)
    {
        var c = 0;
        foreach (var uiNote in notes)
        {
            if (c >= valueCount)
            {
                uiNote.Hide();
            }
            else
            {
                uiNote.Show();
            }

            c++;
        }
    }

}
