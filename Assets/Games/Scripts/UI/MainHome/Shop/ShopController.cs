﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    public HeroShopController heroShop;
    public KnifeShopController knifeShop;
    public UIPlayer_Shop player;
    public UIChiSo uiChiSo;

    public ShopTab tab;

    public void ShowDefault()
    {
        heroShop.Hide();
        knifeShop.Hide();
    }

    public void ShowHeroShop()
    {
        var p = GameManager.ins.PlayerInformation;
        tab = ShopTab.Hero;
        heroShop.maskType = p.playerEquipMask;
        knifeShop.knifeType = p.playerEquipKnife;
        this.Show();
        heroShop.Show();
        knifeShop.Hide();
        player.OnShow(p.playerEquipMask, p.playerEquipKnife);
        uiChiSo.UpdateInformation(GameManager.ins.PlayerInformation.playerEquipKnife,
            GameManager.ins.PlayerInformation.playerEquipMask);
    }

    public void ShowKnifeShop()
    {
        var p = GameManager.ins.PlayerInformation;
        this.Show();
        tab = ShopTab.Blade;
        heroShop.maskType = p.playerEquipMask;
        knifeShop.knifeType = p.playerEquipKnife;
        heroShop.Hide();
        knifeShop.Show();
        player.OnShow(p.playerEquipMask, p.playerEquipKnife);
        uiChiSo.UpdateInformation(GameManager.ins.PlayerInformation.playerEquipKnife,
            GameManager.ins.PlayerInformation.playerEquipMask);
    }

}

public enum ShopTab
{
    Blade,
    Hero
}