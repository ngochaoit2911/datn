﻿
using System;
using UnityEngine;
using UnityEngine.UI.Extensions;
using UnityEngine.UI.Extensions.Examples;

public class ScrollContext
{
    int selectedIndex = -1;

    public int SelectedIndex
    {
        get { return selectedIndex; }
        set
        {
            if (value == selectedIndex)
            {
                return;
            }

            selectedIndex = value;

            if (OnSelectedIndexChanged != null)
            {
                OnSelectedIndexChanged(selectedIndex);
            }
        }
    }

    public Action<KnifeShopItemController> OnPressedCell;
    public Action<int> OnSelectedIndexChanged;
}


