﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class ShopScroll : FancyScrollView<int, ScrollContext>
{
    [SerializeField]
    ScrollPositionController scrollPositionController = null;

    public NoteController note;
    Action<int> onSelectedIndexChanged;

    public void  OnStart()
    {
        scrollPositionController.OnUpdatePosition(p => UpdatePosition(p));
        scrollPositionController.OnItemSelected(HandleItemSelected);

        SetContext(new ScrollContext
        {
            OnPressedCell = OnPressedCell,
            OnSelectedIndexChanged = index =>
            {
                if (onSelectedIndexChanged != null)
                {
                    onSelectedIndexChanged(index);
                }
            }
        });
    }

    public void UpdateData(List<int> data)
    {
        cellData = data;
        scrollPositionController.SetDataCount(cellData.Count);
        UpdateContents();
    }

    public void UpdateSelection(int index)
    {
        if (index < 0 || index >= cellData.Count)
        {
            return;
        }

        scrollPositionController.ScrollTo(index, 0.4f);
        Context.SelectedIndex = index;
        UpdateContents();
    }

    public void OnSelectedIndexChanged(Action<int> onSelectedIndexChanged)
    {
        this.onSelectedIndexChanged = onSelectedIndexChanged;
    }

    public void SelectNextCell()
    {
        UpdateSelection(Context.SelectedIndex + 1);
    }

    public void SelectPrevCell()
    {
        UpdateSelection(Context.SelectedIndex - 1);
    }

    void HandleItemSelected(int selectedItemIndex)
    {
        Context.SelectedIndex = selectedItemIndex;
        note.index = selectedItemIndex;
        note.OnShow();
        UpdateContents();
    }

    void OnPressedCell(KnifeShopItemController cell)
    {
        UpdateSelection(cell.DataIndex);
    }
}

