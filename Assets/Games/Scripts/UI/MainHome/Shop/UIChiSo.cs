﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIChiSo : MonoBehaviour
{
    public TextMeshProUGUI maskNameTxt;
    public Text speedBuff, dodgeBuff, hBuff, bBuff, rotateBuff, addBuff;
    private float lastSpeed, lastDodge, lastRotate;
    private int lastAdd;

    public void UpdateInformation(KnifeType knifeType, MaskType maskType)
    {
        var data = GameController.ins.knifeData.knifeData[(int) knifeType];
        var maskInfor = GameController.ins.maskData.maskData[(int) maskType];

        maskNameTxt.text = maskInfor.maskName;


        var rotate = data.addSpeed + (maskType == data.setTypeBuff ? data.setBuffAddSpeed : 0);
        SelectedColor(rotateBuff, rotate, data.addSpeed, (maskType == data.setTypeBuff ? data.setBuffAddSpeed : 0));
        var add = (data.addKnifeInStart + (maskType == data.setTypeBuff ? data.setBuffAddKnifeInStart : 0));
        SelectedColor(addBuff, add, data.addKnifeInStart, (maskType == data.setTypeBuff ? data.setBuffAddKnifeInStart : 0));
        var dodgeValue = data.avoidObstacle + (maskType == data.setTypeBuff ? data.setBuffAvoidObstacle : 0);
        SelectedColor(dodgeBuff, dodgeValue, data.avoidObstacle, (maskType == data.setTypeBuff ? data.setBuffAvoidObstacle : 0));
        var speed = 0f;
        Anim(dodgeBuff, lastDodge, dodgeValue, 0.3f, () =>
        {
            lastDodge = dodgeValue;
            SelectedColor(dodgeBuff, dodgeValue, data.avoidObstacle, (maskType == data.setTypeBuff ? data.setBuffAvoidObstacle : 0));
        });
        Anim(rotateBuff, lastRotate, rotate, 0.3f, () =>
        {
            lastRotate = rotate;
            SelectedColor(rotateBuff, rotate, data.addSpeed, (maskType == data.setTypeBuff ? data.setBuffAddSpeed : 0));
        });
        Anim(addBuff, lastAdd, add, 0.3f, () =>
        {
            lastAdd = add;
            SelectedColor(addBuff, add, data.addKnifeInStart, (maskType == data.setTypeBuff ? data.setBuffAddKnifeInStart : 0));
        });
        switch (data.buffType)
        {
            case BuffType.None:
                bBuff.text = "NONE";
                break;
            case BuffType.UpDownAttack:
                bBuff.text = "ATK UD";
                break;
            case BuffType.LeftRightAttack:
                bBuff.text = "ATK LR";
                break;
            case BuffType.DamageFour:
                bBuff.text = "DMG1";
                break;
            case BuffType.DamageX:
                bBuff.text = "DMG2";
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        switch (maskInfor.buffType)
        {
            case MaskBuffType.Magnet:
                hBuff.text = "MAGNET";
                speed = 0;
                break;
            case MaskBuffType.Run:
                speed = maskInfor.value;
                hBuff.text = "RUN";
                break;
            case MaskBuffType.More:
                speed = 0;
                hBuff.text = "MORE";
                break;
            case MaskBuffType.Protect:
                speed = 0;
                hBuff.text = "PROTECT";
                break;
            case MaskBuffType.Slow:
                speed = 0;
                hBuff.text = "SLOW";
                break;
            case MaskBuffType.Dull:
                speed = 0;
                hBuff.text = "DULL";
                break;
            case MaskBuffType.None:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        Anim(speedBuff, lastSpeed, speed, 0.3f, () => lastSpeed = speed);
        SelectedColor(speedBuff, speed, speed, 0);

    }

    void Anim(Text t, float last, float des, float time, Action complete)
    {
        if (last != des)
        {
            t.color = des > last ? Color.green : Color.red;

            DOVirtual.Float(last, des, time, value => { t.text = (int) (value * 100) + "%"; }).OnComplete(() =>
            {
                t.color = Color.white;
                complete.Invoke();
            });
        }
    }

    void Anim(Text t, int last, int des, float time, Action complete)
    {
        if (last != des)
        {
            t.color = des > last ? Color.green : Color.red;

            DOVirtual.Float(last, des, time, value => { t.text = (int)(value) + ""; }).OnComplete(() =>
            {
                t.color = Color.white;
                complete.Invoke();
            });
        }
    }

    public void SelectedColor(Text t, float value, float blade, float hero)
    {
        if (value == 0)
        {
            t.color = Color.white;
        }
        else if (blade > 0 && hero == 0)
        {
            t.color = GameController.ins.colorData.bladeColor;
        }
        else if (blade == 0 && hero > 0)
        {
            t.color = GameController.ins.colorData.heroColor;
        }
        else
        {
            t.color = (UI.Ins.main.shop.tab == ShopTab.Blade)
                ? GameController.ins.colorData.bladeColor
                : GameController.ins.colorData.heroColor;
        }
    }

    public void SelectedColor(Text t, int value, int blade, int hero)
    {
        if (value == 0)
        {
            t.color = Color.white;
        }
        else if (blade > 0 && hero == 0)
        {
            t.color = GameController.ins.colorData.bladeColor;
        }
        else if (blade == 0 && hero > 0)
        {
            t.color = GameController.ins.colorData.heroColor;
        }
        else
        {
            t.color = (UI.Ins.main.shop.tab == ShopTab.Blade)
                ? GameController.ins.colorData.bladeColor
                : GameController.ins.colorData.heroColor;
        }
    }

}
