﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINote : MonoBehaviour
{
    private NoteController note;
    public Button selectedBtn;
    public Image icon;
    public int index;

    [ContextMenu("Add")]
    void Add()
    {
        index = int.Parse(transform.name) - 1;
        selectedBtn = GetComponent<Button>();
        icon = GetComponent<Image>();
    }

    private void Start()
    {
        selectedBtn.onClick.AddListener(Selected);
    }

    private void Selected()
    {
        note.Change(index);
    }

    public void SetUp(NoteController note)
    {
        this.note = note;
        icon.sprite = (index == note.index)
            ? GameController.ins.iconData.selectedNote
            : GameController.ins.iconData.NoneSelectedNote;
        icon.SetNativeSize();

    }

}
