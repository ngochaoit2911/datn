﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayer_Shop : MonoBehaviour
{
    public Image heroIm;
    public UI_KnifeController knifeController;

    public void SetUpHero(MaskType maskType)
    {
        var hero = GameController.ins.maskData.maskData.Find(s => s.type == maskType);
        heroIm.sprite = hero.icon;
        heroIm.SetNativeSize();
    }

    public void SetUpKnife(KnifeType knifeType)
    {
        knifeController.ChangeKnife(knifeType);
    }

    public void OnShow(MaskType maskType, KnifeType knifeType)
    {
        var hero = GameController.ins.maskData.maskData.Find(s => s.type == maskType);
        heroIm.sprite = hero.icon;
        heroIm.SetNativeSize();

        knifeController.OnShow(knifeType);

    }

}
