﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIGeneral : MonoBehaviour
{
    public Button addGemBtn, addCoinBtn, settingBtn;

    public Text coinValue, gemValue;
    private int lastCoin, lastGem;
    public float timeSnap = 0.2f;

    private void Start()
    {
        addGemBtn.onClick.AddListener(ShowGemShop);
        settingBtn.onClick.AddListener(ShowSetting);
        addCoinBtn.onClick.AddListener(ShowChangeCoin);
    }

    private void ShowChangeCoin()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.ShowPopup(UI.Ins.main.popup.coinShop);
    }

    private void OnEnable()
    {
        ShowCoin();
        ShowGem();
    }

    private void ShowSetting()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.ShowPopup(UI.Ins.main.popup.setting);
    }

    private void ShowGemShop()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.ShowPopup(UI.Ins.main.popup.gemShop);
    }

    public void ShowCoin()
    {
        var x = GameManager.ins.PlayerCoin;
        if (lastCoin != x)
        {
            DOVirtual.Float(lastCoin, x, timeSnap, value => { coinValue.text = (int) value + ""; });
        }
        else
        {
            coinValue.text = x + "";
        }

        lastCoin = x;
    }

    public void ShowGem()
    {
        var x = GameManager.ins.PlayerGem;
        if (lastGem != x)
        {
            DOVirtual.Float(lastGem, x, timeSnap, value => { gemValue.text = (int)value + ""; });
        }
        else
        {
            gemValue.text = x + "";
        }

        lastGem = x;
    }

}
