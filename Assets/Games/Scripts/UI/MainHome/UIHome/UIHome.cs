﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIHome : MonoBehaviour
{
    public Button playBtn, missionBtn, giftBtn, noAdsBtn, heroShopBtn, knifeShopBtn;
    public Button singleGameBtn, teamGameBtn;
    public UIPlayer uiPLayer;
    private LeaveHomeType type;
    public Animation anim;
    private const string leaveAnim = "LeaveHome";
    public Button motionBtn;

    private void Start()
    {
        playBtn.onClick.AddListener(StartMatching);
        singleGameBtn.onClick.AddListener(SingleGame);
        teamGameBtn.onClick.AddListener(TeamGame);
        missionBtn.onClick.AddListener(ShowMission);
        giftBtn.onClick.AddListener(ShowGift);
        heroShopBtn.onClick.AddListener(ShowHeroShop);
        knifeShopBtn.onClick.AddListener(ShowKnifeShop);
        motionBtn.onClick.AddListener(ShowMotion);
    }

    private void TeamGame()
    {
        UI.Ins.teamBattle.OnShow();
    }

    private void SingleGame()
    {
        UI.Ins.net_Matching.OnShow();
    }

    private void ShowMotion()
    {
        AudioManager.ins.PlayButtonClick();
        this.Hide();
        UI.Ins.main.motion.OnShow();
        ManagerAds.Ins.ShowBanner();
    }

    public void OnShow()
    {
        this.Show();

        if (ManagerAds.Ins != null && ManagerAds.Ins.CanShowAds())
        {
            noAdsBtn.Show();
        }
        else
        {
            noAdsBtn.Hide();
        }

        uiPLayer.OnShow();
    }

    private void ShowKnifeShop()
    {
        AudioManager.ins.PlayButtonClick();
        type = LeaveHomeType.KnifeShop;
        anim.Play(leaveAnim);
        //this.Hide();
        
    }

    private void ShowHeroShop()
    {
        AudioManager.ins.PlayButtonClick();
        type = LeaveHomeType.HeroShop;
        anim.Play(leaveAnim);
        //this.Hide();
        
    }

    public void LeaveToIngame()
    {
        type = LeaveHomeType.GamePlay;
        anim.Play(leaveAnim);
    }

    public void SetUpNoAds()
    {
        ManagerAds.Ins.RemoveAds();
        noAdsBtn.image.DOFade(0, 0);
    }

    private void ShowGift()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.ShowPopup(UI.Ins.main.popup.ui7Day);
    }

    private void ShowMission()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.ShowPopup(UI.Ins.main.popup.mission);
    }

    private void StartMatching()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.ShowPopup(UI.Ins.main.popup.matching);
    }

    public void OnCompleteLeave()
    {
        this.Hide();
        ManagerAds.Ins.HideBanner();

        switch (type)
        {
            case LeaveHomeType.KnifeShop:
                UI.Ins.main.shop.ShowKnifeShop();
                break;
            case LeaveHomeType.HeroShop:
                UI.Ins.main.shop.ShowHeroShop();
                break;
            case LeaveHomeType.GamePlay:
                UI.Ins.main.popup.matching.StartGame();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
      
    }

}

public enum LeaveHomeType
{
    KnifeShop, HeroShop, GamePlay
}