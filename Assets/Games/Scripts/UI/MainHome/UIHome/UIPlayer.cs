﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayer : MonoBehaviour
{
    public Image heroMask;
    public Text playerName;
    public Button changeNameBtn;
    public UIRankPlayer uiRank;
    public UI_KnifeController uiKnife;

    private void Start()
    {
        changeNameBtn.onClick.AddListener(ChangeName);
    }

    private void ChangeName()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.Show();
        UI.Ins.main.popup.rename.OnShow();
    }

    public void OnShow()
    {
        var p = GameManager.ins.PlayerInformation;
        var mask = GameController.ins.maskData.maskData[(int) p.playerEquipMask];
        var knife = GameController.ins.knifeData.knifeData[(int) p.playerEquipKnife];

        heroMask.sprite = mask.icon;
        heroMask.SetNativeSize();
        playerName.text = p.playerName + "";
        
        uiKnife.OnShow(knife.knifeType);

        uiRank.SetUp(p.playerRank, p.playerPoint);
    }

}
