﻿using System.Collections;
using System.Collections.Generic;
using Assets.Games.Photon.Scripts.Controller;
using Assets.Games.Photon.Scripts.NetUI;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIRankPlayer : MonoBehaviour
{
    public Image rankIcon;
    public List<Image> stars;
    public TextMeshProUGUI rankValue;

    public void SetUp(RankType pRank, int pPoint)
    {
        if (pRank >= 0)
        {
            rankIcon.sprite = GameController.ins.rankData.rankIcon[(int)pRank / 3];
            rankValue.text = (((int)pRank) % 3 + 1) + "";
            var x = GameController.ins.rankData.rankData[(int)pRank];
            rankValue.rectTransform.ChangePosY(x.posYText);

        }
        else
        {
            rankIcon.sprite = GameController.ins.rankData.rankIcon[0];
            rankValue.text = (0) + "";
            rankValue.rectTransform.ChangePosY(GameController.ins.rankData.rankData[0].posYText);

        }

        rankIcon.SetNativeSize();

        var index = GameController.ins.rankData.rankData.FindIndex(s => s.rank == pRank + 1);

        if (index == -1)
        {
            foreach (var star in stars)
            {
                star.fillAmount = 1;
            }
        }
        else
        {
            var nextPoint = GameController.ins.rankData.rankData[index].pointToUp;
            ;
            if (pPoint < (float)nextPoint / 5)
            {
                stars[0].DOFillAmount(pPoint / ((float)nextPoint / 5), 0.3f);
                stars[1].DOFillAmount(0, 0.3f);
                stars[2].DOFillAmount(0, 0.3f);
                stars[3].DOFillAmount(0, 0.3f);
                stars[4].DOFillAmount(0, 0.3f);
            }
            else if (pPoint < (float)nextPoint * 2 / 5)
            {
                stars[0].DOFillAmount(1, 0.3f);
                stars[1].DOFillAmount(pPoint / ((float)nextPoint / 5) - 1, 0.3f);
                stars[2].DOFillAmount(0, 0.3f);
                stars[3].DOFillAmount(0, 0.3f);
                stars[4].DOFillAmount(0, 0.3f);
            }
            else if (pPoint < (float)nextPoint * 3 / 5)
            {

                stars[0].DOFillAmount(1, 0.3f);
                stars[1].DOFillAmount(1, 0.3f);
                stars[2].DOFillAmount(pPoint / ((float)nextPoint / 5) - 2, 0.3f);
                stars[3].DOFillAmount(0, 0.3f);
                stars[4].DOFillAmount(0, 0.3f);
            }
            else if (pPoint < (float)nextPoint * 4 / 5)
            {
                stars[0].fillAmount = 1;
                stars[1].fillAmount = 1;
                stars[2].fillAmount = 1;
                stars[3].fillAmount = pPoint / ((float)nextPoint / 5) - 3;
                stars[4].fillAmount = 0;
            }
            else
            {
                stars[0].DOFillAmount(1, 0.3f);
                stars[1].DOFillAmount(1, 0.3f);
                stars[2].DOFillAmount(1, 0.3f);
                stars[3].DOFillAmount(1, 0.3f);
                stars[4].DOFillAmount(pPoint / ((float)nextPoint / 5) - 4, 0.3f);
                if (pPoint / ((float)nextPoint / 5) - 4 >= 1)
                {
                    var infor = GameManager.ins.PlayerInformation;
                    infor.playerPoint -= nextPoint;
                    infor.playerRank = pRank + 1;
                    GameManager.ins.PlayerInformation = infor;
                    this.PostEvent(EventID.UpRank);


                    if (!UI.Ins.isInGamePlay)

                    {
                        UI.Ins.main.home.uiPLayer.OnShow();
                        //UI.Ins.main.popup.Show();
                        //UI.Ins.main.popup.rankUp.SetUp();
                    }
                    else
                    {
                        DOVirtual.DelayedCall(1f, () =>
                        {
                            AudioManager.ins.PlayAudioClip(ClipType.episode_light);
                            UI.Ins.inGame.bonus.uiRankPlayer.SetUp(GameManager.ins.PlayerInformation.playerRank,
                                GameManager.ins.PlayerInformation.playerPoint);
                            UI.Ins.main.popup.Show();
                            UI.Ins.main.popup.rankUp.SetUp();
                        });

                    }

                }

            }

        }

    }

    public void Net_SetUp(RankType pRank, int pPoint)
    {
        if (pRank >= 0)
        {
            rankIcon.sprite = Net_Controller.ins.rankData.rankIcon[(int)pRank / 3];
            rankValue.text = (((int)pRank) % 3 + 1) + "";
            var x = Net_Controller.ins.rankData.rankData[(int)pRank];
            rankValue.rectTransform.ChangePosY(x.posYText);

        }
        else
        {
            rankIcon.sprite = Net_Controller.ins.rankData.rankIcon[0];
            rankValue.text = (0) + "";
            rankValue.rectTransform.ChangePosY(Net_Controller.ins.rankData.rankData[0].posYText);

        }

        rankIcon.SetNativeSize();

        var index = Net_Controller.ins.rankData.rankData.FindIndex(s => s.rank == pRank + 1);

        if (index == -1)
        {
            foreach (var star in stars)
            {
                star.fillAmount = 1;
            }
        }
        else
        {
            var nextPoint = Net_Controller.ins.rankData.rankData[index].pointToUp;
            ;
            if (pPoint < (float)nextPoint / 5)
            {
                stars[0].DOFillAmount(pPoint / ((float)nextPoint / 5), 0.3f);
                stars[1].DOFillAmount(0, 0.3f);
                stars[2].DOFillAmount(0, 0.3f);
                stars[3].DOFillAmount(0, 0.3f);
                stars[4].DOFillAmount(0, 0.3f);
            }
            else if (pPoint < (float)nextPoint * 2 / 5)
            {
                stars[0].DOFillAmount(1, 0.3f);
                stars[1].DOFillAmount(pPoint / ((float)nextPoint / 5) - 1, 0.3f);
                stars[2].DOFillAmount(0, 0.3f);
                stars[3].DOFillAmount(0, 0.3f);
                stars[4].DOFillAmount(0, 0.3f);
            }
            else if (pPoint < (float)nextPoint * 3 / 5)
            {

                stars[0].DOFillAmount(1, 0.3f);
                stars[1].DOFillAmount(1, 0.3f);
                stars[2].DOFillAmount(pPoint / ((float)nextPoint / 5) - 2, 0.3f);
                stars[3].DOFillAmount(0, 0.3f);
                stars[4].DOFillAmount(0, 0.3f);
            }
            else if (pPoint < (float)nextPoint * 4 / 5)
            {
                stars[0].fillAmount = 1;
                stars[1].fillAmount = 1;
                stars[2].fillAmount = 1;
                stars[3].fillAmount = pPoint / ((float)nextPoint / 5) - 3;
                stars[4].fillAmount = 0;
            }
            else
            {
                stars[0].DOFillAmount(1, 0.3f);
                stars[1].DOFillAmount(1, 0.3f);
                stars[2].DOFillAmount(1, 0.3f);
                stars[3].DOFillAmount(1, 0.3f);
                stars[4].DOFillAmount(pPoint / ((float)nextPoint / 5) - 4, 0.3f);
                if (pPoint / ((float)nextPoint / 5) - 4 >= 1)
                {
                    var infor = GameManager.ins.PlayerInformation;
                    infor.playerPoint -= nextPoint;
                    infor.playerRank = pRank + 1;
                    GameManager.ins.PlayerInformation = infor;
                    this.PostEvent(EventID.UpRank);

                    DOVirtual.DelayedCall(1f, () =>
                    {
                        AudioManager.ins.PlayAudioClip(ClipType.episode_light);
                        Net_UI.Ins.inGame.bonus.uiRankPlayer.Net_SetUp(GameManager.ins.PlayerInformation.playerRank,
                            GameManager.ins.PlayerInformation.playerPoint);
                        Net_UI.Ins.rankUp.Net_SetUp();
                    });
                }

            }

        }

    }
}
