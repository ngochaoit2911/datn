﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainHome : MonoBehaviour
{
    public UIHome home;
    public ShopController shop;
    public UIGeneral general;
    public PopupManager popup;
    public MotionController motion;

    public void ShowDefault()
    {
        home.OnShow();
        shop.Hide();
        shop.ShowDefault();
        general.Show();
        popup.Hide();
        popup.ShowDefault();
    }

}
