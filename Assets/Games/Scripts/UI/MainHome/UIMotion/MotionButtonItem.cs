﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotionButtonItem : MonoBehaviour
{
    public Button btn;
    public Image icon;

    public MotionType motionType;

    [ContextMenu("Add")]
    void Add()
    {
        btn = GetComponent<Button>();
        icon = transform.GetChild(1).GetComponent<Image>();
    }

    private void Start()
    {
        btn.onClick.AddListener(OnSelectMotion);
    }

    private void OnSelectMotion()
    {
        UI.Ins.main.popup.motion.SetAction(OnSelectedSuccess);
        UI.Ins.main.popup.motion.OnShow();
    }

    public void OnSetUp(PlayerMotionEquipment motion)
    {
        if (motion.isHasEquip)
        {
            var f = GameController.ins.maskData.maskData.Find(s => s.type == motion.maskEquip);
            icon.sprite = f.icon;
            icon.SetNativeSize();
            icon.Show();
        }
        else
        {
            icon.Hide();
        }
    }

    private void OnSelectedSuccess(MaskType maskType)
    {
        var f = GameManager.ins.Motions;
        var index = f.FindIndex(s => s.motion == motionType);
        f[index].maskEquip = maskType;
        f[index].isHasEquip = true;
        GameManager.ins.Motions = f;

        UI.Ins.main.motion.ShowInfor();
    }

}

