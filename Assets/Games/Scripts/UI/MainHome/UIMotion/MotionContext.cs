﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionContext
{
    int selectedIndex = -1;

    public int SelectedIndex
    {
        get { return selectedIndex; }
        set
        {
            if (value == selectedIndex)
            {
                return;
            }

            selectedIndex = value;

            if (OnSelectedIndexChanged != null)
            {
                OnSelectedIndexChanged(selectedIndex);
            }
        }
    }

    public Action<HeroMotionController> OnPressedCell;
    public Action<int> OnSelectedIndexChanged;
}
