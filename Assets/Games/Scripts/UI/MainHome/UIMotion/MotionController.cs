﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MotionController : MonoBehaviour
{
    public List<MotionButtonItem> items;
    public Button backBtn;

    private void Start()
    {
        backBtn.onClick.AddListener(OnBack);
    }

    private void OnBack()
    {
        this.Hide();
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.home.OnShow();
    }

    public void OnShow()
    {
        this.Show();
        var c = 0;
        foreach (var item in items)
        {
            item.OnSetUp(GameManager.ins.Motions[c]);
            c++;
        }
        Animation();
    }

    public void ShowInfor()
    {
        var c = 0;
        foreach (var item in items)
        {
            item.OnSetUp(GameManager.ins.Motions[c]);
            c++;
        }
    }

    public void Animation()
    {
        var s = DOTween.Sequence();
        foreach (var item in items)
        {
            item.transform.localScale = Vector3.zero;
            s.Join(item.transform.DOScale(1.2f, 0.2f).OnComplete(() => { item.transform.DOScale(1, 0.04f); }));
        }

        s.Restart();
    }

}


public enum MotionType
{
    Start,
    Revive,
    FirstBlood,
    DoubleKill,
    TripleKill,
    QuadraKill,
    PentaKill,
    HexaKill,
    Legendary,
    Shutdown,
    Victory
}

[System.Serializable]
public class PlayerMotionEquipment
{
    public MotionType motion;
    public MaskType maskEquip;
    public bool isHasEquip;
}