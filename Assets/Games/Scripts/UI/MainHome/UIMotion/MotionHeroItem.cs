﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotionHeroItem : MonoBehaviour
{
    public Button btn;
    public Image icon;
    private MaskType mask;

    [ContextMenu("Add")]
    void Add()
    {
        btn = GetComponent<Button>();
        icon = transform.GetChild(0).GetComponent<Image>();
    }

    private void Start()
    {
        btn.onClick.AddListener(OnSelect);
    }

    private void OnSelect()
    {
        UI.Ins.main.popup.motion.InvokeAction(mask);
    }

    public void SetUp(MaskType maskType)
    {
        mask = maskType;
        var f = GameController.ins.maskData.maskData.Find(s => s.type == maskType);
        icon.sprite = f.icon;
        icon.SetNativeSize();
    }

}
