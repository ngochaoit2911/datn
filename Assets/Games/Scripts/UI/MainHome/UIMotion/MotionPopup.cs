﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MotionPopup : MonoBehaviour
{
    public Dictionary<int, List<MaskType>> pages;
    public NoteController notes;

    public Button closeBtn;
    public HeroMotionScrollview scrollView;

    public delegate void OnSelectMotion(MaskType mask);

    private OnSelectMotion onSelect;

    public void SetAction(OnSelectMotion o)
    {
        onSelect = o;
    }

    public void InvokeAction(MaskType mask)
    {
        onSelect?.Invoke(mask);
        Close();
    }

    private void Start()
    {
        closeBtn.onClick.AddListener(Close);
        
    }

    private void HandleSelectedIndexChanged(int obj)
    {
        
    }

    private void Close()
    {
        AudioManager.ins.PlayButtonClick();
        UI.Ins.main.popup.OnClose(() =>
        {
            this.Hide();
            UI.Ins.main.popup.@group.alpha = 1;
            UI.Ins.main.popup.Hide();
        });
    }

    public void OnShow()
    {
        this.Show();
        UI.Ins.main.popup.Show();
        pages = new Dictionary<int, List<MaskType>>();
        var key = 0;
        var count = 0;
        var f = GameManager.ins.PlayerMasks.Where(s => s.isUnlock);
        foreach (var item in f)
        {
            if (count <= 5)
            {
                if (pages.ContainsKey(key))
                {
                    pages[key].Add(item.maskType);
                }
                else
                {
                    pages.Add(key, new List<MaskType> { item.maskType });
                }

                count++;
            }
            else
            {
                key++;

                pages.Add(key, new List<MaskType> { item.maskType });
                count = 1;
            }
        }

        notes.ShowValue(pages.Count);

        scrollView.OnStart();
        scrollView.OnSelectedIndexChanged(HandleSelectedIndexChanged);

        var f1 = new List<int>();
        for (int i = 0; i < pages.Count; i++)
        {
            f1.Add(i);
        }

        scrollView.UpdateData(f1);
        scrollView.UpdateSelection(0);
    }
}
