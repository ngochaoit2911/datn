﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIRename : MonoBehaviour
{
    public LoopVerticalScrollRect scrollView;

    public InputField nameInput, countryInput;

    private string countryName;
    private string pName;
    public int index;
    public Button okBtn;
    public Button closeBtn;
    public List<CountryBotName> temp;

    private void Start()
    {
        closeBtn.onClick.AddListener(() =>
        {
            UI.Ins.main.popup.OnClose(() =>
            {
                this.Hide();
                UI.Ins.main.popup.@group.alpha = 1;
                UI.Ins.main.popup.Hide();
            });
        });
        okBtn.onClick.AddListener(OnCommit);
        countryInput.onEndEdit.AddListener(FindWithString);
    }

    public void OnShow()
    {
        this.Show();
        temp = GameController.ins.botData.botNames;
        scrollView.totalCount = temp.Count;
        
        
        nameInput.text = GameManager.ins.PlayerInformation.playerName;
        countryInput.text = GameManager.ins.PlayerInformation.countryCode;
        countryName = GameManager.ins.PlayerInformation.countryCode;
        index = temp.FindIndex(s => s.countryCode == GameManager.ins.PlayerInformation.countryCode);
        scrollView.ScrollToCell(index, 1000);
        scrollView.RefillCells();
        //selectedIm.SetParent(scrollView);
    }

    public void ShowBotName(List<CountryBotName> botNames, int index)
    {
        temp = botNames;

        scrollView.totalCount = temp.Count;
        scrollView.RefillCells();
        scrollView.ScrollToCell(index, 600);
    }

    public void OnCommit()
    {
        if (GameManager.ins.PlayerCoin < 2000) return;

        GameManager.ins.PlayerCoin -= 2000;

        AudioManager.ins.PlayAudioClip(ClipType.click);
        var s = nameInput.text;
        if (s.Equals(""))
        {
            s = "PlayerName";
        }

        if (s.Length > 12)
        {
            s = s.Substring(0, 11);
        }
        var t = GameManager.ins.PlayerInformation;
        t.playerName = s;
        t.countryCode = countryName;
        t.countryName = countryName;
        GameManager.ins.PlayerInformation = t;

        DatabaseManager.UpdateDisplayName(t.playerName + ":" + t.countryCode);

        UI.Ins.main.home.OnShow();

        UI.Ins.main.popup.OnClose(() =>
        {
            this.Hide();
            UI.Ins.main.popup.@group.alpha = 1;
            UI.Ins.main.popup.Hide();
        });
    }

    public void OnChangeSelected(int index)
    {
        this.index = index;
        scrollView.RefreshCells();
        countryInput.text = temp[index].countryCode;
        countryName = temp[index].countryCode;
    }

    public void FindWithString(string m)
    {
        temp = GameController.ins.botData.botNames.Where(s => Contains(s.countryName, m) || Contains(s.countryCode, m)).ToList();
        ShowBotName(temp, 0);
    }

    bool Contains(string s1, string s2)
    {
        s1 = s1.ToLower();
        s2 = s2.ToLower();
        var arr = s2.ToArray();
        foreach (var a in arr)
        {
            if (s1.Contains(a))
            {
                s1.Replace(a.ToString(), "");
            }
            else
            {
                return false;
            }
        }

        return true;
    }

}
