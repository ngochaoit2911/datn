﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SnapScrollview : MonoBehaviour, IPointerDownHandler, IPointerUpHandler/*, IDragHandler*/
{
    public int maxPage;
    public NoteController note;
    public float minSnapValue = 300;
    public float itemWidth = 660;

    private Vector2 startTouch;
    private Vector2 endTouch;


    public void OnPointerDown(PointerEventData eventData)
    {
        startTouch = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        endTouch = eventData.position;
    }

    public void SetUpBottom()
    {
        note.OnShow();
    }

}
