﻿using Assets.Games.Photon.Scripts.Room;
using Assets.Games.Scripts.UI;
using Assets.Games.Scripts.UI.MainHome.Matching;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    public static UI Ins;
    
    public UIMainHome main;
    public UI_InGame inGame;
    public bool isInGamePlay;
    public Net_MatchingController net_Matching;
    public UITeamBattle teamBattle;
    public UILeaderBoard lb;

    private void Awake()
    {
        Ins = this;
        ShowDefault();
        main.popup.mission.OnRegister();
        main.popup.verification.OnStart();
    }

    public void ShowDefault()
    {
        isInGamePlay = false;
        main.Show();
        main.ShowDefault();
        inGame.Hide();
        inGame.ShowDefault();
        teamBattle.Hide();
        net_Matching.Hide();
        lb.Hide();
    }

    public void ShowGamePlay()
    {
        ShowDefault();
        isInGamePlay = true;
        main.Hide();
        inGame.Show();
        inGame.OnShowGamePlay();
    }

    public void ShowOnEndGame()
    {
        inGame.ShowOnEndGame();
    }

    public void ShowLeaderboard()
    {
        lb.OnShow();
    }

}
