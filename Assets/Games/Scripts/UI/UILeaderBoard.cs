﻿using PlayFab.ClientModels;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Games.Scripts.UI
{
    public class UILeaderBoard : MonoBehaviour
    {

        public GameObject rankItem;
        public RectTransform content;

        public UILeaderBoardItem myItem;
        public Button closeBtn;

        private void Start()
        {
            closeBtn.onClick.AddListener(Close);
        }

        public void OnShow()
        {
            this.Show();
            Poolers.ins.ClearItem(rankItem);
            myItem.Hide();
            StartCoroutine(ILoad());
        }

        private void Close()
        {
            this.Hide();
        }

        IEnumerator ILoad()
        {
            var b = false;
            var k = false;
            var m = new GetLeaderboardResult();
            DatabaseManager.GetLeaderBoard(r =>
            {
                b = true;
                k = true;
                m = r;
            }, e =>
            {
                Debug.Log(e.ErrorMessage.ToString());
                b = true;
            });
            yield return new WaitUntil(() => b);
            if (k)
            {
                foreach (var item in m.Leaderboard)
                {
                    var go = Poolers.ins.GetObject(rankItem, content);
                    go.Cast<UILeaderBoardItem>().OnShow(item);
                }
                Debug.Log(m.Leaderboard.Count);
                var i = m.Leaderboard.Find(s => s.DisplayName == GameManager.ins.PlayerInformation.playerName + ":" + GameManager.ins.PlayerInformation.countryCode);
                if(i != null)
                    myItem.OnShow(i);
            }         
        }
    }
}