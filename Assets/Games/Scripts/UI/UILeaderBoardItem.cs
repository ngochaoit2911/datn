﻿using Assets.Games.Photon.Scripts.Controller;
using PlayFab.ClientModels;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Games.Scripts.UI
{
    public class UILeaderBoardItem : PoolItem
    {

        public Text playerNameTxt;
        public Image flagIm;
        public Image rankIm;
        public TextMeshProUGUI playerRank, rankTxt;

        public void OnShow(PlayerLeaderboardEntry p)
        {
            this.Show();
            m_transform.SetAsLastSibling();
            var s = p.DisplayName.Split(':');
            playerNameTxt.text = s[0];
            var rank = p.StatValue;
            var countryCode = s[1];

            rankTxt.text = "#" + (p.Position + 1);
            Debug.Log(p.Position);
            if (rank >= 0)
            {
                rankIm.sprite = Net_Controller.ins.rankData.rankIcon[rank / 3];
                playerRank.text = (rank % 3 + 1) + "";

            }
            else
            {
                rankIm.sprite = Net_Controller.ins.rankData.rankIcon[0];
                playerRank.text = (0) + "";
            }
            rankIm.SetNativeSize();

            flagIm.sprite = Net_Controller.ins.botData.botNames.Find(m => m.countryCode == countryCode).icon;

        }
    }
}