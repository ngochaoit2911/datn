﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMultiScreen : MonoBehaviour
{
    public List<RectTransform> changePosT;
    public float baseChange;

    void Awake()
    {
        var baseX = 720f / 1280;
        var curX = Screen.width / (float) Screen.height;
        foreach (var m in changePosT)
        {
            m.ChangePosY(m.anchoredPosition.y + (baseX / curX - 1) * baseChange);
        }
    }

}
