﻿
using UnityEngine;
using UnityEngine.UI;

public class UIVerificationAge : MonoBehaviour
{
    public InputField input;
    public Button confirm;
    public GameObject retypeObj;


    private const string IS_CHECK = "CheckAge";

    public int Age
    {
        get => age;
        
    }

    private int age;

    void Start()
    {
        confirm.onClick.AddListener(Confirm);
    }

    private void Confirm()
    {
        var s = int.Parse(input.text);
        if (s > 70 || s < 2)
        {
            // Nothing
            retypeObj.Show();
        }

        else
        {
            EncryptHelper.SetInt(IS_CHECK, s);
            age = s;
            GameManager.ins.InitAds();
            this.Hide();
            UI.Ins.main.popup.Hide();
        }
    }

    public void OnStart()
    {
        this.Hide();
        age = 19;
        GameManager.ins.InitAds();
        //if (PlayerPrefs.HasKey(IS_CHECK))
        //{
        //    this.Hide();
        //    age = EncryptHelper.GetInt(IS_CHECK);
        //    GameManager.ins.InitAds();
        //}
        //else
        //{
        //    this.Show();
        //    UI.Ins.main.popup.Show();
        //}
    }



}
