﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class SpineTest : MonoBehaviour
{
    private SkeletonAnimation anim;
    public bool isLoop;

    [SpineAnimation()] public string animName, animAnim2, a3, a4;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<SkeletonAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            anim.state.ClearTrack(0);
            anim.state.SetAnimation(0, animName, isLoop);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            anim.state.ClearTrack(1);
            anim.state.SetAnimation(1, animAnim2, isLoop);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            anim.state.ClearTrack(2);
            anim.state.SetAnimation(2, a3, isLoop);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            anim.state.ClearTrack(3);
            anim.state.SetAnimation(3, a4, isLoop);
        }
    }
}
